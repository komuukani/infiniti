var buttonPlus = $(".qty-btn-plus");
var buttonMinus = $(".qty-btn-minus");

var incrementPlus = buttonPlus.click(function () {
    var $n = $(this)
        .parent(".qty-container")
        .find(".input-qty");
    $n.val(Number($n.val()) + 1);
});

var incrementMinus = buttonMinus.click(function () {
    var $n = $(this)
        .parent(".qty-container")
        .find(".input-qty");
    var amount = Number($n.val());
    if (amount > 0) {
        $n.val(amount - 1);
    }
});

const toggles = document.querySelectorAll(".faq-toggle");
const toggleTitle = document.querySelectorAll(".faq-title");

toggles.forEach((toggle) => {
    toggle.addEventListener("click", () => {
        toggle.parentElement.classList.toggle("active");
    });
});
toggleTitle.forEach((toggle) => {
    toggle.addEventListener("click", () => {
        toggle.parentElement.classList.toggle("active");
    });
});


/* >> 6. SHARE BUTTON START
        ================================================== */
const shareButton = document.getElementById('share-button');
if (shareButton) {
    shareButton.addEventListener('click', function (event) {
        openShareModal(event, this.dataset.title, this.dataset.fullurl);
    });
}

// Handle share button
function openShareModal(e, title, fullUrl) {
    if (navigator.share) {
        navigator.share({
            title, url: fullUrl,
        }).then(() => {
            console.log('Thanks for sharing!');
        })
            .catch(console.error);
    } else {
        shareModal.style.display = 'flex';
    }
}

/* << SHARE BUTTON END
================================================== */


/* >> TEXT ANIMATION FRAME WISE START
================================================== */
$(document).ready(function () {
    const frames = ['.frame1', '.frame2', '.frame3'];
    const frameDuration = 3000; // Each frame shows for 3 seconds (9000ms total duration / 3)
    let currentFrameIndex = 0;

    function showFrame(index) {
        $(frames[index]).css('display', 'block').animate({opacity: 1}, 1000, function () {
            setTimeout(function () {
                $(frames[index]).animate({opacity: 0}, 1000, function () {
                    $(this).css('display', 'none');
                });
            }, frameDuration - 2000); // 2000ms accounts for fade in and fade out duration
        });
    }

    function setupVideoSync(video) {
        video.addEventListener('timeupdate', function () {
            const currentTime = video.currentTime;
            if (currentTime >= 0 && currentTime < frameDuration / 1000) {
                if (currentFrameIndex !== 0) {
                    currentFrameIndex = 0;
                    showFrame(currentFrameIndex);
                }
            } else if (currentTime >= frameDuration / 1000 && currentTime < (2 * frameDuration) / 1000) {
                if (currentFrameIndex !== 1) {
                    currentFrameIndex = 1;
                    showFrame(currentFrameIndex);
                }
            } else if (currentTime >= (2 * frameDuration) / 1000) {
                if (currentFrameIndex !== 2) {
                    currentFrameIndex = 2;
                    showFrame(currentFrameIndex);
                }
            }
        });

        video.addEventListener('ended', function () {
            currentFrameIndex = 0;
            showFrame(currentFrameIndex);
        });

        video.addEventListener('play', function () {
            currentFrameIndex = 0;
            showFrame(currentFrameIndex); // Start the animation when video starts playing
        });
    }

    const mainVideo = document.getElementById('mainVideo');
    const mainVideoMobile = document.getElementById('mainVideoMobile');

    setupVideoSync(mainVideo);
    setupVideoSync(mainVideoMobile);
});


/* << TEXT ANIMATION FRAME WISE END
================================================== */


/* >> PREVENT BROWSER MENU ON LONG PRESS ON MOBILE START
================================================== */
function absorbEvent_(event) {
    var e = event || window.event;
    e.preventDefault && e.preventDefault();
    e.stopPropagation && e.stopPropagation();
    e.cancelBubble = true;
    e.returnValue = false;
    return false;
}

function preventLongPressMenu(node) {
    node.ontouchstart = absorbEvent_;
    node.ontouchmove = absorbEvent_;
    node.ontouchend = absorbEvent_;
    node.ontouchcancel = absorbEvent_;
}

preventLongPressMenu('img');

/* << PREVENT BROWSER MENU ON LONG PRESS ON MOBILE END
================================================== */


/* >> DROP ZONE START
================================================== */
document.querySelectorAll(".drop-zone__input").forEach((inputElement) => {
    const dropZoneElement = inputElement.closest(".drop-zone");

    dropZoneElement.addEventListener("click", (e) => {
        inputElement.click();
    });

    inputElement.addEventListener("change", (e) => {
        if (inputElement.files.length) {
            updateThumbnail(dropZoneElement, inputElement.files[0]);
        }
    });

    dropZoneElement.addEventListener("dragover", (e) => {
        e.preventDefault();
        dropZoneElement.classList.add("drop-zone--over");
    });

    ["dragleave", "dragend"].forEach((type) => {
        dropZoneElement.addEventListener(type, (e) => {
            dropZoneElement.classList.remove("drop-zone--over");
        });
    });

    dropZoneElement.addEventListener("drop", (e) => {
        e.preventDefault();

        if (e.dataTransfer.files.length) {
            inputElement.files = e.dataTransfer.files;
            updateThumbnail(dropZoneElement, e.dataTransfer.files[0]);
        }

        dropZoneElement.classList.remove("drop-zone--over");
    });
});

/**
 * Updates the thumbnail on a drop zone element.
 *
 * @param {HTMLElement} dropZoneElement
 * @param {File} file
 */
function updateThumbnail(dropZoneElement, file) {
    let thumbnailElement = dropZoneElement.querySelector(".drop-zone__thumb");

    // First time - remove the prompt
    if (dropZoneElement.querySelector(".drop-zone__prompt")) {
        dropZoneElement.querySelector(".drop-zone__prompt").remove();
    }

    // First time - there is no thumbnail element, so lets create it
    if (!thumbnailElement) {
        thumbnailElement = document.createElement("div");
        thumbnailElement.classList.add("drop-zone__thumb");
        dropZoneElement.appendChild(thumbnailElement);
    }

    thumbnailElement.dataset.label = file.name;

    // Show thumbnail for image files
    if (file.type.startsWith("image/")) {
        const reader = new FileReader();

        reader.readAsDataURL(file);
        reader.onload = () => {
            thumbnailElement.style.backgroundImage = `url('${reader.result}')`;
        };
    } else {
        thumbnailElement.style.backgroundImage = null;
    }
}

/* >> DROP ZONE END
================================================== */


/* >> RANGE SLIDER START
================================================== */
function SliderFun(ele) {
    for (let i = 0; i < ele.length; i++) {
        const element = ele[i];

        const values = element.value;
        const dataValue = element.getAttribute("max");
        const fullValue = Math.round((values * 100) / dataValue);
        element.nextSibling.parentNode.querySelector(".active-line").style.width = fullValue + "%";
        if (element.nextSibling.parentNode.querySelector(".active-dot")) {
            element.nextSibling.parentNode.querySelector(".active-dot").style.left = fullValue + "%";
        }

        // if (values % 3 === 0) {
        console.log("The value is an integer.");
        console.log("values", values / 3);
        const vals = values / 3;
        const ulList = element.parentNode.parentElement.querySelectorAll("ul li");
        for (let ids = 0; ids < ulList.length; ids++) {
            if (ids < vals || ids == vals) {
                ulList[ids].classList.add("active");
            } else {
                ulList[ids].classList.remove("active");
            }
        }
        // }
    }
}

SliderFun($(".range-input input"));

$(".range-input input").on("input", function () {
    SliderFun($(this));
});

/* << RANGE SLIDER END
================================================== */


/* << CUSTOM MODAL START
================================================== */
var modal = document.getElementById('popup-modal');
var btn = document.getElementById("open-popup-modal");
var span = document.getElementsByClassName("modal-close")[0];
var span2 = document.getElementById("modal-close");
btn.onclick = function () {
    modal.style.display = "block";
    $('body').addClass('modal-open');
}
span.onclick = function () {
    modal.style.display = "none";
    $('body').removeClass('modal-open');
}
span2.onclick = function () {
    modal.style.display = "none";
    $('body').removeClass('modal-open');
}

window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
        $('body').removeClass('modal-open');
    }
}
/* << CUSTOM MODAL END
================================================== */


/* >> CALCULATE TOTAL COST FOR CUSTOMIZED PRODUCT START
================================================== */
const amount = 550;
var cost = 0;
$(document).on("change", "#armchair,#seater_2,#seater_3,#seater_4,#seater_5", function () {
    let armchair = $('#armchair').val() ? amount * parseInt($('#armchair').val()) : 0;
    let seater_2 = $('#seater_2').val() ? (amount * parseInt($('#seater_2').val()) * 2) : 0;
    let seater_3 = $('#seater_3').val() ? (amount * parseInt($('#seater_3').val()) * 3) : 0;
    let seater_4 = $('#seater_4').val() ? (amount * parseInt($('#seater_4').val()) * 4) : 0;
    let seater_5 = $('#seater_5').val() ? (amount * parseInt($('#seater_5').val()) * 5) : 0;

    cost = armchair + seater_2 + seater_3 + seater_4 + seater_5;

    $('#cost').val(cost);
    $('.cost').html('SAR ' + (cost.toFixed(2)));
});
/* << CALCULATE TOTAL COST FOR CUSTOMIZED PRODUCT END
================================================== */

