
$base_url = location.origin + location.pathname.split('/').slice(0, -1).join('/') + "/";

//file validation.
function filevalidate() {
    var filename = document.getElementById('file').value;
    var extension = filename.substr(filename.lastIndexOf('.') + 1).toLowerCase();
    if (extension == 'jpg' || extension == 'jpeg' || extension == 'png' || extension == 'pdf' || extension == 'webp') {
        $('.file-error').hide();
        $('#file-border-1').removeClass('error-border');
        $('#file-border-2').removeClass('error-border');
        return true;
    } else {
        $('.file-error').show();
        $('#file-border-1').addClass('error-border');
        $('#file-border-2').addClass('error-border');
        return false;
    }
}

//file preview 
function readURL(input, id) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#' + id).attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

// Multiple images preview in browser
function imagesPreview(input) {
    if (input.files) {
    placeToInsertImagePreview = $(input).parent().parent().next();
    $(placeToInsertImagePreview).html('');
        var filesAmount = input.files.length;
        for (i = 0; i < filesAmount; i++) {
            var reader = new FileReader();
            reader.onload = function (event) {
                $($.parseHTML('<img>')).attr('src', event.target.result).attr("width", "60px").css('margin', '10px').appendTo(placeToInsertImagePreview);
            }
            reader.readAsDataURL(input.files[i]);
        }
    }
}

function getAction(element, id, type) { 
    var data = ($('#' + id).is(":checked")) ? {val: 1} : {val: 0};
    var url = $base_url + 'Admin/Login/control_panel/' + element + '/' + type;
    jQuery.post(url, data);
}
 