-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 28, 2024 at 05:24 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_infiniti`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_aboutus`
--

CREATE TABLE `tbl_aboutus` (
  `aboutus_id` int(12) NOT NULL,
  `about` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbl_aboutus`
--

INSERT INTO `tbl_aboutus` (`aboutus_id`, `about`) VALUES
(7, '<p><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"></span></p><h3 style=\"font-family: sans-serif; color: rgb(0, 0, 0);\">About Skin 2 Love</h3><p><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><a href=\"https://skin-2-love.com/\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s; box-shadow: none;\">Skin-2-Love</a></span>&nbsp;was born out of a thirst for knowledge and a desire to act against pollution and waste.&nbsp; As many of you have done, I wanted to know how skincare was made.&nbsp; The purple unicorn, adored by many, understood by few! A few years ago, I was standing in my local Chemist warehouse, looking for a moisturiser as I had run out the night before.&nbsp; At this point I was unemployed (Thanks Covid), and price really was important, however, as someone who applies a moisturiser at least twice a day I did not have an option NOT to get it.&nbsp; I bought the cheapest facial moisturiser I could find ($7 on special) but it was watery and underwhelming, and I felt the difference on my skin immediately.&nbsp; So started my journey…. Why would one product be luxurious while another not, why do some break the bank and others do not?</p><h3>&nbsp;I found that,</h3><ul style=\"margin: 15px 0px 15px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><li style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Moisturisers (and for that matter skincare) contained the same or very similar ingredients.</li><li style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Ingredient lists are complicated to understand, and if you don’t understand the function and interaction of each ingredient you remain in the dark.&nbsp; It’s like having a recipe for cake without knowing what quantities to use of each ingredient.</li><li style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Packaging often cost more than the ingredients for the skincare. Yes, you read that right.&nbsp;</li></ul><p style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><br></p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">I decided that instead of talking about doing something, to start doing something.&nbsp; Skin-2-Love was born.&nbsp; Re-make what’s in the inside and re-use the outside – no compromise on effectiveness or luxuriousness – simple as that.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><i><u>Do you want to know the best part?&nbsp; It’s EASY and FUN!!!</u></i></p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><b>Marnel<br><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Founder</span></b></p>');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `admin_id` int(12) NOT NULL,
  `user_role_id` int(11) NOT NULL,
  `admin_name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `password` varchar(500) NOT NULL,
  `path` varchar(500) NOT NULL COMMENT 'Profile Photo',
  `address` varchar(200) NOT NULL,
  `bio` varchar(1000) NOT NULL,
  `theme` varchar(10) NOT NULL,
  `lastseen` datetime NOT NULL,
  `register_date` date NOT NULL,
  `status` int(11) NOT NULL COMMENT 'Profile Status'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `user_role_id`, `admin_name`, `email`, `phone`, `password`, `path`, `address`, `bio`, `theme`, `lastseen`, `register_date`, `status`) VALUES
(2, 1, 'Admin', 'admin@gmail.com', '+64 (0) 22 079 8832', '341c3862157a967b269645a552fcf45d65e153abebb3edd4b915837cbca8ca2a1f93c81e382746cd27076e9106654d17232f359fc72c43e428c2649dbc9c8990crWE3iM4+YPFHWxae4L9Q9pnXdWofBy6AuSkgPL7flY=', './admin_asset/profile/8ff6e62a2d1ad156ab21f2edb6bca80d.png', 'North Shore, Auckland, New Zealand', '<p>Hello I am admin</p>', 'theme-1', '2024-03-27 02:05:21', '2022-09-19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_banner`
--

CREATE TABLE `tbl_banner` (
  `banner_id` int(12) NOT NULL,
  `title` varchar(500) NOT NULL,
  `subtitle` varchar(500) NOT NULL,
  `path` varchar(1000) NOT NULL,
  `position` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbl_banner`
--

INSERT INTO `tbl_banner` (`banner_id`, `title`, `subtitle`, `path`, `position`) VALUES
(40, 'For Everyone', 'Best value for money at super competitive prices', './admin_asset/banner/961484c76c5660a9681bc1cdd30faecb.png', 1),
(41, 'Widest Variety', 'of sofa choices to indulge the senses of all tastes', './admin_asset/banner/2c04d00f8992b763024140bafa7e1b59.png', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bill`
--

CREATE TABLE `tbl_bill` (
  `bill_id` int(11) NOT NULL,
  `order_id` char(25) NOT NULL,
  `unique_id` varchar(100) NOT NULL,
  `register_id` int(11) NOT NULL,
  `user_type` char(20) NOT NULL DEFAULT 'regular',
  `fname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `subtotal` double NOT NULL,
  `shipping_charge` double NOT NULL,
  `tax` double NOT NULL,
  `netprice` double NOT NULL,
  `currency_code` char(10) NOT NULL,
  `country_name` varchar(50) NOT NULL,
  `country` char(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `address` varchar(200) NOT NULL,
  `postal_code` int(11) NOT NULL,
  `discount` char(10) DEFAULT NULL,
  `notes` varchar(500) NOT NULL,
  `entry_type` char(15) NOT NULL,
  `transaction_id` varchar(100) NOT NULL,
  `card_name` varchar(100) NOT NULL,
  `card_number` char(40) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `cvv` int(11) NOT NULL,
  `payment_status` char(15) NOT NULL,
  `entry_date` date DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tbl_bill`
--

INSERT INTO `tbl_bill` (`bill_id`, `order_id`, `unique_id`, `register_id`, `user_type`, `fname`, `email`, `phone`, `subtotal`, `shipping_charge`, `tax`, `netprice`, `currency_code`, `country_name`, `country`, `city`, `state`, `address`, `postal_code`, `discount`, `notes`, `entry_type`, `transaction_id`, `card_name`, `card_number`, `month`, `year`, `cvv`, `payment_status`, `entry_date`, `status`) VALUES
(1, '65CB9BDB01841560504765', '', 1, 'regular', 'nishant j thummar', 'nishantthummar005@gmail.com', '8460124262', 50, 9.61, 0, 68.55, 'nzd', '', 'New Zealand', 'Levin', 'Levin', '80 Avenue North Road, Levin, Levin 5510', 5510, '0', 'test', '', 'ch_3OjP3XGc96af4g0Y1BqDgaz1', 'nishant bhai', '4242424242424242', 12, 25, 123, 'succeeded', '2024-02-13', NULL),
(2, '65CF0D83DDA3E955486245', '', 0, 'guest', 'demo', 'nishant006@gmail.com', '7894564613', 100, 9.61, 0, 126.05, 'nzd', '', 'New Zealand', 'Levin', 'Levin', '20 Avenue North Road, Levin, Levin 5510', 5510, '0', 'test', '', 'ch_3OkLlxGc96af4g0Y11hadZGz', 'nishan', '4242424242424242', 12, 25, 123, 'succeeded', '2024-02-16', NULL),
(3, '65CF0F769B862156809825', '', 0, 'guest', 'nsiahtn', 'jiten00@gmail.com', '764368456465', 80, 9.61, 0, 103.05, 'nzd', '', 'New Zealand', 'Levin', 'Levin', '20 Avenue North Road, Levin, Levin 5510', 5510, '0', 'testing', '', 'ch_3OkLu0Gc96af4g0Y0aoTVsXj', 'test', '4242424242424242', 12, 25, 123, 'succeeded', '2024-02-16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blog`
--

CREATE TABLE `tbl_blog` (
  `blog_id` int(10) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `path` varchar(300) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(250) NOT NULL,
  `meta_keyword` varchar(500) NOT NULL,
  `meta_desc` varchar(500) NOT NULL,
  `blogdate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbl_blog`
--

INSERT INTO `tbl_blog` (`blog_id`, `title`, `slug`, `path`, `description`, `meta_title`, `meta_keyword`, `meta_desc`, `blogdate`) VALUES
(11, 'Vitamin E', 'vitamin-e', './admin_asset/blog/0fe93e311e6dd17e97c9e153be4525a0.png', '<p class=\"has-medium-font-size\" style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">Just about everyone knows about Vitamin E.&nbsp; I mistakenly believed that the beautiful tissue oil my mother uses worked so well because it was a pure Vitamin E oil.&nbsp; It is not.</p><p class=\"has-medium-font-size\" style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">Vitamin E is an oil-soluble antioxidant. It has an amber colour and is a&nbsp;<span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">very</span>&nbsp;sticky oil.&nbsp; Pure Vitamin E is therefore not fit for direct application to the skin.&nbsp;</p><p class=\"has-medium-font-size\" style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">Regarding Vitamin E and its function as an antioxidant, this is relevant to your skin but also in oils.&nbsp;</p><p class=\"has-medium-font-size\" style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">It protects our skin from the damage caused by excess free radicals (aka oxidative stress) and it slows down the oxidation of oils to prevent rancidity.&nbsp; This makes it a powerful ingredient to include in skincare formulations.&nbsp; It is not the only antioxidant used for this purpose, but it is certainly one of the most common.&nbsp;&nbsp;<a href=\"https://formulabotanica.com/antioxidants-in-cosmetics/#:~:text=Antioxidants%20and%20Cosmetic%20Formulations\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s; cursor: pointer; text-underline-offset: 3px;\">Formulabotanica</a>&nbsp;has an in-depth article that explains how that all works.&nbsp; In short, some ingredients are prone to oxidation, caused by heat, light, metal ions and oxygen exposure, which makes them unstable and therefore reduces their shelf life.</p><p class=\"has-medium-font-size\" style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">The article goes into more details around some common Primary and Secondary antioxidants.&nbsp;</p><p class=\"has-medium-font-size\" style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">Depending on the product, it’s ingredients and purpose, a formulator may use one or a combination of antioxidants to protect the oil in their formulation from oxidising.</p><p class=\"has-medium-font-size\" style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">With that said, many antioxidants used in skincare also act on our skin.&nbsp; This is one of the reasons why Vitamin E is so popular, it acts in two ways and therefore eliminates the need to double up on these ingredients, leaving more “room” for other actives and “goody” ingredients.</p><p class=\"has-medium-font-size\" style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">Secondary antioxidants like Chelating agents binds to metal ions which can cause oxidation and is well placed for formulations with a high percentage of water.&nbsp; Carotenoids (another much less common secondary antioxidant) depending on the product may be best placed.</p><p class=\"has-medium-font-size\" style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">When we are born, our antioxidant production is at a maximum whereas ROS (Reactive Oxygen species) production is very low.&nbsp; As we age, this changes.&nbsp; ROS is an important part of cell regeneration, however, as we age and with pollution around us, the ROS production, and the reduced antioxidant production causes damage to our skin.&nbsp; It is therefore important to wear skincare that will protect you from the elements.</p><figure class=\"wp-block-image size-full\" style=\"margin: 1.5em auto; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\"><img decoding=\"async\" width=\"554\" height=\"493\" src=\"https://skin-2-love.com/wp-content/uploads/2023/05/Antioxidants.png\" alt=\"\" class=\"wp-image-4593\" srcset=\"https://skin-2-love.com/wp-content/uploads/2023/05/Antioxidants.png 554w, https://skin-2-love.com/wp-content/uploads/2023/05/Antioxidants-300x267.png 300w\" sizes=\"(max-width: 554px) 100vw, 554px\" style=\"height: auto; max-width: 100%; margin: 0px auto; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: bottom; display: block;\"></figure><p class=\"has-medium-font-size\" style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">Wear sunscreen.&nbsp; It is such a small thing that makes such a big difference.&nbsp; We won’t be going into the benefits of sunscreen here, however, I’m sure you have a voice inside you saying “yes, that is a good idea” right now.&nbsp;&nbsp;</p><p class=\"has-medium-font-size\" style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">The point is, buying a product specifically because it promotes its Vitamin E content does not mean you are getting more bang for your buck.&nbsp; You are better off looking at what else it contains as an antioxidant with any product containing oil is a given (from a good manufacturing practice perspective)&nbsp;</p><p class=\"has-medium-font-size\" style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">Look for the actives in the product and then do an internet search to the value that an active brings to your skincare routine.</p>', 'Vitamin E', 'Just about everyone knows about Vitamin E.  I mistakenly believed that the beautiful tissue oil my mother uses worked so well because it was a pure Vitamin E oil.  It is not.', 'Just about everyone knows about Vitamin E.  I mistakenly believed that the beautiful tissue oil my mother uses worked so well because it was a pure Vitamin E oil.  It is not.', '2023-09-29'),
(12, 'How do I use skincare products', 'how-do-i-use-skincare-products', './admin_asset/blog/2b4f0dd8f63bf4a070c66ef155964cfe.png', '<p style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">If you are like most people you dip your finger into your skincare and then rub the lotion or cream into your skin until it is all absorbed. You learned from television how to apply your skincare or you saw a relative or friend do it and figured out the rest from there.</p><p style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">Today, you are probably going to learn something new and if you adopt the new ways of applying your skincare you will save money too!</p><p style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">The skin can only absorb so much and must be prepped to absorb your product. When you have been out and about and you have sweat, dirt particles and other impurities on your skin, applying a product is not going to be very useful (the exception is sunblock when you protecting yourself while out)</p><p style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">Your skincare routine does not have to be complicated. 3 Steps is all you need to give your skin the best chance of staying radiant. With that said, these days there are a ton of products and the list is growing week by week.</p><p style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">The table below gives you a suggested order to apply your skincare,</p><p style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">The general rule of thumb is to apply your product from the least viscous (runny) to most viscous (oily serums etc.)</span></p><p style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">That rule isn’t 100% accurate but a good starting point. To understand in which order you should apply what, you really need to understand “what” each product does.</p><div class=\"wp-block-columns is-layout-flex wp-container-2 wp-block-columns-is-layout-flex\" style=\"margin: 1.5em auto; gap: 2em; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; display: flex; align-items: center; flex-wrap: nowrap; max-width: 1250px;\"><div class=\"wp-block-column is-layout-flow wp-block-column-is-layout-flow\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; flex-grow: 0; min-width: 0px; overflow-wrap: break-word; word-break: break-word; flex-basis: 100%;\"></div></div>', 'How do I use skincare products', 'If you are like most people you dip your finger into your skincare and then rub the lotion or cream into your skin until it is all absorbed. You learned from television how to apply your skincare or you saw a relative or friend do it and figured out the rest from there.', 'If you are like most people you dip your finger into your skincare and then rub the lotion or cream into your skin until it is all absorbed. You learned from television how to apply your skincare or you saw a relative or friend do it and figured out the rest from there.', '2023-09-29'),
(13, 'THE SECRET OF SERUMS', 'the-secret-of-serums', './admin_asset/blog/f4417a87966238d208a69bb98f9093b7.png', '<div class=\"entry-content clr\" itemprop=\"text\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; counter-reset: footnotes 0;\"><p style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">Serums offer a way to deliver high quality actives to the deeper layers of the skin.&nbsp; One may think that an oily serum would be heavy on the skin, but in contrast it is most often not. Providing that the ingredients used are of a high quality and are fast absorbing, you will notice that your skin readily accepts oil.</p><p style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">Serums can help balance your skin’s PH and help protect your skin from harsh elements.&nbsp; Our serum is particularly good with reducing fine lines &amp; wrinkles and smoothing out skin texture.</p><div class=\"wp-block-columns is-layout-flex wp-container-2 wp-block-columns-is-layout-flex\" style=\"margin: 1.5em auto; gap: 2em; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; display: flex; align-items: center; flex-wrap: nowrap; max-width: 1250px;\"><div class=\"wp-block-column is-layout-flow wp-block-column-is-layout-flow\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; flex-grow: 0; min-width: 0px; overflow-wrap: break-word; word-break: break-word; flex-basis: 100%;\"><figure class=\"wp-block-table\" style=\"margin-bottom: 1em; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; overflow-x: auto;\"><table style=\"margin: 0px 0px 2.618em; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; border-spacing: 0px; width: 1250px;\"><tbody style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><tr style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><font face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; outline-color: initial; outline-style: initial; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; font-weight: inherit;\">SERUM</span></font></span></td><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><font face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; outline-color: initial; outline-style: initial; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; font-weight: inherit;\">DESCRIPTION</span></font></span></td></tr><tr style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><font face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; outline-color: initial; outline-style: initial; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; font-weight: inherit;\">VITAMIN C</span></font></span></td><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\">Vitamin C serums can be used at the same time as retinoids, however, make sure you apply your Vitamin C serum first, then make sure it dries properly before adding your retinol serum.</td></tr><tr style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><font face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; outline-color: initial; outline-style: initial; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; font-weight: inherit;\">RETINOIDS (Retinol or Vitamin A)</span></font></span></td><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\">Retinol&nbsp;(from Retinoids) form part of the Vitamin A family.<br>Retinol stimulates collagen production and cell renewal improving skin texture and evens tones.&nbsp; It targets fine lines and wrinkles.<br>Must be applied to a dry skin. If you apply your retinol in the morning, please use a sunscreen SPF 30 or higher as it is phototoxic (Makes your skin sensitive to light and cause damage)</td></tr><tr style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><font face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; outline-color: initial; outline-style: initial; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; font-weight: inherit;\">SALICYLIC ACID</span></font></span></td><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\">This treats skin conditions by softening and loosening dry, scaly, or thickened skin.&nbsp; It reduces swelling and redness and clears pores.&nbsp; It’s very good for reducing acne, pimples, white and blackheads.<br>It penetrates the deeper layers of your skin to dissolve keratin.</td></tr><tr style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><font face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; outline-color: initial; outline-style: initial; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; font-weight: inherit;\">HYALURONIC ACID</span></font></span></td><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\">Hyaluronic Acid can hold up to 1000 times its weight in water.&nbsp;<br>Hyaluronic Acid helps your skin to stretch and has also been shown to reduce scarring, reduce wrinkles and help wounds hear faster. It is a powerful moisturiser (plumps the outer layers of skin) and humectant (used to prevent the loss of moisture).</td></tr></tbody></table></figure></div></div><div class=\"wp-block-group is-layout-constrained wp-block-group-is-layout-constrained\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><div class=\"wp-block-group__inner-container\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: var(--wp--style--global--content-size); margin-right: auto !important; margin-left: auto !important;\"></div></div></div><div class=\"post-tags clr\" style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\"><span class=\"owp-tag-text\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">TAGS:&nbsp;</span><a href=\"https://skin-2-love.com/tag/dry-skin/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">DRY SKIN</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span>&nbsp;<a href=\"https://skin-2-love.com/tag/humectant/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">HUMECTANT</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span>&nbsp;<a href=\"https://skin-2-love.com/tag/hyaluronic-acid/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">HYALURONIC ACID</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span>&nbsp;<a href=\"https://skin-2-love.com/tag/pores/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">PORES</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span>&nbsp;<a href=\"https://skin-2-love.com/tag/retinol/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">RETINOL</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span>&nbsp;<a href=\"https://skin-2-love.com/tag/retinols/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">RETINOLS</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span>&nbsp;<a href=\"https://skin-2-love.com/tag/salicylic-acid/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">SALICYLIC ACID</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span>&nbsp;<a href=\"https://skin-2-love.com/tag/serums/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">SERUMS</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span>&nbsp;<a href=\"https://skin-2-love.com/tag/skin-moisture/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">SKIN MOISTURE</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span>&nbsp;<a href=\"https://skin-2-love.com/tag/vitamin-a/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">VITAMIN A</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span>&nbsp;<a href=\"https://skin-2-love.com/tag/vitamin-c/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">VITAMIN C</a></div>', 'THE SECRET OF SERUMS', 'Serums offer a way to deliver high quality actives to the deeper layers of the skin.  One may think that an oily serum would be heavy on the skin, but in contrast it is most often not. Providing that the ingredients used are of a high quality and are fast absorbing, you will notice that your skin readily accepts oil.', 'Serums offer a way to deliver high quality actives to the deeper layers of the skin.  One may think that an oily serum would be heavy on the skin, but in contrast it is most often not. Providing that the ingredients used are of a high quality and are fast absorbing, you will notice that your skin readily accepts oil.', '2023-09-29');
INSERT INTO `tbl_blog` (`blog_id`, `title`, `slug`, `path`, `description`, `meta_title`, `meta_keyword`, `meta_desc`, `blogdate`) VALUES
(14, 'WHAT’S REALLY IN MY SKIN CARE PRODUCTS?', 'whats-really-in-my-skin-care-products', './admin_asset/blog/bbae51e74453b17cc614617952cfb0f2.jpg', '<div class=\"entry-content clr\" itemprop=\"text\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; counter-reset: footnotes 0;\"><p style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">What ingredients are going to work best for you? Skin treatment options focus on the common issues we can have with our skin. We have listed some of our favourite ingredients.</p><div class=\"wp-block-columns is-layout-flex wp-container-2 wp-block-columns-is-layout-flex\" style=\"margin: 1.5em auto; gap: 2em; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; display: flex; align-items: center; flex-wrap: nowrap; max-width: 1250px;\"><div class=\"wp-block-column is-layout-flow wp-block-column-is-layout-flow\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; flex-grow: 0; min-width: 0px; overflow-wrap: break-word; word-break: break-word; flex-basis: 100%;\"><figure class=\"wp-block-table\" style=\"margin-bottom: 1em; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; overflow-x: auto;\"><table style=\"margin: 0px 0px 2.618em; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; border-spacing: 0px; width: 1250px;\"><tbody style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><tr style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">ISSUE</span></td><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">LOOK FOR THESE PRODUCTS IN YOUR SKINCARE</span></td></tr><tr style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><font face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; outline-color: initial; outline-style: initial; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; font-weight: inherit;\">SIGNS OF AGING</span></font></span></td><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\">Bakuchiol, Hyaluronic Acid, Hydrolised Quinoa, L Leucine, L Proline, Marine Collagen, Green Tea extract, Vegetable Glycerin, Argan Oil, Baobab Oil, Jojoba Oil, Lanolin, Rosehip Oil, vitamin A/retinoids, vitamin C, Vitamin E. SPF-boosting ingredients (e.g., Zinc Oxide, Titanium Dioxide) Good Sunscreen to prevent further damage)</td></tr><tr style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><font face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; outline-color: initial; outline-style: initial; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; font-weight: inherit;\">DRYNESS</span></font></span></td><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\">Allantoin, Hyaluronic Acid, Green Tea Extract, Sweet Orange Essential Oil, Baobab Oil, Macadamia oil, Rosehip oil, Vitamin C, Rose Hydrosol and Vitamin E.</td></tr><tr style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><font face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; outline-color: initial; outline-style: initial; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; font-weight: inherit;\">PIGMENTATION</span></font></span></td><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\">Bakuchiol, Niacinamide, Green Tea Extract, Bakuchi Oil, Glycerin, Rose Hydrosol, Kojic Acid, AHAs, BHA, Hydroquinone, Vitamin C.</td></tr><tr style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><font face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; outline-color: initial; outline-style: initial; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; font-weight: inherit;\">ACNE</span></font></span></td><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\">Allantoin, Alpha Bisabolol, Bakuchiol, Kelp Cellular Extract, Niacinamide, Salicylic Acid, Green Tea extract, Witch Hazel Extract, Lactic Acid, Bakuchi Oil. Essential oils like Egyptian Geranium, Grapefruit, Rose Geranium , Sweet Orange and Ylang Ylang. Glycerin, Jojoba Oil, Rosehip Oil. Hydrosols like Lavender, Neroli and Rose hydrosol. Azelaic acid, Vitamin A/Retinoids, Benzoyl Peroxide.</td></tr><tr style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><font face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; outline-color: initial; outline-style: initial; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; font-weight: inherit;\">ROSACEA</span></font></span></td><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\">Bakuchiol, Niacinamide, Green Tea Extract, Bakuchi Oil, Egyptian Geranium Essential Oil, Rose Geranium Essential Oil, Rose Hydrosol and Sulphur.</td></tr><tr style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><font face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; outline-color: initial; outline-style: initial; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; font-weight: inherit;\">PSORIASIS</span></font></span></td><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\">Allantoin, Hyaluronic Acid, Green Tea Extract, Glycerin, Rose Hydrosol, Vitamin A/retinoids, Vitamin D, Salicylic Acid, Urea and Lactic Acid.</td></tr><tr style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><font face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; outline-color: initial; outline-style: initial; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; font-weight: inherit;\">ECZEMA</span></font></span></td><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\">Allantoin, Bakuchiol, Green Tea extract, Glycerin, Rose Hydrosol.</td></tr></tbody></table></figure></div></div><div class=\"wp-block-group is-layout-constrained wp-block-group-is-layout-constrained\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><div class=\"wp-block-group__inner-container\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: var(--wp--style--global--content-size); margin-right: auto !important; margin-left: auto !important;\"></div></div></div><div class=\"post-tags clr\" style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\"><span class=\"owp-tag-text\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">TAGS: </span><a href=\"https://skin-2-love.com/tag/acne/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">ACNE</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/aging/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">AGING</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/argan-oil/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">ARGAN OIL</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/dry-skin/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">DRY SKIN</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/eczema/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">ECZEMA</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/green-tea-extract/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">GREEN TEA EXTRACT</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/hyaluronic-acid/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">HYALURONIC ACID</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/jojoba-oil/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">JOJOBA OIL</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/marine-collagen/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">MARINE COLLAGEN</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/psoriasis/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">PSORIASIS</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/retinols/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">RETINOLS</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/rosacea/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">ROSACEA</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/rosehip/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">ROSEHIP</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/salicylic-acid/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">SALICYLIC ACID</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/skin-care/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">SKIN CARE</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/skin-pigmentation/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">SKIN PIGMENTATION</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/skincare/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">SKINCARE</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/vitamin-c/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">VITAMIN C</a></div>', 'WHAT’S REALLY IN MY SKIN CARE PRODUCTS?', 'What ingredients are going to work best for you? Skin treatment options focus on the common issues we can have with our skin. We have listed some of our favourite ingredients.', 'What ingredients are going to work best for you? Skin treatment options focus on the common issues we can have with our skin. We have listed some of our favourite ingredients.', '2023-09-29');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cart`
--

CREATE TABLE `tbl_cart` (
  `cart_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_attribute_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `netprice` double DEFAULT NULL,
  `unique_id` varchar(200) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `entry_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tbl_cart`
--

INSERT INTO `tbl_cart` (`cart_id`, `product_id`, `product_attribute_id`, `category_id`, `price`, `qty`, `netprice`, `unique_id`, `email`, `entry_date`, `modify_date`) VALUES
(37, 26, 28, 10, 2000, 1, 2000, 'i9bfcdtrk3nih8meqiv6gejphqqechqj', '', '2024-03-27 18:20:20', '2024-03-28 12:45:42');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `category_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `description` longtext NOT NULL,
  `path` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`category_id`, `title`, `slug`, `description`, `path`) VALUES
(6, '3 Seater', '3-seater', '', './admin_asset/category/bb76321bee8abfb50343f2b02d9435ed.png'),
(7, '2 Seater', '2-seater', '', './admin_asset/category/52b41142425cbea5a24f5989bd0586c3.png'),
(8, 'Armchair', 'armchair', '', './admin_asset/category/3c93f514c3a689d2fe542852dc8a2f93.png'),
(9, 'Sofa Set', 'sofa-set', '', './admin_asset/category/a2b7eb8aa5f94a65a87416cdc4edfa71.png'),
(10, 'Corner Sofa', 'corner-sofa', '', './admin_asset/category/18cb736d84b96a4128716e7d78a6fb5a.png'),
(11, '4 Seater', '4-seater', '', './admin_asset/category/f3bfa84d84adfc2405f90ec6b6a439a5.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_client`
--

CREATE TABLE `tbl_client` (
  `client_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `path` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbl_client`
--

INSERT INTO `tbl_client` (`client_id`, `title`, `path`) VALUES
(10, 'Demo Client', './admin_asset/client/46fa3ab5f0eb5af7f7af9bafbf83c0fb.png'),
(11, 'parttner 2', './admin_asset/client/e90f4d7acfbc7d3dcc22d3b11d035cca.png'),
(12, 'insta', './admin_asset/client/445a04d7ee0a92883a17317216b2e482.png'),
(13, 'raja', './admin_asset/client/d9eb75fc13f75c1955d06215d8dda11a.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact`
--

CREATE TABLE `tbl_contact` (
  `contact_id` int(12) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `subject` varchar(2000) NOT NULL,
  `message` varchar(2000) NOT NULL,
  `datetime` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbl_contact`
--

INSERT INTO `tbl_contact` (`contact_id`, `name`, `email`, `phone`, `subject`, `message`, `datetime`) VALUES
(27, 'nishant', 'nishant@gmail.com', '789461320', '', 'demo mail', '2023-08-24 17:50:23'),
(28, 'nishant thummar', 'nishant@gmail.com', '5783453489', '', 'demo mail', '2023-08-24 17:51:25'),
(31, 'anuj', 'anuj@gmail.com', '789456130', 'Question or Comment', 'demo mail', '2023-08-31 19:49:02'),
(32, 'nishatn', 'nishatn@gmail.com', '8460124263', '', 'sdfsdfsdf', '2023-10-01 19:29:09');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_control`
--

CREATE TABLE `tbl_control` (
  `control_id` int(11) NOT NULL,
  `aboutus` enum('1','0') NOT NULL,
  `banner` enum('1','0') NOT NULL,
  `blog` enum('1','0') NOT NULL,
  `category` enum('1','0') NOT NULL,
  `client` enum('1','0') NOT NULL,
  `email_subscriber` enum('1','0') NOT NULL,
  `contact` enum('1','0') NOT NULL,
  `feedback` enum('1','0') NOT NULL,
  `gallery` enum('1','0') NOT NULL,
  `inquiry` enum('1','0') NOT NULL,
  `mainmenu` enum('1','0') NOT NULL,
  `permission` enum('1','0') NOT NULL,
  `policy` enum('1','0') NOT NULL,
  `review` enum('1','0') NOT NULL,
  `role` enum('1','0') NOT NULL,
  `seo` enum('1','0') NOT NULL,
  `subcategory` enum('1','0') NOT NULL,
  `submenu` enum('1','0') NOT NULL,
  `terms` enum('1','0') NOT NULL,
  `team` enum('1','0') NOT NULL,
  `user` enum('1','0') NOT NULL,
  `video` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbl_control`
--

INSERT INTO `tbl_control` (`control_id`, `aboutus`, `banner`, `blog`, `category`, `client`, `email_subscriber`, `contact`, `feedback`, `gallery`, `inquiry`, `mainmenu`, `permission`, `policy`, `review`, `role`, `seo`, `subcategory`, `submenu`, `terms`, `team`, `user`, `video`) VALUES
(1, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_control_feature`
--

CREATE TABLE `tbl_control_feature` (
  `control_feature_id` int(11) NOT NULL,
  `general` varchar(200) NOT NULL,
  `social` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `security` varchar(200) NOT NULL,
  `system` varchar(200) NOT NULL,
  `captcha` varchar(200) NOT NULL,
  `theme` varchar(200) NOT NULL,
  `payment` varchar(200) NOT NULL,
  `language` varchar(200) NOT NULL,
  `backup` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbl_control_feature`
--

INSERT INTO `tbl_control_feature` (`control_feature_id`, `general`, `social`, `email`, `security`, `system`, `captcha`, `theme`, `payment`, `language`, `backup`) VALUES
(1, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'),
(2, 'fas fa-cog', 'fas fa-search', 'fas fa-envelope', 'fas fa-lock', 'fas fa-power-off', 'fas fa-sync-alt', 'fas fa-palette', 'far fa-credit-card', 'fas fa-language', 'fas fa-database'),
(3, 'General settings such as, site title, site description, address and so on.', 'Search engine optimization settings, such as meta tags and social media.', 'Email SMTP settings, notifications and others related to email.', 'Security settings such as change admin login password and others.', 'PHP version settings, time zones and other environments.', 'Google Captcha Security settings for contact, inquiry and other form.', 'Change admin theme and other appearance changes as per your choice.', 'Change payment gateway setting such as razorpay, paypal, paytm, stripe, etc.', 'Change payment gateway setting such as razorpay, paypal, paytm, stripe, etc.', 'Set the backup settings below');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_coupon`
--

CREATE TABLE `tbl_coupon` (
  `coupon_id` int(11) NOT NULL,
  `coupon_code` varchar(50) NOT NULL,
  `coupon_description` varchar(2000) NOT NULL,
  `coupon_percentage` int(11) NOT NULL,
  `expiry_date` date NOT NULL,
  `product` varchar(1000) NOT NULL,
  `category` varchar(1000) NOT NULL,
  `minimum_subtotal` char(11) NOT NULL,
  `maximum_subtotal` char(11) NOT NULL,
  `free_shipping` char(10) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `entry_date` date DEFAULT NULL,
  `modify_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tbl_coupon`
--

INSERT INTO `tbl_coupon` (`coupon_id`, `coupon_code`, `coupon_description`, `coupon_percentage`, `expiry_date`, `product`, `category`, `minimum_subtotal`, `maximum_subtotal`, `free_shipping`, `status`, `entry_date`, `modify_date`) VALUES
(3, 'SUMMER10', 'TEST', 10, '2024-01-19', '[\"19\",\"16\"]', '', '55', '150', NULL, 1, '2024-01-12', '2024-01-18'),
(4, 'neworder', '', 15, '2024-01-13', '[\"14\"]', '[\"9\",\"8\"]', '50', '100', NULL, 1, '2024-01-12', '2024-01-12'),
(5, 'FREESHIP', 'free shipping', 0, '2024-02-24', '[\"14\",\"15\",\"19\"]', '', '40', '200', 'yes', 1, '2024-02-13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_email_subscriber`
--

CREATE TABLE `tbl_email_subscriber` (
  `email_subscriber_id` int(12) NOT NULL,
  `email` varchar(500) NOT NULL,
  `datetime` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbl_email_subscriber`
--

INSERT INTO `tbl_email_subscriber` (`email_subscriber_id`, `email`, `datetime`) VALUES
(9, 'example1@mail.com', '2022-10-06 12:50:35'),
(10, 'example2@mail.com', '2022-10-06 12:50:35'),
(11, 'fgfg@sdf.dfgfd', '2023-09-30 18:55:03');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_faq`
--

CREATE TABLE `tbl_faq` (
  `faq_id` int(11) NOT NULL,
  `question` varchar(2000) NOT NULL,
  `answer` longtext NOT NULL,
  `entry_date` date NOT NULL,
  `modify_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tbl_faq`
--

INSERT INTO `tbl_faq` (`faq_id`, `question`, `answer`, `entry_date`, `modify_date`) VALUES
(13, 'What services does Dhillon Printing offer?', 'Dhillon Printing offers a wide range of printing services, including business cards, flyers, brochures, banners, posters, stationery, and more. We also provide graphic design services to assist with your printing needs.', '2023-09-12', '0000-00-00'),
(14, 'How can I request a quote for a printing project?', 'You can request a quote by visiting our website and using our online quote request form. Alternatively, you can contact our customer service team via phone or email to discuss your project and get a quote.', '2023-09-12', '0000-00-00'),
(15, 'Can you help with design and artwork for my printing project?', 'Yes, we offer graphic design services to help create or enhance artwork for your printing project. Our design team can work with you to ensure your materials look professional and eye-catching.', '2023-09-12', '0000-00-00'),
(16, 'Do you offer bulk printing discounts?', 'Yes, we offer discounts for bulk or large quantity printing orders. The exact discount may vary based on the project, so please reach out to our team for pricing details.', '2023-09-12', '0000-00-00'),
(17, 'What types of paper and finishes are available for printing projects?', 'We offer a variety of paper options, including gloss, matte, and specialty papers. We also provide different finishes such as UV coating, embossing, and more. You can discuss your preferences with our team to choose the best options for your project.', '2023-09-12', '0000-00-00'),
(18, 'Can you handle rush orders or same-day printing?', 'Depending on our current workload, we may be able to accommodate rush orders or same-day printing requests. Please contact us as soon as possible to check availability.', '2023-09-12', '0000-00-00'),
(19, 'What payment methods do you accept?', 'We accept various payment methods, including credit cards, debit cards, checks, and bank transfers. Payment details will be provided when you place your order.', '2023-09-12', '0000-00-00'),
(20, 'How can I place an order with Dhillon Printing?', 'You can place an order by visiting our website and using our online order form, by calling our customer service team, or by visiting our physical location. Our team will guide you through the process.', '2023-09-12', '0000-00-00'),
(21, 'How can I share my Canva design file with Dhillon Printing?', 'To share your Canva design file with Dhillon Printing, follow these steps:\r\n1. Open your Canva design project.\r\n2. Click on the \"Share\" button in the upper right corner.\r\n3. Choose the \"Share a link\" option.\r\n4. Click \"Copy Link\" to copy the shareable link to your clipboard.\r\n5. Send the copied link to Dhillon Printing through email or the preferred communication method.', '2023-09-12', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_feedback`
--

CREATE TABLE `tbl_feedback` (
  `feedback_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `message` varchar(1500) NOT NULL,
  `datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gallery`
--

CREATE TABLE `tbl_gallery` (
  `gallery_id` int(11) NOT NULL,
  `category` varchar(50) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `path` varchar(2000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_inquiry`
--

CREATE TABLE `tbl_inquiry` (
  `inquiry_id` int(12) NOT NULL,
  `name` varchar(500) NOT NULL,
  `email` varchar(500) NOT NULL,
  `phone` varchar(500) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `subject` varchar(500) NOT NULL,
  `message` varchar(2000) NOT NULL,
  `datetime` datetime NOT NULL,
  `status` int(11) NOT NULL COMMENT '0-pending | 1- approve'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mainmenu`
--

CREATE TABLE `tbl_mainmenu` (
  `mainmenu_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `position` int(11) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `controller` varchar(50) NOT NULL,
  `url` varchar(200) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbl_mainmenu`
--

INSERT INTO `tbl_mainmenu` (`mainmenu_id`, `title`, `slug`, `position`, `icon`, `controller`, `url`, `status`) VALUES
(22, 'Manage FAQ\'s', 'manage-faq/?(:any)?/?(:any)?', 1, 'fas fa-question-circle', 'faq', 'Admin/Pages/faq/$2/$3', 1),
(27, 'Manage Product', 'manage-product/?(:any)?/?(:any)?', 2, 'fab fa-product-hunt', 'product', 'Admin/Pages/product/$2/$3', 1),
(28, 'Manage Order', 'manage-bill/?(:any)?/?(:any)?', 3, 'fas fa-file-invoice', 'bill', 'Admin/Pages/bill/$2/$3', 1),
(29, 'Manage Users', 'manage-register/?(:any)?/?(:any)?', 4, 'fas fa-users', 'register', 'Admin/Pages/register/$2/$3', 1),
(30, 'coupon', 'manage-coupon/?(:any)?/?(:any)?', 5, 'fas fa-tags', 'coupon', 'Admin/Pages/coupon/$2/$3', 1),
(31, 'Size', 'manage-size/?(:any)?/?(:any)?', 6, 'far fa-clipboard', 'size', 'Admin/Pages/size/$2/$3', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

CREATE TABLE `tbl_menu` (
  `menu_id` int(11) NOT NULL,
  `text` varchar(100) NOT NULL,
  `icon` varchar(30) NOT NULL,
  `href` varchar(200) NOT NULL,
  `title` varchar(100) NOT NULL COMMENT 'slug',
  `target` varchar(10) NOT NULL,
  `parent` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_permission`
--

CREATE TABLE `tbl_permission` (
  `permission_id` int(11) NOT NULL,
  `menu` char(50) NOT NULL,
  `type` varchar(15) NOT NULL,
  `feature` varchar(30) NOT NULL,
  `all` enum('0','1') NOT NULL,
  `read` enum('0','1') NOT NULL,
  `write` enum('0','1') NOT NULL,
  `edit` enum('0','1') NOT NULL,
  `delete` enum('0','1') NOT NULL,
  `status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbl_permission`
--

INSERT INTO `tbl_permission` (`permission_id`, `menu`, `type`, `feature`, `all`, `read`, `write`, `edit`, `delete`, `status`) VALUES
(1, 'site pages', 'page', 'contact', '0', '1', '0', '0', '1', '0'),
(2, 'site pages', 'page', 'aboutus', '1', '1', '1', '1', '1', '0'),
(3, 'site pages', 'page', 'banner', '1', '1', '1', '1', '1', '0'),
(4, 'site pages', 'page', 'gallery', '1', '1', '1', '1', '1', '0'),
(5, 'site pages', 'page', 'client', '1', '1', '1', '1', '1', '0'),
(6, 'site pages', 'page', 'terms', '1', '1', '1', '1', '1', '0'),
(7, 'site pages', 'page', 'policy', '1', '1', '1', '1', '1', '0'),
(8, 'site pages', 'page', 'review', '1', '1', '1', '1', '1', '0'),
(9, 'site pages', 'page', 'blog', '1', '1', '1', '1', '1', '0'),
(10, 'site pages', 'page', 'feedback', '0', '1', '0', '0', '1', '0'),
(11, 'site pages', 'page', 'team', '1', '1', '1', '1', '1', '0'),
(12, 'site pages', 'page', 'video', '1', '1', '1', '1', '1', '0'),
(13, 'site pages', 'page', 'inquiry', '0', '1', '0', '0', '1', '0'),
(14, 'site pages', 'page', 'seo', '1', '1', '1', '1', '1', '0'),
(15, 'site pages', 'page', 'category', '1', '1', '1', '1', '1', '0'),
(16, 'site pages', 'page', 'subcategory', '1', '1', '1', '1', '1', '0'),
(17, 'site pages', 'page', 'user', '1', '1', '1', '1', '1', '1'),
(18, 'site pages', 'page', 'role', '1', '1', '1', '1', '1', '0'),
(19, 'site pages', 'page', 'permission', '1', '1', '1', '1', '1', '0'),
(20, 'site pages', 'page', 'mainmenu', '1', '1', '1', '1', '1', '0'),
(21, 'site pages', 'page', 'submenu', '1', '1', '1', '1', '1', '0'),
(22, '', 'feature', 'general', '1', '1', '0', '1', '0', '0'),
(23, '', 'feature', 'social', '1', '1', '0', '1', '0', '0'),
(24, '', 'feature', 'email', '1', '1', '0', '1', '0', '0'),
(25, '', 'feature', 'security', '1', '1', '0', '1', '0', '0'),
(26, '', 'feature', 'system', '1', '1', '0', '1', '0', '0'),
(27, '', 'feature', 'captcha', '1', '1', '0', '1', '0', '0'),
(28, '', 'feature', 'theme', '1', '1', '0', '1', '0', '0'),
(29, '', 'feature', 'payment', '1', '1', '0', '1', '0', '0'),
(30, '', 'feature', 'language', '1', '1', '0', '1', '0', '0'),
(31, '', 'feature', 'backup', '1', '1', '0', '1', '0', '0'),
(39, 'faq', 'page', 'faq', '1', '1', '1', '1', '1', '1'),
(44, 'product', 'page', 'product', '1', '1', '1', '1', '1', '1'),
(45, 'bill', 'page', 'bill', '1', '1', '1', '1', '1', '1'),
(46, 'register', 'page', 'register', '1', '1', '1', '1', '1', '1'),
(47, 'coupon', 'page', 'coupon', '1', '1', '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_permission_assign`
--

CREATE TABLE `tbl_permission_assign` (
  `permission_assigned_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `permission_id` varchar(100) NOT NULL,
  `type` varchar(15) NOT NULL,
  `all` enum('1','0') NOT NULL,
  `read` enum('1','0') NOT NULL,
  `write` enum('1','0') NOT NULL,
  `edit` enum('1','0') NOT NULL,
  `delete` enum('1','0') NOT NULL,
  `status` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbl_permission_assign`
--

INSERT INTO `tbl_permission_assign` (`permission_assigned_id`, `role_id`, `permission_id`, `type`, `all`, `read`, `write`, `edit`, `delete`, `status`) VALUES
(601, 1, '[22,23,25,26,28]', 'feature', '1', '1', '1', '1', '1', '1'),
(602, 1, '[22,23,25,26,28]', 'feature', '0', '1', '0', '0', '0', '0'),
(603, 1, '[22,23,25,26,28]', 'feature', '0', '0', '0', '1', '0', '0'),
(604, 1, '[2,3,6,7,15,39,44,45,46]', 'page', '1', '1', '1', '1', '1', '1'),
(605, 1, '[1,2,3,6,7,15,39,44,45,46]', 'page', '0', '1', '0', '0', '0', '0'),
(606, 1, '[2,3,6,7,15,39,44,45,46]', 'page', '0', '0', '1', '0', '0', '0'),
(607, 1, '[2,3,6,7,15,39,44,45,46]', 'page', '0', '0', '0', '1', '0', '0'),
(608, 1, '[1,2,3,6,7,15,39,44,45,46]', 'page', '0', '0', '0', '0', '1', '0'),
(609, 1, '[39,44,45,46]', 'page', '0', '0', '0', '0', '0', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_policy`
--

CREATE TABLE `tbl_policy` (
  `policy_id` int(11) NOT NULL,
  `policy` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbl_policy`
--

INSERT INTO `tbl_policy` (`policy_id`, `policy`) VALUES
(5, '<p style=\"box-sizing: inherit;\">infinit is an e-market that is operated by i.solutions, a Saudi corporation that is incorporated under the commercial law of Saudi Arabia and abided to protect your privacy online.</p><p style=\"box-sizing: inherit;\">This Privacy Policy is a legal agreement that explains the kind of personal information we gather from our platform, how we use this information, and how we keep it safe and secured.</p><p style=\"box-sizing: inherit;\"><u>Please read the following terms very carefully and be informed that:</u></p><p style=\"box-sizing: inherit;\">By accessing this page and using our platform, you acknowledge that you have read, understood, and agreed to all the terms of our privacy policy.</p><p style=\"box-sizing: inherit;\">Should you have any objection and/or rejection to any aspect of these following terms, please exit this page and do not access our platform for any purpose:</p><p style=\"box-sizing: inherit;\">1. Infinit collects your personal information to enable our service delivery to your end, as prescribed without any other usages that are not declared.</p><p style=\"box-sizing: inherit;\">2. Strict security measures are implemented to eliminate any possibility of unauthorized access, use, alteration, disclosure or destruction of your personal information.</p><p style=\"box-sizing: inherit;\">3. Contacts details provided by you (E-mail and/or mobile) will only be used to communicate information in coloration to your order or/and to respond to your enquiries and requests.</p><p style=\"box-sizing: inherit;\">4. We ensure that sensitive data (payment details) are fully encrypted and protected safely, by transmitting it over a Secure Sockets Layer (SSL)-secured communication channel to its payment gateway.&nbsp;</p><p style=\"box-sizing: inherit;\">5. Your payment details are automatically transferred to the world\'s most trusted payment gateway&nbsp;(Amazon payment gateway)&nbsp; via secured connection for the purpose of progressing and executing your payment.</p><p style=\"box-sizing: inherit;\">6. Your contact details may&nbsp;be shared with a&nbsp;3PL carrier for the purpose of shipping your order and fulfilling the delivery</p><p style=\"box-sizing: inherit;\">7. All payment information such as credit/debit cards’ details are not stored and/or used in any way other than collecting your payment through the payment gateway.</p><p style=\"box-sizing: inherit;\">8. infiniti is abided not to share and/or dismiss any part of your personal information with a 3<span style=\"box-sizing: inherit;\">rd</span>&nbsp;part, other than the only two bodies mentioned above.</p><p style=\"box-sizing: inherit;\">9. We will not utilize your persona information broadcasting promotions and offers unless you decide to subscribe to our updates.</p><p style=\"box-sizing: inherit;\">10. Our platform used cookies and other technologies to detect your IP address and record your browsing history to help us shape a better customer experience based on customer preferences.</p><p style=\"box-sizing: inherit;\">11. Should you have any questions or feedback about our privacy policy, feel free to talk to us via E-mail or Whatsapp.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE `tbl_product` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sku` char(50) NOT NULL,
  `title` varchar(500) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `price` char(30) NOT NULL,
  `sale_price` char(20) NOT NULL,
  `description` longtext NOT NULL,
  `meta_title` varchar(250) NOT NULL,
  `meta_keyword` varchar(250) NOT NULL,
  `meta_desc` varchar(250) NOT NULL,
  `featured` int(11) NOT NULL,
  `sale` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `entry_date` date NOT NULL,
  `modify_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`product_id`, `category_id`, `sku`, `title`, `slug`, `price`, `sale_price`, `description`, `meta_title`, `meta_keyword`, `meta_desc`, `featured`, `sale`, `status`, `entry_date`, `modify_date`) VALUES
(22, 9, 'DFSG', 'Oscar - Sofa set', 'oscar-sofa-set', '3620', '', '<p><span style=\"box-sizing: inherit;\">1. Three seater: 230 w x 85 d x 75h (cm) <br style=\"box-sizing: inherit;\">2. Two seater: 165 w x 85 d x 75 h (cm)<br style=\"box-sizing: inherit;\">3. One seater: 100 w x 85 d x 75 h (cm)<br style=\"box-sizing: inherit;\"></span><span style=\"box-sizing: inherit;\">4. Textile Type: Velvet</span><br style=\"box-sizing: inherit;\"><span color=\"#0e0e0e\" style=\"box-sizing: inherit;\">5. Color: Navy </span><span color=\"#0e0e0e\" style=\"box-sizing: inherit;\"><br style=\"box-sizing: inherit;\">6. No. of items:<span style=\"box-sizing: inherit;\"> </span>3<span style=\"box-sizing: inherit;\"> </span>pcs. </span><br></p>', 'Oscar - Sofa set', 'Oscar - Sofa set', 'Oscar - Sofa set', 1, 0, 1, '2024-03-16', '2024-03-16'),
(23, 8, 'FDG5464', 'Palm Archair', 'palm-archair', '3600', '3200', '<ul><li>sf sdf sdf </li><li> s</li><li>df sf</li></ul>', 'fds', 'fsdfsd', 'fsdfds', 1, 1, 1, '2024-03-18', '2024-03-26'),
(24, 6, 'LISN34', 'Luna - 3 Seater', 'luna-3-seater', '1670', '', '<p><span style=\"box-sizing: inherit;\">1. Dimensions:&nbsp;230 w x 85 d x 75 h&nbsp;(cm)</span><br style=\"box-sizing: inherit;\"><span style=\"box-sizing: inherit;\">2. Textile Type:&nbsp;Velvet</span><br style=\"box-sizing: inherit;\"><span color=\"#0e0e0e\" style=\"box-sizing: inherit;\">3. Color: Rose&nbsp;</span><br></p>', 'Luna - 3 Seater', 'Luna - 3 Seater', 'Luna - 3 Seater', 1, 0, 1, '2024-03-19', '0000-00-00'),
(25, 6, 'S345S', 'Swan - 3 Seater', 'swan-3-seater', '1770', '1500', '<p><span style=\"box-sizing: inherit;\">1. Dimensions: 235 w x 85 d x 75 h (cm)</span><br style=\"box-sizing: inherit;\"><span style=\"box-sizing: inherit;\">2. Textile Type: Velvet </span><br style=\"box-sizing: inherit;\"><span color=\"#0e0e0e\" style=\"box-sizing: inherit;\">3. Color: Green</span><br></p>', 'Swan - 3 Seater', '', '', 1, 1, 1, '2024-03-19', '2024-03-27'),
(26, 10, 'SD45654', 'Anastasia - Corner Sofa', 'anastasia-corner-sofa', '2250', '2000', '<p><span data-mce-fragment=\"1\" style=\"box-sizing: inherit;\">1. Dimensions: 230 L x 170  L x 90 d x 80 h (cm)</span><br data-mce-fragment=\"1\" style=\"box-sizing: inherit;\"><span data-mce-fragment=\"1\" style=\"box-sizing: inherit;\">2. Textile Type: Velvet </span><br data-mce-fragment=\"1\" style=\"box-sizing: inherit;\"><span color=\"#0e0e0e\" data-mce-fragment=\"1\" style=\"box-sizing: inherit;\">3. Color: Black <br data-mce-fragment=\"1\" style=\"box-sizing: inherit;\">4. Pillows included: Yes </span><br></p>', 'Anastasia - Corner Sofa', 'Anastasia - Corner Sofa', 'Anastasia - Corner Sofa', 1, 1, 1, '2024-03-19', '2024-03-27'),
(27, 10, 'DF345654', 'Jupiter- Corner Sofa', 'jupiter-corner-sofa', '3180', '', '<p><span data-mce-fragment=\"1\" style=\"box-sizing: inherit;\">1. Dimensions: 370 L x 80 d x 80 h (cm)</span><br data-mce-fragment=\"1\" style=\"box-sizing: inherit;\"><span data-mce-fragment=\"1\" style=\"box-sizing: inherit;\">2. Textile Type: Velvet </span><br data-mce-fragment=\"1\" style=\"box-sizing: inherit;\"><span color=\"#0e0e0e\" data-mce-fragment=\"1\" style=\"box-sizing: inherit;\">3. Color: Dark Grey <br data-mce-fragment=\"1\" style=\"box-sizing: inherit;\">4. Pillows included: Yes </span><br></p>', '', '', '', 1, 0, 1, '2024-03-19', '2024-03-27'),
(28, 9, '', 'Papillon - 3 Seater', 'papillon-3-seater', '1920', '', '<p><span style=\"box-sizing: inherit;\">1. Dimensions: 250 w x 105 d x 85 h (cm)</span><br style=\"box-sizing: inherit;\"><span style=\"box-sizing: inherit;\">2. Textile: Velvet</span><br style=\"box-sizing: inherit;\"><span style=\"box-sizing: inherit;\">3. Color: Tangerine<br style=\"box-sizing: inherit;\">4. Pillows included: No</span><br></p>', 'Papillon - 3 Seater', 'Papillon - 3 Seater', 'Papillon - 3 Seater', 1, 0, 1, '2024-03-23', '2024-03-27'),
(35, 11, '', 'Stream Selector Test', 'stream-selector-test', '2500', '', 'This test helps students determine the most suitable academic stream for their future studies by assessing their skills, interests, and aptitudes across various subjects. It provides guidance in making informed decisions regarding science, humanities, or commerce streams.', 'Stream Selector Test', '', '', 1, 0, 1, '2024-03-27', '2024-03-27'),
(36, 10, '', 'Science Career selector', 'science-career-selector', '3500', '2900', 'Designed for individuals interested in science, this test evaluates their knowledge, skills, and passion for scientific subjects. It assists in identifying potential career paths within the scientific field, such as medicine, research, engineering, or other related disciplines.', 'Science Career selector', '', '', 0, 1, 1, '2024-03-27', '2024-03-27'),
(37, 7, '', 'Humanities Career selector', 'humanities-career-selector', '2200', '', 'Tailored for individuals inclined towards the humanities, this test assesses their understanding, critical thinking, and creativity in subjects like literature, history, philosophy, and social sciences. It offers insights into suitable career options, including teaching, journalism, law, or arts-related fields.', 'Humanities Career selector', '', '', 0, 0, 1, '2024-03-27', '2024-03-27');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_attribute`
--

CREATE TABLE `tbl_product_attribute` (
  `product_attribute_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `sku` char(20) NOT NULL,
  `color` char(25) NOT NULL,
  `product_photos` longtext NOT NULL,
  `entry_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tbl_product_attribute`
--

INSERT INTO `tbl_product_attribute` (`product_attribute_id`, `product_id`, `sku`, `color`, `product_photos`, `entry_date`) VALUES
(20, 22, '', 'grey', './admin_asset/product_photos/10072e88ad2b177ceab29adf5941ad65.png,./admin_asset/product_photos/d8c51eeb90c4fc29b915a43af8acc192.png,./admin_asset/product_photos/9d9d8405c7a960815251bf4bd961886b.png', '2024-03-16'),
(21, 22, '', 'white', './admin_asset/product_photos/60789b69fecf6adf5b7ddae1ec26609e.png,./admin_asset/product_photos/38fda13461bae7d021cedaa3ddf5dc3a.png,./admin_asset/product_photos/63c116af0bb546d18bab9f1a61da6f31.png', '2024-03-16'),
(25, 24, '', 'pink', './admin_asset/product_photos/78ebfd807b12368c7e482e8aabd2afcd.png,./admin_asset/product_photos/df69069f4ae4e0c5a55f7703f9068243.png,./admin_asset/product_photos/310f7c8791e85e6ba10cebddbee02475.png,./admin_asset/product_photos/fe014f146bb781348f7a5efaa12cfae7.png', '2024-03-19'),
(28, 26, 'FG345', 'black', './admin_asset/product_photos/5b49ba161b8ff65f3304558d0847a3e5.png', '2024-03-27'),
(34, 25, 'DFS456', 'green', './admin_asset/product_photos/7cdbdb28a0f67dde92955b49e0fccf67.jpg,./admin_asset/product_photos/200b60e57765ddcfba5a93c8135e5bb3.jpg', '2024-03-27'),
(35, 25, 'DFD345', 'orange', './admin_asset/product_photos/830a61263e3cb673d8b910f0805a442f.jpg', '2024-03-27'),
(38, 28, 'ORA34', 'orange', './admin_asset/product_photos/a421dd54d961993c2218e72fe89d8d12.jpg', '2024-03-27'),
(39, 28, 'MA436', 'black', './admin_asset/product_photos/316458c6f1b0ef493033606f6418230c.jpg', '2024-03-27'),
(40, 27, '654GREY', 'pink', './admin_asset/product_photos/18b23d5afd3d783dfa8a5e9bd19d154a.jpg,./admin_asset/product_photos/8bcac5df925a82a1557a1691ab4e8c7b.jpg,./admin_asset/product_photos/6414f850382b74d96bded13d8aaf8725.jpg', '2024-03-27'),
(41, 27, '123PUR', 'white', './admin_asset/product_photos/12ca9d08224d88c897cae01e6dd54df4.jpg', '2024-03-27'),
(42, 23, '', 'red', './admin_asset/product_photos/92124646d47506e365a07855a2bf5331.jpg,./admin_asset/product_photos/138180ab5dc6f1910a37422874db13f2.jpg', '2024-03-26'),
(43, 23, '', 'blue', './admin_asset/product_photos/41c2eaa265fb27c75f8ce5019ba10a71.jpg,./admin_asset/product_photos/33b9f210e20020f95dc6bb5c53bdbb27.jpg,./admin_asset/product_photos/386155a768d9e2310e416e20138e3e55.jpg', '2024-03-26'),
(44, 23, '', 'black', './admin_asset/product_photos/64d5aff5a922cfd10a01ba4d2986ebde.jpg,./admin_asset/product_photos/9f4ac212082f2a8df82106ab8521e2d1.jpg', '2024-03-26'),
(47, 31, 'dfg345', 'orange', '', '2024-03-27'),
(48, 37, 'sdf345', 'orange', './admin_asset/product_photos/a3344060792b19298b729fc44da967e4.jpg', '2024-03-27'),
(49, 36, 'sdf534645', 'white', './admin_asset/product_photos/f308a531d1c4784d12f6afb78a8c4135.jpg', '2024-03-27'),
(50, 35, 'f345', 'pink', './admin_asset/product_photos/441f2814f197627b94d31f588180b263.jpg', '2024-03-27');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_review`
--

CREATE TABLE `tbl_product_review` (
  `product_review_id` int(11) NOT NULL,
  `register_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `rating` char(50) DEFAULT NULL,
  `review` varchar(4000) DEFAULT NULL,
  `entry_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tbl_product_review`
--

INSERT INTO `tbl_product_review` (`product_review_id`, `register_id`, `product_id`, `rating`, `review`, `entry_date`) VALUES
(1, 1, 3, '3', 'this is demo rate and review', '2023-03-27'),
(2, 1, 3, '3', 'this is demo second rate and review', '2023-03-27'),
(3, 1, 1, '5', 'hello', '2023-03-27'),
(4, 1, 4, '5', 'deee', '2023-04-01'),
(5, 1, 3, '5', 'dsf sdf sd fds f', '2023-04-01'),
(6, 1, 3, '4', '', '2023-04-01'),
(7, 1, 3, '3', 'sfsd fsfdsf', '2023-04-01'),
(8, 1, 3, '5', 'sfsd fsfdsf sdfsf sdf', '2023-04-01'),
(9, 1, 3, '1', 'sfsd fsfdsf sdf sdf sdf sdf sd', '2023-04-01'),
(10, 1, 3, '2', 'revv', '2023-04-01');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_register`
--

CREATE TABLE `tbl_register` (
  `register_id` int(11) NOT NULL,
  `fname` char(50) DEFAULT NULL,
  `email` char(70) DEFAULT NULL,
  `phone` char(18) DEFAULT NULL,
  `gender` char(10) DEFAULT NULL,
  `country` char(50) NOT NULL,
  `state` char(50) DEFAULT NULL,
  `city` char(50) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `postal_code` char(15) DEFAULT NULL,
  `path` varchar(500) DEFAULT NULL,
  `password` varchar(500) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `delete_status` int(11) DEFAULT NULL,
  `payment_status` tinyint(4) DEFAULT NULL,
  `register_date` date DEFAULT NULL,
  `modify_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tbl_register`
--

INSERT INTO `tbl_register` (`register_id`, `fname`, `email`, `phone`, `gender`, `country`, `state`, `city`, `address`, `postal_code`, `path`, `password`, `status`, `delete_status`, `payment_status`, `register_date`, `modify_date`) VALUES
(1, 'nishant j thummar', 'nishant@gmail.com', '8460124262', 'male', 'india', 'gujarat', 'surat', '170 shubham row house', '395006', './admin_asset/customer/4f2d8225f76dbf5a59aabbc4911833ea.png', '7eafa30109230865c2bbf7139d086a9879ff2668230c679cfaa1eb0a4f95b6420509d0a06fc64767cdfc6a950dfd1ce951666bd1d388fd3ddde04df59222d2cbHm9HETnADcFlumuWTBTrSP/NOgC1b3fR64+N5kkcGwE=', NULL, 0, 0, '2022-12-16', NULL),
(2, 'komal ukani', 'komal@gmail.com', '9925400512', '', '', '0', NULL, NULL, '', '', '4d6e8b769f205da8c0983f9bbba9fc9696dfd7185bf36a17d2e052775ddd3bbc353c106e3fd038272458c3f3458a8540ded40d5568c2742bc0b2a09cc0f6d10fv8kSJoYbygoQr+otDfRDvlZjA5P2JPisHhhnz3rTVcM=', NULL, 0, 0, '2022-12-17', NULL),
(4, 'Nisha Patel', 'nisha@gmail.com', '84601242633', 'female', '', '12', '1041', '45 avenue park near new coloni', '395006', '', '36dbcecedc19b51772406053dc7ff246732f11f5a20237977286c99e5b0931dcee8997d7982752baa78be59460e97220327005a5d6da5133136268d445ec3a78HE8ixBP8J9un9GquerppaodVyUUJT0PTSNuZzToJdIc=', 1, 0, 0, '2023-01-27', NULL),
(8, 'Jiten Chauhan', 'jiten@gmail.com', '8460124263', 'male', '', '15', '1285', 'demo ', '789798', '', '6bd3d48c27bb35f0d795edd5a7a27390f3d5ef2d1145aea33d8d83c1a56bf1a664567c789cd9fe3c344e4f02d7f12cf8198badabf6f90d7089bd2e9dec8fdc9a0I7NFo/v9NsNUF6w0QubPQTqlBNyVVYxQDyLE1BALGk=', 1, 0, 0, '2023-01-29', NULL),
(9, 'Ronak Sir', 'ronak@gmail.com', '9925544000', 'male', '', '31', '3119', 'dsf', '345345', '', 'bb7e5d53fb39fa5d4c98fb680b5bc26d27b551c0b87da1542ec68e75094e43fef97bb74b23553b7fcead05f65c386870debc04c9e3f4d4ecdbd581e5d40cc043b5WlckAGnMtXImMENfrbcKjXHdghVcZzJaQI9oECJio=', 1, 0, 0, '2023-01-30', NULL),
(12, 'vira patel', 'vira@gmail.com', '789456130111', 'female', '', NULL, NULL, '170 shubham row house', '789456', './admin_asset/customer/b8a03b2ffec1a2ac986976bf7eede281.png', 'd551707827a817adb285e30185a49318fdd2484b04537d59e69728ebb37446da5f02fc1d0dd92a0f093d03f6835ede641d72553103a5979be2fcd7b50a237758f3Oo36vZKLGY+urZD/kg3GbJmQz9npmbQdVc0H1pkf0=', 1, NULL, NULL, '2023-02-16', NULL),
(13, 'komal', 'komal123@gmail.com', '784561300000', NULL, '', NULL, NULL, NULL, NULL, NULL, '998f3f81a7955afd87a0d0c471e65b4e744c49ad434be85049309f4e680aaee6fa526555d8638a85b6d7749cb41968c7fc85b630a3941dc373a15f4e9f4fe33dJSC2ZwAAmhD4bP3L+OuC+nLMhzl1jUHxBb9tytnSasE=', 1, NULL, NULL, '2023-04-14', NULL),
(14, 'karan kikani', 'karan@gmail.com', '7485125263', 'male', 'nz', 'wall', 'wall', 'wall', '4652', './admin_asset/customer/5092498f2d0a40c6726acdd77452b93f.png', '0864bc47f866c80536f1d711e06c7d7b932e322defd3f341fab0e4606090d89c504b19135c90dfef5356f87be9161e811a94c4fcc182a8d8e28d5984f5c12f6dno4m093LFdX0PboyjXhW45XLLqil2BISkkWe2Gzav+M=', 1, NULL, NULL, '2023-10-04', NULL),
(15, 'hira thakur', 'hira@gmail.com', '6541324456', NULL, '', NULL, NULL, NULL, NULL, NULL, '8c733e47b0e151d0ab23dd31c6f36f7b9c58d6f3b773d728a5ff9b568e83f5e3af21aa0db3af3d9dd4b1e036ae7ad10b30cad1c81c998eaff6ae5889526833aefhCGo8uoX2qj/CnXZL8mvCLGIef1HQGYgYe8URjSIFw=', 1, NULL, NULL, '2023-10-30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_review`
--

CREATE TABLE `tbl_review` (
  `review_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `review` varchar(2000) NOT NULL,
  `datetime` datetime NOT NULL,
  `path` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbl_review`
--

INSERT INTO `tbl_review` (`review_id`, `username`, `review`, `datetime`, `path`) VALUES
(18, 'John William', 'With a deep understanding of our client’s business needs areas, extensive resources, and a client people focused approach, Collabera has developed specialized recruiting teams who provide resources in various areas.', '2022-10-06 00:00:00', './admin_asset/review/32ea0dacee64f1c86f829b5230cb9394.jpg'),
(19, 'Mayur Patel', 'Dhillon Printing take care of people because they care! They pay attention to the little things and provide outstanding employee care throughout their journey from the day an offer is made.', '2023-06-28 00:00:00', './admin_asset/review/f40e1630b8794e16238c1b045461b210.jpg'),
(20, 'Herry Tom', 'Dhillon Printing did an incredible job in creating our new marketing & sales pieces. Their prices are very competitive, and their quality is excellent. Thank you for the excellent service on the brochure reprints and the new display posters.', '2023-08-24 00:00:00', './admin_asset/review/6fd690e7baa1584679c03b1c44e1caab.jpg'),
(21, 'John Carter', 'Our client’s business needs areas, extensive resources, and a client people focused approach, Collabera has developed specialized recruiting teams who provide resources in various areas.', '2023-10-02 00:00:00', './admin_asset/review/bb1d941f11e058c10812d53b5291e4d8.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_role`
--

CREATE TABLE `tbl_role` (
  `role_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `remark` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbl_role`
--

INSERT INTO `tbl_role` (`role_id`, `title`, `remark`) VALUES
(1, 'admin', 'admin role');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_seo`
--

CREATE TABLE `tbl_seo` (
  `seo_id` int(12) NOT NULL,
  `page` varchar(500) NOT NULL,
  `title` varchar(2000) NOT NULL,
  `description` text NOT NULL,
  `keyword` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbl_seo`
--

INSERT INTO `tbl_seo` (`seo_id`, `page`, `title`, `description`, `keyword`) VALUES
(33, 'index', 'Home - Skin2Love', 'Skin-2-Love was born out of a thirst for knowledge and a desire to act against pollution and waste.  As many of you have done, I wanted to know how skincare was made. ', 'Skin-2-Love was born out of a thirst for knowledge and a desire to act against pollution and waste.  As many of you have done, I wanted to know how skincare was made. ');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_size`
--

CREATE TABLE `tbl_size` (
  `size_id` int(11) NOT NULL,
  `size` int(11) NOT NULL,
  `measurement` char(20) NOT NULL,
  `entry_date` date NOT NULL,
  `modify_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tbl_size`
--

INSERT INTO `tbl_size` (`size_id`, `size`, `measurement`, `entry_date`, `modify_date`) VALUES
(1, 1, 'kg', '0000-00-00', '0000-00-00'),
(2, 10, 'gm', '0000-00-00', '0000-00-00'),
(3, 50, 'gm', '0000-00-00', '0000-00-00'),
(4, 100, 'gm', '0000-00-00', '0000-00-00'),
(5, 250, 'gm', '0000-00-00', '0000-00-00'),
(6, 500, 'gm', '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subcategory`
--

CREATE TABLE `tbl_subcategory` (
  `subcategory_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` longtext NOT NULL,
  `path` varchar(500) NOT NULL,
  `parent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_submenu`
--

CREATE TABLE `tbl_submenu` (
  `submenu_id` int(11) NOT NULL,
  `mainmenu_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `position` int(11) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `url` varchar(200) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_team`
--

CREATE TABLE `tbl_team` (
  `team_id` int(11) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `path` varchar(1000) NOT NULL,
  `facebook` varchar(1000) NOT NULL,
  `instagram` varchar(1000) NOT NULL,
  `email` varchar(1000) NOT NULL,
  `phone` varchar(1000) NOT NULL,
  `linkedin` varchar(1000) NOT NULL,
  `twitter` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbl_team`
--

INSERT INTO `tbl_team` (`team_id`, `name`, `path`, `facebook`, `instagram`, `email`, `phone`, `linkedin`, `twitter`) VALUES
(9, 'Demo User', './admin_asset/team/fb1973a4c4376ac6528f6eb0d51ef998.png', 'https://www.fb.com', 'https://www.instagram.com', 'demo@mail.com', '7894561230', 'https://www.linkedin.com', 'https://www.twitter.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_terms`
--

CREATE TABLE `tbl_terms` (
  `terms_id` int(12) NOT NULL,
  `terms` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbl_terms`
--

INSERT INTO `tbl_terms` (`terms_id`, `terms`) VALUES
(6, '<p style=\"box-sizing: inherit;\">infinit is an e-market that is operated by i.solutions, a Saudi corporation &nbsp;that is incorporated under the commercial law of Saudi Arabia. These terms of use (as listed hereunder) are between infiniti and you as an end user for our platform.</p><p style=\"box-sizing: inherit;\">1. It is your responsibility to carefully read, agree, and accept the terms and conditions listed below, on every occasion you use our platform</p><p style=\"box-sizing: inherit;\">2. This platform (<a href=\"http://www.infiniti.sa.com/\" style=\"box-sizing: inherit; text-underline-offset: 0.3rem; text-decoration-thickness: 0.1rem; transition: text-decoration-thickness var(--duration-short) ease;\">infiniti.sa.com</a>) is provided as a marketplace and channel to allow you and other users to browse and/or order our wide range of products.</p><p style=\"box-sizing: inherit;\">3. infinity reserves the right to add, delete, or modify the content of the platform at any time for any reason without prior notification</p><p style=\"box-sizing: inherit;\">4. You warrant that your provided info upon the submission of any order on our platform are: true, accurate, up-to-date, and complete and that such information shall be treated as per our Privacy Policy</p><p style=\"box-sizing: inherit;\">5. You hereby confirm that using our platform with any type of information that do not belong to you and/or you are not authorized to use, shall be considered as an illegal act that falls under your sole responsibility.</p><p style=\"box-sizing: inherit;\">6. You agree to comply with all legal requirements of the applicable legal jurisdiction(s) with regard to your use of our platform and its contents, and you acknowledge that you are entirely responsible for ensuring your own familiarity with such requirements and your own compliance with the same.</p><p style=\"box-sizing: inherit;\">7. Our service on this platform include: a) Provision of our products; b) Order Checkout; c) Procurement for carriers to deliver your order; d) customer support for reaching us via the respective form.</p><p style=\"box-sizing: inherit;\">8. You acknowledge your acceptance to all our corporate policies which include each of: a) privacy policy; b) refund and return policy; c) shipping policy</p><p style=\"box-sizing: inherit;\">9. infiniti does not stand liable for any delay in your order if your credit/debit card is not validated by your issuer.</p><p style=\"box-sizing: inherit;\">10. infiniti does not stand liable if you made an order by mistake on our platform or/and if you did not notice certain product specs that are presented by us clearly, accurately, and comprehensively.</p><p style=\"box-sizing: inherit;\">11. You are responsible to thoroughly check and understand each of: a) your order’s color; b) your order’s price; c) your order’s components; d) your order’s dimensions.</p><p style=\"box-sizing: inherit;\">12. Minors /under the age of 18 shall not use our platform to transact and purchase unless they are permitted to do so from their guardians.</p><p style=\"box-sizing: inherit;\">13.&nbsp;We accept payments online in Saudi Riyal currency using the following mediums: Visa and MasterCard credit/debit cards, Mada Card payments, STC Pay, and Apple Pay.</p><p style=\"box-sizing: inherit;\">14. Our&nbsp;Terms &amp; Conditions may be changed or updated occasionally to meet the requirements and standards. Therefore Customers’ are encouraged to frequently visit these sections in order to be updated about the changes on the website. Modi?cations will be e?ective on the day they are posted.</p><p style=\"box-sizing: inherit;\">15. All credit/debit cards details and personally identifiable information will NOT be stored, sold, shared, rented or leased to any third parties.<br style=\"box-sizing: inherit;\"><br style=\"box-sizing: inherit;\">16. Saudi Arabia is our country of domicile<br style=\"box-sizing: inherit;\"><br style=\"box-sizing: inherit;\">17. Website &nbsp;will NOT deal or provide any services or products to any of OFAC (Office of Foreign Assets Control) sanctions countries in accordance with the law of Saudi Arabia</p><p style=\"box-sizing: inherit;\">18. We accept payments online using Visa and MasterCard credit/debit card in SAR.&nbsp;</p><p style=\"box-sizing: inherit;\">19. Refunds will be done only through the Original Mode of Payment</p><p style=\"box-sizing: inherit; margin-bottom: 0px;\">20.&nbsp;Any dispute or claim arising out of or in connection with our&nbsp;platform shall be governed and construed in accordance with the laws of Saudi Arabia</p>');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_timezone`
--

CREATE TABLE `tbl_timezone` (
  `timezone_id` int(11) NOT NULL,
  `identifier` varchar(50) NOT NULL,
  `timezone` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbl_timezone`
--

INSERT INTO `tbl_timezone` (`timezone_id`, `identifier`, `timezone`) VALUES
(1, 'Africa', 'Africa/Abidjan'),
(2, 'Africa', 'Africa/Accra'),
(3, 'Asia', 'Asia/Aden'),
(4, 'Asia', 'Asia/Dhaka'),
(5, 'Asia', 'Asia/Almaty'),
(6, 'Asia', 'Asia/Karachi'),
(7, 'Asia', 'Asia/Kathmandu'),
(8, 'Asia', 'Asia/Kolkata'),
(9, 'Asia', 'Asia/Shanghai'),
(10, 'Asia', 'Asia/Tokyo'),
(11, 'Asia', 'Asia/Bangkok');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaction`
--

CREATE TABLE `tbl_transaction` (
  `transaction_id` int(11) NOT NULL,
  `bill_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `order_type` char(15) NOT NULL,
  `size` varchar(50) NOT NULL,
  `availableQty` int(11) NOT NULL,
  `price` double NOT NULL,
  `qty` int(11) NOT NULL,
  `netprice` double NOT NULL,
  `currency_code` char(10) NOT NULL,
  `country_name` varchar(50) NOT NULL,
  `entry_type` char(15) NOT NULL,
  `unique_id` varchar(100) NOT NULL,
  `register_id` int(11) NOT NULL,
  `entry_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tbl_transaction`
--

INSERT INTO `tbl_transaction` (`transaction_id`, `bill_id`, `product_id`, `category_id`, `order_type`, `size`, `availableQty`, `price`, `qty`, `netprice`, `currency_code`, `country_name`, `entry_type`, `unique_id`, `register_id`, `entry_date`) VALUES
(1, 1, 20, 7, 'refill', '2', 58, 50, 1, 50, '', '', '', '', 1, '2024-02-13'),
(2, 2, 20, 7, 'refill', '2', 57, 50, 1, 50, '', '', '', '', 0, '2024-02-16'),
(3, 2, 19, 8, 'refill', '3', 62, 50, 1, 50, '', '', '', '', 0, '2024-02-16'),
(4, 3, 14, 7, 'standard', '3', 20, 80, 1, 80, '', '', '', '', 0, '2024-02-16');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_video`
--

CREATE TABLE `tbl_video` (
  `video_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `video` varchar(500) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `entry_date` date NOT NULL,
  `modify_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbl_video`
--

INSERT INTO `tbl_video` (`video_id`, `brand_id`, `title`, `video`, `status`, `entry_date`, `modify_date`) VALUES
(11, 1, 'sdfsd', 'fsdfdsf', 1, '2023-08-18', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_web_data`
--

CREATE TABLE `tbl_web_data` (
  `web_data_id` int(11) NOT NULL,
  `project_name` varchar(100) NOT NULL,
  `email_address` varchar(500) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `instagram` varchar(100) NOT NULL,
  `twitter` varchar(100) NOT NULL,
  `linkedin` varchar(100) NOT NULL,
  `youtube` varchar(100) NOT NULL,
  `whatsapp` varchar(50) NOT NULL,
  `office` varchar(20) NOT NULL,
  `address` varchar(200) NOT NULL,
  `map` varchar(1000) NOT NULL,
  `logo` varchar(300) NOT NULL,
  `favicon` varchar(300) NOT NULL,
  `footer` varchar(500) NOT NULL,
  `mail_smtp_host` varchar(200) NOT NULL,
  `mail_protocol` varchar(100) NOT NULL,
  `mail_port` int(11) NOT NULL,
  `mail_email` varchar(100) NOT NULL,
  `mail_password` varchar(50) NOT NULL,
  `forgot_mail_subject` varchar(200) NOT NULL,
  `forgot_mail_message` varchar(2000) NOT NULL,
  `welcome_mail_subject` varchar(200) NOT NULL,
  `welcome_mail_message` varchar(2000) NOT NULL,
  `inquiry_mail_subject` varchar(200) NOT NULL,
  `inquiry_mail_message` varchar(2000) NOT NULL,
  `active_mail_subject` varchar(200) NOT NULL,
  `active_mail_message` varchar(2000) NOT NULL,
  `inactive_mail_subject` varchar(200) NOT NULL,
  `inactive_mail_message` varchar(2000) NOT NULL,
  `captcha_site_key` varchar(100) NOT NULL,
  `captcha_secret_key` varchar(100) NOT NULL,
  `captcha_visibility` int(11) NOT NULL,
  `date_format` varchar(50) NOT NULL,
  `time_format` varchar(50) NOT NULL,
  `timezone` varchar(50) NOT NULL,
  `meta_keyword` varchar(200) NOT NULL,
  `meta_desc` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbl_web_data`
--

INSERT INTO `tbl_web_data` (`web_data_id`, `project_name`, `email_address`, `phone`, `facebook`, `instagram`, `twitter`, `linkedin`, `youtube`, `whatsapp`, `office`, `address`, `map`, `logo`, `favicon`, `footer`, `mail_smtp_host`, `mail_protocol`, `mail_port`, `mail_email`, `mail_password`, `forgot_mail_subject`, `forgot_mail_message`, `welcome_mail_subject`, `welcome_mail_message`, `inquiry_mail_subject`, `inquiry_mail_message`, `active_mail_subject`, `active_mail_message`, `inactive_mail_subject`, `inactive_mail_message`, `captcha_site_key`, `captcha_secret_key`, `captcha_visibility`, `date_format`, `time_format`, `timezone`, `meta_keyword`, `meta_desc`) VALUES
(1, 'INFINITI', 'customersupport@infiniti.sa.com', '+966539467010', '#', 'https://www.instagram.com/infiniti_arabia/', 'https://twitter.com/infiniti_arabia', '#', '#', '+96653946701', '', '', '', './admin_asset/logo/33c60a2288b7dfd35b888bbfcc80d101.png', './admin_asset/favicon/2f7439d1db8384612ba8bea4c50dca40.png', 'INFINITI © 2024 | CR 1010869873 | VAT 302035094700003', 'smtp', 'ssmtp', 465, 'demo@gmail.com', 'demo123', 'Recover your password - Enzo Admin', '<p>Hello user,</p><p>Click on below link to reset your current password.</p><p><br></p><p>Thanks & Regards,</p><p><b><i>Enzo Admin.</i></b></p>', 'Register Success - Welcome to Enzo Admin ', '<p>Hello user,</p><p>Firstly Thank you for register with us. You have successfully created account in our admin. You can access all the granted functionality in your account. If you want any further help regarding account, drop your issues on admin mail</p><p>Thanks & Regards,</p><p><b><i>Enzo Admin.</i></b></p>', 'Inquiry Submitted - Enzo Admin ', '<p>Hello user,</p><p>We have received your inquiry and we look into it very soon.Â </p><p><br></p><p>Thanks & Regards.</p><p><b><i>Enzo Admin.</i></b></p>', 'Profile Activated Successfully - Enzo Admin', '<p>Hello user,</p><p>Congratulations, Your Enzo admin <b>profile is now activated successfully</b>. You are eligible to access our fully functional admin panel.</p><p><br></p><p>Thanks & Regards</p><p><b><i>Enzo Admin.</i></b></p>', 'Your Profile Inactivated - Enzo Admin', '<p>Hello user,</p><p>Your Enzo admin&nbsp;<span style=\"font-weight: bolder;\">profile is now inactivated due to some reason</span>. You are not eligible to access our fully functional admin panel. For more information contact to our admin or team.</p><p><br></p><p>Thanks &amp; Regards</p><p><span style=\"font-weight: bolder;\"><i>Enzo Admin.</i></span></p>', '6LfksB8oAAAAADIqBWS9VBInmh4lmGL73RnLyqQu', '6LfksB8oAAAAAMwwau7hBdvdfsPMo4buMoy3bt8E', 1, 'd-M-Y', 'h', '8', 'infiniti is a Sofa market that is especialised in manufacturing and selling reliable state-of-art sofas that are premium and affordable, along a rich and unique blend of advantages for customers.', 'infiniti is a Sofa market that is especialised in manufacturing and selling reliable state-of-art sofas that are premium and affordable, along a rich and unique blend of advantages for customers.');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_wishlist`
--

CREATE TABLE `tbl_wishlist` (
  `wishlist_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `unique_id` char(100) NOT NULL,
  `email` char(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tbl_wishlist`
--

INSERT INTO `tbl_wishlist` (`wishlist_id`, `product_id`, `unique_id`, `email`) VALUES
(14, 19, '', 'hira@gmail.com'),
(18, 15, '', 'nishant@gmail.com'),
(19, 19, '', 'jj@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_aboutus`
--
ALTER TABLE `tbl_aboutus`
  ADD PRIMARY KEY (`aboutus_id`);

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_banner`
--
ALTER TABLE `tbl_banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `tbl_bill`
--
ALTER TABLE `tbl_bill`
  ADD PRIMARY KEY (`bill_id`),
  ADD KEY `Register ID` (`register_id`);

--
-- Indexes for table `tbl_blog`
--
ALTER TABLE `tbl_blog`
  ADD PRIMARY KEY (`blog_id`);

--
-- Indexes for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `tbl_client`
--
ALTER TABLE `tbl_client`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `tbl_control`
--
ALTER TABLE `tbl_control`
  ADD PRIMARY KEY (`control_id`);

--
-- Indexes for table `tbl_control_feature`
--
ALTER TABLE `tbl_control_feature`
  ADD PRIMARY KEY (`control_feature_id`);

--
-- Indexes for table `tbl_coupon`
--
ALTER TABLE `tbl_coupon`
  ADD PRIMARY KEY (`coupon_id`),
  ADD UNIQUE KEY `coupon_code` (`coupon_code`);

--
-- Indexes for table `tbl_email_subscriber`
--
ALTER TABLE `tbl_email_subscriber`
  ADD PRIMARY KEY (`email_subscriber_id`);

--
-- Indexes for table `tbl_faq`
--
ALTER TABLE `tbl_faq`
  ADD PRIMARY KEY (`faq_id`);

--
-- Indexes for table `tbl_feedback`
--
ALTER TABLE `tbl_feedback`
  ADD PRIMARY KEY (`feedback_id`);

--
-- Indexes for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
  ADD PRIMARY KEY (`gallery_id`);

--
-- Indexes for table `tbl_inquiry`
--
ALTER TABLE `tbl_inquiry`
  ADD PRIMARY KEY (`inquiry_id`);

--
-- Indexes for table `tbl_mainmenu`
--
ALTER TABLE `tbl_mainmenu`
  ADD PRIMARY KEY (`mainmenu_id`);

--
-- Indexes for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `tbl_permission`
--
ALTER TABLE `tbl_permission`
  ADD PRIMARY KEY (`permission_id`);

--
-- Indexes for table `tbl_permission_assign`
--
ALTER TABLE `tbl_permission_assign`
  ADD PRIMARY KEY (`permission_assigned_id`),
  ADD KEY `Property ID` (`permission_id`);

--
-- Indexes for table `tbl_policy`
--
ALTER TABLE `tbl_policy`
  ADD PRIMARY KEY (`policy_id`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `tbl_product_attribute`
--
ALTER TABLE `tbl_product_attribute`
  ADD PRIMARY KEY (`product_attribute_id`);

--
-- Indexes for table `tbl_product_review`
--
ALTER TABLE `tbl_product_review`
  ADD PRIMARY KEY (`product_review_id`);

--
-- Indexes for table `tbl_register`
--
ALTER TABLE `tbl_register`
  ADD PRIMARY KEY (`register_id`);

--
-- Indexes for table `tbl_review`
--
ALTER TABLE `tbl_review`
  ADD PRIMARY KEY (`review_id`);

--
-- Indexes for table `tbl_role`
--
ALTER TABLE `tbl_role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `tbl_seo`
--
ALTER TABLE `tbl_seo`
  ADD PRIMARY KEY (`seo_id`);

--
-- Indexes for table `tbl_size`
--
ALTER TABLE `tbl_size`
  ADD PRIMARY KEY (`size_id`);

--
-- Indexes for table `tbl_subcategory`
--
ALTER TABLE `tbl_subcategory`
  ADD PRIMARY KEY (`subcategory_id`),
  ADD KEY `parent` (`parent`);

--
-- Indexes for table `tbl_submenu`
--
ALTER TABLE `tbl_submenu`
  ADD PRIMARY KEY (`submenu_id`),
  ADD KEY `Main Menu ID` (`mainmenu_id`);

--
-- Indexes for table `tbl_team`
--
ALTER TABLE `tbl_team`
  ADD PRIMARY KEY (`team_id`);

--
-- Indexes for table `tbl_terms`
--
ALTER TABLE `tbl_terms`
  ADD PRIMARY KEY (`terms_id`);

--
-- Indexes for table `tbl_timezone`
--
ALTER TABLE `tbl_timezone`
  ADD PRIMARY KEY (`timezone_id`);

--
-- Indexes for table `tbl_transaction`
--
ALTER TABLE `tbl_transaction`
  ADD PRIMARY KEY (`transaction_id`),
  ADD KEY `Bill ID` (`bill_id`);

--
-- Indexes for table `tbl_video`
--
ALTER TABLE `tbl_video`
  ADD PRIMARY KEY (`video_id`);

--
-- Indexes for table `tbl_web_data`
--
ALTER TABLE `tbl_web_data`
  ADD PRIMARY KEY (`web_data_id`);

--
-- Indexes for table `tbl_wishlist`
--
ALTER TABLE `tbl_wishlist`
  ADD PRIMARY KEY (`wishlist_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_aboutus`
--
ALTER TABLE `tbl_aboutus`
  MODIFY `aboutus_id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `admin_id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_banner`
--
ALTER TABLE `tbl_banner`
  MODIFY `banner_id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `tbl_bill`
--
ALTER TABLE `tbl_bill`
  MODIFY `bill_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_blog`
--
ALTER TABLE `tbl_blog`
  MODIFY `blog_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  MODIFY `cart_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tbl_client`
--
ALTER TABLE `tbl_client`
  MODIFY `client_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
  MODIFY `contact_id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `tbl_control`
--
ALTER TABLE `tbl_control`
  MODIFY `control_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_control_feature`
--
ALTER TABLE `tbl_control_feature`
  MODIFY `control_feature_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_coupon`
--
ALTER TABLE `tbl_coupon`
  MODIFY `coupon_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_email_subscriber`
--
ALTER TABLE `tbl_email_subscriber`
  MODIFY `email_subscriber_id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_faq`
--
ALTER TABLE `tbl_faq`
  MODIFY `faq_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `tbl_feedback`
--
ALTER TABLE `tbl_feedback`
  MODIFY `feedback_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
  MODIFY `gallery_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tbl_inquiry`
--
ALTER TABLE `tbl_inquiry`
  MODIFY `inquiry_id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_mainmenu`
--
ALTER TABLE `tbl_mainmenu`
  MODIFY `mainmenu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `tbl_permission`
--
ALTER TABLE `tbl_permission`
  MODIFY `permission_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `tbl_permission_assign`
--
ALTER TABLE `tbl_permission_assign`
  MODIFY `permission_assigned_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=610;

--
-- AUTO_INCREMENT for table `tbl_policy`
--
ALTER TABLE `tbl_policy`
  MODIFY `policy_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `tbl_product_attribute`
--
ALTER TABLE `tbl_product_attribute`
  MODIFY `product_attribute_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `tbl_product_review`
--
ALTER TABLE `tbl_product_review`
  MODIFY `product_review_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_register`
--
ALTER TABLE `tbl_register`
  MODIFY `register_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tbl_review`
--
ALTER TABLE `tbl_review`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `tbl_role`
--
ALTER TABLE `tbl_role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_seo`
--
ALTER TABLE `tbl_seo`
  MODIFY `seo_id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `tbl_size`
--
ALTER TABLE `tbl_size`
  MODIFY `size_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_subcategory`
--
ALTER TABLE `tbl_subcategory`
  MODIFY `subcategory_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_submenu`
--
ALTER TABLE `tbl_submenu`
  MODIFY `submenu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_team`
--
ALTER TABLE `tbl_team`
  MODIFY `team_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_terms`
--
ALTER TABLE `tbl_terms`
  MODIFY `terms_id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_timezone`
--
ALTER TABLE `tbl_timezone`
  MODIFY `timezone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_transaction`
--
ALTER TABLE `tbl_transaction`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_video`
--
ALTER TABLE `tbl_video`
  MODIFY `video_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_web_data`
--
ALTER TABLE `tbl_web_data`
  MODIFY `web_data_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_wishlist`
--
ALTER TABLE `tbl_wishlist`
  MODIFY `wishlist_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_subcategory`
--
ALTER TABLE `tbl_subcategory`
  ADD CONSTRAINT `maincategory` FOREIGN KEY (`parent`) REFERENCES `tbl_category` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_submenu`
--
ALTER TABLE `tbl_submenu`
  ADD CONSTRAINT `Main Menu ID` FOREIGN KEY (`mainmenu_id`) REFERENCES `tbl_mainmenu` (`mainmenu_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_transaction`
--
ALTER TABLE `tbl_transaction`
  ADD CONSTRAINT `Bill ID` FOREIGN KEY (`bill_id`) REFERENCES `tbl_bill` (`bill_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
