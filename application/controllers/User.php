<?php
/* * *
 * Project:    T-Build
 * Name:       Admin Side User
 * Package:    User.php
 * About:      A controller that handle backend of all pages
 * Copyright:  (C) 2022
 * Author:     Nishant Thummar
 * Website:    https://nishantthummar.tech/
 * * */
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

    // Global Variables
    public $website, $developer, $web_link, $table_prefix, $web_data, $admin_data, $savepath = './admin_asset/';

    // Constructor
    public function __construct()
    {
        parent::__construct();

        $this->developer = DEVELOPER;
        $this->web_link = WEBLINK;
        $this->table_prefix = TABLE_PREFIX;     // SET TABLE PREFIX (TBL_) 
        // Check if database already connected or not [STATUS = PRE/POST] 
        $settings = INSTALLER_SETTING;
        if ($settings['status'] == "pre") {
            redirect('/install');
        }
        $this->web_data = $this->md->select('tbl_web_data');    // SELECT WEBSITE DATA
        $this->admin_data = $this->md->select_where('tbl_admin', array('email' => $this->session->userdata('aemail'))); // SELECT ADMIN DATA
        date_default_timezone_set(($this->web_data) ? $this->md->getItemName('tbl_timezone', 'timezone_id', 'timezone', $this->web_data[0]->timezone) : DEFAULT_TIMEZONE);
    }

    /*
      P A G E S
     */

    // Security for without login do not access page
    public function security()
    {
        if ($this->session->userdata('email') == "") {
            redirect('user-login');
        }
    }

    // Get ID Address or Unique ID of user
    public function getIPAddress()
    {
        return $this->input->cookie('unique_id');
    }

    // Page not found 404
    public function page_not_found()
    {
        $data = $this->setParameter(__FUNCTION__, "Page Not Found", false);
        $this->load->view('master', $data);
    }

    protected function setParameter($page, $title, $page_breadcumb)
    {
        $this->load->helper('language');
        // Get the language from session
        $language = $this->session->userdata('site_lang');
        if (!$language) {
            $language = 'english'; // default language
            $this->session->set_userdata('site_lang', 'english');
        }
        $this->lang->load('messages', $language);

        // generate unique_id
        $this->load->helper('cookie');
        if (!$this->input->cookie('unique_id')) {
            $uid = $this->input->cookie('ci_session');
            set_cookie('unique_id', $uid, 60 * 60 * 24 * 365);
        }
        $data = [];
        $data['page_title'] = $title;
        $data['web_data'] = $this->web_data;    // get all Website data
        $data['admin_data'] = $this->admin_data;    // get all Admin data
        $data['website_title'] = $this->website;
        $data['developer'] = $this->developer;
        $data['web_link'] = $this->web_link;
        $data['page_breadcumb'] = $page_breadcumb;
        $data['page'] = 'user/' . $page;
        if ($this->db->table_exists($this->table_prefix . $page))
            $data[$page] = $this->md->select($this->table_prefix . $page);
        return $data;
    }

    // Login
    public function login()
    {
        $data = $this->setParameter(__FUNCTION__, "Login to Account", true);
        if ($this->input->post('login')) {
            $this->form_validation->set_rules('username', '', 'required', array("required" => "Enter Email or Phone!"));
            $this->form_validation->set_rules('password', 'Password', 'required', array("required" => "Enter Password!"));
            if ($this->form_validation->run() == TRUE) {
                //                $captchaStatus = $this->md->verify_recaptcha($this->input->post('g-recaptcha-response')); // Verify Google recaptcha 
                //                if ($captchaStatus) {
                $t['email'] = $this->input->post('username');
                $r = $this->md->select_where('tbl_register', $t);
                if (!empty($r) && ($this->encryption->decrypt($r[0]->password) == $this->input->post('password'))) {
                    $this->session->set_userdata('userType', 'regular');
                    $this->session->set_userdata('email', $t['email']);
                    $this->md->update('tbl_wishlist', array('email' => $t['email'], 'unique_id' => ''), array('unique_id' => $this->getIPAddress()));
                    $this->md->update('tbl_cart', array('register_id' => $r[0]->register_id, 'unique_id' => ''), array('unique_id' => $this->getIPAddress()));
                    redirect('welcome');
                } else {
                    $data['error'] = 'Sorry, Authentication Fail';
                }
                //                } else {
                //                    $data['error'] = 'Oops, Validate google reCaptcha!';
                //                }
            } else {
                $data['error'] = 'Sorry, Fillout email and password!';
            }
        }
        $this->load->view('master', $data);
    }

    // Register
    public function register()
    {
        $data = $this->setParameter(__FUNCTION__, "Register", true);
        if ($this->input->post('register')) {
            $this->form_validation->set_rules('fname', 'Full Name', 'required|regex_match[/^[a-zA-Z ]+$/]', array("required" => "Enter Full Name!"));
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|is_unique[tbl_register.email]', array("required" => "Enter Email Address!", "is_unique" => "Oops, Email Already Exists!"));
            $this->form_validation->set_rules('phone', 'Phone Number', 'required|numeric|min_length[8]|max_length[14]|is_unique[tbl_register.phone]', array("required" => "Enter Phone Number!", "is_unique" => "Oops, Phone Already Exists!"));
            $this->form_validation->set_rules('password', 'Password', 'required', array("required" => "Enter Password!"));
            if ($this->form_validation->run() == TRUE) {
                //                $captchaStatus = $this->md->verify_recaptcha($this->input->post('g-recaptcha-response')); // Verify Google recaptcha 
                //                if ($captchaStatus) {
                $insert_data['fname'] = $this->input->post('fname');
                $insert_data['email'] = $this->input->post('email');
                $insert_data['phone'] = $this->input->post('phone');
                $insert_data['password'] = $this->encryption->encrypt($this->input->post('password'));
                $insert_data['status'] = 1;
                $insert_data['register_date'] = date('Y-m-d');
                if ($this->md->insert('tbl_register', $insert_data)) {
                    $registerId = $this->db->insert_id();
                    //                    $body = "<p><b>Name : </b>" . $this->input->post('fname') . "</p>";
                    //                    $body .= "<p><b>Email : </b>" . $this->input->post('email') . "</p>";
                    //                    $body .= "<p><b>Phone : </b>" . $this->input->post('phone') . "</p>";
                    //                    $body .= "<p><b>Password : </b>" . $this->input->post('password') . "</p>";
                    //                    $body .= "<p><b>Register Date : </b>" . date('Y-m-d H:i:s') . "</p>";
                    //                    $this->md->my_mailer($this->input->post('email'), 'register', $body);
                    $data['success'] = 'Registration successfully.';
                    $this->session->set_userdata('userType', 'regular');
                    $this->session->set_userdata('email', $this->input->post('email'));
                    $this->md->update('tbl_wishlist', array('email' => $this->input->post('email'), 'unique_id' => ''), array('unique_id' => $this->getIPAddress()));
                    $this->md->update('tbl_cart', array('register_id' => $registerId, 'unique_id' => ''), array('unique_id' => $this->getIPAddress()));
                    redirect('profile-success');
                } else {
                    $data['error'] = 'Sorry, Registration Fail!';
                }
                //                } else {
                //                    $data['error'] = 'Oops, Validate google reCaptcha!';
                //                }
            } else {
                $data['error'] = 'Sorry, Fillout all the fields!';
            }
        }
        $this->load->view('master', $data);
    }

    // Forgot Password
    public function forgot_password()
    {
        $data = $this->setParameter(__FUNCTION__, "Forgot Password", true);
        if ($this->input->post('forgot')) {
            $this->form_validation->set_rules('username', '', 'required', array("required" => "Enter Email or Phone!"));
            if ($this->form_validation->run() == TRUE) {
                //                $captchaStatus = $this->md->verify_recaptcha($this->input->post('g-recaptcha-response')); // Verify Google recaptcha 
                //                if ($captchaStatus) {
                $username = $this->input->post('username');
                $userExisted = $this->md->my_query('SELECT * FROM `tbl_register` WHERE (`phone` = "' . $username . '" OR `email`="' . $username . '") AND delete_status = 0')->result();
                if (!empty($userExisted)) {
                    if ($userExisted[0]->status) {
                        $username = $userExisted[0]->phone;
                        $otp = rand(100000, 999999); // OTP
                        $this->md->sendTextSMS($username, $otp);    // Send Text SMS on Mobile
                        $this->session->set_userdata('email', $userExisted[0]->email);
                        $this->session->set_userdata('show_menu', false);
                        $this->session->set_userdata('otp', $otp);
                        $this->session->set_userdata('page', "forgot");
                        redirect('user-verify');
                    } else {
                        $data['error'] = 'Sorry, Your profile has been deactivated!';
                    }
                } else {
                    $data['error'] = 'Sorry, Authentication Fail!';
                }
                //                } else {
                //                    $data['error'] = 'Oops, Validate google reCaptcha!';
                //                }
            } else {
                $data['error'] = 'Sorry, Fillout email and password!';
            }
        }
        $this->load->view('master', $data);
    }

    // Verify
    public function verify()
    {
        $data = $this->setParameter(__FUNCTION__, "Verify", true);
        $user = $this->session->userdata('email');
        $userData = $this->md->select_where('tbl_register', array('email' => $user));
        if ($this->input->post('verify')) {
            $this->form_validation->set_rules('otp', 'OTP', 'required|numeric|exact_length[6]', array("required" => "Enter Verification OTP!"));
            if ($this->form_validation->run() == TRUE) {
                //                $captchaStatus = $this->md->verify_recaptcha($this->input->post('g-recaptcha-response')); // Verify Google recaptcha 
                //                if ($captchaStatus) {
                $otp = $this->session->userdata('otp');
                if ($otp == $this->input->post('otp')) {
                    if ($this->md->update('tbl_register', array('status' => 1), array('email' => $user))) {
                        $type = $this->session->userdata('page');
                        if ($type == "forgot") {
                            redirect('update-password');
                        } else if ($type == "register") {
                            $this->session->set_userdata('show_menu', true);
                            redirect('pricing');
                        } else {
                            redirect('404');
                        }
                    } else {
                        $data['error'] = 'Sorry, verification not completed!';
                    }
                } else {
                    $data['error'] = 'Sorry, Invalid OTP entered!';
                }
                //                } else {
                //                    $data['error'] = 'Oops, Validate google reCaptcha!';
                //                }
            } else {
                $data['error'] = 'Sorry, Enter Valid OTP!';
            }
        }
        if ($this->input->post('resend')) {
            if (!empty($userData)) {
                $otp = rand(100000, 999999); // OTP 
                $this->session->set_userdata('otp', $otp);
                if ($userData[0]->phone != '') {
                    $this->md->sendTextSMS($userData[0]->phone, $otp);    // Send Text SMS on Mobile 
                    $data['success'] = "OTP has been sent successfully!";
                } else {
                    $data['error'] = "Sorry, You have not registered any phone number!";
                }
            } else {
                $data['error'] = 'Sorry, Unauthorized access!';
            }
        }
        $this->load->view('master', $data);
    }

    // Cart
    public function cart()
    {
        $data = $this->setParameter(__FUNCTION__, "Your Shopping Cart", true);
        $this->load->view('master', $data);
    }

    // checkout
    public function checkout()
    {
        $data = $this->setParameter(__FUNCTION__, "Checkout", true);

        $cart = $this->md->select_where('tbl_cart', array('unique_id' => $this->getIPAddress()));
        $total = $this->md->my_query("SELECT sum(netprice) as total FROM `tbl_cart` WHERE `unique_id` = '" . $this->getIPAddress() . "'")->result();
        if (empty($cart)) {
            redirect('home');
        } else {
//            $orderId = date('dmyhis-') . $this->md->generateRandomString(5);  // Order ID
//            $totalPayment = $total[0]->total;   // Total payable Amount
//            $description = "Order Placed";
//            $fname = "Nishant";
//            $email = "demo@gmail.com";
//            $phone = "0550000000";
//
//            $response = $this->tabbyPayment($orderId, $totalPayment, $description, $fname, $email, $phone); // Send Payment Request to Tabby
//
//            $response = json_decode($response, true);

//            echo "<pre>";
//            print_r($response);
//            die;

//            $transactionId = $response['id'];   // Transaction ID
//
//            $url = $response['configuration']['available_products']['installments'][0]['web_url'];  // Payment URL
//            $qr = $response['configuration']['available_products']['installments'][0]['qr_code'];   // Payment QR Code
//
//            $this->session->set_userdata('orderId', $orderId);  // Store Order ID
//            $this->session->set_userdata('transactionId', $transactionId);  // Store Transaction ID
//            $this->session->set_userdata('url', $url);  // Store Pay URL
//            $this->session->set_userdata('qr', $qr);  // Store QR Code
        }
        $this->load->view('master', $data);
    }

    // My order
    public function myorder()
    {
        $this->security();
        $data = $this->setParameter(__FUNCTION__, "My Order", true);
        $user = $this->session->userdata('email');
        $user_id = $this->md->select_where('tbl_register', array('email' => $user));
        $data['user_info'] = $this->md->select_where('tbl_register', array('email' => $user));
        $data['subpage'] = __FUNCTION__;
        $data['orders'] = $this->md->select_where('tbl_bill', array('register_id' => $user_id[0]->register_id));
        $this->load->view('master', $data);
    }

    // Order Success Page
    public function success()
    {
        $data = $this->setParameter(__FUNCTION__, "Order Placed Successfully", true);
        $data['msg'] = $this->session->flashdata('paymentMsg');
        $this->session->unset_userdata('paymentMsg');
        $this->load->view('master', $data);
    }

    // User Dashboard
    public function welcome()
    {
        $this->security();
        $data = $this->setParameter(__FUNCTION__, "Welcome User", true);
        $user = $this->session->userdata('email');
        $data['user_info'] = $this->md->select_where('tbl_register', array('email' => $user));
        $data['subpage'] = __FUNCTION__;
        $this->load->view('master', $data);
    }

    // Change Password
    public function change_password()
    {
        $this->security();
        $data = $this->setParameter(__FUNCTION__, "Change Password", true);
        $user = $this->session->userdata('email');
        if ($this->input->post('update')) {
            $this->form_validation->set_rules('current_password', 'Current Password', 'required', array("required" => "Enter Current Password!"));
            $this->form_validation->set_rules('new_password', 'New Password', 'required', array("required" => "Enter New Password!"));
            $this->form_validation->set_rules('con_password', 'Confirm Password', 'required|matches[new_password]', array("required" => "Enter Confirm Password!"));
            if ($this->form_validation->run() == TRUE) {
                $current = $this->input->post('current_password');
                $new = $this->input->post('new_password');
                $user = $this->session->userdata('email');
                $dt = $this->md->select_where('tbl_register', array('email' => $user));
                if (!empty($dt)) {
                    $nn = $this->encryption->decrypt($dt[0]->password);
                    if ($nn == $current) {
                        $this->md->update('tbl_register', array('password' => $this->encryption->encrypt($new)), array('email' => $user));
                        $data['success'] = 'Password Changed Successfully.';
                    } else {
                        $data['error'] = 'Sorry, Current Password not matched!';
                    }
                } else {
                    $data['error'] = 'Sorry, Unauthorized access!';
                }
            } else {
                $data['error'] = 'Sorry, fillout all the field properly!';
            }
        }
        $data['user_info'] = $this->md->select_where('tbl_register', array('email' => $user));
        $data['subpage'] = __FUNCTION__;
        $this->load->view('master', $data);
    }

    // Edit Profile
    public function edit_profile()
    {
        $this->security();
        $data = $this->setParameter(__FUNCTION__, "Edit Profile", true);
        $user = $this->session->userdata('email');
        if ($this->input->post('save')) {
            $this->form_validation->set_rules('fname', 'Full Name', 'required', array("required" => "Enter Full Name!"));
            $this->form_validation->set_rules('phone', 'Phone Number', 'required|numeric|min_length[8]|max_length[14]', array("required" => "Enter Phone Number!"));
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email', array("required" => "Enter Email Address!"));
            $this->form_validation->set_rules('state', '', 'required', array("required" => "Enter State!"));
            $this->form_validation->set_rules('country', '', 'required', array("required" => "Enter Country!"));
            $this->form_validation->set_rules('city', '', 'required', array("required" => "Enter City!"));
            $this->form_validation->set_rules('address', '', 'required', array("required" => "Enter Address!"));
            $this->form_validation->set_rules('postal_code', 'Postal Code', 'required', array("required" => "Enter Postal Code!"));
            if ($this->form_validation->run() == TRUE) {
                $data1['fname'] = $this->input->post('fname');
                $data1['phone'] = $this->input->post('phone');
                $data1['email'] = $this->input->post('email');
                $data1['gender'] = $this->input->post('gender');
                $data1['country'] = $this->input->post('country');
                $data1['state'] = $this->input->post('state');
                $data1['city'] = $this->input->post('city');
                $data1['address'] = $this->input->post('address');
                $data1['postal_code'] = $this->input->post('postal_code');
                // Check already photo is exist or not, if yes than it will remove and than upload.
                $this->input->post('updateStatus') && ($this->input->post('oldPath') ? (($this->input->post('oldPath') != FILENOTFOUND) ? unlink($this->input->post('oldPath')) : '') : '');  // Remove Old Photo
                $this->input->post('updateStatus') && ($data1['path'] = $this->md->uploadFile('customer'));    // Upload Photo and return path from model
                $this->md->update('tbl_register', $data1, array('email' => $user));
                $this->session->set_userdata('email', $this->input->post('email'));
                $data['success'] = 'Data Updated successfully.';
                redirect('edit-profile');
            } else {
                $data['error'] = 'Sorry, fillout all the field properly!';
            }
        }
        $data['user_info'] = $this->md->select_where('tbl_register', array('email' => $user));
        $data['subpage'] = __FUNCTION__;
        $this->load->view('master', $data);
    }

    // Profile Success
    public function profile_success()
    {
        $this->security();
        $data = $this->setParameter(__FUNCTION__, "Profile Created Successfully", true);
        $this->load->view('master', $data);
    }

    // Customer/ User Logout
    public function logout()
    {
        $this->security();
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('otp');
        $this->session->unset_userdata('show_menu');
        $this->session->unset_userdata('page');
        redirect('user-login');
    }

    /*
        TABBY PAYMENT GATEWAY INTEGRATION
       */

    // Checkout - Send Payment Request to Tabby
    public function tabbyPayment($orderId, $amount, $description, $customerName, $customerEmail, $customerPhone)
    {
        $currency = "SAR";

        $successUrl = base_url('payment-success');
        $failedUrl = base_url('payment-failed');
        $cancelledUrl = base_url('payment-cancelled');

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.tabby.ai/api/v2/checkout',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
                "payment": {
                    "amount": "' . $amount . '", 
                    "currency": "' . $currency . '", 
                    "description": "' . $description . '",
                    "buyer": {
                        "phone": "' . $customerPhone . '", 
                        "email": "' . $customerEmail . '", 
                        "name": "' . $customerName . '", 
                        "dob": "1995-08-24" 
                    },
                    "buyer_history": {
                        "registered_since": "2024-02-24T14:15:22Z", 
                        "loyalty_level": 0,
                        "wishlist_count": 0, 
                        "is_social_networks_connected": true,
                        "is_phone_number_verified": true, 
                        "is_email_verified": true 
                    },
                    "order": {
                        "tax_amount": "0.00",
                        "shipping_amount": "0.00",
                        "discount_amount": "0.00",
                        "updated_at": "2019-08-24T14:15:22Z",
                        "reference_id": "string", 
                        "items": [
                            {
                                "title": "Order at Infiniti", 
                                "description": "' . $description . '", 
                                "quantity": 1, 
                                "unit_price": "0.00", 
                                "discount_amount": "0.00",
                                "reference_id": "string",
                                "image_url": "http://example.com",
                                "product_url": "http://example.com",
                                "gender": "Male",
                                "category": "string",  
                                "color": "string",
                                "product_material": "string",
                                "size_type": "string",
                                "size": "string",
                                "brand": "string"
                            }
                        ]
                    },
                    "order_history": [
                        {
                            "purchased_at": "2024-08-24T14:15:22Z", 
                            "amount": "0.00", 
                            "payment_method": "card", 
                            "status": "new",
                            "buyer": { 
                                "phone": "string", 
                                "email": "user@example.com", 
                                "name": "string", 
                                "dob": "2019-08-24" 
                            },
                            "shipping_address": {
                                "city": "string", 
                                "address": "string", 
                                "zip": "string" 
                            },
                            "items": [
                                {
                                    "title": "string",
                                    "description": "string",
                                    "quantity": 1,
                                    "unit_price": "0.00",
                                    "discount_amount": "0.00",
                                    "reference_id": "string",
                                    "image_url": "http://example.com",
                                    "product_url": "http://example.com",
                                    "ordered": 0,
                                    "captured": 0,
                                    "shipped": 0,
                                    "refunded": 0,
                                    "gender": "Male",
                                    "category": "string",
                                    "color": "string",
                                    "product_material": "string",
                                    "size_type": "string",
                                    "size": "string",
                                    "brand": "string"
                                }
                            ]
                        }
                    ],
                    "shipping_address": {
                        "city": "string", 
                        "address": "string", 
                        "zip": "string" 
                    },
                    "meta": {
                        "order_id": "#' . $orderId . '", 
                        "customer": "#customer-id" 
                    },
                    "attachment": {
                        "body": "{\\"flight_reservation_details\\": {\\"pnr\\": \\"TR9088999\\",\\"itinerary\\": [...],\\"insurance\\": [...],\\"passengers\\": [...],\\"affiliate_name\\": \\"some affiliate\\"}}",
                        "content_type": "application/vnd.tabby.v1+json"
                    }
                },
                "lang": "en", 
                "merchant_code": "DOM", 
                "merchant_urls": {
                    "success": "' . $successUrl . '",
                    "cancel": "' . $cancelledUrl . '",
                    "failure": "' . $failedUrl . '"
                }
            }',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer sk_e35ab74d-69e8-4bf7-9ab2-f1828e0053d4',
                'Content-Type: application/json',
                'Cookie: _cfuvid=SUGM0f6fvAG.E62mV2Jmdt4pqK_57sKTH1HnfQP0Kjs-1710413986266-0.0.1.1-604800000'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;

    }

    // Payment Success
    public function paymentSuccess()
    {
        $data = $this->setParameter(__FUNCTION__, "Payment Success", true);

        $user = $this->session->userdata('email');
        $user_data = $cart = array();
        $total = array();
        if (isset($user) && $this->session->userdata('transactionId')) :
            $user_data = $this->md->select_where('tbl_register', array('email' => $user));
            $cart = $this->md->select_where('tbl_cart', array('register_id' => ($user_data ? $user_data[0]->register_id : '')));
            $total = $this->md->my_query('SELECT sum(netprice) as total FROM `tbl_cart` WHERE `register_id` = ' . ($user_data ? $user_data[0]->register_id : ''))->result();

            if ($cart && $user_data) {
                $dt['orderId'] = $this->session->userdata('orderId');
                $dt['payment_id'] = $this->session->userdata('transactionId');
                $dt['register_id'] = $user_data[0]->register_id;

                $billExist = $this->md->select_where('tbl_bill', $dt);
                if (!$billExist) {
                    $dt['fname'] = $user_data[0]->fname;
                    $dt['email'] = $user_data[0]->email;
                    $dt['phone'] = $user_data[0]->phone;
                    $dt['subtotal'] = $total ? $total[0]->total : '';
                    $dt['netprice'] = $total ? $total[0]->total : '';
                    $dt['country'] = $user_data[0]->country;
                    $dt['city'] = $user_data[0]->city;
                    $dt['state'] = $user_data[0]->state;
                    $dt['address'] = $user_data[0]->address;
                    $dt['postal_code'] = $user_data[0]->postal_code;
                    $dt['entry_date'] = date('Y-m-d H:i:s');
                    $this->md->insert('tbl_bill', $dt); // Bill Entry
                    $billId = $this->db->insert_id();   // Get Last inserted Bill ID

                    // Insert Transaction Data from Cart
                    foreach ($cart as $cartData) {
                        $traIns['register_id'] = $cartData->register_id;
                        $traIns['bill_id'] = $billId;
                        $traIns['package_id'] = $cartData->package_id;
                        $traIns['price'] = $cartData->price;
                        $traIns['person'] = $cartData->person;
                        $traIns['netprice'] = $cartData->netprice;
                        $traIns['visiting_date'] = $cartData->visiting_date;
                        $traIns['notes'] = $cartData->notes;
                        $traIns['type'] = $cartData->type;
                        $traIns['entry_date'] = date('Y-m-d');
                        $this->md->insert('tbl_transaction', $traIns);
                    }

                    // Delete Cart Item
                    $this->md->delete('tbl_cart', array('register_id' => ($user_data ? $user_data[0]->register_id : '')));
                }

            }
        endif;

        $this->load->view('master', $data);
    }

    // Payment Failed
    public function paymentFailed()
    {
        $data = $this->setParameter(__FUNCTION__, "Payment Failed", true);
        $data['payment_id'] = $this->input->get('payment_id');
        $this->load->view('master', $data);
    }

    // Payment Cancelled
    public function paymentCancelled()
    {
        $data = $this->setParameter(__FUNCTION__, "Payment Cancelled", true);
        $data['payment_id'] = $this->input->get('payment_id');
        $this->load->view('master', $data);
    }

    // get city
    public function get_city()
    {
        $countryId = $this->input->post('countryId');
        $query = "SELECT * FROM `tbl_city` WHERE `state_id` in (SELECT `id` FROM `tbl_state` WHERE `country_id` = $countryId)";
        $cities = $this->md->my_query($query)->result();
        if ($cities) {
            echo '<option value="">City</option>';
            foreach ($cities as $city) {
                echo '<option value="' . ($city->id) . '">' . ($city->name) . '</option>';
            }
        }
    }

}
