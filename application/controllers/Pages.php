<?php
/* * *
 * Project:    Dhillon Printing
 * Name:       Admin Side Pages
 * Package:    Pages.php
 * About:      A controller that handle backend of all pages
 * Copyright:  (C) 2023
 * Author:     Nishant Thummar
 * Website:    https://nishantthummar.in/
 * * */
defined('BASEPATH') or exit('No direct script access allowed');

class Pages extends CI_Controller
{

    // Global Variables
    public $website, $developer, $web_link, $table_prefix, $web_data, $admin_data, $savepath = './admin_asset/';

    // Constructor
    public function __construct()
    {
        parent::__construct();
        $this->developer = DEVELOPER;
        $this->web_link = WEBLINK;
        $this->table_prefix = TABLE_PREFIX;     // SET TABLE PREFIX (TBL_) 
        // Check if database already connected or not [STATUS = PRE/POST] 
        $settings = INSTALLER_SETTING;
        if ($settings['status'] == "pre") {
            redirect('/install');
        }
        $this->web_data = $this->md->select('tbl_web_data');    // SELECT WEBSITE DATA
        $this->admin_data = $this->md->select_where('tbl_admin', array('email' => $this->session->userdata('aemail'))); // SELECT ADMIN DATA
        date_default_timezone_set(($this->web_data) ? $this->md->getItemName('tbl_timezone', 'timezone_id', 'timezone', $this->web_data[0]->timezone) : DEFAULT_TIMEZONE);
        $this->load->library("pagination");
        $this->perPage = PER_PAGE;

        // Load & Get the language from session
        $this->load->helper('language');
        $language = $this->session->userdata('site_lang');
        if (!$language) {
            $language = 'english'; // default language
            $this->session->set_userdata('site_lang', 'english');
        }
        $this->lang->load('messages', $language);
    }

    /*
      P A G E S
     */

    // Get ID Address or Unique ID of user
    public function getIPAddress()
    {
        return $this->input->cookie('unique_id');
    }

    // Page not found 404
    public function page_not_found()
    {
        $data = $this->setParameter(__FUNCTION__, "Page Not Found", false);
        $this->load->view('master', $data);
    }


    protected function setParameter($page, $title, $page_breadcumb)
    {
        // generate unique_id
        if (!$this->input->cookie('unique_id')) {
            $this->load->helper('cookie');
            $uid = $this->input->cookie('ci_session');
            set_cookie('unique_id', $uid, 60 * 60 * 24 * 365);
        }
        $data = [];
        $data['page_title'] = $title;
        $data['web_data'] = $this->web_data;    // get all Website data
        $data['admin_data'] = $this->admin_data;    // get all Admin data
        $data['website_title'] = $this->website;
        $data['developer'] = $this->developer;
        $data['web_link'] = $this->web_link;
        $data['page_breadcumb'] = $page_breadcumb;
        $data['page'] = $page;
        if ($this->db->table_exists($this->table_prefix . $page))
            $data[$page] = $this->md->select($this->table_prefix . $page);
        return $data;
    }

    // Set page language
    public function set_language()
    {
        $language = $this->uri->segment(2) ? $this->uri->segment(2) : 'english';
        $this->session->set_userdata('site_lang', $language);
        redirect($_SERVER['HTTP_REFERER']);
    }

    // Home Page 
    public function index()
    {
        $data = $this->setParameter(__FUNCTION__, "Home", false);
        if ($this->session->userdata('limit')) {
            $data['allProducts'] = $this->md->my_query('SELECT * FROM `tbl_product` WHERE `status` = 1 LIMIT 0,' . $this->session->userdata('limit'))->result();
        } else {
            $this->session->set_userdata('limit', $this->perPage);
            $data['allProducts'] = $this->md->my_query('SELECT * FROM `tbl_product` WHERE `status` = 1 LIMIT 0,' . $this->perPage)->result();
        }
        $this->load->view('master', $data);
    }

    // Contact Page
    public function contact()
    {
        $data = $this->setParameter(__FUNCTION__, "Get in Touch", true);
        if ($this->input->post('send')) {
            $this->form_validation->set_rules('fname', 'First Name', 'required|regex_match[/^[a-zA-Z ]+$/]', array("required" => "Enter Full Name!"));
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email', array("required" => "Enter Email Address!"));
            $this->form_validation->set_rules('phone', 'Phone Number', 'required|numeric|min_length[8]|max_length[14]', array("required" => "Enter Phone Number!"));
            $this->form_validation->set_rules('message', 'Message', 'required', array("required" => "Enter Message!"));
            if ($this->form_validation->run() == TRUE) {
                $captchaStatus = $this->md->verify_recaptcha($this->input->post('g-recaptcha-response')); // Verify Google recaptcha
                if ($captchaStatus) {
                    if ($this->input->post('extra_field')) {
                        $data['error'] = 'Sorry, You are a bot!';
                    } else {
                        $insert_data['name'] = $this->input->post('fname');
                        $insert_data['email'] = $this->input->post('email');
                        $insert_data['phone'] = $this->input->post('phone');
                        $insert_data['message'] = $this->input->post('message');
                        $insert_data['datetime'] = date('Y-m-d H:i:s');
                        if ($this->md->insert($this->table_prefix . __FUNCTION__, $insert_data)) {
                            // Send Notification to admin for New Form added.
                            $templateData = array(
                                "fname" => $this->input->post('fname'),
                                "email" => $this->input->post('email'),
                                "phone" => $this->input->post('phone'),
                                "message" => $this->input->post('message')
                            );
                            $adminEmail = array("support@skintolove.co.nz"); // Receiver Email
                            $this->md->sendMailSMTP2Go($adminEmail, '', '', '3647344', $templateData);

                            $data['success'] = 'Contact Submitted successfully.';
                        } else {
                            $data['error'] = 'Sorry, Contact not submitted!';
                        }
                    }
                } else {
                    $data['error'] = 'Oops, Validate google reCaptcha!';
                }
            }
        }
        $this->load->view('master', $data);
    }

    // Aboutus Page
    public function aboutus()
    {
        $data = $this->setParameter(__FUNCTION__, "About us", true);
        $this->load->view('master', $data);
    }

    // Privacy policy Page
    public function policy()
    {
        $data = $this->setParameter(__FUNCTION__, "Privacy & Policy", true);
        $this->load->view('master', $data);
    }

    // Terms & Condition Page
    public function terms()
    {
        $data = $this->setParameter(__FUNCTION__, "Terms & Condition", true);
        $this->load->view('master', $data);
    }

    // Shipping Policy Page
    public function shippingPolicy()
    {
        $data = $this->setParameter(__FUNCTION__, "Shipping Policy", true);
        $this->load->view('master', $data);
    }

    // Return Refund Policy Page
    public function returnRefundPolicy()
    {
        $data = $this->setParameter(__FUNCTION__, "Return Refund Policy", true);
        $this->load->view('master', $data);
    }

    // pricing Policy Page
    public function pricingPolicy()
    {
        $data = $this->setParameter(__FUNCTION__, "Pricing Policy", true);
        $this->load->view('master', $data);
    }

    // quality Policy Page
    public function qualityPolicy()
    {
        $data = $this->setParameter(__FUNCTION__, "Quality Policy", true);
        $this->load->view('master', $data);
    }

    // Faq Page
    public function faq()
    {
        $data = $this->setParameter(__FUNCTION__, "Frequently Asked Questions", true);
        $this->load->view('master', $data);
    }

    // services Page
    public function services()
    {
        $data = $this->setParameter(__FUNCTION__, "Our Services", true);
        $this->load->view('master', $data);
    }

    // customize Page
    public function customize()
    {
        $data = $this->setParameter(__FUNCTION__, "Customize", true);
        if ($this->input->post('submit')) {
            $this->form_validation->set_rules('fname', 'First Name', 'required|regex_match[/^[a-zA-Z ]+$/]', array("required" => "Enter Full Name!"));
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email', array("required" => "Enter Email Address!"));
            $this->form_validation->set_rules('phone', 'Phone Number', 'required|numeric|min_length[8]|max_length[14]', array("required" => "Enter Phone Number!"));
            if ($this->form_validation->run() == TRUE) {

                $textile_type = ($this->input->post('textile_type') == 0 ? 'velvet' : ($this->input->post('textile_type') == 50 ? 'fabric' : 'boucle'));
                $foam_density = ($this->input->post('foam_density') == 0 ? 'soft' : ($this->input->post('foam_density') == 50 ? 'comfy' : 'hard'));

                $insert_data['fname'] = $this->input->post('fname');
                $insert_data['email'] = $this->input->post('email');
                $insert_data['phone'] = $this->input->post('phone');
                $insert_data['country'] = $this->input->post('country') ? $this->input->post('country') : 0;
                $insert_data['city'] = $this->input->post('city') ? $this->input->post('city') : 0;
                $insert_data['product_id'] = $this->input->post('product_id') ? $this->input->post('product_id') : 0;
                (!empty($_FILES['custom_photo']['name']) && $insert_data['photo'] = $this->md->uploadFile('custom_photo')); // check if file is selected than only it will upload
                $insert_data['armchair'] = $this->input->post('armchair') ? $this->input->post('armchair') : 0;
                $insert_data['seater_2'] = $this->input->post('seater_2') ? $this->input->post('seater_2') : 0;
                $insert_data['seater_3'] = $this->input->post('seater_3') ? $this->input->post('seater_3') : 0;
                $insert_data['seater_4'] = $this->input->post('seater_4') ? $this->input->post('seater_4') : 0;
                $insert_data['seater_5'] = $this->input->post('seater_5') ? $this->input->post('seater_5') : 0;
                $insert_data['corner_sofa_length'] = $this->input->post('corner_sofa_length');
                $insert_data['textile_type'] = $textile_type;
                $insert_data['textile_colour'] = $this->input->post('textile_colour');
                $insert_data['foam_density'] = $foam_density;
                $insert_data['cost'] = $this->input->post('cost') ? $this->input->post('cost') : 0;
                $insert_data['entry_date'] = date('Y-m-d H:i:s');
                if ($this->md->insert($this->table_prefix . __FUNCTION__, $insert_data)) {

                    // Send Notification to admin for New Form added.
                    $templateData = array(
                        "fname" => $this->input->post('fname'),
                        "email" => $this->input->post('email'),
                        "phone" => $this->input->post('phone'),
                        "message" => $this->input->post('message')
                    );
                    $adminEmail = array("support@skintolove.co.nz"); // Receiver Email
                    //$this->md->sendMailSMTP2Go($adminEmail, '', '', '3647344', $templateData);

                    $data['success'] = 'Customized Request send successfully!';
                } else {
                    $data['error'] = 'Sorry, Request not submitted!';
                }
            }
        }
        $this->load->view('master', $data);
    }

    // Blog Page
    public function blog()
    {
        $slug = $this->uri->segment(2) ? $this->uri->segment(2) : ''; // Blog Slug
        $pro = '';
        if ($slug) :
            $pro = $this->md->select_where('tbl_blog', array('slug' => $slug));
            if (empty($pro)) :
                redirect('404');
            endif;
        endif;
        $data = $this->setParameter(($pro ? 'blog_detail' : __FUNCTION__), ($pro ? ucfirst($pro[0]->title) : "Blog"), true);
        (!empty($pro)) && ($data['blog_data'] = $pro);
        $this->load->view('master', $data);
    }

    // Newsletter Page
    public function newsletter()
    {
        if ($this->input->post('newsletter')) {
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|is_unique[tbl_email_subscriber.email]', array("required" => "Enter Email Address!"));
            if ($this->form_validation->run() == TRUE) {
                $insert_data['email'] = $this->input->post('email');
                $insert_data['datetime'] = date('Y-m-d H:i:s');
                if ($this->md->insert($this->table_prefix . 'email_subscriber', $insert_data)) {
                    $this->session->set_flashdata('newsletter', 'Email Registered successfully.');
                    redirect($_SERVER['HTTP_REFERER'], 'refresh');
                } else {
                    $data['error'] = 'Sorry, Email not registered!';
                }
            }
        }
    }

    // Collection
    public function collection()
    {
        $data = $this->setParameter('product', "INFINITI Collection", true);
        $this->load->view('master', $data);
    }

    // For home page
    public function loadMore()
    {
        // Load & Get the language from session
        $this->load->helper('language');
        $language = $this->session->userdata('site_lang');
        if (!$language) {
            $language = 'english'; // default language
            $this->session->set_userdata('site_lang', 'english');
        }
        $this->lang->load('messages', $language);

        $limit = $this->input->post('limit');
        $offset = $this->input->post('offset');
        $products = $this->md->get_products($limit, $offset, array('status' => 1));
        $total_products = count($this->md->select_where('tbl_product', array('status' => 1)));

        $html = '';
        foreach ($products as $product) {
            $html .= $this->load->view('loadMoreProduct', ['product' => $product], true);
        }

        $more_products = ($offset + count($products)) < $total_products;

        echo json_encode([
            'html' => $html,
            'more_products' => $more_products
        ]);
    }

    // For inner product / collection pages
    public function loadMore2()
    {
        // Load & Get the language from session
        $this->load->helper('language');
        $language = $this->session->userdata('site_lang');
        if (!$language) {
            $language = 'english'; // default language
            $this->session->set_userdata('site_lang', 'english');
        }
        $this->lang->load('messages', $language);

        $limit = $this->input->post('limit');
        $offset = $this->input->post('offset');
        $sortby = $this->input->post('sortby');
        $categorySlug = $this->input->post('categorySlug');

        if ($categorySlug) :
            $category = $this->md->select_where('tbl_category', array('slug' => $categorySlug));
            if ($category) {
                $total_products = count($this->md->select_where('tbl_product', array('status' => 1, 'category_id' => $category[0]->category_id)));

                if ($sortby == 'lowtohigh') {
                    $products = $this->md->my_query('SELECT * FROM `tbl_product` WHERE `category_id` = ' . $category[0]->category_id . ' AND `status` = 1 order by `price` LIMIT ' . $limit . ' OFFSET ' . $offset)->result_array();
                } elseif ($sortby == 'hightolow') {
                    $products = $this->md->my_query('SELECT * FROM `tbl_product` WHERE `category_id` = ' . $category[0]->category_id . ' AND `status` = 1 order by `price` desc LIMIT ' . $limit . ' OFFSET ' . $offset)->result_array();
                } elseif ($sortby == 'oldtonew') {
                    $products = $this->md->my_query('SELECT * FROM `tbl_product` WHERE `category_id` = ' . $category[0]->category_id . ' AND `status` = 1 order by `product_id` asc LIMIT ' . $limit . ' OFFSET ' . $offset)->result_array();
                } elseif ($sortby == 'newtoold') {
                    $products = $this->md->my_query('SELECT * FROM `tbl_product` WHERE `category_id` = ' . $category[0]->category_id . ' AND `status` = 1 order by `product_id` desc LIMIT ' . $limit . ' OFFSET ' . $offset)->result_array();
                } else {
                    $products = $this->md->my_query('SELECT * FROM `tbl_product` WHERE `category_id` = ' . $category[0]->category_id . ' AND `status` = 1 LIMIT ' . $limit . ' OFFSET ' . $offset)->result_array();
                }
            } else {
                $products = [];
            }
        endif;

        $html = '';
        foreach ($products as $product) {
            $html .= $this->load->view('loadMoreProduct', ['product' => $product], true);
        }

        $more_products = ($offset + count($products)) < $total_products;

        echo json_encode([
            'html' => $html,
            'more_products' => $more_products
        ]);
    }

    // load More Products
    public function loadMoreProducts()
    {
        $lastId = $this->input->post('lastId');
        $this->session->set_userdata('limit', $lastId);
        $category = $this->input->post('category');
        $sortby = $this->input->post('sortby');
        $limit = $lastId;
        if ($category) {
            if ($sortby == 'lowtohigh') {
                $allProducts = $this->md->my_query('SELECT * FROM `tbl_product` WHERE `category_id` = ' . $category . ' AND `status` = 1 order by `price`  LIMIT ' . $limit . ',' . $this->perPage)->result();
            } elseif ($sortby == 'hightolow') {
                $allProducts = $this->md->my_query('SELECT * FROM `tbl_product` WHERE `category_id` = ' . $category . ' AND `status` = 1 order by `price` desc  LIMIT ' . $limit . ',' . $this->perPage)->result();
            } elseif ($sortby == 'oldtonew') {
                $allProducts = $this->md->my_query('SELECT * FROM `tbl_product` WHERE `category_id` = ' . $category . ' AND `status` = 1 order by `product_id` asc  LIMIT ' . $limit . ',' . $this->perPage)->result();
            } elseif ($sortby == 'newtoold') {
                $allProducts = $this->md->my_query('SELECT * FROM `tbl_product` WHERE `category_id` = ' . $category . ' AND `status` = 1 order by `product_id` desc  LIMIT ' . $limit . ',' . $this->perPage)->result();
            } else {
                $allProducts = $this->md->my_query('SELECT * FROM `tbl_product` WHERE `category_id` = ' . $category . ' AND `status` = 1 LIMIT ' . $limit . ',' . $this->perPage)->result();
            }
        } else {
            $allProducts = $this->md->my_query('SELECT * FROM `tbl_product` WHERE `status` = 1 LIMIT ' . $limit . ',' . $this->perPage)->result();
        }
        $data['allProducts'] = $allProducts;
        $this->load->view('load-more-product', $data);
    }

    // Product Page
    public function product()
    {
        $pro = $data = array('');
        if ($this->uri->segment(3) != '') :
            $proSlug = $this->uri->segment(3); // product id
            if ($proSlug) :
                $pro = $this->md->select_where('tbl_product', array('product_id' => $proSlug, 'status' => 1));
                if (empty($pro)) :
                    redirect('404');
                endif;
            endif;
            $data = $this->setParameter(($pro ? 'product_detail' : __FUNCTION__), ($pro ? ucfirst($pro[0]->title) : "Products"), true);
            $data['product'] = $this->md->select_where('tbl_product', array('status' => 1));
            $data['product_data'] = (!empty($pro)) ? $pro : '';

//        elseif ($this->uri->segment(2) != '') :
//            $catSlug = $this->uri->segment(2); // Category Slug
//            $cateData = $this->md->select_where('tbl_category', array('slug' => $catSlug));   // Get Category ID
//            $pro = $this->md->select_where('tbl_product', array('category_id' => ($cateData ? $cateData[0]->category_id : 0), "status" => 1));
//            $data = $this->setParameter(__FUNCTION__, ($pro ? ucfirst($cateData[0]->title) : "Products"), true);
//            $data['product'] = ($pro) ? $pro : array();
        else:
            $products = $this->md->select_where('tbl_product', array('status' => 1));
            $data = $this->setParameter(__FUNCTION__, "Products", true);
            $data['heading'] = "INFINITI Collection";
            $data['products'] = (!empty($products)) ? $products : '';
        endif;

//        if ($this->input->post('submit_review')) {
//            $this->form_validation->set_rules('register_id', 'Customer', 'required', array("required" => "Enter Customer!"));
//            $this->form_validation->set_rules('rating', 'Rate', 'required', array("required" => "Selct Rate!"));
//            if ($this->form_validation->run() == TRUE) {
//                $insert_data['register_id'] = $this->input->post('register_id');
//                $insert_data['product_id'] = $this->uri->segment(3); // Product ID
//                $insert_data['rating'] = $this->input->post('rating');
//                $insert_data['review'] = $this->input->post('review');
//                $insert_data['entry_date'] = date('Y-m-d');
//                if ($this->md->insert($this->table_prefix . 'product_review', $insert_data)) {
//                    $data['success'] = 'Review Submitted successfully.';
//                } else {
//                    $data['error'] = 'Sorry, Review not submitted!';
//                }
//            } else {
//                $data['error'] = 'Please fillout all the field!';
//            }
//        }
        $this->load->view('master', $data);
    }

    // Add To cart
    public function addToCart()
    {
        if ($this->input->post('addToCart')) {

            $pro_id = $this->input->post('productId');  // Product ID
            $proAttId = $this->input->post('productAttributeId');  // Product Attribute ID
            $categoryId = $this->input->post('categoryId');
            $price = $this->input->post('price');
            $qty = $this->input->post('quantity');  // Qty

            $ins['product_id'] = $pro_id;
            $ins['product_attribute_id'] = $proAttId;
            $ins['category_id'] = $categoryId;
            $ins['unique_id'] = $this->getIPAddress();
            $exist = $this->md->select_where('tbl_cart', $ins);
            if (empty($exist)) {
                $ins['price'] = $price;
                $ins['qty'] = $qty ? $qty : 1;
                $ins['netprice'] = $price * ($qty ? $qty : 1);
                $ins['entry_date'] = date('Y-m-d H:i:s');
                if ($this->md->insert("tbl_cart", $ins)) {
                    $this->session->set_flashdata('success', 'Product added in cart.');
                } else {
                    $this->session->set_flashdata('error', 'Sorry, Something went wrong!');
                }
            } else {
                $wh['cart_id'] = $exist[0]->cart_id;
                if ($qty == 0) {
                    $this->md->delete('tbl_cart', $wh);
                    $this->session->set_flashdata('success', 'Item has been removed from cart.');
                } else {
                    $qty = ($qty == 1) ? ($qty + 1) : $qty;
                    $up['qty'] = $qty ? ($qty) : 1;
                    $up['netprice'] = ($qty ? ($qty) : 1) * $price;
                    $up['modify_date'] = date('Y-m-d H:i:s');
                    $this->md->update('tbl_cart', $up, $wh);
                    $this->session->set_flashdata('success', 'Product updated in cart.');
                }
            }
            redirect($_SERVER['HTTP_REFERER'], 'refresh');

        }   // Add to cart
    }

    // Remove Cart Item
    public function removeCartItem()
    {
        $cartId = $this->uri->segment(3);
        if ($cartId) {
            $exist = $this->md->select_where('tbl_cart', array('cart_id' => $cartId));
            if ($exist) {
                $this->md->delete('tbl_cart', array('cart_id' => $cartId)); // Delete Cart Item
                $this->session->set_flashdata('success', 'Sorry, item not found!');
            } else {
                $this->session->set_flashdata('error', 'Sorry, item not found!');
            }
        }
        redirect($_SERVER['HTTP_REFERER'], 'refresh');
    }

    // Update QTY in cart
    public function updateQty()
    {
        $cartid = $this->input->post('cartId');
        $quantity = $this->input->post('quantity');

        if ($cartid != "" || $quantity != "") {
            $cartData = $this->md->select_where('tbl_cart', array('cart_id' => $cartid));

            if ($cartData) {
                if ($quantity == 0) {
                    $this->md->delete('tbl_cart', array('cart_id' => $cartid));
                    $this->session->set_flashdata('success', 'Item has been removed from cart.');
                } else {
                    $up['qty'] = $quantity ? ($quantity) : 1;
                    $up['netprice'] = ($quantity ? ($quantity) : 1) * $cartData[0]->price;
                    $up['modify_date'] = date('Y-m-d H:i:s');
                    $this->md->update('tbl_cart', $up, array('cart_id' => $cartid));
                    $this->session->set_flashdata('success', 'Product updated in cart.');
                }
            } else {
                $data['error'] = 'Sorry, Data not found!';
            }
        } else {
            $data['error'] = 'Sorry, Data not found!';
        }

        redirect($_SERVER['HTTP_REFERER'], 'refresh');
    }

    // AJAX CALL GET PHOTOS
    public function getPhotos()
    {
        $attributeId = $this->input->post('attributeId');
        $attributes = $this->md->select_where('tbl_product_attribute', array('product_attribute_id' => $attributeId));
        if (empty($attributes)) {
            echo "<div class='alert alert-warning'><strong>Sorry, Attribute not found!</strong></div>";
        } else {
            $product_data = $this->md->select_where('tbl_product', array('product_id' => $attributes[0]->product_id));
            if ($product_data) {
                $photos = $attributes[0]->product_photos ? explode(",", $attributes[0]->product_photos) : '';
                ?>
                <div
                    class="product-single-carousel owl-carousel owl-theme owl-nav-inner row cols-1">
                    <?php
                    foreach ($photos as $photo) {
                        ?>
                        <figure class="product-image">
                            <img src="<?php echo base_url($photo); ?>"
                                 data-zoom-image="<?php echo base_url($photo); ?>"
                                 title="<?php echo $product_data[0]->title; ?>"
                                 alt="<?php echo $product_data[0]->title; ?>" width="800"
                                 height="900">
                        </figure>
                        <?php
                    }
                    ?>
                </div>
                <div class="product-thumbs-wrap">
                    <div class="product-thumbs">
                        <?php
                        foreach ($photos as $key => $photo) {
                            ?>
                            <div class="product-thumb <?php echo $key == 0 ? 'active' : ''; ?>">
                                <img src="<?php echo base_url($photo); ?>"
                                     title="<?php echo $product_data[0]->title; ?>"
                                     alt="<?php echo $product_data[0]->title; ?>" width="109"
                                     height="122">
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <button class="thumb-up disabled"><i class="fas fa-chevron-left"></i>
                    </button>
                    <button class="thumb-down disabled"><i class="fas fa-chevron-right"></i>
                    </button>
                </div>
                <?php
            }
        }
    }

}
