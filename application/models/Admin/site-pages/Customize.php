<?php

class Customize extends CI_Model
{

    // Global Variables
    public $tbl = 'tbl_customize';

    // get data from table set in datatable
    function getData($postData = null)
    {

        $webData = $this->md->getWebData(); // Get Website Data
        ## Read value
        $draw = $postData['draw'];
        $start = $postData['start'];
        $rowperpage = $postData['length']; // Rows display per page
        $columnIndex = $postData['order'][0]['column']; // Column index
        $columnName = $postData['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
        $searchValue = $postData['search']['value']; // Search value
        ## Search
        $searchQuery = "";
        if ($searchValue != '') {
            $searchQuery = " (fname like '%" . $searchValue . "%' or email like '%" . $searchValue . "%' or phone like'%" . $searchValue . "%' or entry_date like'%" . $searchValue . "%' ) ";
        }

        ## Total number of records without filtering
        $this->db->select('count(*) as allcount');
        $records = $this->db->get($this->tbl)->result();
        $totalRecords = $records[0]->allcount;

        ## Total number of record with filtering
        $this->db->select('count(*) as allcount');
        if ($searchQuery != '')
            $this->db->where($searchQuery);
        $records = $this->db->get($this->tbl)->result();
        $totalRecordwithFilter = $records[0]->allcount;

        ## Fetch records
        $this->db->select('*');
        if ($searchQuery != '')
            $this->db->where($searchQuery);
        $this->db->order_by("customize_id", "desc");
        $this->db->limit($rowperpage, $start);
        $records = $this->db->get($this->tbl)->result();
        $data = array();
        foreach ($records as $record) {
            $country = $this->md->select_where('tbl_country', array('id' => $record->country));
            $city = $this->md->select_where('tbl_city', array('id' => $record->city));
            $product = $this->md->select_where('tbl_product', array('product_id' => $record->product_id));
            $data[] = array(
                "fname" => $record->fname,
                "email" => $record->email,
                "phone" => $record->phone,
                "country" => ($country) ? $country[0]->name : '<mark>Country Removed!</mark>',
                "city" => ($city) ? $city[0]->name : '<mark>City Removed!</mark>',
                "product" => ($product) ? $product[0]->title : '<mark>Product Removed!</mark>',
                "photo" => '<a href="' . base_url($record->photo ? $record->photo : FILENOTFOUND) . '" target="_blank" data-toggle="tooltip" data-placement="right" title="" data-original-title="Show Full Category"><img src="' . base_url($record->photo ? $record->photo : FILENOTFOUND) . '" width="30" height="30" style="object-fit:contain" /></a>',
                "armchair" => $record->armchair,
                "seater_2" => $record->seater_2,
                "seater_3" => $record->seater_3,
                "seater_4" => $record->seater_4,
                "seater_5" => $record->seater_5,
                "corner_sofa_length" => $record->corner_sofa_length,
                "textile_type" => $record->textile_type,
                "textile_colour" => $record->textile_colour,
                "foam_density" => $record->foam_density,
                "cost" => $record->cost,
                "entry_date" => date(($webData ? $webData[0]->date_format : 'd-M-Y'), strtotime($record->entry_date)),
                "delete" => '<a id="deleteItem" data-itemid="' . $record->customize_id . '" class="text-danger cursor-pointer" data-toggle="tooltip" data-placement="bottom" data-original-title="Remove Data"><i class="far fa-trash-alt" ></i></a>'
            );
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return $response;
    }

}
