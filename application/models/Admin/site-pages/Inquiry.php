<?php

class Inquiry extends CI_Model {

    // Global Variables
    public $tbl = 'tbl_inquiry';

    // get data from table set in datatable
    function getData($postData = null) {

        $webData = $this->md->getWebData(); // Get Website Data
        ## Read value
        $draw = $postData['draw'];
        $start = $postData['start'];
        $rowperpage = $postData['length']; // Rows display per page
        $columnIndex = $postData['order'][0]['column']; // Column index
        $columnName = $postData['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
        $searchValue = $postData['search']['value']; // Search value
        ## Search 
        $searchQuery = "";
        if ($searchValue != '') {
            $searchQuery = " (name like '%" . $searchValue . "%' or email like '%" . $searchValue . "%' or phone like'%" . $searchValue . "%' or message like'%" . $searchValue . "%'  ) ";
        }

        ## Total number of records without filtering
        $this->db->select('count(*) as allcount');
        $records = $this->db->get($this->tbl)->result();
        $totalRecords = $records[0]->allcount;

        ## Total number of record with filtering
        $this->db->select('count(*) as allcount');
        if ($searchQuery != '')
            $this->db->where($searchQuery);
        $records = $this->db->get($this->tbl)->result();
        $totalRecordwithFilter = $records[0]->allcount;

        ## Fetch records
        $this->db->select('*');
        if ($searchQuery != '')
            $this->db->where($searchQuery);
        $this->db->order_by("inquiry_id", "desc");
        $this->db->limit($rowperpage, $start);
        $records = $this->db->get($this->tbl)->result();

        $data = array();
        foreach ($records as $record) {
            $data[] = array(
                "name" => $record->name,
                "email" => $record->email,
                "phone" => $record->phone,
                "ssc" => ($record->ssc != "||||||||") ? $record->ssc : '-',
                "hsc" => ($record->hsc != "||||||||") ? $record->hsc : '-',
                "diploma" => ($record->diploma != "||||||||") ? $record->diploma : '-',
                "graduation" => ($record->graduation != "||||||||") ? $record->graduation : '-',
                "pg" => ($record->pg != "||||||||") ? $record->pg : '-',
                "reference" => $record->reference,
                "score" => $record->score,
                "notes" => $record->notes,
                "datetime" => date(($webData ? $webData[0]->date_format : 'd-M-Y'), strtotime($record->datetime)),
                "delete" => '<a id="deleteItem" data-itemid="' . $record->inquiry_id . '"  class="text-danger cursor-pointer" data-toggle="tooltip" data-placement="bottom"  data-original-title="Remove Data"><i class="far fa-trash-alt" ></i></a>'
            );
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return $response;
    }

}
