<?php
echo $page_head;  //  Load Head Link and Scripts
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
$control_fields = $this->db->list_fields('tbl_control');    // Get all pages
$control_fields_feature = $this->db->list_fields('tbl_control_feature');    // Get all features
?>  
<body>
    <div id="app">
        <div class="main-wrapper">
            <div class="navbar-bg"></div>
            <?php echo $page_header; //  Load Header  ?>  
            <?php echo $page_sidebar; //  Load Sidebar  ?> 
            <!-- >> Main Content Start
            ================================================== -->
            <div class="main-content">
                <section class="section">
                    <?php echo $page_breadcrumb; // Load Breadcrumb ?> 
                    <div class="section-body">
                        <?php $this->load->view('admin/common/page_header'); // Load Page Header (Title / Navigation)  ?> 
                        <div class="row">
                            <?php
                            if ($page_type == "add" || $page_type == "edit"):
                                ?>
                                <!-- >> ADD/EDIT Data Start
                                ================================================== -->
                                <div class="col-md-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-body"> 
                                            <?php
                                            if (isset($updata)):
                                                if ($permission['all'] || $permission['edit']):  // If user has edit/update permission
                                                    ?>
                                                    <!-- >> Edit Form Start
                                                    ================================================== --> 
                                                    <form name="update" method="post">  
                                                        <div class="panel-body row">    
                                                            <div class="form-group col-md-3"> 
                                                                <label class="control-label">Select Permission Type</label> <br/> 
                                                                <div class="custom-control custom-radio custom-control-inline">
                                                                    <input onclick="$('#page_permi').show();$('#feature_permi').hide();" type="radio" id="page_type" <?php echo ($updata[0]->type == 'page' ? 'checked' : ''); ?> value="page" name="type" class="custom-control-input">
                                                                    <label class="custom-control-label" for="page_type">Page</label>
                                                                </div> 
                                                                <div class="custom-control custom-radio custom-control-inline">
                                                                    <input onclick="$('#page_permi').hide();$('#feature_permi').show();" type="radio" id="feature_type" <?php echo ($updata[0]->type == 'feature' ? 'checked' : ''); ?> value="feature" name="type" class="custom-control-input">
                                                                    <label class="custom-control-label" for="feature_type">Feature</label>
                                                                </div>  
                                                            </div>
                                                            <div class="col-md-9">
                                                                <div class="row" id="page_permi" style="display: <?php echo ($updata[0]->type == 'page' ? 'flex' : 'none'); ?>">
                                                                    <div class="form-group col-md-4">
                                                                        <label class="control-label">Menu</label>
                                                                        <input type="text" name="menu" value="<?php echo $updata[0]->menu; ?>" placeholder="Enter Menu Name" class="form-control <?php if (form_error('menu')) { ?> is-invalid <?php } ?>">  
                                                                        <div class="error-text">
                                                                            <?php
                                                                            if (form_error('menu')) {
                                                                                echo form_error('menu');
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>     
                                                                    <div class="form-group col-md-4">
                                                                        <label class="control-label">Page</label>
                                                                        <input type="text" name="page" value="<?php echo $updata[0]->feature; ?>" placeholder="Enter Page Name" class="form-control <?php if (form_error('page')) { ?> is-invalid <?php } ?>">  
                                                                        <div class="error-text">
                                                                            <?php
                                                                            if (form_error('page')) {
                                                                                echo form_error('page');
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>     
                                                                    <div class="form-group col-md-8"> 
                                                                        <label class="control-label">Select Permission Option</label> <br/> 
                                                                        <div class="custom-control custom-checkbox custom-control-inline">
                                                                            <input type="checkbox" id="all_opt" <?php echo ($updata[0]->all ? 'checked' : ''); ?>  onchange="checkAllPermission(this);" value="all" name="page_option[]" class="custom-control-input">
                                                                            <label class="custom-control-label" for="all_opt">All</label>
                                                                        </div> 
                                                                        <div class="custom-control custom-checkbox custom-control-inline">
                                                                            <input type="checkbox" id="read_opt" <?php echo ($updata[0]->read ? 'checked' : ''); ?>  value="read" name="page_option[]" class="custom-control-input">
                                                                            <label class="custom-control-label" for="read_opt">Read</label>
                                                                        </div> 
                                                                        <div class="custom-control custom-checkbox custom-control-inline">
                                                                            <input type="checkbox" id="write_opt" <?php echo ($updata[0]->write ? 'checked' : ''); ?>  value="write" name="page_option[]" class="custom-control-input">
                                                                            <label class="custom-control-label" for="write_opt">Write</label>
                                                                        </div> 
                                                                        <div class="custom-control custom-checkbox custom-control-inline">
                                                                            <input type="checkbox" id="edit_opt" <?php echo ($updata[0]->edit ? 'checked' : ''); ?>  value="edit" name="page_option[]" class="custom-control-input">
                                                                            <label class="custom-control-label" for="edit_opt">Edit</label>
                                                                        </div> 
                                                                        <div class="custom-control custom-checkbox custom-control-inline">
                                                                            <input type="checkbox" id="delete_opt" <?php echo ($updata[0]->delete ? 'checked' : ''); ?>  value="delete" name="page_option[]" class="custom-control-input">
                                                                            <label class="custom-control-label" for="delete_opt">Delete</label>
                                                                        </div>  
                                                                        <div class="custom-control custom-checkbox custom-control-inline">
                                                                            <input type="checkbox" id="status_opt" <?php echo ($updata[0]->status ? 'checked' : ''); ?>  value="status" name="page_option[]" class="custom-control-input">
                                                                            <label class="custom-control-label" for="status_opt">Status(Active/Inactive)</label>
                                                                        </div>  
                                                                        <div class="error-text">
                                                                            <?php
                                                                            if (form_error('page_option')) {
                                                                                echo form_error('page_option');
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div> 
                                                                </div>
                                                                <div class="row" id="feature_permi" style="display: <?php echo ($updata[0]->type == 'page' ? 'none' : 'flex'); ?>">
                                                                    <div class="form-group col-md-4">
                                                                        <label class="control-label">Feature</label>
                                                                        <input type="text" name="feature" value="<?php echo $updata[0]->feature; ?>" placeholder="Enter Feature Name" class="form-control <?php if (form_error('feature')) { ?> is-invalid <?php } ?>"> 
                                                                        <div class="error-text">
                                                                            <?php
                                                                            if (form_error('feature')) {
                                                                                echo form_error('feature');
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>     
                                                                    <div class="form-group col-md-8"> 
                                                                        <label class="control-label">Select Permission Option</label> <br/> 
                                                                        <div class="custom-control custom-checkbox custom-control-inline">
                                                                            <input type="checkbox" id="all_opt_feature" <?php echo ($updata[0]->all ? 'checked' : ''); ?> value="all" onchange="checkAllPermission(this);" name="feature_option[]" class="custom-control-input">
                                                                            <label class="custom-control-label" for="all_opt_feature">All</label>
                                                                        </div> 
                                                                        <div class="custom-control custom-checkbox custom-control-inline">
                                                                            <input type="checkbox" id="read_opt_feature" <?php echo ($updata[0]->read ? 'checked' : ''); ?> value="read" name="feature_option[]" class="custom-control-input">
                                                                            <label class="custom-control-label" for="read_opt_feature">Read</label>
                                                                        </div>  
                                                                        <div class="custom-control custom-checkbox custom-control-inline">
                                                                            <input type="checkbox" id="edit_opt_feature" <?php echo ($updata[0]->edit ? 'checked' : ''); ?> value="edit" name="feature_option[]" class="custom-control-input">
                                                                            <label class="custom-control-label" for="edit_opt_feature">Edit</label>
                                                                        </div>  
                                                                        <div class="error-text">
                                                                            <?php
                                                                            if (form_error('feature_option')) {
                                                                                echo form_error('feature_option');
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div> 
                                                                </div>
                                                            </div> 
                                                        </div>
                                                        <footer class="panel-footer">
                                                            <button type="submit" class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info" value="send" name="update">Update Permission</button>
                                                            <a href="<?php echo base_url($current_page . '/show'); ?>" class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light" >Cancel</a> 
                                                        </footer>
                                                    </form>
                                                    <!-- << Edit Form End
                                                    ================================================== -->
                                                    <?php
                                                else:
                                                    $this->load->view('admin/common/access_denied');
                                                endif;
                                            else:
                                                if ($permission['all'] || $permission['write']):  // If user has write/add permission
                                                    ?>  
                                                    <!-- >> Add Form Start
                                                     ================================================== -->
                                                    <form name="add" method="post">
                                                        <div class="panel-body row">   
                                                            <div class="form-group col-md-3"> 
                                                                <label class="control-label">Select Permission Type</label> <br/> 
                                                                <div class="custom-control custom-radio custom-control-inline">
                                                                    <input onclick="$('#page_permi').show();$('#feature_permi').hide();" type="radio" id="page_type" checked="" value="page" name="type" class="custom-control-input">
                                                                    <label class="custom-control-label" for="page_type">Page</label>
                                                                </div> 
                                                                <div class="custom-control custom-radio custom-control-inline">
                                                                    <input onclick="$('#page_permi').hide();$('#feature_permi').show();" type="radio" id="feature_type" value="feature" name="type" class="custom-control-input">
                                                                    <label class="custom-control-label" for="feature_type">Feature</label>
                                                                </div>  
                                                            </div>
                                                            <div class="col-md-9">
                                                                <div class="row" id="page_permi">
                                                                    <div class="form-group col-md-4">
                                                                        <label class="control-label">Menu Name</label>
                                                                        <input type="text" name="menu" value="<?php
                                                                        if (set_value('menu') && !isset($success)) {
                                                                            echo set_value('menu');
                                                                        }
                                                                        ?>" placeholder="Enter Menu Name" class="form-control <?php if (form_error('menu')) { ?> is-invalid <?php } ?>"> 
                                                                        <div class="error-text">
                                                                            <?php
                                                                            if (form_error('menu')) {
                                                                                echo form_error('menu');
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>     
                                                                    <div class="form-group col-md-4">
                                                                        <label class="control-label">Page Name</label>
                                                                        <input type="text" name="page" value="<?php
                                                                        if (set_value('page') && !isset($success)) {
                                                                            echo set_value('page');
                                                                        }
                                                                        ?>" placeholder="Enter Page Name" class="form-control <?php if (form_error('page')) { ?> is-invalid <?php } ?>"> 
                                                                        <div class="error-text">
                                                                            <?php
                                                                            if (form_error('page')) {
                                                                                echo form_error('page');
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>     
                                                                    <div class="form-group col-md-8"> 
                                                                        <label class="control-label">Select Permission Option</label> <br/> 
                                                                        <div class="custom-control custom-checkbox custom-control-inline">
                                                                            <input type="checkbox" id="all_opt" onchange="checkAllPermission(this);" value="all" name="page_option[]" class="custom-control-input">
                                                                            <label class="custom-control-label" for="all_opt">All</label>
                                                                        </div> 
                                                                        <div class="custom-control custom-checkbox custom-control-inline">
                                                                            <input type="checkbox" id="read_opt" value="read" name="page_option[]" class="custom-control-input">
                                                                            <label class="custom-control-label" for="read_opt">Read</label>
                                                                        </div> 
                                                                        <div class="custom-control custom-checkbox custom-control-inline">
                                                                            <input type="checkbox" id="write_opt" value="write" name="page_option[]" class="custom-control-input">
                                                                            <label class="custom-control-label" for="write_opt">Write</label>
                                                                        </div> 
                                                                        <div class="custom-control custom-checkbox custom-control-inline">
                                                                            <input type="checkbox" id="edit_opt" value="edit" name="page_option[]" class="custom-control-input">
                                                                            <label class="custom-control-label" for="edit_opt">Edit</label>
                                                                        </div> 
                                                                        <div class="custom-control custom-checkbox custom-control-inline">
                                                                            <input type="checkbox" id="delete_opt" value="delete" name="page_option[]" class="custom-control-input">
                                                                            <label class="custom-control-label" for="delete_opt">Delete</label>
                                                                        </div> 
                                                                        <div class="custom-control custom-checkbox custom-control-inline">
                                                                            <input type="checkbox" id="status_opt" value="status" name="page_option[]" class="custom-control-input">
                                                                            <label class="custom-control-label" for="status_opt">Status(Active/Inactive)</label>
                                                                        </div> 
                                                                        <div class="error-text">
                                                                            <?php
                                                                            if (form_error('page_option')) {
                                                                                echo form_error('page_option');
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div> 
                                                                </div>
                                                                <div class="row" id="feature_permi" style="display: none">
                                                                    <div class="form-group col-md-4">
                                                                        <label class="control-label">Feature</label>
                                                                        <input type="text" name="feature" value="<?php
                                                                        if (set_value('feature') && !isset($success)) {
                                                                            echo set_value('feature');
                                                                        }
                                                                        ?>" placeholder="Enter Feature Name" class="form-control <?php if (form_error('feature')) { ?> is-invalid <?php } ?>">
                                                                        <div class="error-text">
                                                                            <?php
                                                                            if (form_error('feature')) {
                                                                                echo form_error('feature');
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>     
                                                                    <div class="form-group col-md-8"> 
                                                                        <label class="control-label">Select Permission Option</label> <br/> 
                                                                        <div class="custom-control custom-checkbox custom-control-inline">
                                                                            <input type="checkbox" id="all_opt_feature" value="all" onchange="checkAllPermission(this);" name="feature_option[]" class="custom-control-input">
                                                                            <label class="custom-control-label" for="all_opt_feature">All</label>
                                                                        </div> 
                                                                        <div class="custom-control custom-checkbox custom-control-inline">
                                                                            <input type="checkbox" id="read_opt_feature" value="read" name="feature_option[]" class="custom-control-input">
                                                                            <label class="custom-control-label" for="read_opt_feature">Read</label>
                                                                        </div>  
                                                                        <div class="custom-control custom-checkbox custom-control-inline">
                                                                            <input type="checkbox" id="edit_opt_feature" value="edit" name="feature_option[]" class="custom-control-input">
                                                                            <label class="custom-control-label" for="edit_opt_feature">Edit</label>
                                                                        </div>  
                                                                        <div class="error-text">
                                                                            <?php
                                                                            if (form_error('feature_option')) {
                                                                                echo form_error('feature_option');
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div> 
                                                                </div>
                                                            </div> 
                                                        </div>
                                                        <footer class="panel-footer">
                                                            <button type="submit" class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info" value="send" name="add">Add User Permission</button>
                                                            <button class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light" type="reset">Reset Form</button> 
                                                        </footer>
                                                    </form>
                                                    <!-- << Add Form End
                                                    ================================================== -->
                                                    <?php
                                                else:
                                                    $this->load->view('admin/common/access_denied');
                                                endif;
                                            endif;
                                            ?> 
                                        </div>
                                    </div> 
                                </div>
                                 <!-- << ADD/EDIT Data END
                                ================================================== -->
                                <?php
                            else:
                                if ($permission['all'] || $permission['read']):  // If user has read/show data permission
                                    ?> 
                                    <!-- >> Table Data Start
                                    ================================================== -->
                                    <div class="col-md-12">
                                        <div class="main-card mb-3 card">
                                            <div class="card-body"> 
                                                <div class="">
                                                    <table class="table mb-none table-hover" id="permissionTable">
                                                        <thead>
                                                            <tr>  
                                                                <th>Type</th>  
                                                                <th>Name</th>  
                                                                <th>All</th>  
                                                                <th>Read</th>  
                                                                <th>Edit</th>  
                                                                <th>Write</th>  
                                                                <th>Delete</th>  
                                                                <th>Status</th>  
                                                                <?php
                                                                if ($permission['all'] || $permission['edit']):
                                                                    echo '<th>Edit</th>';
                                                                endif;
                                                                if ($permission['all'] || $permission['delete']):
                                                                    echo '<th>Delete</th>';
                                                                endif;
                                                                ?>  
                                                            </tr>
                                                        </thead> 
                                                    </table>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                    <!-- << Table Data End
                                    ================================================== -->
                                    <?php
                                else:
                                    $this->load->view('admin/common/access_denied');
                                endif;
                            endif;
                            ?>
                        </div>
                    </div> 
                </section>
            </div> 
            <!-- << Main Content End
            ================================================== -->
            <?php echo $page_footer;  //  Load Footer  ?> 
        </div>
    </div> 
    <?php
    $alert_data['success'] = $success;
    $alert_data['error'] = $error;
    $this->load->view('admin/common/alert', $alert_data);  // Load Notification Alert Message & Footer script
    ?> 
    <script>
        // List column which will be display in the table
        const column = [
            {data: 'type'},
            {data: 'feature'},
            {data: 'per_all'},
            {data: 'per_read'},
            {data: 'per_edit'},
            {data: 'per_write'},
            {data: 'per_delete'},
            {data: 'per_status'},
<?php
if ($permission['all'] || $permission['edit']):
    echo '{data: "edit"},';
endif;
if ($permission['all'] || $permission['delete']):
    echo '{data: "delete"},';
endif;
?>
        ];
        // Parameter --> [TABLE_NAME, GET_DATA_URL, DISPLAY_COLUMN, PHOTO_COLUMN_NAME_FOR_DELETE] 
        getDataTable('<?php echo $active_page; ?>', '<?php echo base_url($current_page . '/getdata') ?>', column);
    </script>
</body>