<?php
echo $page_head;  //  Load Head Link and Scripts
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
?>  
<body>
    <div id="app">
        <div class="main-wrapper">
            <div class="navbar-bg"></div>
            <?php echo $page_header; //  Load Header  ?>  
            <?php echo $page_sidebar; //  Load Sidebar  ?> 
            <!-- >> Main Content Start
            ================================================== -->
            <div class="main-content">
                <section class="section">
                    <?php echo $page_breadcrumb; // Load Breadcrumb ?> 
                    <div class="section-body">
                        <?php $this->load->view('admin/common/page_header'); // Load Page Header (Title / Navigation)  ?> 
                        <div class="row">
                            <?php
                            if ($page_type == "add" || $page_type == "edit"):
                                ?>
                                <!-- >> ADD/EDIT Data Start
                                ================================================== -->
                                <div class="col-md-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-body"> 
                                            <?php
                                            if (isset($updata)):
                                                if ($permission['all'] || $permission['edit']):  // If user has edit/update permission
                                                    ?>
                                                    <!-- >> Edit Form Start
                                                   ================================================== --> 
                                                    <form name="update" method="post" enctype="multipart/form-data">
                                                        <div class="panel-body row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Title</label>
                                                                    <input type="text" name="title" value="<?php echo $updata[0]->title; ?>" placeholder="Enter Client Title" class="form-control <?php if (form_error('title')) { ?> is-invalid <?php } ?>">
                                                                    <div class="error-text">
                                                                        <?php
                                                                        if (form_error('title')) {
                                                                            echo form_error('title');
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>   
                                                            </div>   
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Select Client <span style="font-size: 12px" class="text-info">*(Upload only .jpg | .jpeg | .png files.)</span> </label>
                                                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                        <div class="input-append"> 
                                                                            <input type="file" id="file" onchange="readURL(this, 'blah');$('#updateStatus').val('yes');" name="client" class="" accept="image/*">  
                                                                            <input type="hidden" id="updateStatus" name="updateStatus" />
                                                                            <input type="hidden" value="<?php echo $updata[0]->path; ?>" name="oldPath" />
                                                                        </div>
                                                                    </div>
                                                                    <?php
                                                                    if ($updata[0]->path):
                                                                        ?>
                                                                        <img src="<?php echo base_url($updata[0]->path); ?>" class="mt-20 center-block" width="50" id="blah" />
                                                                        <?php
                                                                    else:
                                                                        ?>
                                                                        <img class="mt-20 center-block" width="50" id="blah" />
                                                                    <?php
                                                                    endif;
                                                                    ?> 
                                                                    <p class="error-text file-error" style="display: none">Select a valid file!</p>  
                                                                </div> 
                                                            </div> 
                                                        </div>
                                                        <footer class="panel-footer">
                                                            <button type="submit" class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info" value="send" name="update">Update Client</button>
                                                            <a href="<?php echo base_url($current_page . '/show'); ?>" class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light" >Cancel</a> 
                                                        </footer>
                                                    </form>
                                                      <!-- << Edit Form End
                                                    ================================================== -->
                                                    <?php
                                                else:
                                                    $this->load->view('admin/common/access_denied');
                                                endif;
                                            else:
                                                if ($permission['all'] || $permission['write']):  // If user has write/add permission
                                                    ?>  
                                                    <!-- >> Add Form Start
                                                    ================================================== -->
                                                    <form name="add" method="post" enctype="multipart/form-data" onsubmit="return filevalidate();">
                                                        <div class="panel-body row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Title</label>
                                                                    <input type="text" name="title" value="<?php
                                                                    if (set_value('title') && !isset($success)) {
                                                                        echo set_value('title');
                                                                    }
                                                                    ?>" placeholder="Enter Client Title" class="form-control <?php if (form_error('title')) { ?> is-invalid <?php } ?>">
                                                                    <div class="error-text">
                                                                        <?php
                                                                        if (form_error('title')) {
                                                                            echo form_error('title');
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>   
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Select Client <span style="font-size: 12px" class="text-info">*(Upload only .jpg | .jpeg | .png files.)</span> </label>
                                                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                        <div class="input-append"> 
                                                                            <input type="file" id="file" onchange="readURL(this, 'blah');" name="client" class="" accept="image/*">  
                                                                        </div>
                                                                    </div>
                                                                    <img src="" class="mt-20 center-block" width="50" id="blah" />
                                                                    <div class="error-text file-error" style="display: none">Select a valid file!</div> 
                                                                </div> 
                                                            </div>
                                                        </div>
                                                        <footer class="panel-footer">
                                                            <button type="submit" class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info" value="send" name="add">Upload Client</button>
                                                            <button class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light" type="reset">Reset Form</button> 
                                                        </footer>
                                                    </form>
                                                    <!-- << Add Form End
                                                    ================================================== -->
                                                    <?php
                                                else:
                                                    $this->load->view('admin/common/access_denied');
                                                endif;
                                            endif;
                                            ?> 
                                        </div>
                                    </div> 
                                </div>
                                <!-- << ADD/EDIT Data END
                                ================================================== -->
                                <?php
                            else:
                                if ($permission['all'] || $permission['read']):  // If user has read/show data permission
                                    ?> 
                                    <!-- >> Table Data Start
                                    ================================================== -->
                                    <div class="col-md-12">
                                        <div class="main-card mb-3 card">
                                            <div class="card-body"> 
                                                <div class="table-responsive">
                                                    <table class="table mb-none table-hover" id="clientTable">
                                                        <thead>
                                                            <tr> 
                                                                <th>Title</th> 
                                                                <th>Client</th>
                                                                <?php
                                                                if ($permission['all'] || $permission['edit']):
                                                                    echo '<th>Edit</th>';
                                                                endif;
                                                                if ($permission['all'] || $permission['delete']):
                                                                    echo '<th>Delete</th>';
                                                                endif;
                                                                ?>  
                                                            </tr>
                                                        </thead> 
                                                    </table>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                    <!-- << Table Data End
                                    ================================================== -->
                                    <?php
                                else:
                                    $this->load->view('admin/common/access_denied');
                                endif;
                            endif;
                            ?>
                        </div> 
                    </div>
                </section> 
            </div>
            <!-- << Main Content End
            ================================================== -->
            <?php echo $page_footer;  //  Load Footer  ?> 
        </div>
    </div>  
    <?php
    $alert_data['success'] = $success;
    $alert_data['error'] = $error;
    $this->load->view('admin/common/alert', $alert_data);  // Load Notification Alert Message & Footer script
    ?> 
    <script>
        // List column which will be display in the table
        const column = [
            {data: 'title'},
            {data: 'path'},
<?php
if ($permission['all'] || $permission['edit']):
    echo '{data: "edit"},';
endif;
if ($permission['all'] || $permission['delete']):
    echo '{data: "delete"},';
endif;
?>
        ];
        // Parameter --> [TABLE_NAME, GET_DATA_URL, DISPLAY_COLUMN, PHOTO_COLUMN_NAME_FOR_DELETE] 
        getDataTable('<?php echo $active_page; ?>', '<?php echo base_url($current_page . '/getdata') ?>', column, 'path');
    </script>
</body>