<?php
$web_data = $this->md->select('tbl_web_data')[0];
?>
<!DOCTYPE html>
<html>
<head>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta charset="utf-8">
    <title>Invoice No.: <?php echo $invoiceData ? $invoiceData[0]->order_id : '' ?> - SkinToLove</title>
    <base href="<?php echo base_url('assets/'); ?>"/>
    <!-- Favicon Icon -->
    <link rel="apple-touch-icon" sizes="180x180"
          href="<?php echo base_url(($web_data) ? $web_data->favicon : FILENOTFOUND); ?>">
    <link rel="icon" type="image/png" sizes="32x32"
          href="<?php echo base_url(($web_data) ? $web_data->favicon : FILENOTFOUND); ?>">
    <link rel="icon" type="image/png" sizes="16x16"
          href="<?php echo base_url(($web_data) ? $web_data->favicon : FILENOTFOUND); ?>">
    <link href="<?php echo base_url('admin_asset/'); ?>css/responsive.css" rel="stylesheet">
    <!-- Report CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('admin_asset/'); ?>/css/report.css"/>
</head>
<body>
<!-- Define header and footer blocks before your content -->
<!--<header>-->
<!--    Dronachryas-->
<!--</header>-->

<footer>
    <div class="page-counter">
        Invoice No.: <?php echo $invoiceData ? $invoiceData[0]->order_id : '' ?>
    </div>
    <div class="clearfix"></div>
</footer>

<!-- Wrap the content of your PDF inside a main tag -->
<main>
    <!-- Page - 1 -->
    <div style="page-break-after: never;" class="page-1">
        <div align="center">
            <h1 class="mb-10 text-transform-uppercase letter-spacing-3 font-weight-100">
                || <?php echo $web_data ? $web_data->project_name : ''; ?> ||</h1>
            <p>Phone Number: <?php echo $web_data ? $web_data->phone : ''; ?></p>
            <p><?php echo $web_data ? $web_data->address : ''; ?></p>
        </div>
        <table cellspacing="0" cellpadding="10">
            <tr style="border:1px solid #000;">
                <td>
                    <b class="subheading">Bill To</b>
                    <b>Name: </b><?php echo $invoiceData ? ucfirst($invoiceData[0]->fname) : '' ?><br/>
                    <b>Email: </b><?php echo $invoiceData ? $invoiceData[0]->email : 'not mentioned!' ?> <br/>
                    <b>Phone: </b><?php echo $invoiceData ? $invoiceData[0]->phone : 'not mentioned!' ?><br/>
                    <b>Address: </b><?php echo $invoiceData ? ucfirst($invoiceData[0]->address) : 'not mentioned!' ?>
                </td>
                <td style="border:1px solid #000;width: 50%">
                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td class="strong">Invoice#</td>
                            <td>:</td>
                            <td><?php echo $invoiceData ? $invoiceData[0]->order_id : '' ?></td>
                        </tr>
                        <tr>
                            <td class="strong">Invoice Date</td>
                            <td>:</td>
                            <td><?php echo $invoiceData ? (date('d-M-Y', strtotime($invoiceData[0]->entry_date))) : '' ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="border:1px solid #000;">
                <td colspan="2">
                    <table border="1" bordercolor="#f5f5f5" cellspacing="0" cellpadding="3" width="100%">
                        <tr>
                            <th>No</th>
                            <th>Item</th>
                            <th>Qty</th>
                            <th>Price</th>
                            <th>Net Price</th>
                        </tr>
                        <?php
                        $subTotal = 0;
                        $items = $this->md->select_where('tbl_transaction', array('bill_id' => $invoiceData[0]->bill_id));
                        if ($items) {
                            foreach ($items as $key => $item) {
                                $sizeData = $this->md->select_where('tbl_size', array('size_id' => $item->size));
                                $subTotal = $subTotal + $item->netprice;
                                ?>
                                <tr class="text-center">
                                    <td><?php echo $key + 1; ?></td>
                                    <td>
                                        <?php
                                        echo $this->md->getItemName('tbl_product', 'product_id', 'title', $item->product_id);
                                        echo "<br/>";
                                        echo ucfirst($item->order_type) . "  " . ($sizeData ? ($sizeData[0]->size . $sizeData[0]->measurement) : '');
                                        ?>
                                    </td>
                                    <td><?php echo $item->qty; ?></td>
                                    <td>$<?php echo $item->price; ?></td>
                                    <td>$<?php echo $item->netprice; ?></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        <tr>
                            <td colspan="4" align="right">Sub Total</td>
                            <td align="center">
                                $<?php echo $subTotal != 0 ? number_format($subTotal, 2) : '0'; ?></td>
                        </tr>
                        <tr>
                            <td colspan="4" align="right">Coupon Discount</td>
                            <td align="center">
                                <?php
                                $discount = $invoiceData[0]->discount ? $invoiceData[0]->discount : 0;
                                ?>
                                $<?php echo $discount != 0 ? number_format($discount, 2) : 0.00; ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="right">Shipping</td>
                            <td align="center">
                                <?php
                                $shippingCharge = $invoiceData[0]->shipping_charge ? $invoiceData[0]->shipping_charge : 0;
                                ?>
                                $<?php echo $shippingCharge != 0 ? number_format($shippingCharge, 2) : 0.00; ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="right">Tax(15%)</td>
                            <td align="center">
                                <?php
                                $tax = (($subTotal - $discount) + $shippingCharge) * 15 / 100;
                                //$tax = $invoiceData[0]->tax ? $invoiceData[0]->tax : 0;
                                ?>
                                $<?php echo $tax ? number_format($tax, 2) : ''; ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="right" class="font-20 font-weight-bold">Grand Total</td>
                            <td align="center" class="font-20 font-weight-bold">
                                $<?php
                                $grandTotal = ($subTotal - $discount) + $shippingCharge + $tax;
                                echo number_format($grandTotal,2);
                                /*
                                 if ($invoiceData[0]->shipping_charge) {
                                    echo $invoiceData ? ($invoiceData[0]->netprice ? number_format($invoiceData[0]->netprice, 2) : '') : '';
                                 } else {
                                        echo $invoiceData ? ($invoiceData[0]->netprice ? number_format($invoiceData[0]->netprice + $tax, 2) : '') : '';
                                 }
                                */
                                ?> </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="border:1px solid #000;">
                <td align="center">
                    <Br/>
                    <i>Thanks for your business!</i>
                    <Br/><Br/><Br/>
                </td>
                <td align="center" style="border:1px solid #000;width: 50%">
                    <br/>
                    <i> Authorized Signature</i>
                    <Br/><Br/><Br/>
                </td>
            </tr>
        </table>
    </div>

</main>
</body>
</html>