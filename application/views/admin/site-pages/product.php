<?php
echo $page_head;  //  Load Head Link and Scripts
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
$fields = $this->md->select('tbl_category');    // get all category
$sizeFields = $this->md->my_query('SELECT * FROM tbl_size ORDER BY size ASC')->result();    // get all Size
$colours = $this->md->select('tbl_colours');    // get all colours
?>
<body>
<div id="app">
    <div class="main-wrapper">
        <div class="navbar-bg"></div>
        <?php echo $page_header; //  Load Header  ?>
        <?php echo $page_sidebar; //  Load Sidebar  ?>
        <!-- >> Main Content Start
        ================================================== -->
        <div class="main-content">
            <section class="section">
                <?php echo $page_breadcrumb; // Load Breadcrumb ?>
                <div class="section-body">
                    <?php $this->load->view('admin/common/page_header'); // Load Page Header (Title / Navigation)  ?>
                    <div class="row">
                        <?php
                        if ($page_type == "add" || $page_type == "edit"):
                            ?>
                            <!-- >> ADD/EDIT Data Start
                            ================================================== -->
                            <div class="col-md-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-body">
                                        <?php
                                        if (isset($updata)):
                                            if ($permission['all'] || $permission['edit']):  // If user has edit/update permission
                                                ?>
                                                <!-- >> Edit Form Start
                                                ================================================== -->
                                                <form method="post" name="edit" enctype="multipart/form-data">
                                                    <div class="panel-body row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Category</label>
                                                                <select name="category_id"
                                                                        class="form-control select2 text-capitalize <?php if (form_error('category_id')) { ?> is-invalid <?php } ?>">
                                                                    <option value="">Select Category</option>
                                                                    <?php
                                                                    if (!empty($fields)):
                                                                        foreach ($fields as $fields_val):
                                                                            ?>
                                                                            <option
                                                                                <?php echo $fields_val->category_id == $updata[0]->category_id ? 'selected' : ''; ?>
                                                                                value="<?php echo $fields_val->category_id; ?>"><?php echo $fields_val->title; ?></option>
                                                                        <?php
                                                                        endforeach;
                                                                    endif;
                                                                    ?>
                                                                </select>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('category_id')) {
                                                                        echo form_error('category_id');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Product Title in
                                                                    English</label>
                                                                <input type="text" name="title" value="<?php
                                                                if (set_value('title') && !isset($success)) {
                                                                    echo set_value('title');
                                                                } else {
                                                                    echo $updata[0]->title;
                                                                }
                                                                ?>" placeholder="Enter Product Title"
                                                                       id="productTitle"
                                                                       class="form-control <?php if (form_error('title')) { ?> is-invalid <?php } ?>">
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('title')) {
                                                                        echo form_error('title');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Product Title in
                                                                    Arabic
                                                                    (عنوان
                                                                    المنتج باللغة العربية)</label>
                                                                <input type="text" name="arabic_title"
                                                                       value="<?php
                                                                       if (set_value('arabic_title') && !isset($success)) {
                                                                           echo set_value('arabic_title');
                                                                       } else {
                                                                           echo $updata[0]->arabic_title;
                                                                       }
                                                                       ?>"
                                                                       placeholder="عنوان المنتج باللغة العربية"
                                                                       style="direction: rtl"
                                                                       class="form-control <?php if (form_error('arabic_title')) { ?> is-invalid <?php } ?>">
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('arabic_title')) {
                                                                        echo form_error('arabic_title');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Product Description in
                                                                    English</label>
                                                                <textarea name="description"
                                                                          class="summernote"><?php
                                                                    if (set_value('description') && !isset($success)) {
                                                                        echo set_value('description');
                                                                    } else {
                                                                        echo $updata[0]->description;
                                                                    }
                                                                    ?></textarea>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('description')) {
                                                                        echo form_error('description');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Product Description in
                                                                    Arabic (أدخل وصف المنتج باللغة العربية)</label>
                                                                <textarea name="arabic_desc"
                                                                          style="min-height: 330px;direction: rtl"
                                                                          class="form-control"><?php
                                                                    if (set_value('arabic_desc') && !isset($success)) {
                                                                        echo set_value('arabic_desc');
                                                                    } else {
                                                                        echo $updata[0]->arabic_desc;
                                                                    }
                                                                    ?></textarea>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('arabic_desc')) {
                                                                        echo form_error('arabic_desc');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="addContent mb-20">
                                                                <?php
                                                                $productId = $updata[0]->product_id;
                                                                $attributes = $this->md->select_where('tbl_product_attribute', array('product_id' => $productId));
                                                                if ($attributes) {
                                                                    foreach ($attributes as $key => $attribute) {
                                                                        ?>
                                                                        <div class="row item bg-F5F5F5">
                                                                            <div class="form-group col-md-3">
                                                                                <label class="control-label">Product
                                                                                    Color</label>
                                                                                <input type="hidden" name="attid[]"
                                                                                       value="<?php echo $attribute->product_attribute_id; ?>"/>
                                                                                <select name="color[]"
                                                                                        class="form-control">
                                                                                    <option value="">Select Colour
                                                                                    </option>
                                                                                    <?php
                                                                                    if ($colours) {
                                                                                        foreach ($colours as $colour) {
                                                                                            echo '<option ' . ($attribute->color == (strtolower($colour->colour)) ? 'selected' : '') . ' value="' . strtolower($colour->colour) . '">' . $colour->colour . '</option>';
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                </select>
                                                                            </div>
                                                                            <div class="form-group col-md-3">
                                                                                <label class="control-label">Product
                                                                                    Sku</label>
                                                                                <input type="text" name="sku[]"
                                                                                       value="<?php
                                                                                       if (set_value('sku') && !isset($success)) {
                                                                                           echo set_value('sku');
                                                                                       } else {
                                                                                           echo $attribute->sku;
                                                                                       }
                                                                                       ?>"
                                                                                       placeholder="Enter Product Sku"
                                                                                       class="form-control <?php if (form_error('sku')) { ?> is-invalid <?php } ?>">
                                                                                <div class="error-text">
                                                                                    <?php
                                                                                    if (form_error('sku')) {
                                                                                        echo form_error('sku');
                                                                                    }
                                                                                    ?>
                                                                                </div>
                                                                            </div>
                                                                            <?php
                                                                            //                                                                            echo ($key > 0) ? '<button title="Remove Item" class="btn btn-md btn-danger removeLast" type="button"><i class="fa fa-minus"></i></button>' : '<button title="Add Item" class="btn btn-md btn-primary addMore" type="button"><i class="fa fa-plus"></i></button>';
                                                                            ?>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                } else {
                                                                    ?>
                                                                    <div class="row item bg-F5F5F5">
                                                                        <div class="form-group col-md-3">
                                                                            <label class="control-label">Product
                                                                                Color</label>
                                                                            <input type="text" name="color[]"
                                                                                   value="<?php
                                                                                   if (set_value('color') && !isset($success)) {
                                                                                       echo set_value('color');
                                                                                   }
                                                                                   ?>" placeholder="Enter Product Color"
                                                                                   class="form-control <?php if (form_error('color')) { ?> is-invalid <?php } ?>">
                                                                            <div class="error-text">
                                                                                <?php
                                                                                if (form_error('color')) {
                                                                                    echo form_error('color');
                                                                                }
                                                                                ?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group col-md-3">
                                                                            <label class="control-label">Product
                                                                                Sku</label>
                                                                            <input type="text" name="sku[]"
                                                                                   value="<?php
                                                                                   if (set_value('sku') && !isset($success)) {
                                                                                       echo set_value('sku');
                                                                                   }
                                                                                   ?>"
                                                                                   placeholder="Enter Product Sku"
                                                                                   class="form-control <?php if (form_error('sku')) { ?> is-invalid <?php } ?>">
                                                                            <div class="error-text">
                                                                                <?php
                                                                                if (form_error('sku')) {
                                                                                    echo form_error('sku');
                                                                                }
                                                                                ?>
                                                                            </div>
                                                                        </div>
                                                                        <button title="Add Item"
                                                                                class="btn btn-md btn-primary addMore"
                                                                                type="button"><i class="fa fa-plus"></i>
                                                                        </button>
                                                                    </div>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Meta Title <span
                                                                        class="text-danger">*Meta title maximum
                                                                            length is 70 </span></label>
                                                                <input type="text" name="meta_title" value="<?php
                                                                if (set_value('meta_title') && !isset($success)) {
                                                                    echo set_value('meta_title');
                                                                } else {
                                                                    echo $updata[0]->meta_title;
                                                                }
                                                                ?>" placeholder="Enter Meta Title"
                                                                       class="form-control <?php if (form_error('meta_title')) { ?> is-invalid <?php } ?>">
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('meta_title')) {
                                                                        echo form_error('meta_title');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Enter Meta keywords
                                                                    <span class="text-danger">*Meta keywords allow
                                                                            maximum 30 words </span></label>
                                                                <textarea class="form-control"
                                                                          placeholder="Enter Meta Keywords"
                                                                          style="height: 80px"
                                                                          name="meta_keyword"><?php
                                                                    if (set_value('meta_keyword') && !isset($success)) {
                                                                        echo set_value('meta_keyword');
                                                                    } else {
                                                                        echo $updata[0]->meta_keyword;
                                                                    }
                                                                    ?></textarea>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('meta_keyword')) {
                                                                        echo form_error('meta_keyword');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Enter Meta Description
                                                                    <span class="text-danger">*Meta description
                                                                            length is between 150 to 160 </span>
                                                                </label>
                                                                <textarea class="form-control"
                                                                          placeholder="Enter Meta Description"
                                                                          style="height: 80px"
                                                                          name="meta_desc"><?php
                                                                    if (set_value('meta_desc') && !isset($success)) {
                                                                        echo set_value('meta_desc');
                                                                    } else {
                                                                        echo $updata[0]->meta_desc;
                                                                    }
                                                                    ?></textarea>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('meta_desc')) {
                                                                        echo form_error('meta_desc');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label">Product Price</label>
                                                            <input type="number" step="any" name="price" value="<?php
                                                            if (set_value('price') && !isset($success)) {
                                                                echo set_value('price');
                                                            } else {
                                                                echo $updata[0]->price;
                                                            }
                                                            ?>" placeholder="Enter Product Price"
                                                                   id="productPrice"
                                                                   class="form-control <?php if (form_error('price')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('price')) {
                                                                    echo form_error('price');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="form-group mb-5">
                                                                <label class="control-label">
                                                                    <input type="checkbox"
                                                                           name="featured"
                                                                        <?php echo $updata[0]->featured == 1 ? 'checked' : ''; ?>
                                                                           value="1"
                                                                           class="zoom-14 vertical-middle"/> Set to
                                                                    Featured Product
                                                                </label>
                                                            </div>
                                                            <div class="form-group mb-5">
                                                                <label class="control-label">
                                                                    <input type="checkbox"
                                                                           name="customize"
                                                                        <?php echo $updata[0]->customize == 1 ? 'checked' : ''; ?>
                                                                           value="1"
                                                                           class="zoom-14 vertical-middle"/> Set to
                                                                    Customize Product
                                                                </label>
                                                            </div>
                                                            <div class="form-group mb-10">
                                                                <label class="control-label">
                                                                    <input type="checkbox"
                                                                           name="sale"
                                                                        <?php echo $updata[0]->sale == 1 ? 'checked' : ''; ?>
                                                                           value="1"
                                                                           onchange="($(this).is(':checked')) ? ($('#productSalePrice').show()) : ($('#productSalePrice').hide()) "
                                                                           class="zoom-14 vertical-middle"/> Set to
                                                                    Sale Product
                                                                </label>
                                                            </div>
                                                            <div class="form-group" id="productSalePrice"
                                                                 style="display:  <?php echo $updata[0]->sale == 1 ? 'block' : 'none'; ?>">
                                                                <label class="control-label">Product Sale Price</label>
                                                                <input type="text" name="sale_price" value="<?php
                                                                if (set_value('sale_price') && !isset($success)) {
                                                                    echo set_value('sale_price');
                                                                } else {
                                                                    echo $updata[0]->sale_price;
                                                                }
                                                                ?>" placeholder="Enter Product Sale Price"
                                                                       class="form-control <?php if (form_error('sale_price')) { ?> is-invalid <?php } ?>">
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('sale_price')) {
                                                                        echo form_error('sale_price');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <footer class="panel-footer">
                                                        <button type="submit"
                                                                class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info"
                                                                value="send" name="update">Update Product
                                                        </button>
                                                        <a href="<?php echo base_url($current_page . '/show'); ?>"
                                                           class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light">Cancel</a>
                                                    </footer>
                                                </form>
                                                <!-- << Edit Form End
                                                ================================================== -->
                                            <?php
                                            else:
                                                $this->load->view('admin/common/access_denied');
                                            endif;
                                        else:
                                            if ($permission['all'] || $permission['write']):  // If user has write/add permission
                                                ?>
                                                <!-- >> Import Excel/CSV Start
                                                              ================================================== -->
                                                <form name="import" method="post" enctype="multipart/form-data">
                                                    <div
                                                        class="panel-body row mb-30 bg-F4F4F4 text-000 border shadow p-20">
                                                        <div class="col-md-7">
                                                            <div>
                                                                <label
                                                                    class="control-label font-weight-bold vertical-sub mr-10">Import
                                                                    CSV or Excel File
                                                                    <span
                                                                        style="font-size: 13px" class="text-danger">*(Upload only .csv | .xlsx | .xls files.)</span>
                                                                </label>
                                                                <a href="<?php echo base_url('admin_asset/sample/product.xlsx') ?>"
                                                                   download="Product Import Sample"
                                                                   class="mt-1 badge badge-primary">Sample File Download
                                                                </a>
                                                                <div class="fileupload fileupload-new"
                                                                     data-provides="fileupload">
                                                                    <div class="input-append">
                                                                        <input type="file" required
                                                                               name="importedFile">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 text-center">
                                                            <button type="submit"
                                                                    class="mt-3 btn-hover-shine btn btn-shadow btn-info"
                                                                    value="import Data" name="importData">Import Data
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                                <!-- << Import Excel/CSV End
                                                   ================================================== -->
                                                <hr/>
                                                <!-- >> Add Form Start
                                                ================================================== -->
                                                <form method="post" name="add" enctype="multipart/form-data">
                                                    <div class="panel-body row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Category</label>
                                                                <select name="category_id"
                                                                        class="form-control select2 text-capitalize <?php if (form_error('category_id')) { ?> is-invalid <?php } ?>">
                                                                    <option value="">Select Category</option>
                                                                    <?php
                                                                    if (!empty($fields)):
                                                                        foreach ($fields as $fields_val):
                                                                            ?>
                                                                            <option
                                                                                value="<?php echo $fields_val->category_id; ?>"><?php echo $fields_val->title; ?></option>
                                                                        <?php
                                                                        endforeach;
                                                                    endif;
                                                                    ?>
                                                                </select>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('category_id')) {
                                                                        echo form_error('category_id');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Product Title in
                                                                    English</label>
                                                                <input type="text" name="title" value="<?php
                                                                if (set_value('title') && !isset($success)) {
                                                                    echo set_value('title');
                                                                }
                                                                ?>" placeholder="Enter Product Title"
                                                                       id="productTitle"
                                                                       class="form-control <?php if (form_error('title')) { ?> is-invalid <?php } ?>">
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('title')) {
                                                                        echo form_error('title');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Product Title in
                                                                    Arabic
                                                                    (عنوان
                                                                    المنتج باللغة العربية)</label>
                                                                <input type="text" name="arabic_title"
                                                                       value="<?php
                                                                       if (set_value('arabic_title') && !isset($success)) {
                                                                           echo set_value('arabic_title');
                                                                       }
                                                                       ?>"
                                                                       placeholder="عنوان المنتج باللغة العربية"
                                                                       style="direction: rtl"
                                                                       class="form-control <?php if (form_error('arabic_title')) { ?> is-invalid <?php } ?>">
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('arabic_title')) {
                                                                        echo form_error('arabic_title');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Product Description in
                                                                    English</label>
                                                                <textarea name="description"
                                                                          class="summernote"><?php
                                                                    if (set_value('description') && !isset($success)) {
                                                                        echo set_value('description');
                                                                    }
                                                                    ?></textarea>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('description')) {
                                                                        echo form_error('description');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Product Description in
                                                                    Arabic (أدخل وصف المنتج باللغة العربية)</label>
                                                                <textarea name="arabic_desc"
                                                                          style="min-height: 330px;direction: rtl"
                                                                          class="form-control"><?php
                                                                    if (set_value('arabic_desc') && !isset($success)) {
                                                                        echo set_value('arabic_desc');
                                                                    }
                                                                    ?></textarea>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('arabic_desc')) {
                                                                        echo form_error('arabic_desc');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="addContent mb-20">
                                                                <div class="row item bg-F5F5F5">
                                                                    <div class="form-group col-md-3 mb-1">
                                                                        <label class="control-label">Product
                                                                            Color</label>
                                                                        <select name="color[]" class="form-control">
                                                                            <option value="">Select Colour</option>
                                                                            <?php
                                                                            if ($colours) {
                                                                                foreach ($colours as $colour) {
                                                                                    echo '<option value="' . strtolower($colour->colour) . '">' . $colour->colour . '</option>';
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                    <div class="form-group col-md-3">
                                                                        <label class="control-label">Product SKU</label>
                                                                        <input type="text" name="sku[]"
                                                                               value="<?php
                                                                               if (set_value('sku') && !isset($success)) {
                                                                                   echo set_value('sku');
                                                                               }
                                                                               ?>" placeholder="Enter Product SKU"
                                                                               class="form-control">
                                                                    </div>

                                                                    <button title="Add Item"
                                                                            class="btn btn-md btn-primary addMore"
                                                                            type="button"><i class="fa fa-plus"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Meta Title <span
                                                                        class="text-danger">*Meta title maximum
                                                                            length is 70 </span></label>
                                                                <input type="text" name="meta_title" value="<?php
                                                                if (set_value('meta_title') && !isset($success)) {
                                                                    echo set_value('meta_title');
                                                                }
                                                                ?>" placeholder="Enter Meta Title"
                                                                       class="form-control <?php if (form_error('meta_title')) { ?> is-invalid <?php } ?>">
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('meta_title')) {
                                                                        echo form_error('meta_title');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Enter Meta keywords
                                                                    <span class="text-danger">*Meta keywords allow
                                                                            maximum 30 words </span></label>
                                                                <textarea class="form-control"
                                                                          placeholder="Enter Meta Keywords"
                                                                          style="height: 80px"
                                                                          name="meta_keyword"></textarea>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('meta_keyword')) {
                                                                        echo form_error('meta_keyword');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Enter Meta Description
                                                                    <span class="text-danger">*Meta description
                                                                            length is between 150 to 160 </span>
                                                                </label>
                                                                <textarea class="form-control"
                                                                          placeholder="Enter Meta Description"
                                                                          style="height: 80px"
                                                                          name="meta_desc"></textarea>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('meta_desc')) {
                                                                        echo form_error('meta_desc');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group col-md-3">
                                                                <label class="control-label">Product Price</label>
                                                                <input type="number" step="any" name="price"
                                                                       value="<?php
                                                                       if (set_value('price') && !isset($success)) {
                                                                           echo set_value('price');
                                                                       }
                                                                       ?>" placeholder="Enter Product Price"
                                                                       id="productPrice"
                                                                       class="form-control <?php if (form_error('price')) { ?> is-invalid <?php } ?>">
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('price')) {
                                                                        echo form_error('price');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="form-group mb-5">
                                                                <label class="control-label">
                                                                    <input type="checkbox"
                                                                           name="featured"
                                                                           value="1"
                                                                           class="zoom-14 vertical-middle"/> Set to
                                                                    Featured Product
                                                                </label>
                                                            </div>
                                                            <div class="form-group mb-5">
                                                                <label class="control-label">
                                                                    <input type="checkbox"
                                                                           name="customize"
                                                                           value="1"
                                                                           class="zoom-14 vertical-middle"/> Set to
                                                                    Customize Product
                                                                </label>
                                                            </div>
                                                            <div class="form-group mb-10">
                                                                <label class="control-label">
                                                                    <input type="checkbox"
                                                                           name="sale"
                                                                           value="1"
                                                                           onchange="($(this).is(':checked')) ? ($('#productSalePrice').show()) : ($('#productSalePrice').hide()) "
                                                                           class="zoom-14 vertical-middle"/> Set to
                                                                    Sale Product
                                                                </label>
                                                            </div>
                                                            <div class="form-group" id="productSalePrice"
                                                                 style="display: none">
                                                                <label class="control-label">Product Sale Price</label>
                                                                <input type="text" name="sale_price" value="<?php
                                                                if (set_value('sale_price') && !isset($success)) {
                                                                    echo set_value('sale_price');
                                                                }
                                                                ?>" placeholder="Enter Product Sale Price"
                                                                       class="form-control <?php if (form_error('sale_price')) { ?> is-invalid <?php } ?>">
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('sale_price')) {
                                                                        echo form_error('sale_price');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <footer class="panel-footer">
                                                        <button type="submit"
                                                                class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info"
                                                                value="send" name="add">Add Product
                                                        </button>
                                                        <button
                                                            class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light"
                                                            type="reset">Reset Form
                                                        </button>
                                                    </footer>
                                                </form>
                                                <!-- << Add Form End
                                                ================================================== -->
                                            <?php
                                            else:
                                                $this->load->view('admin/common/access_denied');
                                            endif;
                                        endif;
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <!-- << ADD/EDIT Data END
                            ================================================== -->
                        <?php
                        else:
                            if ($permission['all'] || $permission['read']):  // If user has read/show data permission
                                ?>
                                <!-- >> Table Data Start
                                ================================================== -->
                                <div class="col-md-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table mb-none table-hover" id="productTable">
                                                    <thead>
                                                    <tr>
                                                        <th>Status</th>
                                                        <th>Category</th>
                                                        <th>Title</th>
                                                        <th>Price</th>
                                                        <th class="none">Sale Price</th>
                                                        <th class="none">Entry Date</th>
                                                        <th>Add Photos</th>
                                                        <?php
                                                        if ($permission['all'] || $permission['status']):
                                                            echo '<th>Action</th>';
                                                        endif;
                                                        if ($permission['all'] || $permission['edit']):
                                                            echo '<th>Edit</th>';
                                                        endif;
                                                        if ($permission['all'] || $permission['delete']):
                                                            echo '<th>Delete</th>';
                                                        endif;
                                                        ?>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- << Table Data End
                                ================================================== -->
                            <?php
                            else:
                                $this->load->view('admin/common/access_denied');
                            endif;
                        endif;
                        ?>
                    </div>
                </div>
            </section>
        </div>
        <!-- << Main Content End
        ================================================== -->
        <?php echo $page_footer;  //  Load Footer  ?>
    </div>
</div>
<?php
$alert_data['success'] = $success;
$alert_data['error'] = $error;
$this->load->view('admin/common/alert', $alert_data);  // Load Notification Alert Message & Footer script
?>
<script>
    // List column which will be display in the table
    const column = [
        {data: 'status'},
        {data: 'category'},
        {data: 'title'},
        {data: 'price'},
        {data: 'sale_price'},
        {data: 'datetime'},
        {data: 'addPhotos'},
        <?php
        if ($permission['all'] || $permission['status']):
            echo '{data: "action"},';
        endif;
        if ($permission['all'] || $permission['edit']):
            echo '{data: "edit"},';
        endif;
        if ($permission['all'] || $permission['delete']):
            echo '{data: "delete"},';
        endif;
        ?>
    ];
    // Parameter --> [TABLE_NAME, GET_DATA_URL, DISPLAY_COLUMN, PHOTO_COLUMN_NAME_FOR_DELETE]
    getDataTable('<?php echo $active_page; ?>', '<?php echo base_url($current_page . '/getdata') ?>', column, 'photos', true);
</script>
</body>  