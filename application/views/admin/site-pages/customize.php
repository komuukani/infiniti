<?php echo $page_head; //  Load Head Link and Scripts       ?>
<body>
<div id="app">
    <div class="main-wrapper">
        <div class="navbar-bg"></div>
        <?php echo $page_header; //  Load Header  ?>
        <?php echo $page_sidebar; //  Load Sidebar  ?>
        <!-- >> Main Content Start
        ================================================== -->
        <div class="main-content">
            <section class="section">
                <?php echo $page_breadcrumb; // Load Breadcrumb ?>
                <?php
                if ($permission['all'] || $permission['read']):  // If user has read/show data permission
                    ?>
                    <div class="section-body">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped mb-none" id="customizeTable">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th class="none">Country</th>
                                            <th class="none">City</th>
                                            <th>Product</th>
                                            <th>Photo</th>
                                            <th class="none">Armchair</th>
                                            <th class="none">2 Seater</th>
                                            <th class="none">3 Seater</th>
                                            <th class="none">4 Seater</th>
                                            <th class="none">5 Seater</th>
                                            <th class="none">Corner Sofa</th>
                                            <th>Textile Type</th>
                                            <th>Textile Colours</th>
                                            <th>Foam Density</th>
                                            <th>Cost</th>
                                            <th>Request Date</th>
                                            <?php
                                            if ($permission['all'] || $permission['delete']):
                                                echo '<th>Delete</th>';
                                            endif;
                                            ?>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                else:
                    $this->load->view('admin/common/access_denied');
                endif;
                ?>
            </section>
        </div>
        <!-- >> Main Content Start
        ================================================== -->
        <?php echo $page_footer;  //  Load Footer   ?>
    </div>
</div>
<?php echo $page_footerscript;  //  Load Footer script  ?>
<script>
    // List column which will be display in the table
    const column = [
        {data: 'fname'},
        {data: 'email'},
        {data: 'phone'},
        {data: 'country'},
        {data: 'city'},
        {data: 'product'},
        {data: 'photo'},
        {data: 'armchair'},
        {data: 'seater_2'},
        {data: 'seater_3'},
        {data: 'seater_4'},
        {data: 'seater_5'},
        {data: 'corner_sofa_length'},
        {data: 'textile_type'},
        {data: 'textile_colour'},
        {data: 'foam_density'},
        {data: 'cost'},
        {data: 'entry_date'},
        <?php
        if ($permission['all'] || $permission['delete']):
            echo '{data: "delete"},';
        endif;
        ?>
    ];
    // Parameter --> [TABLE_NAME, GET_DATA_URL, DISPLAY_COLUMN, PHOTO_COLUMN_NAME_FOR_DELETE]
    getDataTable('<?php echo $active_page; ?>', '<?php echo base_url($current_page . '/getdata') ?>', column, 'photo', true);
</script>
</body> 
