<?php
echo $page_footerscript;  //  Load Footer script  
if (isset($success)) {
    ?>
    <script>
        $(document).ready(() => {
            iziToast.success({
                title: 'Success',
                message: '<?php echo $success; ?>',
                position: 'topRight'
            });
        });
    </script>  
    <?php
}
if (isset($error)) {
    ?>
    <script>
        $(document).ready(() => {
            iziToast.error({
                title: 'Oops!',
                message: '<?php echo $error; ?>',
                position: 'topRight'
            });
        });
    </script>  
    <?php
}
$this->session->unset_userdata('success');
$this->session->unset_userdata('error');
?>