<?php
echo $page_head;
?>
<body>

<?php echo $page_header; ?>
<main id="content" class="wrapper layout-page">

    <?php echo $page_breadcumb; ?>

    <section class="pb-14">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php
                    if (empty($aboutus)) :
                        echo "Sorry, content not available";
                    else :
                        foreach ($aboutus as $key => $about_data) {
                            echo $about_data->about;
                        }
                    endif;
                    ?>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </section>

</main>
<?php echo $page_footer; ?>

<?php echo $page_footerscript; ?>
</body>