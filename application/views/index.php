<?php
echo $page_head;
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
$blogs = $this->md->select_limit_order('tbl_blog', 3, 'blog_id', 'desc');
$products = $this->md->select_limit_order('tbl_product', 12, 'product_id', 'desc', array('featured' => 1, 'sale' => 0, 'status' => 1));
$saleProducts = $this->md->select_limit_order('tbl_product', 12, 'product_id', 'desc', array('sale' => 1, 'status' => 1));
$category = $this->md->select_limit_order('tbl_category', 100, 'position', 'asc');
$reviews = $this->md->select('tbl_review');
$web_data = ($web_data) ? $web_data[0] : '';
$active_page = $this->uri->segment(1) ? $this->uri->segment(1) : 'index';
?>
<body class="home" style="top: 0 !important;">
<div class="page-wrapper">
    <?php echo $page_header; ?>
    <main class="main">
        <div class="page-content">
            <?php $this->load->view('slider'); ?>


            <section class="container pt-3 pb-5 appear-animate categorySection">
                <div class="owl-carousel owl-theme homeCategorySection">
                    <?php
                    if (empty($category)) {
                        echo '<li><div class="alert alert-warning col-md-12">Sorry, Category not available!</div></li>';
                    } else {
                        foreach ($category as $category_data) {
                            $englishTitle = $category_data->title;
                            $arabicTitle = $category_data->title_arabic;
                            if ($category_data->title == '2 Seater' || $category_data->title == '3 Seater' || $category_data->title == '4 Seater') {
                                $englishTitle = $category_data->title . " Sofa";
                                $arabicTitle = $category_data->title_arabic;
                            }
                            ?>
                            <div class="category category-circle overlay-zoom appear-animate" data-animation-options="{
                            'name': 'fadeInLeftShorter',
                            'delay': '.3s'
                        }">
                                <a href="<?php echo base_url('collection/' . $category_data->slug); ?>">
                                    <div>
                                        <img
                                            src="<?php echo base_url($category_data->path ? $category_data->path : FILENOTFOUND); ?>"
                                            title="<?php echo $category_data->title; ?>"
                                            alt="<?php echo $category_data->title; ?>"
                                            style="width: 100% !important;"
                                        />
                                        <h6 class="englishText"><?php echo $englishTitle; ?></h6>
                                        <h6 style="direction: rtl"
                                            class="arabicText"><?php echo $arabicTitle; ?></h6>
                                        <span><?php echo lang('collection'); ?></span>
                                    </div>
                                </a>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="category category-circle overlay-zoom appear-animate" data-animation-options="{
                            'name': 'fadeInLeftShorter',
                            'delay': '.3s'
                        }">
                            <a href="<?php echo base_url('customize'); ?>">
                                <div>
                                    <img
                                        src="<?php echo base_url('assets/images/customized.png'); ?>"
                                        title="customize"
                                        alt="customize"
                                        style="width: 100%;"
                                    />
                                    <h6><?php echo lang('customize'); ?></h6>
                                    <span><?php echo lang('your_own'); ?></span>
                                </div>
                            </a>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </section>


            <section class="product-wrapper container mt-2 pt-2 appear-animate" id="featuredProductSection">
                <h2 class="title title-simple  font-weight-bold textFrameAnimation">
                    <?php echo lang('featured_collection'); ?>
                </h2>
                <div class="owl-theme row cols-lg-4 cols-md-3 cols-2">
                    <?php
                    if ($products) {
                        foreach ($products as $product) {
                            //$category = $this->md->select_where('tbl_category', array('category_id' => $product->category_id));
                            $attributes = $this->md->select_where('tbl_product_attribute', array('product_id' => $product->product_id));

                            $photos = array(FILENOTFOUND);
                            if ($attributes) {
                                $photos = $attributes[0]->product_photos ? explode(",", $attributes[0]->product_photos) : array(FILENOTFOUND);
                            }

                            $url = base_url('product/' . $product->slug . "/" . $product->product_id);
                            ?>
                            <div class="product mb-20">
                                <?php
                                if ($product->sale) {
                                    echo '<span class="saleBadge">' . lang('sale') . '</span>';
                                }
                                ?>
                                <figure
                                    class="product-media">
                                    <a href="<?php echo $url; ?>">
                                        <img
                                            src="<?php echo base_url($photos ? ($photos[0] ? $photos[0] : FILENOTFOUND) : FILENOTFOUND); ?>"
                                            alt="<?php echo $product->title; ?>"
                                            title="<?php echo $product->title; ?>"
                                            class="imgPreview"
                                            style="width: 100%;height: 330px;object-fit: cover"
                                        >
                                        <?php
                                        if (array_key_exists(1, $photos)) {
                                            ?>
                                            <img
                                                src="<?php echo base_url($photos ? ($photos[1] ? $photos[1] : FILENOTFOUND) : FILENOTFOUND); ?>"
                                                alt="<?php echo $product->title; ?>"
                                                title="<?php echo $product->title; ?>"
                                                style="width: 100%;height: 330px;object-fit: cover"
                                            >
                                            <?php
                                        }
                                        ?>
                                    </a>
                                    <div class="customShoppingCart">
                                        <form method="post" action="<?php echo base_url('Pages/addToCart'); ?>"
                                              name="addToCartForm">
                                            <?php
                                            $wh['product_id'] = $product->product_id;
                                            $wh['product_attribute_id'] = $attributes ? $attributes[0]->product_attribute_id : '';
                                            $wh['category_id'] = $product->category_id;
                                            $wh['unique_id'] = $this->input->cookie('unique_id');
                                            $exist = $this->md->select_where('tbl_cart', $wh);
                                            ?>
                                            <input type="hidden" name="quantity"
                                                   value="<?php echo $exist ? ($exist[0]->qty + 1) : 1; ?>"/>
                                            <input type="hidden" name="productId"
                                                   value="<?php echo $product->product_id; ?>"/>
                                            <input type="hidden" name="productAttributeId"
                                                   value="<?php echo $attributes ? $attributes[0]->product_attribute_id : ''; ?>"/>
                                            <input type="hidden" name="categoryId"
                                                   value="<?php echo $product->category_id; ?>"/>
                                            <input type="hidden" name="price"
                                                   value="<?php echo $product->sale ? $product->sale_price : $product->price; ?>"/>
                                            <button name="addToCart" value="addToCart" type="submit" class="cartIcon">
                                                <svg width="24" height="24" xmlns="http://www.w3.org/2000/svg"
                                                     fill-rule="evenodd" clip-rule="evenodd">
                                                    <path
                                                        d="M13.5 18c-.828 0-1.5.672-1.5 1.5 0 .829.672 1.5 1.5 1.5s1.5-.671 1.5-1.5c0-.828-.672-1.5-1.5-1.5zm-3.5 1.5c0 .829-.672 1.5-1.5 1.5s-1.5-.671-1.5-1.5c0-.828.672-1.5 1.5-1.5s1.5.672 1.5 1.5zm14-16.5l-.743 2h-1.929l-3.473 12h-13.239l-4.616-11h2.169l3.776 9h10.428l3.432-12h4.195zm-12 4h3v2h-3v3h-2v-3h-3v-2h3v-3h2v3z"/>
                                                </svg>
                                            </button>
                                        </form>
                                    </div>
                                </figure>
                                <div class="product-details  p-10 pl-0">
                                    <h3 class="englishText product-name font-16 font-weight-700 text-000">
                                        <a href="<?php echo $url; ?>"><?php echo $product->title; ?></a>
                                    </h3>
                                    <h3
                                        class="arabicText product-name font-16 font-weight-700 text-000">
                                        <a href="<?php echo $url; ?>"><?php echo $product->arabic_title; ?></a>
                                    </h3>
                                    <div class="product-price">
                                        <?php
                                        if ($product->sale) {
                                            ?>
                                            <del
                                                class="price d-xs-block text-999 font-weight-normal"><?php echo number_format($product->price); ?>
                                                SAR
                                            </del>
                                            <span
                                                class="ml-lg-2 ml-md-2 ml-xs-0 price font-weight-normal"><?php echo $product->sale_price ? number_format($product->sale_price) : $product->price; ?> SAR</span>
                                            <?php
                                        } else {
                                            ?>
                                            <span
                                                class="price font-weight-normal"><?php echo number_format($product->price); ?> SAR</span>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <div class="product-variations justify-content-start">
                                        <?php
                                        $allattributes = $this->md->my_query('select * from `tbl_product_attribute` where `product_id` =' . $product->product_id)->result();
                                        if ($allattributes) {
                                            foreach ($allattributes as $key => $attribute) {
                                                $photos = $attribute->product_photos ? explode(",", $attribute->product_photos) : array(FILENOTFOUND);
                                                $colour = $this->md->select_where('tbl_colours', array('colour' => $attribute->color));
                                                ?>
                                                <a class="color <?php echo $key == 0 ? 'active' : ''; ?> mr-1 <?php echo $key >= 6 ? 'hiddenColor' : ''; ?>"
                                                   data-src="<?php echo base_url($photos[0]); ?>"
                                                   href="javascript:void(0)"
                                                   style="
                                                       background-color: <?php echo $colour ? $colour[0]->code : '#fff'; ?>;
                                                   <?php echo $attribute->color == 'white' ? 'border:1px solid #dfdfdf' : ''; ?>;
                                                   <?php echo $key >= 6 ? 'display:none' : 'display:block'; ?>
                                                       "
                                                ></a>
                                                <?php
                                            }
                                            if (count($allattributes) >= 6) {
                                                ?>
                                                <img src="<?php echo base_url('assets/images/plus.png'); ?>"
                                                     width="15px" alt="plus" class="ml-1 mr-1 showHiddenColor"/>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    } else {
                        echo '<center>';
                        echo "<img src='" . base_url('assets/images/no-product.jpg') . "' style='width:40%' />";
                        echo '</center>';
                    }
                    ?>
                </div>
            </section>

            <section class="product-wrapper container mt-2 pt-2 appear-animate">
                <h2 class="title title-simple font-weight-bold">
                    <?php echo lang('promo_of_the_month'); ?>
                </h2>
                <div class="owl-theme row cols-lg-4 cols-md-3 cols-2">
                    <?php
                    if ($saleProducts) {
                        foreach ($saleProducts as $product) {
                            //$category = $this->md->select_where('tbl_category', array('category_id' => $product->category_id));
                            $attributes = $this->md->select_where('tbl_product_attribute', array('product_id' => $product->product_id));

                            $photos = array(FILENOTFOUND);
                            if ($attributes) {
                                $photos = $attributes[0]->product_photos ? explode(",", $attributes[0]->product_photos) : array(FILENOTFOUND);
                            }

                            $url = base_url('product/' . $product->slug . "/" . $product->product_id);
                            ?>
                            <div class="product mb-20">
                                <span class="saleBadge"><?php echo lang('sale'); ?></span>
                                <figure
                                    class="product-media">
                                    <a href="<?php echo $url; ?>">
                                        <img
                                            src="<?php echo base_url($photos ? ($photos[0] ? $photos[0] : FILENOTFOUND) : FILENOTFOUND); ?>"
                                            alt="<?php echo $product->title; ?>"
                                            title="<?php echo $product->title; ?>"
                                            class="imgPreview"
                                            style="width: 100%;height: 330px;object-fit: cover"
                                        >
                                        <?php
                                        if (array_key_exists(1, $photos)) {
                                            ?>
                                            <img
                                                src="<?php echo base_url($photos ? ($photos[1] ? $photos[1] : FILENOTFOUND) : FILENOTFOUND); ?>"
                                                alt="<?php echo $product->title; ?>"
                                                title="<?php echo $product->title; ?>"
                                                style="width: 100%;height: 330px;object-fit: cover"
                                            >
                                            <?php
                                        }
                                        ?>
                                    </a>
                                    <div class="customShoppingCart">
                                        <form method="post" action="<?php echo base_url('Pages/addToCart'); ?>"
                                              name="addToCartForm">
                                            <?php
                                            $wh['product_id'] = $product->product_id;
                                            $wh['product_attribute_id'] = $attributes ? $attributes[0]->product_attribute_id : '';
                                            $wh['category_id'] = $product->category_id;
                                            $wh['unique_id'] = $this->input->cookie('unique_id');
                                            $exist = $this->md->select_where('tbl_cart', $wh);
                                            ?>
                                            <input type="hidden" name="quantity"
                                                   value="<?php echo $exist ? ($exist[0]->qty + 1) : 1; ?>"/>
                                            <input type="hidden" name="productId"
                                                   value="<?php echo $product->product_id; ?>"/>
                                            <input type="hidden" name="productAttributeId"
                                                   value="<?php echo $attributes ? $attributes[0]->product_attribute_id : ''; ?>"/>
                                            <input type="hidden" name="categoryId"
                                                   value="<?php echo $product->category_id; ?>"/>
                                            <input type="hidden" name="price"
                                                   value="<?php echo $product->sale ? $product->sale_price : $product->price; ?>"/>
                                            <button name="addToCart" value="addToCart" type="submit" class="cartIcon">
                                                <svg width="24" height="24" xmlns="http://www.w3.org/2000/svg"
                                                     fill-rule="evenodd" clip-rule="evenodd">
                                                    <path
                                                        d="M13.5 18c-.828 0-1.5.672-1.5 1.5 0 .829.672 1.5 1.5 1.5s1.5-.671 1.5-1.5c0-.828-.672-1.5-1.5-1.5zm-3.5 1.5c0 .829-.672 1.5-1.5 1.5s-1.5-.671-1.5-1.5c0-.828.672-1.5 1.5-1.5s1.5.672 1.5 1.5zm14-16.5l-.743 2h-1.929l-3.473 12h-13.239l-4.616-11h2.169l3.776 9h10.428l3.432-12h4.195zm-12 4h3v2h-3v3h-2v-3h-3v-2h3v-3h2v3z"/>
                                                </svg>
                                            </button>
                                        </form>
                                    </div>
                                </figure>
                                <div class="product-details  p-10 pl-0">
                                    <h3 class="englishText product-name font-16 font-weight-600 text-000">
                                        <a href="<?php echo $url; ?>"><?php echo $product->title; ?></a>
                                    </h3>
                                    <h3
                                        class="arabicText product-name font-16 font-weight-700 text-000">
                                        <a href="<?php echo $url; ?>"><?php echo $product->arabic_title; ?></a>
                                    </h3>
                                    <div class="product-price">
                                        <?php
                                        if ($product->sale) {
                                            ?>
                                            <del
                                                class="price d-xs-block text-999 font-weight-normal"><?php echo number_format($product->price); ?>
                                                SAR
                                            </del>
                                            <span
                                                class="ml-lg-2 ml-md-2 ml-xs-0 price font-weight-normal"><?php echo $product->sale_price ? number_format($product->sale_price) : $product->price; ?> SAR</span>
                                            <?php
                                        } else {
                                            ?>
                                            <span
                                                class="price font-weight-normal"><?php echo number_format($product->price); ?> SAR</span>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <div class="product-variations justify-content-start">
                                        <?php
                                        $allattributes = $this->md->my_query('select * from `tbl_product_attribute` where `product_id` =' . $product->product_id)->result();
                                        if ($allattributes) {
                                            foreach ($allattributes as $key => $attribute) {
                                                $photos = $attribute->product_photos ? explode(",", $attribute->product_photos) : array(FILENOTFOUND);
                                                $colour = $this->md->select_where('tbl_colours', array('colour' => $attribute->color));
                                                ?>
                                                <a class="color <?php echo $key == 0 ? 'active' : ''; ?> mr-1 <?php echo $key >= 6 ? 'hiddenColor' : ''; ?>"
                                                   data-src="<?php echo base_url($photos[0]); ?>"
                                                   href="javascript:void(0)"
                                                   style="
                                                       background-color: <?php echo $colour ? $colour[0]->code : '#fff'; ?>;
                                                   <?php echo $attribute->color == 'white' ? 'border:1px solid #dfdfdf' : ''; ?>;
                                                   <?php echo $key >= 6 ? 'display:none' : 'display:block'; ?>
                                                       "
                                                ></a>
                                                <?php
                                            }
                                            if (count($allattributes) >= 6) {
                                                ?>
                                                <img src="<?php echo base_url('assets/images/plus.png'); ?>"
                                                     width="15px" alt="plus" class="ml-1 mr-1 showHiddenColor"/>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    } else {
                        echo '<center>';
                        echo "<img src='" . base_url('assets/images/no-product.jpg') . "' style='width:40%' />";
                        echo '</center>';
                    }
                    ?>
                </div>
            </section>

            <section class="product-wrapper container mt-2 pt-2 appear-animate">
                <h2 class="title title-simple  font-weight-bold"><?php echo lang('all_products'); ?></h2>
                <div class="owl-theme row cols-lg-4 cols-md-3 cols-2" id="productList">

                </div>
                <div align="center" class="mb-50">
                    <button type="button"
                            id="loadMore"
                            style="border-radius: 30px;text-transform: none;padding-left: 30px;padding-right: 30px;"
                            class="btn-product pt-15 pb-15 cursor-pointer text-000 border-none font-weight-normal letter-spacing-1 font-16">
                        <?php echo lang('load_more'); ?>
                    </button>
                </div>
            </section>

        </div>
    </main>
    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
<style>
    .categorySection .owl-theme .owl-nav .owl-prev {
        left: -20px;
        zoom: 0.7;
        background-color: #d9c12f;
    }

    .categorySection .owl-theme .owl-nav .owl-next {
        right: -20px;
        zoom: 0.7;
        background-color: #d9c12f;
    }

    .categorySection .owl-carousel {
        display: flex !important; /* To override display:block I added !important */
        justify-content: center; /* To center the carousel */
    }

    body {
        top: 0 !important;
    }

</style>
</body>