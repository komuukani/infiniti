<?php
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
$category = $this->md->select_limit_order('tbl_category', 100, 'position', 'asc');
$web_data = ($web_data) ? $web_data[0] : '';
$cartProduct = $this->md->select_where('tbl_cart', array('unique_id' => $this->input->cookie('unique_id')));
?>
<a id="scroll-top" href="#top" title="Top" role="button" class="scroll-top"><i class="d-icon-arrow-up"></i></a>
<div class="mobile-menu-wrapper">
    <div class="mobile-menu-overlay">
    </div>

    <a class="mobile-menu-close" href="javascript:void(0)"><i class="d-icon-times"></i></a>

    <div class="mobile-menu-container scrollable position-relative">

        <div class="ml-xs-60">
            <!--            <a href="--><?php //echo base_url(); ?><!--" class="logo">-->
            <!--                <img src="-->
            <?php //echo base_url($web_data ? $web_data->logo : FILENOTFOUND); ?><!--"-->
            <!--                     alt="--><?php //echo $web_data ? $web_data->project_name : ''; ?><!--"-->
            <!--                     width="120"-->
            <!--                     height="44"-->
            <!--                     title="--><?php //echo $web_data ? $web_data->project_name : ''; ?><!--"/>-->
            <!--            </a>-->
        </div>

        <ul class="mobile-menu mmenu-anim">
            <li class="active">
                <a href="<?php echo base_url(); ?>">
                    <!--                    <div>-->
                    <!--                        <img-->
                    <!--                            src="-->
                    <?php //echo base_url('assets/images/home.png'); ?><!--"-->
                    <!--                            alt="home"-->
                    <!--                            style="width: 60px;height: 60px;object-fit: contain;"-->
                    <!--                        />-->
                    <!--                    </div>-->
                    <?php echo lang('home'); ?>
                </a>
            </li>
            <?php
            if (empty($category)) {
                echo '<li><div class="alert alert-warning col-md-12">Sorry, Category not available!</div></li>';
            } else {
                foreach ($category as $category_data) {
                    ?>
                    <li class="englishText">
                        <a href="<?php echo base_url('collection/' . $category_data->slug); ?>">
                            <!--                                            <div>-->
                            <!--                                                <img-->
                            <!--                                                    src="-->
                            <?php //echo base_url($category_data->path ? $category_data->path : FILENOTFOUND); ?><!--"-->
                            <!--                                                    title="-->
                            <?php //echo $category_data->title; ?><!--"-->
                            <!--                                                    alt="-->
                            <?php //echo $category_data->title; ?><!--"-->
                            <!--                                                    style="width: 60px;height: 60px;object-fit: contain;"-->
                            <!--                                                />-->
                            <!--                                            </div>-->
                            <?php echo $category_data->title; ?>
                        </a>
                    </li>
                    <li class="arabicText notranslate" translate="no" style="direction: rtl">
                        <a href="<?php echo base_url('collection/' . $category_data->slug); ?>">
                            <?php echo $category_data->title_arabic; ?>
                        </a>
                    </li>
                    <?php
                }
                ?>
                <li>
                    <a href="<?php echo base_url('customize'); ?>">
                        <!--                                    <div>-->
                        <!--                                        <img-->
                        <!--                                            src="-->
                        <?php //echo base_url('assets/images/home.png'); ?><!--"-->
                        <!--                                            alt="home"-->
                        <!--                                            style="width: 60px;height: 60px;object-fit: contain;"-->
                        <!--                                        />-->
                        <!--                                    </div>-->
                        <?php echo lang('customize'); ?>
                    </a>
                </li>
                <?php
            }
            ?>
        </ul>

        <div class="mt-40" style="position: absolute;bottom: 0">
            <div class="social-links mb-xs-10">
                <a href="<?php echo $web_data ? $web_data->twitter : ''; ?>" title="social-link"
                   class="social-link social-twitter"><i class="fab fa-twitter"></i></a>
                <a href="<?php echo $web_data ? $web_data->instagram : ''; ?>" title="social-link"
                   class="social-link social-instagram"><i class="fab fa-instagram"></i></a>
                <a href="https://tiktok.com/@infiniti_arabia" title="social-link"
                   class="social-link">
                    <svg aria-hidden="true" focusable="false" class="icon icon-tiktok" width="16" height="18"
                         fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M8.02 0H11s-.17 3.82 4.13 4.1v2.95s-2.3.14-4.13-1.26l.03 6.1a5.52 5.52 0 11-5.51-5.52h.77V9.4a2.5 2.5 0 101.76 2.4L8.02 0z"
                            fill="currentColor">
                        </path>
                    </svg>
                </a>
                <a href="<?php echo $web_data ? $web_data->instagram : ''; ?>" title="social-link"
                   class="social-link social-instagram"><i class="fab fa-snapchat-ghost"></i></a>
            </div>
        </div>

    </div>
</div>

<!--Search Modal-->
<div class="searchModal" style="display: none"
     onclick="$(this).hide();$('.searchModalBody').hide();$('body').css('overflow-y','auto')">
</div>
<div class="searchModalBody" style="display: none">
    <div class="">
        <div class="row justify-content-center">
            <div class="col-md-6 col-10">
                <form method="post">
                    <div class="position-relative">
                        <input type="text" placeholder="<?php echo lang('header_search_placeholder'); ?>" name="search"
                               class="form-control font-18"/>
                        <div class="position-absolute searchBtn">
                            <i class="fa fa-search font-18"></i>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-1 col-2">
                <div class="mt-2">
                    <i class="fa fa-times font-26 cursor-pointer"
                       onclick="$('.searchModal').toggle();$('.searchModalBody').hide();$('body').css('overflow-y','auto')"></i>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Item has been removed from cart.-->
<!--Cart Modal-->
<div class="cartModal"
     style="display: <?php echo ($success == 'Product updated in cart.' || $success == 'Product added in cart.') ? 'block' : 'none'; ?>"
     onclick="cartModalOpen1(this);">
</div>
<div class="cartModalBody"
     style="<?php
     echo ($success == 'Product updated in cart.' || $success == 'Product added in cart.') ? 'display: block;transform:translateX(0px)' : 'display: none;transform:translateX(350px)';
     ?>">
    <div
        class="cart-dropdown <?php echo ($success == 'Product updated in cart.' || $success == 'Product added in cart.') ? 'opened' : ''; ?> off-canvas">
        <div class="dropdown-box">
            <div class="canvas-header">
                <?php
                if ($cartProduct) {
                    ?>
                    <h4
                        class="canvas-title text-transform-capitalize font-weight-bold font-24"><?php echo lang('your_cart'); ?></h4>
                    <?php
                }
                ?>
                <a href="javascript:void(0)" class="btn btn-dark btn-link btn-icon-right btn-close"
                   onclick="closeCartModal();"><i
                        class="d-icon-close"></i><span class="sr-only"><?php echo lang('cart'); ?></span></a>
            </div>
            <?php
            if ($cartProduct) {
                ?>
                <div class="d-flex justify-content-between mt-20">
                    <span
                        class="font-10 text-transform-uppercase text-000 letter-spacing-2"><?php echo lang('product'); ?></span>
                    <span
                        class="font-10 text-transform-uppercase text-000 letter-spacing-2"><?php echo lang('total'); ?></span>
                </div>
                <?php
            }
            ?>
            <div class="products scrollable"
                 style="<?php echo (!$cartProduct) ? 'height: 100% !important; max-height: 100% !important; display: flex; align-items: center; flex-direction: column; justify-content: center;' : ''; ?>">
                <?php
                $total = 0;
                if ($cartProduct) {
                    foreach ($cartProduct as $cartitem) {
                        $productData = $this->md->select_where('tbl_product', array('product_id' => $cartitem->product_id));
                        if ($productData) {
                            $product = $productData[0];
                            $attributes = $this->md->select_where('tbl_product_attribute', array('product_attribute_id' => $cartitem->product_attribute_id));

                            $photos = array(FILENOTFOUND);
                            if ($attributes) {
                                $photos = $attributes[0]->product_photos ? explode(",", $attributes[0]->product_photos) : array(FILENOTFOUND);
                            }

                            $url = base_url('product/' . $product->slug . "/" . $product->product_id . "/?color=" . $attributes[0]->color);
                            $total = $total + $cartitem->netprice; // Net total
                            ?>
                            <div class="product product-cart">
                                <figure class="product-media">
                                    <a href="<?php echo $url ?>">
                                        <img
                                            src="<?php echo base_url($photos ? ($photos[0] ? $photos[0] : FILENOTFOUND) : FILENOTFOUND); ?>"
                                            alt="<?php echo $product->title; ?>"
                                            title="<?php echo $product->title; ?>"
                                            style="width: 80px;height: 88px;object-fit: contain"
                                        >
                                    </a>
                                </figure>
                                <div class="product-detail">
                                    <a href="<?php echo $url; ?>"
                                       class="product-name englishText font-weight-bold font-16 text-000 mb-0 letter-spacing-1"><?php echo $productData ? $productData[0]->title : ''; ?>
                                    </a>
                                    <a href="<?php echo $url; ?>"
                                       class="product-name arabicText font-weight-bold font-16 text-000 mb-0 letter-spacing-1"><?php echo $productData ? $productData[0]->arabic_title : ''; ?>
                                    </a>
                                    <div class="price-box">
                                        <!--                                                    <span class="product-quantity">-->
                                        <?php //echo $cartitem->qty; ?><!--</span>-->
                                        <span
                                            translate="no"
                                            class="product-price notranslate text-000 font-14"><?php echo $cartitem->price ? number_format($cartitem->price) : ''; ?> SAR</span>
                                    </div>
                                    <form method="post" action="<?php echo base_url('Pages/updateQty'); ?>"
                                          class="mt-2"
                                          name="updateCartForm">
                                        <input type="hidden" name="cartId"
                                               value="<?php echo $cartitem->cart_id; ?>"/>
                                        <div class="input-group">
                                            <div class="qty-container">
                                                <button class="qty-btn-minus btn-light" type="submit"
                                                        onclick="$(this).parentsUntil('.product-quantity').submit();">
                                                    <i
                                                        class="fa fa-minus"></i></button>
                                                <input type="text" name="quantity"
                                                       value="<?php echo $cartitem->qty; ?>"
                                                       onchange="$(this).parentsUntil('.product-quantity').submit();"
                                                       class="input-qty"/>
                                                <button class="qty-btn-plus btn-light" type="submit"
                                                        onclick="$(this).parentsUntil('.product-quantity').submit();">
                                                    <i
                                                        class="fa fa-plus"></i></button>
                                            </div>
                                        </div>
                                        <a href="<?php echo base_url('Pages/removeCartItem/' . $cartitem->cart_id); ?>"
                                           class="btn btn-link btn-close"
                                           style="display: flex;height: 2.2rem;padding: 0;border: none">
                                            <i class="fas fa-trash-alt font-16"></i>
                                        </a>
                                    </form>
                                    <span
                                        translate="no"
                                        class="modalPrice product-price notranslate text-000 font-16"><?php echo $cartitem->netprice ? number_format($cartitem->netprice) : ''; ?> <br/>SAR</span>
                                </div>
                            </div>
                            <?php
                        }
                    }
                } else {
                    echo "<h4 class='text-center font-weight-bold'>" . lang('empty_cart') . "</h4>";
                    echo '<a href="' . base_url('checkout') . '" class="checkoutBtn2 text-center d-block" style="width: 75%">' . lang("continue_shopping") . '</a>';
                }
                ?>
            </div>
            <?php
            if ($cartProduct) {
                ?>
                <div class="cartSidebarFooter">
                    <div class="cart-total mb-0 border-bottom-none font-16" style="border-top: 1px solid #ddd">
                        <label class="font-weight-bold letter-spacing-1"><?php echo lang('subtotal'); ?></label>
                        <span
                            class="price font-20"><?php echo $total != 0 ? number_format($total) : 0; ?> SAR</span>
                    </div>
                    <div>
                        <p class="mb-1 font-weight-normal text-000 letter-spacing-1 font-14"><?php echo lang('vat_included'); ?></p>
                    </div>

                    <div class="cart-action">
                        <!--                                <a href="-->
                        <?php //echo base_url('cart'); ?><!--" class="btn btn-dark btn-link">View Cart</a>-->
                        <a href="<?php echo base_url('checkout'); ?>"
                           class="checkoutBtn2 text-center d-block"><?php echo lang('checkout'); ?></a>
                    </div>
                </div>
                <?php
            }
            ?>

        </div>
    </div>
</div>
