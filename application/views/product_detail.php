<?php
echo $page_head;
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
if (!$product_data) {
    redirect('404');
}
$web_data = ($web_data) ? $web_data[0] : '';
$category = $this->md->select('tbl_category');
//$photos = explode(",", $product_data[0]->photos);
$pro_id = $product_data[0]->product_id;  // Product ID
$color = $this->input->get('color') ? trim($this->input->get('color', TRUE)) : '';
$attributes = '';
if ($color) {
    $attributes = $this->md->my_query('select * from `tbl_product_attribute` where `product_id` =' . $pro_id . ' AND color = "' . $color . '"')->result();
} else {
    $attributes = $this->md->my_query('select * from `tbl_product_attribute` where `product_id` =' . $pro_id)->result();
}
$allProducts = $this->md->select_limit_order('tbl_product', 10, 'product_id', 'desc', array('status' => 1));
?>
<body>
<link rel="stylesheet" type="text/css" href="vendor/photoswipe/photoswipe.min.css">
<link rel="stylesheet" href="vendor/image-zoom/css/image-zoom.css">
<!--<link rel="stylesheet" href="vendor/image-zoom/css/style.css">-->
<div class="page-wrapper">
    <?php echo $page_header; ?>
    <main class="main single-product mt-150 mt-xs-50">
        <div class="page-content mb-10 pb-6">
            <div class="container">
                <div class="product product-single row mb-4 mt-xs-90">
                    <?php
                    if ($attributes) {
                        $photos = $attributes[0]->product_photos ? explode(",", $attributes[0]->product_photos) : array(FILENOTFOUND);
                        ?>
                        <div class="col-md-6">
                            <div class="product-gallery showPhotos border-radius-16">
                                <div
                                    class="product-single-carousel owl-carousel owl-theme owl-nav-inner">
                                    <?php
                                    foreach ($photos as $photo) {
                                        ?>
                                        <figure class="product-image">
                                            <img src="<?php echo base_url($photo); ?>"
                                                 class="zoomImg"
                                                 data-zoom-image="<?php echo base_url($photo); ?>"
                                                 title="<?php echo $product_data[0]->title; ?>"
                                                 alt="<?php echo $product_data[0]->title; ?>"
                                                 style="border-radius: 20px !important;"/>
                                        </figure>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="product-thumbs-wrap d-xs-none mt-20">
                                    <div class="product-thumbs">
                                        <?php
                                        foreach ($photos as $key => $photo) {
                                            ?>
                                            <div class="product-thumb <?php echo $key == 0 ? 'active' : ''; ?>">
                                                <img src="<?php echo base_url($photo); ?>"
                                                     title="<?php echo $product_data[0]->title; ?>"
                                                     alt="<?php echo $product_data[0]->title; ?>"
                                                     style="width: 150px !important;height: 140px !important;object-fit: cover"/>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="col-md-6">
                        <div class="product-details">
                            <h5 class="text-777 font-16"><?php echo lang('infiniti'); ?></h5>
                            <h1 class="product-name englishText font-32 font-weight-bold"><?php echo $product_data[0]->title; ?></h1>
                            <h1 translate="no"
                                class="product-name arabicText notranslate font-32 font-weight-bold"><?php echo $product_data[0]->arabic_title; ?></h1>
                            <!--                                    <div class="product-meta">-->
                            <!--                                                                                SKU: <span class="product-sku">--><?php //echo $product_data[0]->sku; ?>
                            <!--                                                                                </span>-->
                            <!--                                        CATEGORY: <span-->
                            <!--                                            class="product-brand">-->
                            <?php //echo $this->md->getItemName('tbl_category', 'category_id', 'title', $product_data[0]->category_id); ?><!--</span>-->
                            <!--                                    </div>-->
                            <div class="d-flex" style="align-items:center">
                                        <span
                                            class="product-price font-weight-normal mb-0 font-20 display-inline-block"
                                            style="line-height: normal">
                                            <?php
                                            if ($product_data[0]->sale) {
                                                echo '<del class="font-16 mb-0 mr-2">' . number_format($product_data[0]->price) . "SAR</del> &nbsp; ";
                                                echo(number_format($product_data[0]->sale_price) . " SAR");
                                            } else {
                                                echo(number_format($product_data[0]->price) . " SAR");
                                            }
                                            ?>
                                        </span>
                                <?php
                                if ($product_data[0]->sale) {
                                    echo '<span class="ml-20 mr-20 saleBadge-2">' . lang('sale') . '</span>';
                                }
                                ?>
                            </div>

                            <div class="product-form product-color mt-30 display-block">
                                <?php
                                $allattributes = $this->md->my_query('select * from `tbl_product_attribute` where `product_id` =' . $pro_id)->result();
                                if ($allattributes) {
                                    ?>
                                    <div><span><?php echo lang('colour'); ?>:</span>
                                        <strong><?php echo ucfirst($color ? $color : $allattributes[0]->color); ?></strong>
                                    </div>
                                    <?php
                                }
                                ?>
                                <div class="product-variations">
                                    <?php
                                    $allattributes = $this->md->my_query('select * from `tbl_product_attribute` where `product_id` =' . $pro_id)->result();
                                    if ($allattributes) {
                                        foreach ($allattributes as $attribute) {
                                            $colour = $this->md->select_where('tbl_colours', array('colour' => $attribute->color));
                                            ?>
                                            <a class="color"
                                               href="<?php echo current_url() . '?color=' . $attribute->color; ?>"
                                               style="border:1px solid #000;background-color: <?php echo $colour ? $colour[0]->code : '#fff'; ?>"></a>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>

                            <hr class="product-divider">
                            <form method="post" action="<?php echo base_url('Pages/addToCart'); ?>"
                                  name="addToCartForm">
                                <?php
                                $wh['product_id'] = $product_data[0]->product_id;
                                $wh['product_attribute_id'] = $attributes ? $attributes[0]->product_attribute_id : '';
                                $wh['category_id'] = $product_data[0]->category_id;
                                $wh['unique_id'] = $this->input->cookie('unique_id');
                                $exist = $this->md->select_where('tbl_cart', $wh);
                                ?>
                                <input type="hidden" name="productId"
                                       value="<?php echo $product_data[0]->product_id; ?>"/>
                                <input type="hidden" name="productAttributeId"
                                       value="<?php echo $attributes ? $attributes[0]->product_attribute_id : ''; ?>"/>
                                <input type="hidden" name="categoryId"
                                       value="<?php echo $product_data[0]->category_id; ?>"/>
                                <input type="hidden" name="price"
                                       value="<?php echo $product_data[0]->sale ? $product_data[0]->sale_price : $product_data[0]->price; ?>"/>
                                <div class="product-form product-qty d-block">
                                    <div class="row">
                                        <div class="col-md-2 col-2">
                                            <label class="font-14"><?php echo lang('quantity'); ?>:</label>
                                        </div>
                                        <div class="col-md-3 col-4">
                                            <div class="product-form-group d-block">
                                                <div class="">
                                                    <div class="qty-container">
                                                        <button class="qty-btn-minus btn-light" type="button"><i
                                                                class="fa fa-minus"></i></button>
                                                        <input type="text" name="quantity"
                                                               value="<?php echo $exist ? ($exist[0]->qty) : 1; ?>"
                                                               class="input-qty"/>
                                                        <button class="qty-btn-plus btn-light" type="button"><i
                                                                class="fa fa-plus"></i></button>
                                                    </div>
                                                    <!--                                                    <button type="button" class="quantity-minus d-icon-minus"></button>-->
                                                    <!--                                                    <input class="quantity form-control" type="number" min="1"-->
                                                    <!--                                                           value="-->
                                                    <?php //echo $exist ? $exist[0]->qty : 1; ?><!--"-->
                                                    <!--                                                           name="quantity"-->
                                                    <!--                                                           max="100">-->
                                                    <!--                                                    <button type="button" class="quantity-plus d-icon-plus"></button>-->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-7 col-6">
                                            <button name="addToCart" value="addToCart" type="submit"
                                                    style="width: 100% !important;border-radius: 0px;text-transform: none"
                                                    class="btn-product cursor-pointer text-000 border-none font-weight-normal letter-spacing-1">
                                                <?php echo lang('add_to_cart'); ?>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button name="addToCart" value="addToCart" type="submit"
                                                style="width: 100% !important;border-radius: 0px;text-transform: none"
                                                class="btn-product mt-xs-10 cursor-pointer text-000 border-none font-weight-normal letter-spacing-1">
                                            <?php echo lang('buy_now'); ?>
                                        </button>
                                    </div>
                                </div>
                            </form>

                            <div class="mt-6">
                                <h5 class="font-weight-bold"><?php echo lang('pro_desc'); ?></h5>
                                <div class="englishText description">
                                    <?php echo $product_data[0]->description; ?>
                                </div>
                                <div translate="no" class="arabicText notranslate description">
                                    <?php echo $product_data[0]->arabic_desc; ?>
                                </div>
                            </div>

                            <div>
                                <div class="faq-container">
                                    <div class="faq">
                                        <h3 class="faq-title">
                                            <img
                                                src="<?php echo base_url('assets/images/truck.png'); ?>"
                                                width="22px" class="mr-1 ml-1"
                                                style="vertical-align: top"/> <?php echo lang('delivery_timeline'); ?>
                                        </h3>
                                        <p class="faq-text">
                                            <?php echo lang('delivery_info_1'); ?> <br/>
                                            <?php echo lang('delivery_info_2'); ?>
                                        </p>
                                        <button class="faq-toggle">
                                            <i class="fas fa-chevron-down"></i>
                                            <i class="fas fa-chevron-up"></i>
                                        </button>
                                    </div>
                                    <div class="faq">
                                        <h3 class="faq-title"><img
                                                src="<?php echo base_url('assets/images/star.png'); ?>"
                                                width="22px" class="mr-1 ml-1"
                                                style="vertical-align: top"/><?php echo lang('quality_guarantee'); ?>
                                        </h3>
                                        <p class="faq-text">
                                            <?php echo lang('quality_guarantee_info'); ?>
                                        </p>
                                        <button class="faq-toggle">
                                            <i class="fas fa-chevron-down"></i>
                                            <i class="fas fa-chevron-up"></i>
                                        </button>
                                    </div>
                                    <div class="faq">
                                        <h3 class="faq-title"><img
                                                src="<?php echo base_url('assets/images/heart.png'); ?>"
                                                width="22px" class="mr-1 ml-1"
                                                style="vertical-align: top"/> <?php echo lang('country_of_origin'); ?>
                                        </h3>
                                        <p class="faq-text">
                                            <?php echo lang('country_of_origin_info'); ?>
                                        </p>
                                        <button class="faq-toggle">
                                            <i class="fas fa-chevron-down"></i>
                                            <i class="fas fa-chevron-up"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="mt-30">
                                <a href="javascript:void(0)" data-title="<?php echo $product_data[0]->title; ?>"
                                   data-fullurl="<?php echo current_url(); ?>"
                                   id="share-button"><i
                                        class="fas fa-external-link-alt mr-1 ml-1"></i> <?php echo lang('share'); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <section class="product-wrapper container mt-4 pt-4 pb-4 appear-animate">
                <h2 class="title title-simple  font-weight-bold"><?php echo lang('you_may_also_like'); ?></h2>
                <div class="owl-theme row cols-lg-5 cols-md-4 cols-2">
                    <?php
                    if ($allProducts) {
                        foreach ($allProducts as $product) {
                            //$category = $this->md->select_where('tbl_category', array('category_id' => $product->category_id));
                            $attributes = $this->md->select_where('tbl_product_attribute', array('product_id' => $product->product_id));

                            $photos = array(FILENOTFOUND);
                            if ($attributes) {
                                $photos = $attributes[0]->product_photos ? explode(",", $attributes[0]->product_photos) : array(FILENOTFOUND);
                            }

                            $url = base_url('product/' . $product->slug . "/" . $product->product_id);
                            ?>
                            <div class="product mb-30">
                                <?php
                                if ($product->sale) {
                                    echo '<span class="saleBadge">' . lang('sale') . '</span>';
                                }
                                ?>
                                <figure
                                    class="product-media">
                                    <a href="<?php echo $url; ?>">
                                        <img
                                            src="<?php echo base_url($photos ? ($photos[0] ? $photos[0] : FILENOTFOUND) : FILENOTFOUND); ?>"
                                            alt="<?php echo $product->title; ?>"
                                            title="<?php echo $product->title; ?>"
                                            class="imgPreview"
                                            style="width: 100%;height: 270px;object-fit: cover"
                                        >
                                        <?php
                                        if (array_key_exists(1, $photos)) {
                                            ?>
                                            <img
                                                src="<?php echo base_url($photos ? ($photos[1] ? $photos[1] : FILENOTFOUND) : FILENOTFOUND); ?>"
                                                alt="<?php echo $product->title; ?>"
                                                title="<?php echo $product->title; ?>"
                                                style="width: 100%;height: 270px;object-fit: cover"
                                            >
                                            <?php
                                        }
                                        ?>
                                    </a>
                                    <div class="customShoppingCart">
                                        <form method="post" action="<?php echo base_url('Pages/addToCart'); ?>"
                                              name="addToCartForm">
                                            <?php
                                            $wh['product_id'] = $product->product_id;
                                            $wh['product_attribute_id'] = $attributes ? $attributes[0]->product_attribute_id : '';
                                            $wh['category_id'] = $product->category_id;
                                            $wh['unique_id'] = $this->input->cookie('unique_id');
                                            $exist = $this->md->select_where('tbl_cart', $wh);
                                            ?>
                                            <input type="hidden" name="quantity"
                                                   value="<?php echo $exist ? ($exist[0]->qty + 1) : 1; ?>"/>
                                            <input type="hidden" name="productId"
                                                   value="<?php echo $product->product_id; ?>"/>
                                            <input type="hidden" name="productAttributeId"
                                                   value="<?php echo $attributes ? $attributes[0]->product_attribute_id : ''; ?>"/>
                                            <input type="hidden" name="categoryId"
                                                   value="<?php echo $product->category_id; ?>"/>
                                            <input type="hidden" name="price"
                                                   value="<?php echo $product->sale ? $product->sale_price : $product->price; ?>"/>
                                            <button name="addToCart" value="addToCart" type="submit" class="cartIcon">
                                                <svg width="24" height="24" xmlns="http://www.w3.org/2000/svg"
                                                     fill-rule="evenodd" clip-rule="evenodd">
                                                    <path
                                                        d="M13.5 18c-.828 0-1.5.672-1.5 1.5 0 .829.672 1.5 1.5 1.5s1.5-.671 1.5-1.5c0-.828-.672-1.5-1.5-1.5zm-3.5 1.5c0 .829-.672 1.5-1.5 1.5s-1.5-.671-1.5-1.5c0-.828.672-1.5 1.5-1.5s1.5.672 1.5 1.5zm14-16.5l-.743 2h-1.929l-3.473 12h-13.239l-4.616-11h2.169l3.776 9h10.428l3.432-12h4.195zm-12 4h3v2h-3v3h-2v-3h-3v-2h3v-3h2v3z"/>
                                                </svg>
                                            </button>
                                        </form>
                                    </div>
                                </figure>
                                <div class="product-details  p-10 pl-0">
                                    <h3 class="englishText product-name font-16 font-weight-600 text-000">
                                        <a href="<?php echo $url; ?>"><?php echo $product->title; ?></a>
                                    </h3>
                                    <h3 translate="no"
                                        class="arabicText notranslate product-name font-16 font-weight-700 text-000">
                                        <a href="<?php echo $url; ?>"><?php echo $product->arabic_title; ?></a>
                                    </h3>
                                    <div translate="no" class="product-price notranslate">
                                        <?php
                                        if ($product->sale) {
                                            ?>
                                            <del
                                                class="price d-xs-block text-999 font-weight-normal"><?php echo number_format($product->price); ?>
                                                SAR
                                            </del>
                                            <span
                                                class="ml-lg-2 ml-md-2 ml-xs-0 price font-weight-normal"><?php echo $product->sale_price ? number_format($product->sale_price) : $product->price; ?> SAR</span>
                                            <?php
                                        } else {
                                            ?>
                                            <span
                                                class="price font-weight-normal"><?php echo number_format($product->price); ?> SAR</span>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <div class="product-variations justify-content-start">
                                        <?php
                                        $allattributes = $this->md->my_query('select * from `tbl_product_attribute` where `product_id` =' . $product->product_id)->result();
                                        if ($allattributes) {
                                            foreach ($allattributes as $key => $attribute) {
                                                $photos = $attribute->product_photos ? explode(",", $attribute->product_photos) : array(FILENOTFOUND);
                                                $colour = $this->md->select_where('tbl_colours', array('colour' => $attribute->color));
                                                ?>
                                                <a class="color <?php echo $key == 0 ? 'active' : ''; ?> mr-1 <?php echo $key >= 6 ? 'hiddenColor' : ''; ?>"
                                                   data-src="<?php echo base_url($photos[0]); ?>"
                                                   href="javascript:void(0)"
                                                   style="
                                                       background-color: <?php echo $colour ? $colour[0]->code : '#fff'; ?>;
                                                   <?php echo $attribute->color == 'white' ? 'border:1px solid #dfdfdf' : ''; ?>;
                                                   <?php echo $key >= 6 ? 'display:none' : 'display:block'; ?>
                                                       "
                                                ></a>
                                                <?php
                                            }
                                            if (count($allattributes) >= 6) {
                                                ?>
                                                <img src="<?php echo base_url('assets/images/plus.png'); ?>"
                                                     width="15px" alt="plus" class="ml-1 mr-1 showHiddenColor"/>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    } else {
                        echo '<center>';
                        echo "<img src='" . base_url('assets/images/no-product.jpg') . "' style='width:40%' />";
                        echo '</center>';
                    }
                    ?>
                </div>
            </section>
        </div>
    </main>
    <?php echo $page_footer; ?>
</div>

<!--<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">-->
<!---->
<!--    <div class="pswp__bg"></div>-->
<!---->
<!--    <div class="pswp__scroll-wrap">-->
<!---->
<!--        <div class="pswp__container">-->
<!--            <div class="pswp__item"></div>-->
<!--            <div class="pswp__item"></div>-->
<!--            <div class="pswp__item"></div>-->
<!--        </div>-->
<!---->
<!--        <div class="pswp__ui pswp__ui--hidden">-->
<!--            <div class="pswp__top-bar">-->
<!---->
<!--                <div class="pswp__counter"></div>-->
<!--                <button class="pswp__button pswp__button--close" aria-label="Close (Esc)"></button>-->
<!--                <button class="pswp__button pswp__button--zoom" aria-label="Zoom in/out"></button>-->
<!--                <div class="pswp__preloader">-->
<!--                    <div class="loading-spin"></div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">-->
<!--                <div class="pswp__share-tooltip"></div>-->
<!--            </div>-->
<!--            <button class="pswp__button--arrow--left" aria-label="Previous (arrow left)"></button>-->
<!--            <button class="pswp__button--arrow--right" aria-label="Next (arrow right)"></button>-->
<!--            <div class="pswp__caption">-->
<!--                <div class="pswp__caption__center"></div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->


<?php echo $page_footerscript; ?>
<script src="vendor/image-zoom/js/image-zoom.js" type="text/javascript"></script>
<script src="vendor/photoswipe/photoswipe.min.js"></script>
<script src="vendor/photoswipe/photoswipe-ui-default.min.js"></script>
<style>
    .product-image-full {
        display: none;
    }

    .owl-nav-inner .owl-nav .owl-next, .owl-nav-inner .owl-nav .owl-prev {
        opacity: 1 !important;
        visibility: visible !important;
    }
</style>
<script>
    $(document).ready(function () {
        var zoomImages = $('.zoomImg');
        zoomImages.each(function () {
            $(this).imageZoom({zoom: 200});
        });
    });
</script>
<script type="text/javascript"
        src="https://platform-api.sharethis.com/js/sharethis.js#property=642c03d2faaa470019ff1ae6&product=inline-share-buttons&source=platform"
        async="async"></script>
</body>