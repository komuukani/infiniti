<?php
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');

$admin_data = $this->md->select('tbl_admin')[0];
$web_data = ($web_data) ? $web_data[0] : '';
$meta_data = $this->md->select_where('tbl_seo', array('page' => $page));
$category = $this->md->select_limit_order('tbl_category', 100, 'position', 'asc');
$cartProduct = $this->md->select_where('tbl_cart', array('unique_id' => $this->input->cookie('unique_id')));

$active_page = $this->uri->segment(1) ? $this->uri->segment(1) : 'index';
?>
<!DOCTYPE html>
<html lang="en" data-bs-theme="light">
<!--<![endif]-->
<?php ob_start(); ?>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <base href="<?php echo base_url('assets/'); ?>"/>

    <title><?php echo $page_title . ' - ' . $web_data->project_name; ?></title>
    <!-- Favicon Icon -->
    <link rel="apple-touch-icon" sizes="180x180"
          href="<?php echo base_url(($web_data) ? $web_data->favicon : FILENOTFOUND); ?>">
    <link rel="icon" type="image/png" sizes="32x32"
          href="<?php echo base_url(($web_data) ? $web_data->favicon : FILENOTFOUND); ?>">
    <link rel="icon" type="image/png" sizes="16x16"
          href="<?php echo base_url(($web_data) ? $web_data->favicon : FILENOTFOUND); ?>">

    <?php
    if (!empty($meta_data)) {
        ?>
        <meta name="author" content="<?php echo $meta_data ? $meta_data[0]->title : ''; ?>">
        <meta name="title" content="<?php echo $meta_data ? $meta_data[0]->title : ''; ?>">
        <meta name="description" content="<?php echo $meta_data ? $meta_data[0]->description : ''; ?>">
        <meta name="keywords" content="<?php echo $meta_data ? $meta_data[0]->keyword : ''; ?>">
        <title><?php echo $meta_data ? ($meta_data[0]->title . " | " . $web_data->project_name) : (' | ' . $web_data->project_name); ?></title>

        <?php
    } else {

        if (isset($blog_data)) {
            ?>
            <meta name="title"
                  content="<?php echo $blog_data[0]->meta_title ? $blog_data[0]->meta_title : $blog_data[0]->title; ?>">
            <meta name="description" content="<?php echo $blog_data[0]->meta_desc; ?>">
            <meta name="keywords" content="<?php echo $blog_data[0]->meta_keyword; ?>">
            <?php
        }
        if (isset($product_data)) {
            $attributes = $this->md->select_where('tbl_product_attribute', array('product_id' => $product_data[0]->product_id));

            $photos = array(FILENOTFOUND);
            if ($attributes) {
                $photos = $attributes[0]->product_photos ? explode(",", $attributes[0]->product_photos) : array(FILENOTFOUND);
            }
            ?>
            <meta name="title"
                  content="<?php echo $product_data[0]->meta_title ? $product_data[0]->meta_title : $product_data[0]->title; ?>">
            <meta name="description" content="<?php echo $product_data[0]->meta_desc; ?>">
            <meta name="keywords" content="<?php echo $product_data[0]->meta_keyword; ?>">
            <!-- Favicon Icon -->
            <meta property="og:site_name" content="INFINITI">
            <meta property="og:url" content="<?php echo current_url(); ?>">
            <meta property="og:title"
                  content="<?php echo $product_data[0]->meta_title ? $product_data[0]->meta_title : $product_data[0]->title; ?>">
            <meta property="og:type" content="product">
            <meta property="og:description"
                  content="<?php echo $product_data[0]->meta_desc; ?>">
            <meta property="og:image"
                  content="<?php echo base_url($photos ? ($photos[0] ? $photos[0] : (($web_data) ? $web_data->favicon : FILENOTFOUND)) : (($web_data) ? $web_data->favicon : FILENOTFOUND)); ?>">
            <?php
        }
    }
    ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

    <link href="<?php echo base_url('admin_asset/'); ?>css/responsive.css" rel="stylesheet">

    <link rel="preload" href="fonts/riode115b.ttf?5gap68" as="font" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" href="vendor/fontawesome-free/webfonts/fa-solid-900.woff2" as="font" type="font/woff2"
          crossorigin="anonymous">
    <link rel="preload" href="vendor/fontawesome-free/webfonts/fa-brands-400.woff2" as="font" type="font/woff2"
          crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Questrial&display=swap" rel="stylesheet">


    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Tajawal:wght@200;300;400;500;700;800;900&display=swap"
          rel="stylesheet">

    <script>
        WebFontConfig = {
            google: {families: ['questrial:300,400,400italic,500,600,700,800,900italic']}
        };
        (function (d) {
            var wf = d.createElement('script'), s = d.scripts[0];
            wf.src = 'js/webfont.js';
            wf.async = true;
            s.parentNode.insertBefore(wf, s);
        })(document);
    </script>
    <link rel="stylesheet" type="text/css" href="vendor/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.min.css">

    <!--    <link rel="stylesheet" type="text/css" href="vendor/magnific-popup/magnific-popup.min.css">-->
    <link rel="stylesheet" type="text/css" href="vendor/owl-carousel/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="vendor/photoswipe/photoswipe.min.css">
    <link rel="stylesheet" type="text/css" href="vendor/photoswipe/default-skin/default-skin.min.css">
    <link rel="stylesheet" type="text/css" href="vendor/sticky-icon/stickyicon.css">

    <link rel="stylesheet" type="text/css" href="css/style.min.css?v=536">

    <link rel="stylesheet" type="text/css" href="css/demo12.min.css?v=334">
    <link rel="stylesheet" type="text/css" href="css/custom.css?v=78">

</head>
<!-- page wrapper -->
<?php $page_head = ob_get_clean(); ?>
<?php ob_start(); ?>
<header id="navbar-header" class="header border-bottom-none bg-transparent"
        style="position:fixed !important;top: 0 !important;z-index: 200;width: 100% !important;">
    <!--    <div class="header-top d-block bg-000">-->
    <!--        <div class="text-center">-->
    <!--            <div class="">-->
    <!--                <p style="padding: 9px !important;" class="welcome-msg text-center font-15 font-weight-500 text-FFF">The-->
    <!--                    Largest Sofa Market for KSA</p>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->

    <div class="header-middle sticky-header fix-top sticky-content pt-2 pb-2" style="box-shadow: none !important;">
        <div class="container">
            <div class="header-left">
                <a href="javascript:void(0)" class="mobile-menu-toggle showMobileMenu">
                    <i class="d-icon-bars"></i>
                </a>
                <div class="header-search hs-toggle search-left mr-3 ml-3">
                    <a href="javascript:void(0)"
                       onclick="$('.searchModal').toggle();$('.searchModalBody').toggle();$('body').css('overflow','hidden')"
                       class="search-toggle d-flex align-items-center">
                        <i class="d-icon-search"></i>
                    </a>
                </div>
            </div>
            <div class="header-center">
                <a href="<?php echo base_url(); ?>" class="logo">
                    <img src="<?php echo base_url($web_data ? $web_data->logo : FILENOTFOUND); ?>"
                         alt="<?php echo $web_data ? $web_data->project_name : ''; ?>"
                         width="120"
                         height="44"
                         title="<?php echo $web_data ? $web_data->project_name : ''; ?>"/>
                </a>
            </div>
            <div class="header-right">
                <div class="mr-3 ml-3">
                    <?php
                    if ($this->session->userdata('site_lang') == 'english') {
                        ?>
                        <a href="<?php echo base_url('site-lang/arabic'); ?>"
                           class="font-16 font-weight-bold">
                            <img src="<?php echo base_url('assets/images/language/arab.jpg'); ?>" width="24px"
                                 alt="arab"/>
                        </a>
                        <?php
                    } else {
                        ?>
                        <a href="<?php echo base_url('site-lang/english'); ?>"
                           class="font-16 font-weight-bold">
                            <img src="<?php echo base_url('assets/images/language/us.jpg'); ?>" width="24px" alt="en"/>
                        </a>
                        <?php
                    }
                    ?>
                </div>

                <div
                    class="dropdown cart-dropdown type2 off-canvas <?php echo ($success == 'Product updated in cart.' || $success == 'Product added in cart.') ? 'opened' : ''; ?>">
                    <a href="javascript:void(0)" class="cart-toggle label-block link"
                       onclick="cartModalOpen2();">
                        <i class="d-icon-bag">
                            <?php
                            if ($cartProduct) {
                                ?>
                                <span class="cart-count"><?php echo count($cartProduct); ?></span>
                                <?php
                            }
                            ?>
                        </i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom sticky-header fix-top sticky-content">
        <div class="container">
            <div class="header-center justify-content-center">
                <nav class="main-nav">
                    <ul class="menu">
                        <li class="active">
                            <div class="text-center">
                                <a href="<?php echo base_url(); ?>">
                                    <!--                                    <div>-->
                                    <!--                                        <img-->
                                    <!--                                            src="-->
                                    <?php //echo base_url('assets/images/home.png'); ?><!--"-->
                                    <!--                                            alt="home"-->
                                    <!--                                            style="width: 60px;height: 60px;object-fit: contain;"-->
                                    <!--                                        />-->
                                    <!--                                    </div>-->
                                    <?php echo lang('home'); ?>
                                </a>
                            </div>
                        </li>
                        <?php
                        if (empty($category)) {
                            echo '<li><div class="alert alert-warning col-md-12">Sorry, Category not available!</div></li>';
                        } else {
                            foreach ($category as $category_data) {
                                ?>
                                <li class="englishText">
                                    <div class="text-center">
                                        <a href="<?php echo base_url('collection/' . $category_data->slug); ?>">
                                            <!--                                            <div>-->
                                            <!--                                                <img-->
                                            <!--                                                    src="-->
                                            <?php //echo base_url($category_data->path ? $category_data->path : FILENOTFOUND); ?><!--"-->
                                            <!--                                                    title="-->
                                            <?php //echo $category_data->title; ?><!--"-->
                                            <!--                                                    alt="-->
                                            <?php //echo $category_data->title; ?><!--"-->
                                            <!--                                                    style="width: 60px;height: 60px;object-fit: contain;"-->
                                            <!--                                                />-->
                                            <!--                                            </div>-->
                                            <?php echo $category_data->title; ?>
                                        </a>
                                    </div>
                                </li>
                                <li class="arabicText notranslate" translate="no" style="direction: rtl">
                                    <div class="text-center">
                                        <a href="<?php echo base_url('collection/' . $category_data->slug); ?>">
                                            <?php echo $category_data->title_arabic; ?>
                                        </a>
                                    </div>
                                </li>
                                <?php
                            }
                        }
                        ?>
                        <li>
                            <div class="text-center">
                                <a href="<?php echo base_url('customize'); ?>">
                                    <!--                                    <div>-->
                                    <!--                                        <img-->
                                    <!--                                            src="-->
                                    <?php //echo base_url('assets/images/home.png'); ?><!--"-->
                                    <!--                                            alt="home"-->
                                    <!--                                            style="width: 60px;height: 60px;object-fit: contain;"-->
                                    <!--                                        />-->
                                    <!--                                    </div>-->
                                    <?php echo lang('customize_your_sofa'); ?>
                                </a>
                            </div>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>
<!-- Main Header Area End Here -->
<?php $page_header = ob_get_clean(); ?>
<?php ob_start(); ?>
<section class="z-index-2 position-relative pb-2 mb-12">
    <div class="bg-body-secondary mb-3">
        <div class="container">
            <nav class="py-4 lh-30px" aria-label="breadcrumb">
                <ol class="breadcrumb justify-content-center py-1">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><?php echo lang('home'); ?></a></li>
                    <?php
                    if (isset($blog_data)) {
                        echo '<li class="breadcrumb-item"><a href="' . base_url('blog') . '">Blog</a></li>';
                    }
                    ?>
                    <li class="breadcrumb-item active" aria-current="page"><?php echo ucfirst($page_title); ?></li>
                </ol>
            </nav>
        </div>
    </div>
</section>
<?php $page_breadcumbs = ob_get_clean(); ?>
<?php ob_start(); ?>
<footer class="footer">
    <div class="container">
        <div class="footer-middle border-none">
            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <div class="widget widget-info pl-xs-30 pr-xs-30">
                        <h4 class="widget-title font-18 font-weight-bold"><?php echo lang('about_us'); ?></h4>
                        <p style="color: rgba(255, 255, 255, 0.75);font-size: 16.8px">
                            <?php echo lang('about_us_p1'); ?>
                        </p>
                        <p style="color: rgba(255, 255, 255, 0.75);font-size: 16.8px">
                            <?php echo lang('about_us_p2'); ?>
                        </p>
                        <p style="color: rgba(255, 255, 255, 0.75);font-size: 16.8px">
                            <?php echo lang('about_us_p3'); ?>
                        </p>
                    </div>

                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="widget widget-service pl-xs-30 pr-xs-30">
                        <h4 class="widget-title font-18 font-weight-bold"><?php echo lang('quick_links'); ?></h4>
                        <ul class="widget-body mt-xs-30">
                            <li>
                                <a href="<?php echo base_url('contact'); ?>"><?php echo lang('contact_information'); ?></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('faq'); ?>"><?php echo lang('faq'); ?></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('quality-policy'); ?>"><?php echo lang('quality_policy'); ?></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('pricing-policy'); ?>"><?php echo lang('pricing_policy'); ?></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('return-refund-policy'); ?>"><?php echo lang('return_policy'); ?></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('policy'); ?>"><?php echo lang('privacy_policy'); ?> </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('terms'); ?>"><?php echo lang('t_c'); ?></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('shipping-policy'); ?>"><?php echo lang('shipping_policy'); ?></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="widget widget-info pl-xs-30 pr-xs-30">
                        <h4 class="widget-title mt-xs-30 font-18 font-weight-bold"><?php echo lang('subscribe_to_our_offers'); ?></h4>
                        <form method="post">
                            <div class="newsletterForm">
                                <input type="email" placeholder="<?php echo lang('email'); ?>" required/>
                                <button type="submit" name="submit">
                                    <svg viewBox="0 0 14 10" fill="none" aria-hidden="true" focusable="false"
                                         class="icon icon-arrow" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M8.537.808a.5.5 0 01.817-.162l4 4a.5.5 0 010 .708l-4 4a.5.5 0 11-.708-.708L11.793 5.5H1a.5.5 0 010-1h10.793L8.646 1.354a.5.5 0 01-.109-.546z"
                                              fill="currentColor">
                                        </path>
                                    </svg>
                                </button>
                            </div>
                        </form>
                    </div>

                </div>
                <!--                <div class="col-lg-4 col-sm-6">-->
                <!--                    <div class="widget widget-about">-->
                <!--                        <div class="widget-contact-info widget-collapsible">-->
                <!--                            <ul class="contact-info font-16">-->
                <!--                                <li class="info phone"><label>PHONE:</label><a-->
                <!--                                        href="tel:-->
                <?php //echo $web_data ? $web_data->phone : ''; ?><!--">Toll-->
                <!--                                        Free -->
                <?php //echo $web_data ? $web_data->phone : ''; ?><!--</a></li>-->
                <!--                                <li class="info email"><label>EMAIL:</label><a-->
                <!--                                        href="-->
                <?php //echo $web_data ? $web_data->email_address : ''; ?><!--">-->
                <?php //echo $web_data ? $web_data->email_address : ''; ?><!--</a>-->
                <!--                                </li>-->
                <!--                                <li class="info addr"><label>ADDRESS:</label><a-->
                <!--                                        href="javascript:void(0)">-->
                <?php //echo $web_data ? $web_data->address : ''; ?><!--</a>-->
                <!--                                </li>-->
                <!--                            </ul>-->
                <!--                        </div>-->
                <!--                    </div>-->
                <!---->
                <!--                </div>-->
            </div>
        </div>

        <div class="footer-bottom d-block pb-xs-5 pt-xs-5">
            <div class="footer-center text-center mb-xs-5">
                <div class="social-links mb-50 mb-xs-10">
                    <a href="http://instagram.com/infiniti_arabia" title="social-link"
                       class="social-link"><i class="fab fa-instagram"></i></a>
                    <a href="https://tiktok.com/@infiniti_arabia" title="social-link"
                       class="social-link">
                        <svg aria-hidden="true" focusable="false" class="icon icon-tiktok" width="16" height="18"
                             fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M8.02 0H11s-.17 3.82 4.13 4.1v2.95s-2.3.14-4.13-1.26l.03 6.1a5.52 5.52 0 11-5.51-5.52h.77V9.4a2.5 2.5 0 101.76 2.4L8.02 0z"
                                fill="currentColor">
                            </path>
                        </svg>
                    </a>
                    <a href="<?php echo $web_data ? $web_data->twitter : ''; ?>" title="social-link"
                       class="social-link"><i class="fab fa-twitter"></i></a>
                    <a
                        href="https://www.snapchat.com/add/infiniti_arabia"
                        class="social-link">
                        <i class="fab fa-snapchat-ghost"></i>
                    </a>

                </div>
                <p class="copyright font-10 pt-60 text-999 letter-spacing-1"><?php echo $web_data ? $web_data->footer : ''; ?></p>
            </div>
        </div>

    </div>
</footer>
<?php
$page_footer = ob_get_clean();
?>
<?php ob_start(); ?>
<?php
$this->load->view('modal');
?>
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/parallax/parallax.min.js"></script>
<!--<script src="vendor/elevatezoom/jquery.elevatezoom.min.js"></script>-->
<!--<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>-->
<script src="vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
<script src="vendor/owl-carousel/owl.carousel.min.js"></script>
<script src="js/main.min.js?v=3"></script>
<script src="js/notify.min.js"></script>
<script src="js/custom.js?v=1"></script>
<script>
    // For home page
    var limit = <?php echo PER_PAGE; ?>; // Number of products to load each time
    var offset = 0; // Starting offset
    var expirationTime = 3600000; // 1 hour in milliseconds

    function loadProducts() {
        $.ajax({
            url: '<?php echo base_url('Pages/loadMore'); ?>',
            type: 'POST',
            data: {
                limit: limit,
                offset: offset
            },
            success: function (data) {
                var response = JSON.parse(data);
                $('#productList').append(response.html);
                offset += limit; // Update the offset for the next load

                // Hide the load more button if there are no more products to load
                if (!response.more_products) {
                    $('#loadMore').hide();
                }

                // Store loaded products, offset, and current timestamp in session storage
                var timestamp = new Date().getTime();
                sessionStorage.setItem('product_list', $('#productList').html());
                sessionStorage.setItem('product_offset', offset);
                sessionStorage.setItem('hide_load_more', !response.more_products);
                sessionStorage.setItem('timestamp', timestamp);
                setDefaultLanguage();
            }
        });
    }

    $(document).ready(function () {

        var currentTime = new Date().getTime();
        var savedTime = sessionStorage.getItem('timestamp');

        // Check if data is expired
        if (savedTime && (currentTime - savedTime > expirationTime)) {
            sessionStorage.clear();
        }

        // Check if there are products stored in session storage
        if (sessionStorage.getItem('product_list')) {
            $('#productList').html(sessionStorage.getItem('product_list'));
            offset = parseInt(sessionStorage.getItem('product_offset'), 10);

            // Check if more products button should be hidden
            if (sessionStorage.getItem('hide_load_more') === 'true') {
                $('#loadMore').hide();
            }
        } else {
            loadProducts(); // Initial load
            setDefaultLanguage();
        }

        $('#loadMore').click(function () {
            loadProducts(); // Load more products on button click
            setDefaultLanguage();
        });
    });
</script>


<script type="text/javascript">

    //$(document).on("click", "#loadMore", function () {
    //    let lastId = $(this).attr('lastId'); // get Last id
    //    let category = $(this).attr('category'); // get category id
    //    let sortby = $(this).attr('sortby'); // get sortby
    //
    //    var data = {
    //        lastId: lastId,
    //        category: category,
    //        sortby: sortby
    //    };
    //    var url = "<?php //echo base_url('Pages/loadMoreProducts'); ?>//";
    //    jQuery.post(url, data, function (data) {
    //        if (data === 'empty') {
    //            $('#loadMore').hide();
    //        } else {
    //            let limit = parseInt(lastId) + parseInt(<?php //echo PER_PAGE; ?>//);
    //            jQuery('#loadMore').attr('lastId', limit);
    //            jQuery('#productList').append(data);
    //
    //            setDefaultLanguage();
    //        }
    //    });
    //});

    /*--------------------
    SHOW HIDDEN COLOUR
    ---------------------*/
    // $(document).on("click", ".showHiddenColor", function () {
    //     if ($(this).hasClass('fa-plus')) {
    //         $(this).parent().find('.hiddenColor').show();
    //         $(this).removeClass('fa-plus').addClass('fa-minus');
    //     } else {
    //         $(this).parent().find('.hiddenColor').hide();
    //         $(this).addClass('fa-plus').removeClass('fa-minus');
    //     }
    // });

    /*--------------------
    NOTIFY.JS INITIALIZATION
    ---------------------*/
    $.notify.defaults({
        position: "bottom right",
        gap: '5',
        autoHideDelay: 7000
    });

    function cartModalOpen1(e) {
        $(e).hide();
        $('.cartModalBody').hide();
        $('body').css('overflow-y', 'auto');
        if ($("body").hasClass("arabic")) {
            $('.cartModalBody').css('transform', 'translateX(-350px)');
        } else {
            $('.cartModalBody').css('transform', 'translateX(350px)');
        }
        $('.cart-dropdown').removeClass('opened');
    }

    function cartModalOpen2() {
        $('.cartModal').toggle();
        $('.cartModalBody').toggle();
        $('body').css('overflow', 'hidden');
        $('.cartModalBody').css('transform', 'translateX(0px)');
        $('.cart-dropdown').addClass('opened');
    }

    function closeCartModal() {
        $('.cartModal').hide();
        $('.cartModalBody').hide();
        $('body').css('overflow-y', 'auto');
        if ($("body").hasClass("arabic")) {
            $('.cartModalBody').css('transform', 'translateX(-350px)');
        } else {
            $('.cartModalBody').css('transform', 'translateX(350px)');
        }
        $('.cart-dropdown').removeClass('opened');
    }

    /*--------------------
     GET CITY FROM STATE
     ---------------------*/
    $(document).on("change", "#country", function () {
        let countryId = $(this).val(); // get country id
        jQuery('#city').html('Loading...');
        var data = {
            countryId: countryId
        };
        var url = "<?php echo base_url('User/get_city'); ?>";
        jQuery.post(url, data, function (data) {
            jQuery('#city').html(data);
        });
    });

    // Navbar show on page scroll up
    var lastScrollTop = 0;
    $(window).scroll(function () {
        var st = $(this).scrollTop();
        var banner = $('#navbar-header');
        setTimeout(function () {
            if (st === 0) {
                $('#navbar-header').addClass('bg-transparent').removeClass('bg-FFF');
            } else {
                if (st > lastScrollTop) {
                    banner.addClass('hide bg-transparent').removeClass('bg-FFF');
                } else {
                    banner.removeClass('hide bg-transparent').addClass('bg-FFF');
                }
            }
            lastScrollTop = st;
        }, 100);
    });

    // Show Mobile menu Icon & Close Icon
    $(document).on("click", ".showMobileMenu", function () {
        if ($(this).hasClass("mobile-menu-toggle")) {
            $(this).removeClass('mobile-menu-toggle').addClass('mobile-menu-close');
            $(this).html('<i class="d-icon-times"></i>');
            $('header').removeClass('bg-transparent').addClass('bg-FFF');
            $('body').addClass('mmenu-active');
            $('.header-search').addClass('ml-xs-50');
        } else if ($(this).hasClass("mobile-menu-close")) {
            $(this).html('<i class="d-icon-bars"></i>');
            $(this).removeClass('mobile-menu-close').addClass('mobile-menu-toggle');
            $('header').removeClass('bg-FFF').addClass('bg-transparent');
            $('body').removeClass('mmenu-active');
            $('.header-search').removeClass('ml-xs-50');
        }
    });

    setInterval(function () {
        if ($('body').hasClass("mmenu-active")) {
            $('body').css('overflow', 'hidden');
        } else {
            if ($('.cart-dropdown').hasClass('opened')) {
                $('body').css('overflow', 'hidden');
            } else {
                $('body').css('overflow-y', 'scroll');
            }
            $(".showMobileMenu").html('<i class="d-icon-bars"></i>');
            $(".showMobileMenu").removeClass('mobile-menu-close').addClass('mobile-menu-toggle');
            $('.header-search').removeClass('ml-xs-50');
        }
    }, 50);


    /*--------------------
     GET PRODUCT FROM CATEGORY
     ---------------------*/
    $(document).on("change", "#category", function () {
        let catId = $(this).val(); // get category id
        jQuery('#get_product').html('Loading...');
        var data = {
            catId: catId
        };
        var url = "<?php echo base_url('User/get_product'); ?>";
        jQuery.post(url, data, function (data) {
            jQuery('#show_product_select').html(data);
            jQuery('#getProduct').niceSelect();
        });
    });


    /*-------------------------
    GOOGLE LANGUAGE TRANSLATE
    ---------------------------*/

    $(window).ready(function () {
        setDefaultLanguage();
    });

    function setDefaultLanguage() {
        // Set default language from session
        let lng = '<?php echo $this->session->userdata('site_lang'); ?>';    // english / arabic
        if (lng !== null) {
            if (lng === 'arabic') {
                $('html').css('direction', 'rtl');
                $('body').addClass('arabic');
            } else {
                $('html').css('direction', 'ltr');
                $('body').addClass('english');
            }
        } else {
            $('html').css('direction', 'ltr');
            $('body').addClass('english');
        }

        // Show arabic text & hide english
        if ($("body").hasClass("arabic")) {
            $(".arabicText").show();
            $(".englishText").hide();
        } else {
            $(".arabicText").hide();
            $(".englishText").show();
        }
    }

</script>
<script>
    $(document).ready(function () {
        $('.product-single-carousel2').owlCarousel({
            nav: true,
            dots: true,
            animateIn: 'fadeIn',
            animateOut: 'fadeOut',
            items: 1,
            rtl: $("body").hasClass("arabic") ? true : false,
            touchDrag: false,
            mouseDrag: false,
            autoplay: false,
            autoPlaySpeed: 5000,
            autoPlayTimeout: 5000,
            autoplayHoverPause: true,
            smartSpeed: 200,
            responsiveClass: true,
            checkVisible: false
        });

        $('.categorySectionSlider').owlCarousel({
            nav: true,
            dots: false,
            stagePadding: 50,
            items: 1,
            autoWidth: true,
            margin: 20,
            loop: false,
            rtl: $("body").hasClass("arabic") ? true : false,
        });

        $('.homeCategorySection').owlCarousel({
            nav: true,
            dots: false,
            items: 4,
            margin: 5,
            loop: false,
            navText: ["<img src=\"images/left.png\" width=\"15px\" alt=\"left\"/>", "<img src=\"images/right.png\" width=\"15px\" alt=\"right\"/>"],
            rtl: $("body").hasClass("arabic") ? true : false,
            responsive: {
                '0': {
                    'items': 3
                },
                '480': {
                    'items': 3
                },
                '768': {
                    'items': 3
                },
                '992': {
                    'items': 8,
                    'dots': false
                }
            }
        });
    });
</script>
<?php
if ($success == 'Product updated in cart.' || $success == 'Product added in cart.') {
    ?>
    <script>
        $('.cartModal').show();
        $('.cartModalBody').show();
        $('body').css('overflow', 'hidden');
        $('.cartModalBody').css('transform', 'translateX(0px)');
        $('.cart-dropdown').addClass('opened');
    </script>
    <?php
}

if (isset($success)) {
    ?>
    <script>
        //$.notify('<?php //echo $success; ?>//', 'success');
    </script>
    <?php
}
if (isset($error)) {
    ?>
    <script>
        //$.notify('<?php //echo $error; ?>//', 'error');
    </script>
    <?php
}
$page_footerscript = ob_get_clean(); ?>
<?php
$this->load->view($page, array(
    'page_title' => $page_title,
    'page_head' => $page_head,
    'page_header' => $page_header,
    'page_breadcumb' => ($page_breadcumb) ? $page_breadcumbs : '',
    'page_footer' => $page_footer,
    'page_footerscript' => $page_footerscript
));
$this->session->unset_userdata('success');
$this->session->unset_userdata('error');
?>
<?php
if ($this->session->userdata('site_lang') == 'arabic') {
    ?>
    <style>
        body.arabic h1, h2, h3, h4, h5, h6, p, a, ul, li, span, label, div, input, textarea, del, button {
            font-family: "Tajawal", sans-serif !important;
            font-style: normal !important;
        }
    </style>
    <?php
}
?>
<style>

    .error-text {
        width: 100%;
        margin-top: 5px;
        font-size: 13px;
        color: #dc3545;
        background: #ff4073e6;
        padding-left: 10px;
    }

    .error-text p {
        margin-bottom: 0 !important;
        color: #ffe7e9 !important;
        margin-left: 12px;
        font-weight: 500;
    }

</style>

</html>