<?php
echo $page_head;
$admin_data = $this->md->select('tbl_web_data')[0];
?>
<body>

<div id="canvas">
    <div id="box_wrapper">
        <!-- template sections -->
        <?php echo $page_header; ?>
        <?php echo $page_breadcumb; ?>

        <section id="contact-single"
                 class="ls section_padding_top_150 section_padding_bottom_150 columns_padding_30 columns_margin_bottom_20">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <div
                            class="file-upload-wrapper cs main_color2 background_cover with_padding big-padding text-center">
                            <h2 class="section_header">Application Form</h2>
                            <span style="line-height: 30px"
                                  class="under_heading grey">Please submit your application materials <br/>as .pdf files.</span>
                            <div class="toppadding_15"></div>
                            <div class="toppadding_15 visible-md visible-lg"></div>
                            <div class="toppadding_20 visible-lg"></div>
                            <form class="contact-form" method="post" enctype="multipart/form-data">
                                <?php
                                if (isset($error)) {
                                    ?>
                                    <div class="alert alert-danger p-1">
                                        <?php echo $error; ?>
                                    </div>
                                    <?php
                                }
                                if (isset($success)) {
                                    ?>
                                    <div class="alert alert-success p-1">
                                        <?php echo $success; ?>
                                    </div>
                                    <?php
                                }
                                ?>
                                <div class="row columns_margin_bottom_20 bottommargin_0">
                                    <div class="col-xs-12">
                                        <div class="inputfile"><input type="file" name="cover_letter" id="coverfile"
                                                                      accept="application/pdf">
                                            <label for="coverfile"
                                                   class="theme_button bg_button color1"><span>Add Your Cover Letter (Only .PDF File)</span></label>
                                        </div>
                                    </div>

                                    <div class="col-xs-12">
                                        <div class="inputfile"><input type="file" name="document" id="file"
                                                                      accept="application/pdf">
                                            <label for="file"
                                                   class="theme_button bg_button color1"><span>Add Your Resume / CV (Only .PDF File)</span></label>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group"><label for="name">Full Name</label>
                                            <input type="text"
                                                   aria-required="true"
                                                   size="30"
                                                   name="fname"
                                                   id="name"
                                                   class="form-control"
                                                   value="<?php
                                                   if (set_value('fname') && !isset($success)) {
                                                       echo set_value('fname');
                                                   }
                                                   ?>"
                                                   placeholder="Full Name">
                                            <div class="error-text p-0 m-0">
                                                <?php
                                                if (form_error('fname')) {
                                                    echo form_error('fname');
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group"><label for="email">Email address</label>
                                            <input
                                                type="email" aria-required="true" size="30" name="email"
                                                value="<?php
                                                if (set_value('email') && !isset($success)) {
                                                    echo set_value('email');
                                                }
                                                ?>"
                                                id="email" class="form-control" placeholder="Email Address">
                                            <div class="error-text p-0 m-0">
                                                <?php
                                                if (form_error('email')) {
                                                    echo form_error('email');
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="form-group"><label for="phone">Phone Number</label>
                                            <input
                                                type="tel" size="30" name="phone" id="phone"
                                                value="<?php
                                                if (set_value('phone') && !isset($success)) {
                                                    echo set_value('phone');
                                                }
                                                ?>"
                                                class="form-control" placeholder="Phone Number">
                                            <div class="error-text p-0 m-0">
                                                <?php
                                                if (form_error('phone')) {
                                                    echo form_error('phone');
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="form-group"><label for="message">Which Job are you Applying
                                                For?</label>
                                            <select class="form-control" name="job">
                                                <option value="">Which Job are you Applying For?</option>
                                                <option>Outside Sales Representative and Business Developer</option>
                                                <option>Senior Graphic Designer</option>
                                            </select>
                                            <div class="error-text p-0 m-0">
                                                <?php
                                                if (form_error('job')) {
                                                    echo form_error('job');
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="form-group"><label for="message">Your Message</label>
                                            <textarea
                                                aria-required="true" rows="3" cols="45" name="message" id="message"
                                                class="form-control" placeholder="Your Message"><?php
                                                if (set_value('message') && !isset($success)) {
                                                    echo set_value('message');
                                                }
                                                ?></textarea>
                                            <div class="error-text p-0 m-0">
                                                <?php
                                                if (form_error('message')) {
                                                    echo form_error('message');
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <?php
                                        if ($admin_data->captcha_visibility) :
                                            echo '<div class="g-recaptcha" data-sitekey="' . $admin_data->captcha_site_key . '"></div>';
                                        endif;
                                        ?>
                                    </div>
                                    <div class="col-xs-12 bottommargin_0">
                                        <div class="contact-form-submit">
                                            <button type="submit" id="contact_form_submit" name="send" value="send"
                                                    class="theme_button color1">Submit message
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php echo $page_footer; ?>
    </div>
</div>

<?php echo $page_footerscript; ?>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
</body>