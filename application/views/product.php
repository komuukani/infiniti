<?php
echo $page_head;
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
$sortby = $this->input->get('sortby') ? trim($this->input->get('sortby', TRUE)) : '';
?>
<body>
<div class="page-wrapper">
    <?php echo $page_header; ?>
    <main class="main mt-100 mt-xs-50">
        <div class="page-content">
            <section class="product-wrapper container pt-7 pb-3 appear-animate">
                <nav class="d-flex justify-content-center">
                    <label class="mr-2" style="margin-top: 7px;">Sort by: </label>
                    <div class="toolbox-item toolbox-sort select-menu">
                        <select name="sortby" class="form-control border-none font-14"
                                onchange="window.location.href = '<?php echo current_url(); ?>?sortby=' + this.value">
                            <option value="">Select Sort</option>
                            <option <?php echo $sortby == 'lowtohigh' ? 'selected' : ''; ?> value="lowtohigh">Price, low
                                to high
                            </option>
                            <option <?php echo $sortby == 'hightolow' ? 'selected' : ''; ?> value="hightolow">Price,
                                high to low
                            </option>
                            <option <?php echo $sortby == 'oldtonew' ? 'selected' : ''; ?> value="oldtonew">Date, old
                                to new
                            </option>
                            <option <?php echo $sortby == 'newtoold' ? 'selected' : ''; ?> value="newtoold">Date, new
                                to old
                            </option>
                        </select>
                    </div>
                </nav>
                <!--                <h2 class="title title-simple ">--><?php //echo $heading; ?><!--</h2>-->
                <div
                    class="owl-theme row cols-lg-4 cols-md-3 cols-2"
                    id="productList2">

                </div>
                <div align="center" class="mb-50">
                    <button type="button"
                            id="loadMore2"
                            style="border-radius: 30px;text-transform: none;padding-left: 30px;padding-right: 30px;"
                            class="btn-product pt-15 pb-15 cursor-pointer text-000 border-none font-weight-normal letter-spacing-1 font-16">
                        Load More
                    </button>
                </div>
            </section>
        </div>
    </main>
    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
<script>
    // For inner product / collection page
    var limit2 = <?php echo PER_PAGE; ?>; // Number of products to load each time
    var offset2 = 0; // Starting offset
    var expirationTime = 3600000; // 1 hour in milliseconds
    var sortby = '<?php echo $sortby; ?>'; // Sort by
    var categorySlug = '<?php echo $this->uri->segment(2); ?>'; // Category Slug

    function loadProducts2() {
        $.ajax({
            url: '<?php echo base_url('Pages/loadMore2'); ?>',
            type: 'POST',
            data: {
                limit: limit2,
                offset: offset2,
                sortby: sortby ? sortby : '',
                categorySlug: categorySlug ? categorySlug : ''
            },
            success: function (data) {
                var response = JSON.parse(data);
                $('#productList2').append(response.html);
                offset2 += limit2; // Update the offset for the next load

                // Hide the load more button if there are no more products to load
                if (!response.more_products) {
                    $('#loadMore2').hide();
                }

                // Store loaded products, offset, and current timestamp in session storage
                var timestamp = new Date().getTime();
                sessionStorage.setItem('product_list2', $('#productList2').html());
                sessionStorage.setItem('product_offset2', offset2);
                sessionStorage.setItem('sortby', sortby);
                sessionStorage.setItem('categorySlug', categorySlug);
                sessionStorage.setItem('hide_load_more2', !response.more_products);
                sessionStorage.setItem('timestamp', timestamp);

                setDefaultLanguage();
            }
        });
    }

    $(document).ready(function () {

        var currentTime = new Date().getTime();
        var savedTime = sessionStorage.getItem('timestamp');

        // Check if data is expired
        if (savedTime && (currentTime - savedTime > expirationTime)) {
            sessionStorage.clear();
        }

        // Check if there are products stored in session storage
        if (sessionStorage.getItem('categorySlug') === categorySlug && sessionStorage.getItem('sortby') === sortby) {
            if (sessionStorage.getItem('product_list2')) {
                $('#productList2').html(sessionStorage.getItem('product_list2'));
                offset2 = parseInt(sessionStorage.getItem('product_offset2'), 10);

                // Check if more products button should be hidden
                if (sessionStorage.getItem('hide_load_more2') === 'true') {
                    $('#loadMore2').hide();
                }
            } else {
                loadProducts2(); // Initial load
                setDefaultLanguage();
            }
        } else {
            loadProducts2(); // Initial load
            setDefaultLanguage();
        }


        $('#loadMore2').click(function () {
            loadProducts2(); // Load more products on button click
            setDefaultLanguage();
        });

        setDefaultLanguage();
    });

    function setDefaultLanguage() {
        // Set default language from session
        let lng = '<?php echo $this->session->userdata('site_lang'); ?>';    // english / arabic
        if (lng !== null) {
            if (lng === 'arabic') {
                $('html').css('direction', 'rtl');
                $('body').addClass('arabic');
            } else {
                $('html').css('direction', 'ltr');
                $('body').addClass('english');
            }
        } else {
            $('html').css('direction', 'ltr');
            $('body').addClass('english');
        }

        // Show arabic text & hide english
        if ($("body").hasClass("arabic")) {
            $(".arabicText").show();
            $(".englishText").hide();
        } else {
            $(".arabicText").hide();
            $(".englishText").show();
        }
    }
</script>
</body>