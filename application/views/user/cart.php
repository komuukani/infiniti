<?php
echo $page_head;
$cartProduct = $this->md->select_where('tbl_cart', array('unique_id' => $this->input->cookie('unique_id')));
$products = $this->md->select_limit_order('tbl_product', 8, 'product_id', 'desc', array('featured' => 1, 'status' => 1));
?>

<body>
<!--<link rel="stylesheet" type="text/css" href="css/style.min.css">-->
<div class="page-wrapper">
    <?php echo $page_header; ?>
    <main class="main mt-100 mt-xs-50">
        <div class="page-content pt-7 pb-10">
            <div class="container mt-7 mb-2">
                <div class="">
                    <h2 class="font-weight-bold">Your cart</h2>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 pr-lg-4">
                        <table class="shop-table cart-table">
                            <thead>
                            <tr>
                                <th><span>Product</span></th>
                                <th><span>Price</span></th>
                                <th><span>quantity</span></th>
                                <th>Total</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $total = 0;
                            if ($cartProduct) {
                                foreach ($cartProduct as $cartitem) {
                                    $productData = $this->md->select_where('tbl_product', array('product_id' => $cartitem->product_id));
                                    if ($productData) {
                                        $product = $productData[0];
                                        $attributes = $this->md->select_where('tbl_product_attribute', array('product_attribute_id' => $cartitem->product_attribute_id));

                                        $photos = array(FILENOTFOUND);
                                        if ($attributes) {
                                            $photos = $attributes[0]->product_photos ? explode(",", $attributes[0]->product_photos) : array(FILENOTFOUND);
                                        }

                                        $url = base_url('product/' . $product->slug . "/" . $product->product_id . "/?color=" . $attributes[0]->color);
                                        $total = $total + $cartitem->netprice; // Net total
                                        ?>
                                        <tr>
                                            <td class="product-thumbnail" width="50%">
                                                <div class="d-flex w-100">
                                                    <figure>
                                                        <a href="<?php echo $url ?>">
                                                            <img
                                                                src="<?php echo base_url($photos ? ($photos[0] ? $photos[0] : FILENOTFOUND) : FILENOTFOUND); ?>"
                                                                alt="<?php echo $product->title; ?>"
                                                                title="<?php echo $product->title; ?>"
                                                                style="width: 100px;height: 100px;object-fit: contain"
                                                            >
                                                        </a>
                                                    </figure>
                                                    <a class="ml-20 englishText font-weight-bold font-16"
                                                       href="<?php echo $url; ?>"><?php echo $productData ? ($productData[0]->title . " - " . ($attributes ? ucfirst($attributes[0]->color) : '')) : ''; ?>
                                                    </a>
                                                    <a class="mr-20 arabicText font-weight-bold font-16"
                                                       href="<?php echo $url; ?>"><?php echo $productData ? ($productData[0]->arabic_title . " - " . ($attributes ? ucfirst($attributes[0]->color) : '')) : ''; ?>
                                                    </a>
                                                </div>
                                            </td>
                                            <td class="product-subtotal" width="10%">
                                                <span
                                                    class="amount"><?php echo $cartitem->price ? number_format($cartitem->price) : ''; ?> SAR</span>
                                            </td>
                                            <td class="product-quantity" width="25%">
                                                <form method="post" action="<?php echo base_url('Pages/updateQty'); ?>"
                                                      name="updateCartForm">
                                                    <input type="hidden" name="cartId"
                                                           value="<?php echo $cartitem->cart_id; ?>"/>
                                                    <div class="input-group">
                                                        <div class="qty-container">
                                                            <button class="qty-btn-minus btn-light" type="submit"
                                                                    onclick="$(this).parentsUntil('.product-quantity').submit();">
                                                                <i
                                                                    class="fa fa-minus"></i></button>
                                                            <input type="text" name="quantity"
                                                                   value="<?php echo $cartitem->qty; ?>"
                                                                   onchange="$(this).parentsUntil('.product-quantity').submit();"
                                                                   class="input-qty"/>
                                                            <button class="qty-btn-plus btn-light" type="submit"
                                                                    onclick="$(this).parentsUntil('.product-quantity').submit();">
                                                                <i
                                                                    class="fa fa-plus"></i></button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </td>
                                            <td translate="no" class="product-price notranslate" width="10%">
                                                <span
                                                    class="amount"><?php echo $cartitem->netprice ? number_format($cartitem->netprice) : ''; ?> SAR</span>
                                            </td>
                                            <td class="product-close" width="5%">
                                                <a href="<?php echo base_url('Pages/removeCartItem/' . $cartitem->cart_id); ?>"
                                                   class="product-remove" title="Remove this product">
                                                    <i class="fas fa-trash-alt"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <div class="cart-actions mb-6 pt-4">
                            <span>&nbsp;</span>
                            <div align="right">
                                <p class="font-18">
                                    <b>Subtotal</b> &nbsp;
                                    <?php echo $total != 0 ? number_format($total) : 0; ?> SAR
                                    <br/>
                                    <small>VAT included. Shipping cost at checkout</small>
                                </p>
                                <a href="<?php echo base_url('checkout'); ?>"
                                   class="checkoutBtn2">Check out</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <section class="product-wrapper container mt-4 pt-4 pb-4 appear-animate">
                <h2 class="title title-simple  font-weight-bold">Featured collection</h2>
                <div class="owl-theme row cols-lg-4 cols-md-3 cols-2">
                    <?php
                    if ($products) {
                        foreach ($products as $product) {
                            //$category = $this->md->select_where('tbl_category', array('category_id' => $product->category_id));
                            $attributes = $this->md->select_where('tbl_product_attribute', array('product_id' => $product->product_id));

                            $photos = array(FILENOTFOUND);
                            if ($attributes) {
                                $photos = $attributes[0]->product_photos ? explode(",", $attributes[0]->product_photos) : array(FILENOTFOUND);
                            }

                            $url = base_url('product/' . $product->slug . "/" . $product->product_id);
                            ?>
                            <div class="product mb-30">
                                <?php
                                if ($product->sale) {
                                    echo '<span class="saleBadge">' . lang('sale') . '</span>';
                                }
                                ?>
                                <figure
                                    class="product-media">
                                    <a href="<?php echo $url; ?>">
                                        <img
                                            src="<?php echo base_url($photos ? ($photos[0] ? $photos[0] : FILENOTFOUND) : FILENOTFOUND); ?>"
                                            alt="<?php echo $product->title; ?>"
                                            title="<?php echo $product->title; ?>"
                                            class="imgPreview"
                                            style="width: 100%;height: 330px;object-fit: cover"
                                        >
                                        <?php
                                        if (array_key_exists(1, $photos)) {
                                            ?>
                                            <img
                                                src="<?php echo base_url($photos ? ($photos[1] ? $photos[1] : FILENOTFOUND) : FILENOTFOUND); ?>"
                                                alt="<?php echo $product->title; ?>"
                                                title="<?php echo $product->title; ?>"
                                                style="width: 100%;height: 330px;object-fit: cover"
                                            >
                                            <?php
                                        }
                                        ?>
                                    </a>
                                    <div class="customShoppingCart">
                                        <form method="post" action="<?php echo base_url('Pages/addToCart'); ?>"
                                              name="addToCartForm">
                                            <?php
                                            $wh['product_id'] = $product->product_id;
                                            $wh['product_attribute_id'] = $attributes ? $attributes[0]->product_attribute_id : '';
                                            $wh['category_id'] = $product->category_id;
                                            $wh['unique_id'] = $this->input->cookie('unique_id');
                                            $exist = $this->md->select_where('tbl_cart', $wh);
                                            ?>
                                            <input type="hidden" name="quantity"
                                                   value="<?php echo $exist ? ($exist[0]->qty + 1) : 1; ?>"/>
                                            <input type="hidden" name="productId"
                                                   value="<?php echo $product->product_id; ?>"/>
                                            <input type="hidden" name="productAttributeId"
                                                   value="<?php echo $attributes ? $attributes[0]->product_attribute_id : ''; ?>"/>
                                            <input type="hidden" name="categoryId"
                                                   value="<?php echo $product->category_id; ?>"/>
                                            <input type="hidden" name="price"
                                                   value="<?php echo $product->sale ? $product->sale_price : $product->price; ?>"/>
                                            <button name="addToCart" value="addToCart" type="submit" class="cartIcon">
                                                <svg width="24" height="24" xmlns="http://www.w3.org/2000/svg"
                                                     fill-rule="evenodd" clip-rule="evenodd">
                                                    <path
                                                        d="M13.5 18c-.828 0-1.5.672-1.5 1.5 0 .829.672 1.5 1.5 1.5s1.5-.671 1.5-1.5c0-.828-.672-1.5-1.5-1.5zm-3.5 1.5c0 .829-.672 1.5-1.5 1.5s-1.5-.671-1.5-1.5c0-.828.672-1.5 1.5-1.5s1.5.672 1.5 1.5zm14-16.5l-.743 2h-1.929l-3.473 12h-13.239l-4.616-11h2.169l3.776 9h10.428l3.432-12h4.195zm-12 4h3v2h-3v3h-2v-3h-3v-2h3v-3h2v3z"/>
                                                </svg>
                                            </button>
                                        </form>
                                    </div>
                                </figure>
                                <div class="product-details  p-10 pl-0">
                                    <h3 class="englishText product-name font-16 font-weight-600 text-000">
                                        <a href="<?php echo $url; ?>"><?php echo $product->title; ?></a>
                                    </h3>
                                    <h3
                                        class="arabicText product-name font-16 font-weight-700 text-000">
                                        <a href="<?php echo $url; ?>"><?php echo $product->arabic_title; ?></a>
                                    </h3>
                                    <div class="product-price">
                                        <?php
                                        if ($product->sale) {
                                            ?>
                                            <del
                                                class="price d-xs-block text-999 font-weight-normal"><?php echo number_format($product->price); ?>
                                                SAR
                                            </del>
                                            <span
                                                class="ml-lg-2 ml-md-2 ml-xs-0 price font-weight-normal"><?php echo $product->sale_price ? number_format($product->sale_price) : $product->price; ?> SAR</span>
                                            <?php
                                        } else {
                                            ?>
                                            <span
                                                class="price font-weight-normal"><?php echo number_format($product->price); ?> SAR</span>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <div class="product-variations justify-content-start">
                                        <?php
                                        $allattributes = $this->md->my_query('select * from `tbl_product_attribute` where `product_id` =' . $product->product_id)->result();
                                        if ($allattributes) {
                                            foreach ($allattributes as $key => $attribute) {
                                                $photos = $attribute->product_photos ? explode(",", $attribute->product_photos) : array(FILENOTFOUND);
                                                $colour = $this->md->select_where('tbl_colours', array('colour' => $attribute->color));
                                                ?>
                                                <a class="color <?php echo $key == 0 ? 'active' : ''; ?> mr-1 <?php echo $key >= 6 ? 'hiddenColor' : ''; ?>"
                                                   data-src="<?php echo base_url($photos[0]); ?>"
                                                   href="javascript:void(0)"
                                                   style="
                                                       background-color: <?php echo $colour ? $colour[0]->code : '#fff'; ?>;
                                                   <?php echo $attribute->color == 'white' ? 'border:1px solid #dfdfdf' : ''; ?>;
                                                   <?php echo $key >= 6 ? 'display:none' : 'display:block'; ?>
                                                       "
                                                ></a>
                                                <?php
                                            }
                                            if (count($allattributes) >= 6) {
                                                ?>
                                                <img src="<?php echo base_url('assets/images/plus.png'); ?>"
                                                     width="15px" alt="plus" class="ml-1 mr-1 showHiddenColor"/>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    } else {
                        echo '<center>';
                        echo "<img src='" . base_url('assets/images/no-product.jpg') . "' style='width:40%' />";
                        echo '</center>';
                    }
                    ?>
                </div>
            </section>

        </div>
    </main>
    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
</body>