<?php
echo $page_head;
$admin_data = $this->md->select('tbl_web_data')[0];

$user = $this->session->userdata('email');
$user_Type = $this->session->userdata('userType');
$userdata = $product = array();
if (isset($user)) :
    $userdata = $this->md->select_where('tbl_register', array('email' => $user));
    if ($user_Type == 'guest') {
        $product = $this->md->select_where('tbl_cart', array('email' => $user));
    } else {
        $product = $this->md->select_where('tbl_cart', array('register_id' => ($userdata ? $userdata[0]->register_id : '')));
    }
else:
    $product = $this->md->select_where('tbl_cart', array('unique_id' => $this->input->cookie('unique_id')));
endif;

?>
<link href="assets/css/paymentform.css" rel="stylesheet"/>
<body>
<?php echo $page_header; ?>
<main id="content">
    <?php echo $page_breadcumb; ?>
    <section class="pb-lg-13 pb-11">
        <div class="container-fluid">
            <h2 class="text-center my-9">Check Out</h2>

            <div class="row">
                <div class="col-lg-4 pb-lg-0 pb-11 order-lg-last">
                    <div class="card border-0 p-5" style="box-shadow: 0 0 10px 0 rgba(0,0,0,0.1)">
                        <h4 class="fs-4 mb-5 mt-2">Order Summary</h4>
                        <table class="table table-bordered">
                            <?php
                            $total = 0;
                            if (empty($product)) {
                                echo '<tr><td colspan="6"><div class="alert alert-warning p-3 border-radius-10 m-2">Sorry, Product not available!</div></td></tr>';
                            } else {
                                $couponDiscount = 0;
                                foreach ($product as $pro_data) {
                                    $product_data = $this->md->select_where('tbl_product', array('product_id' => $pro_data->product_id));
                                    if ($product_data) :
                                        $product_data = $product_data[0];

                                        $cateData = $this->md->select_where('tbl_category', array('category_id' => $product_data->category_id));   // Get Category Data
                                        $url = base_url('product/' . ($cateData[0]->slug) . '/' . strtolower($product_data->slug));
                                        $img = explode(",", $product_data->photos);

                                        // Check coupon code
                                        $coupon_code = $this->session->userdata('coupon_code');
                                        $wh22['coupon_id'] = $coupon_code;
                                        $check_codes = $this->md->select_where('tbl_coupon', $wh22);
                                        // Get Size Data
                                        $sizeData = $this->md->select_where('tbl_size', array('size_id' => $pro_data->size));
                                        ?>
                                        <tr>
                                            <td colspan="2">
                                                <a target="_blank" href="<?php echo $url; ?>" class="font-14">
                                                    <?php echo $product_data->title; ?>
                                                </a>
                                                <p class="font-12 mb-0"><?php echo ucfirst($pro_data->order_type) . " - " . ($sizeData[0]->size . $sizeData[0]->measurement); ?></p>
                                                <p class="font-weight-500 mb-0 font-14 text-uppercase">
                                                    <?php
                                                    if (!empty($check_codes)) {
                                                        $discount_product = json_decode($check_codes[0]->product);
                                                        if ($discount_product) {
                                                            if (in_array($pro_data->product_id, $discount_product)) {
                                                                // Applied
                                                                echo "<span class='font-12'>Coupon Applied: <label class='badge text-bg-primary rounded-pill font-12 text-uppercase'>" . $check_codes[0]->coupon_code . "</label></span>";
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                </p>
                                            </td>
                                            <td>
                                                <?php
                                                if (!empty($check_codes)) {
                                                    $discount_product = json_decode($check_codes[0]->product);
                                                    $pro_check = "no";
                                                    if ($discount_product) {
                                                        if (in_array($pro_data->product_id, $discount_product)) {
                                                            // Applied
                                                            $pro_check = "yes";
                                                            $coupon_pro_discount = ($pro_data->price * $check_codes[0]->coupon_percentage) / 100;
                                                            echo "<p class='font-12 text-muted m-0'><del>" . "$" . $pro_data->price . "</del></p>";
                                                            echo "$";
                                                            echo number_format($pro_data->price - $coupon_pro_discount,2);
                                                            $total = $total + (($pro_data->price - $coupon_pro_discount) * $pro_data->qty);
                                                            $couponDiscount = $couponDiscount + ($coupon_pro_discount * $pro_data->qty);
                                                        } else {
                                                            // Not Applied
                                                            echo "$";
                                                            echo $pro_data->price;
                                                            $total = $total + ($pro_data->price * $pro_data->qty);
                                                        }
                                                    }
                                                } else {
                                                    echo $pro_data->price;
                                                    $total = $total + ($pro_data->price * $pro_data->qty);
                                                }
                                                ?>

                                                X <?php echo $pro_data->qty; ?>
                                            </td>
                                            <!--                                            <td><p class="mb-0">-->
                                            <!--                                                    $-->
                                            <?php //echo $pro_data->netprice ? number_format($pro_data->netprice, 2) : '';
                                            ?><!--</p>-->
                                            <!--                                            </td>-->
                                        </tr>
                                    <?php
                                    endif;
                                }
                            }
                            ?>
                            <tr>
                                <td colspan="2" class="font-weight-bold font-16">
                                    Sub Total
                                </td>
                                <td class="font-16 font-weight-bold">
                                    $<?php echo($total == 0 ? '0.00' : ($total ? number_format($total, 2) : '')); ?></td>
                            </tr>
                            <?php
                            //echo "coupon dis: " . $couponDiscount;
                            //echo "total: " . $total;
                            $coupon_code = $this->session->userdata('coupon_code');
                            if ($coupon_code != "") {
                                $check_code = $this->md->select_where('tbl_coupon', array('coupon_id' => $coupon_code));
                                if (!empty($check_code)) {

                                    //$total = ($check_code[0]->coupon_percentage == 100 ? $total : ($total - $couponDiscount));
                                    ?>
                                    <tr>
                                        <td colspan="2" class="font-weight-bold font-16">
                                            Shipping Charges
                                        </td>
                                        <td class="font-16 font-weight-bold <?php echo ($check_code[0]->free_shipping != 'yes') ? 'shippingCharge' : ''; ?>">
                                            <input type="hidden" id="shippingStatus"
                                                   value="<?php echo ($check_code[0]->free_shipping == 'yes') ? 'yes' : 'no'; ?>">
                                            <?php echo ($check_code[0]->free_shipping == 'yes') ? 'FREE' : (($total >= 120) ? 'FREE' : '+ $0.00'); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="font-weight-bold font-16">
                                            <div class="d-flex align-items-center">
                                                <span>Coupon Applied:
                                                    <label
                                                        class="badge border text-000 text-uppercase font-13"><?php echo $check_code[0]->coupon_code; ?></label>
                                                </span>
                                            </div>
                                            <form method="post">
                                                <button class="btn btn-danger btn-xs" name="coupon_remove"
                                                        value="remove" type="submit">Remove
                                                </button>
                                            </form>
                                        </td>
                                        <td class="font-16 font-weight-bold">
                                            $<?php
                                            echo number_format($couponDiscount, 2);
                                            ?>
                                            <br/>
                                            <small>Total Discount</small>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            } else {
                                ?>
                                <tr>
                                    <td colspan="2" class="font-weight-bold font-16">
                                        Shipping Charges
                                    </td>
                                    <td class="font-16 font-weight-bold shippingCharge">
                                        <input type="hidden" id="shippingStatus" value="no">
                                        <?php echo ($total >= 120) ? 'FREE' : '+ $0.00'; ?>
                                    </td>
                                </tr>
                                <?php
                            }
                            // Calculate Tax
                            $tax = ($total != 0 ? ($total ? number_format(($total) * 15 / 100, 2) : 0) : '0');
                            ?>
                            <tr>
                                <td colspan="2" class="font-weight-bold font-16">
                                    Tax (15%)
                                    <input type="hidden" form="paymentFrm" value="<?php echo $tax; ?>" name="tax" id="tax">
                                </td>
                                <td class="font-16 font-weight-bold tax">
                                    + $<?php echo $tax; ?>
                                </td>
                            </tr>
                            <tr class="bg-F2F2F2">
                                <td colspan="2" class="font-weight-bold font-20">
                                    Grand Total
                                </td>
                                <td class="font-22 text-success font-weight-bold grandTotal">
                                    $<?php echo($total == 0 ? '0.00' : ($total ? number_format($total + $tax, 2) : '')); ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-lg-8 pr-xl-15 order-lg-first form-control-01 bg-white">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="border p-5 shadow">
                                Have a coupon?
                                <a data-bs-toggle="collapse" href="#collapsecoupon" role="button"
                                   aria-expanded="false"
                                   aria-controls="collapsecoupon">Click here to enter your code</a>

                                <div class="mt-5 mb-5 collapse" id="collapsecoupon">
                                    <div class="card bg-FFF mw-60 border shadow">
                                        <div class="card-body py-5 px-8 border">
                                            <p class="card-text text-body-emphasis mb-8">
                                                If you have a coupon code, please apply it below.</p>
                                            <form method="post">
                                                <div class="input-group position-relative">
                                                    <input type="text"
                                                           class="form-control bg-body rounded-end border"
                                                           name="coupon_code"
                                                           placeholder="Enter Coupon Code*">
                                                    <button type="submit"
                                                            name="apply_coupon" value="Apply coupon"
                                                            class="btn btn-dark btn-hover-bg-primary btn-hover-border-primary">
                                                        Apply Coupon
                                                    </button>
                                                </div>
                                                <div class="error-text p-0 m-0">
                                                    <?php
                                                    if (form_error('coupon_code')) {
                                                        echo form_error('coupon_code');
                                                    }
                                                    ?>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    if ($user == "") {
                        if (isset($error)) {
                            ?>
                            <div class="alert alert-danger p-1">
                                <?php echo $error; ?>
                            </div>
                            <?php
                        }
                        if (isset($success)) {
                            ?>
                            <div class="alert alert-success p-1">
                                <?php echo $success; ?>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="contact-form p-30">
                                    <h4 class="fs-34 mb-6">Returning customers</h4>
                                    <p class="fs-16 mb-7">Don't have an account yet? <a
                                            href="<?php echo base_url('user-register'); ?>"
                                            class="text-secondary border-bottom text-decoration-none">Sign up</a> for
                                        free
                                    </p>
                                    <form method="post" name="login">
                                        <div class="mb-8">
                                            <input type="text" autofocus="" name="username" value="<?php
                                            if (set_value('username') && !isset($success)) {
                                                echo set_value('username');
                                            }
                                            ?>" class="form-control border-0" placeholder="Your Email or Phone">
                                            <div class="error-text p-0 m-0">
                                                <?php
                                                if (form_error('username')) {
                                                    echo form_error('username');
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="mb-8">
                                            <input type="password" value="<?php
                                            if (set_value('password') && !isset($success)) {
                                                echo set_value('password');
                                            }
                                            ?>" class="form-control border-0" name="password"
                                                   placeholder="Your Password">
                                            <div class="error-text p-0 m-0">
                                                <?php
                                                if (form_error('password')) {
                                                    echo form_error('password');
                                                }
                                                ?>
                                            </div>
                                        </div>

                                        <button type="submit" value="send" name="login"
                                                class="btn btn-dark btn-hover-bg-primary btn-hover-border-primary px-11">
                                            SIGN IN
                                        </button>

                                        <a href="<?php echo base_url('user-register'); ?>"
                                           class="mt-10 text-decoration-underline text-center d-block">Don't have an
                                            account yet?</a>
                                    </form>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="contact-form p-30">
                                    <h4 class="fs-34 mb-6">Guest Checkout</h4>
                                    <p class="fs-16 mb-7">You can check out without creating an account. You will have a
                                        chance to create an account later.
                                    </p>
                                    <form method="post" name="guest">
                                        <div class="mb-8">
                                            <input type="email" required name="g_username" value="<?php
                                            if (set_value('g_username') && !isset($success)) {
                                                echo set_value('g_username');
                                            }
                                            ?>" class="form-control border-0" placeholder="Your Email">
                                            <div class="error-text p-0 m-0">
                                                <?php
                                                if (form_error('g_username')) {
                                                    echo form_error('g_username');
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="mb-8">
                                            <?php
                                            if ($web_data[0]->captcha_visibility):
                                                echo '<div class="g-recaptcha" data-sitekey="' . $web_data[0]->captcha_site_key . '"></div>';
                                            endif;
                                            ?>
                                        </div>

                                        <button type="submit" value="send" name="guestCheckout"
                                                class="btn btn-dark btn-hover-bg-primary btn-hover-border-primary px-11">
                                            CHECKOUT AS GUEST
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <?php
                    } else {
                        ?>
                        <!--                        <form method="post" name="checkout" id="paymentFrm"-->
                        <!--                              action="--><?php //echo base_url('User/callBack'); ?><!--">-->
                        <form method="post" name="paymentFrm" id="paymentFrm">
                            <h4 class="fs-24 pt-1 mb-9 mt-5">Personal Information</h4>
                            <div class="mb-5">
                                <label class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase">Full
                                    name</label>
                                <div class="row">
                                    <div class="col-md-12 mb-md-0 mb-4">
                                        <input type="text" class="form-control border-0" id="first-name"
                                               name="fname"
                                               required=""
                                               value="<?php echo $userdata ? $userdata[0]->fname : ''; ?>">
                                        <div class="error-text p-0 m-0">
                                            <?php
                                            if (form_error('fname')) {
                                                echo form_error('fname');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-5">
                                <label
                                    class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase">info</label>
                                <div class="row">
                                    <div class="col-md-6 mb-md-0 mb-4">
                                        <input type="email" class="form-control border-0" id="email" name="email"
                                               placeholder="Email" required=""
                                               value="<?php echo $userdata ? $userdata[0]->email : $this->session->userdata('email'); ?>">
                                        <div class="error-text p-0 m-0">
                                            <?php
                                            if (form_error('email')) {
                                                echo form_error('email');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="number" class="form-control border-0" id="phone" name="phone"
                                               placeholder="Phone number" required=""
                                               value="<?php echo $userdata ? $userdata[0]->phone : ''; ?>">
                                        <div class="error-text p-0 m-0">
                                            <?php
                                            if (form_error('phone')) {
                                                echo form_error('phone');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h4 class="fs-24 pt-1 mt-13 mb-9">Shipping Address</h4>
                            <div class="mb-5">
                                <div class="row">
                                    <div class="col-md-12 mb-md-0 mb-4">
                                        <label for="street-address"
                                               class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase">Street
                                            Address <code>*Enter only street name!</code></label>
                                        <div class="input-group">
                                            <input type="text" class="form-control border-0"
                                                   id="street-address"
                                                   name="address"
                                                   required=""
                                                   value=""/>
                                            <div class="input-group-append">
                                                <div id="findAddress">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="popup">

                                        </div>
                                        <div class="error-text p-0 m-0">
                                            <?php
                                            if (form_error('address')) {
                                                echo form_error('address');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-5 addressDetail">
                                <div class="row">
                                    <div class="col-md-3 mb-md-0 mb-4">
                                        <label for="city"
                                               class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase">Country</label>
                                        <input type="text" class="form-control border-0" id="country" name="country"
                                               required=""
                                               value="<?php echo $userdata ? $userdata[0]->country : ''; ?>">
                                        <div class="error-text p-0 m-0">
                                            <?php
                                            if (form_error('country')) {
                                                echo form_error('country');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-3 mb-md-0 mb-4">
                                        <label for="state"
                                               class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase">State</label>
                                        <input type="text" class="form-control border-0" id="state" name="state"
                                               required=""
                                               value="<?php echo $userdata ? $userdata[0]->state : ''; ?>">
                                        <div class="error-text p-0 m-0">
                                            <?php
                                            if (form_error('state')) {
                                                echo form_error('state');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-3 mb-md-0 mb-4">
                                        <label for="city"
                                               class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase">City</label>
                                        <input type="text" class="form-control border-0" id="city" name="city"
                                               required=""
                                               value="<?php echo $userdata ? $userdata[0]->city : ''; ?>">
                                        <div class="error-text p-0 m-0">
                                            <?php
                                            if (form_error('city')) {
                                                echo form_error('city');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-3 mb-md-0 mb-4">
                                        <label for="zip"
                                               class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase">zip
                                            code</label>
                                        <input type="number" class="form-control border-0" id="zip"
                                               name="postal_code"
                                               required=""
                                               value="<?php echo $userdata ? $userdata[0]->postal_code : ''; ?>">
                                        <div class="error-text p-0 m-0">
                                            <?php
                                            if (form_error('postal_code')) {
                                                echo form_error('postal_code');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-5">
                                <div class="row">
                                    <div class="col-md-12 mb-md-0 mb-4">
                                        <label for="notes"
                                               class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase">Extra
                                            Notes</label>
                                        <textarea class="form-control border-0" id="notes" name="notes"></textarea>
                                    </div>
                                </div>
                            </div>
                            <h4 class="fs-24 pt-1 mt-13 mb-9">Payment Information</h4>
                            <section class="paymentForm">
                                <div class="container preload">
                                    <div class="creditcard">
                                        <div class="front">
                                            <div id="ccsingle"></div>
                                            <svg version="1.1" id="cardfront" xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                 x="0px" y="0px" viewBox="0 0 750 471"
                                                 style="enable-background:new 0 0 750 471;" xml:space="preserve">
                                                    <g id="Front">
                                                        <g id="CardBackground">
                                                            <g id="Page-1_1_">
                                                                <g id="amex_1_">
                                                                    <path id="Rectangle-1_1_" class="lightcolor grey"
                                                                          d="M40,0h670c22.1,0,40,17.9,40,40v391c0,22.1-17.9,40-40,40H40c-22.1,0-40-17.9-40-40V40
                                                                            C0,17.9,17.9,0,40,0z"/>
                                                                </g>
                                                            </g>
                                                            <path class="darkcolor greydark"
                                                                  d="M750,431V193.2c-217.6-57.5-556.4-13.5-750,24.9V431c0,22.1,17.9,40,40,40h670C732.1,471,750,453.1,750,431z"/>
                                                        </g>
                                                        <text transform="matrix(1 0 0 1 60.106 295.0121)" id="svgnumber"
                                                              class="st2 st3 st4">0123 4567
                                                            8910 1112
                                                        </text>
                                                        <text transform="matrix(1 0 0 1 54.1064 428.1723)" id="svgname"
                                                              class="st2 st5 st6">JOHN DOE
                                                        </text>
                                                        <text transform="matrix(1 0 0 1 54.1074 389.8793)"
                                                              class="st7 st5 st8">cardholder name
                                                        </text>
                                                        <text transform="matrix(1 0 0 1 479.7754 388.8793)"
                                                              class="st7 st5 st8">expiration
                                                        </text>
                                                        <text transform="matrix(1 0 0 1 65.1054 241.5)"
                                                              class="st7 st5 st8">card number
                                                        </text>
                                                        <g>
                                                            <text transform="matrix(1 0 0 1 574.4219 433.8095)"
                                                                  id="svgexpire" class="st2 st5 st9">
                                                                01/23
                                                            </text>
                                                            <text transform="matrix(1 0 0 1 479.3848 417.0097)"
                                                                  class="st2 st10 st11">VALID
                                                            </text>
                                                            <text transform="matrix(1 0 0 1 479.3848 435.6762)"
                                                                  class="st2 st10 st11">THRU
                                                            </text>
                                                            <polygon class="st2"
                                                                     points="554.5,421 540.4,414.2 540.4,427.9 		"/>
                                                        </g>
                                                        <g id="cchip">
                                                            <g>
                                                                <path class="st2" d="M168.1,143.6H82.9c-10.2,0-18.5-8.3-18.5-18.5V74.9c0-10.2,8.3-18.5,18.5-18.5h85.3
                                                                c10.2,0,18.5,8.3,18.5,18.5v50.2C186.6,135.3,178.3,143.6,168.1,143.6z"/>
                                                            </g>
                                                            <g>
                                                                <g>
                                                                    <rect x="82" y="70" class="st12" width="1.5"
                                                                          height="60"/>
                                                                </g>
                                                                <g>
                                                                    <rect x="167.4" y="70" class="st12" width="1.5"
                                                                          height="60"/>
                                                                </g>
                                                                <g>
                                                                    <path class="st12" d="M125.5,130.8c-10.2,0-18.5-8.3-18.5-18.5c0-4.6,1.7-8.9,4.7-12.3c-3-3.4-4.7-7.7-4.7-12.3
                                                                    c0-10.2,8.3-18.5,18.5-18.5s18.5,8.3,18.5,18.5c0,4.6-1.7,8.9-4.7,12.3c3,3.4,4.7,7.7,4.7,12.3
                                                                    C143.9,122.5,135.7,130.8,125.5,130.8z M125.5,70.8c-9.3,0-16.9,7.6-16.9,16.9c0,4.4,1.7,8.6,4.8,11.8l0.5,0.5l-0.5,0.5
                                                                    c-3.1,3.2-4.8,7.4-4.8,11.8c0,9.3,7.6,16.9,16.9,16.9s16.9-7.6,16.9-16.9c0-4.4-1.7-8.6-4.8-11.8l-0.5-0.5l0.5-0.5
                                                                    c3.1-3.2,4.8-7.4,4.8-11.8C142.4,78.4,134.8,70.8,125.5,70.8z"/>
                                                                </g>
                                                                <g>
                                                                    <rect x="82.8" y="82.1" class="st12" width="25.8"
                                                                          height="1.5"/>
                                                                </g>
                                                                <g>
                                                                    <rect x="82.8" y="117.9" class="st12" width="26.1"
                                                                          height="1.5"/>
                                                                </g>
                                                                <g>
                                                                    <rect x="142.4" y="82.1" class="st12" width="25.8"
                                                                          height="1.5"/>
                                                                </g>
                                                                <g>
                                                                    <rect x="142" y="117.9" class="st12" width="26.2"
                                                                          height="1.5"/>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                <g id="Back">
                                                </g>
                                            </svg>
                                        </div>
                                        <div class="back">
                                            <svg version="1.1" id="cardback" xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                 x="0px" y="0px" viewBox="0 0 750 471"
                                                 style="enable-background:new 0 0 750 471;" xml:space="preserve">
                                                <g id="Front">
                                                    <line class="st0" x1="35.3" y1="10.4" x2="36.7" y2="11"/>
                                                </g>
                                                <g id="Back">
                                                    <g id="Page-1_2_">
                                                        <g id="amex_2_">
                                                            <path id="Rectangle-1_2_" class="darkcolor greydark" d="M40,0h670c22.1,0,40,17.9,40,40v391c0,22.1-17.9,40-40,40H40c-22.1,0-40-17.9-40-40V40
                                                            C0,17.9,17.9,0,40,0z"/>
                                                        </g>
                                                    </g>
                                                    <rect y="61.6" class="st2" width="750" height="78"/>
                                                    <g>
                                                        <path class="st3" d="M701.1,249.1H48.9c-3.3,0-6-2.7-6-6v-52.5c0-3.3,2.7-6,6-6h652.1c3.3,0,6,2.7,6,6v52.5
                                                         C707.1,246.4,704.4,249.1,701.1,249.1z"/>
                                                        <rect x="42.9" y="198.6" class="st4" width="664.1"
                                                              height="10.5"/>
                                                        <rect x="42.9" y="224.5" class="st4" width="664.1"
                                                              height="10.5"/>
                                                        <path class="st5"
                                                              d="M701.1,184.6H618h-8h-10v64.5h10h8h83.1c3.3,0,6-2.7,6-6v-52.5C707.1,187.3,704.4,184.6,701.1,184.6z"/>
                                                    </g>
                                                    <text transform="matrix(1 0 0 1 621.999 227.2734)"
                                                          id="svgsecurity"
                                                          class="st6 st7">985
                                                    </text>
                                                    <g class="st8">
                                                        <text transform="matrix(1 0 0 1 518.083 280.0879)"
                                                              class="st9 st6 st10">security code
                                                        </text>
                                                    </g>
                                                    <rect x="58.1" y="378.6" class="st11" width="375.5"
                                                          height="13.5"/>
                                                    <rect x="58.1" y="405.6" class="st11" width="421.7"
                                                          height="13.5"/>
                                                    <text transform="matrix(1 0 0 1 59.5073 228.6099)"
                                                          id="svgnameback"
                                                          class="st12 st13">John Doe
                                                    </text>
                                                </g>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-container">
                                    <div class="field-container">
                                        <label class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase"
                                               for="name">Name</label>
                                        <input id="name" maxlength="20" type="text" name="card_name"
                                               required="">
                                        <div class="error-text p-0 m-0">
                                            <?php
                                            if (form_error('card_name')) {
                                                echo form_error('card_name');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="field-container">
                                        <label class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase"
                                               for="cardnumber">Card Number</label>
                                        <input id="cardnumber" type="text" inputmode="numeric"
                                               name="card_number" required>
                                        <svg id="ccicon" class="ccicon" width="750" height="471"
                                             viewBox="0 0 750 471"
                                             version="1.1" xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink">
                                        </svg>
                                        <div class="error-text p-0 m-0">
                                            <?php
                                            if (form_error('card_number')) {
                                                echo form_error('card_number');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="field-container">
                                        <label class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase"
                                               for="expirationdate">Expiration (mm/yy)</label>
                                        <input id="expirationdate" type="text" inputmode="numeric">
                                        <input type="hidden" id="stripe-card-expiry-month" name="month">
                                        <input type="hidden" id="stripe-card-expiry-year" name="year">
                                        <div class="error-text p-0 m-0">
                                            <?php
                                            if (form_error('year')) {
                                                echo form_error('year');
                                            }
                                            ?>
                                        </div>
                                        <div class="error-text p-0 m-0">
                                            <?php
                                            if (form_error('month')) {
                                                echo form_error('month');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="field-container">
                                        <label class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase"
                                               for="securitycode">Security Code</label>
                                        <input id="securitycode" type="text" pattern="[0-9]*" inputmode="numeric"
                                               name="cvv" required="">
                                        <div class="error-text p-0 m-0">
                                            <?php
                                            if (form_error('cvv')) {
                                                echo form_error('cvv');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <span id="success-msg" class="payment-errors"></span>

                            <input type="hidden" name="subtotal" id="sub-total" value="<?php echo $total; ?>">
                            <input type="hidden" name="amount" id="grandTotal" value="<?php echo $total; ?>">
                            <input type="hidden" name="couponDiscount"
                                   value="<?php echo $couponDiscount; ?>">
                            <input type="hidden" id="shipping-charge" name="shipping_charge">

                            <button type="submit"
                                    id="payBtn"
                                    value="place order" name="place_order"
                                    class="btn btn-dark btn-hover-bg-primary btn-hover-border-primary px-11 mt-md-7 mt-4">
                                Place Order
                            </button>
                        </form>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </section>
</main>
<?php echo $page_footer; ?>
<?php echo $page_footerscript; ?>
<script>

    /*--------------------
    FIND DELIVERY ADDRESS
    ---------------------*/
    // $('#street-address').keypress(function (e) {
    //     if (e.which === 32)
    //         return false;
    // });

    $(document).on("keyup", "#street-address", function () {
        const val = $.trim($('#street-address').val());
        if (val.length >= 5) {
            $('#findAddress').html('<i class="fa fa-spinner"></i> Searching ...');
            var data = {
                val: val
            };
            var url = "<?php echo base_url('User/getDeliveryAddress'); ?>";
            jQuery.post(url, data, function (data) {
                $('#findAddress').html('');
                $('.popup').show();
                $('.popup').html(data);
            });
        }
    });

    /*--------------------
    FIND ADDRESS DETAILS FROM DPID
    ---------------------*/
    $(document).on("click", ".options tr td", function () {
        const val = $.trim($(this).attr('id'));
        $('#findAddress').html('<i class="fa fa-spinner"></i> Fetching ...');
        $('.popup').html('');
        $('.popup').hide();
        const address = $(this).text();
        var data = {
            val: val
        };
        var url = "<?php echo base_url('User/getAddressDetail'); ?>";
        jQuery.post(url, data, function (data) {
            var grandTotal = parseInt($('#grandTotal').val());
            if (grandTotal <= 120) {
                var shippingStatus = $('#shippingStatus').val();
                if (shippingStatus === 'no') {
                    calculateShippingCharge(val);  // Calculate Shipping Charge
                }
            }
            $('.popup').html('');
            $('.popup').hide();
            $('#findAddress').html('');
            $('#street-address').val(address);
            $('.addressDetail').html(data);
        });
    });

    function calculateShippingCharge(dpid) {
        var data = {
            dpid: dpid
        };
        $('.shippingCharge').html('Loading...');
        var url = "<?php echo base_url('User/getCalculateShippingCharge'); ?>";
        jQuery.post(url, data, function (data) {
            if (data === "ERR.") {
                $('#shipping-charge').val('');
                $('.shippingCharge').html(data);
            } else {
                let subTotal = parseFloat($('#sub-total').val());
                let grandTotal = subTotal + parseFloat(data);
                $('#shipping-charge').val(data);
                $('.shippingCharge').html('$' + data);
                // Calculate Tax
                let tax = grandTotal * 15 / 100;
                $('.tax').html('$' + tax.toFixed(2));
                $('#tax').val(tax.toFixed(2));
                // Grand Total + Tax
                $('.grandTotal').html('$' + ((grandTotal + tax).toFixed(2)));
                $('#grandTotal').val((grandTotal + tax).toFixed(2));
            }
        });
    }


</script>
<script src="assets/js/imask.min.js"></script>
<script src="assets/js/paymentform.js"></script>
<script src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
    //set your publishable key
    Stripe.setPublishableKey('<?php echo STRIPE_PUBLISHABLE_KEY; ?>');

    //callback to handle the response from stripe
    function stripeResponseHandler(status, response) {
        if (response.error) {
            //enable the submit button
            $('#payBtn').removeAttr("disabled");
            //display the errors on the form
            $(".payment-errors").html('<div class="alert alert-danger">' + response.error.message + '</div>');
        } else {
            var form$ = $("#paymentFrm");
            //get token id
            var token = response['id'];
            //insert the token into the form
            form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
            //submit form to the server
            form$.get(0).submit();
        }
    }

    $(document).ready(function () {
        //on form submit
        $("#paymentFrm").submit(function (event) {
            //disable the submit button to prevent repeated clicks
            $('#payBtn').attr("disabled", "disabled");
            //create single-use token to charge the user
            Stripe.createToken({
                number: $('#cardnumber').val(),
                cvc: $('#securitycode').val(),
                exp_month: $('#stripe-card-expiry-month').val(),
                exp_year: $('#stripe-card-expiry-year').val()
            }, stripeResponseHandler);
            //submit from callback
            return false;
        });
    });
</script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
</body>