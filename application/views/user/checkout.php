<?php
echo $page_head;
$web_data = ($web_data) ? $web_data[0] : '';
$cartProduct = $this->md->select_where('tbl_cart', array('unique_id' => $this->input->cookie('unique_id')));
$countries = $this->md->select('tbl_country');
?>
<body class="home">
<div class="page-wrapper">
    <header class="checkout-header">
        <div class="container p-0">
            <div class="d-flex justify-content-between">
                <div class="col-md-6">
                    <a href="<?php echo base_url(); ?>" class="logo">
                        <img src="<?php echo base_url($web_data ? $web_data->logo : FILENOTFOUND); ?>"
                             alt="<?php echo $web_data ? $web_data->project_name : ''; ?>"
                             width="150"
                             height="44"
                             title="<?php echo $web_data ? $web_data->project_name : ''; ?>"/>
                    </a>
                </div>
<!--                <div class="col-md-6">-->
<!--                    <div class="text-right mt-3">-->
<!--                        <a href="--><?php //echo base_url('cart'); ?><!--" class="cart-toggle label-block link">-->
<!--                            <i class="d-icon-bag font-20"></i>-->
<!--                        </a>-->
<!--                    </div>-->
<!--                </div>-->
            </div>
        </div>
    </header>

    <main class="checkout-body">

        <div class="container-fluid p-0 d-block d-md-none d-lg-none">
            <div class="mb-30">
                <div class="custom-accordion bg-F5F5F5">
                    <div class="accordion-item">
                        <div class="accordion-item-header pr-1">
                            <div class="acc-title">
                                <?php echo lang('show_order_summary'); ?>
                            </div>
                            <span class="font-18 font-weight-bold totalAmount2">Loading...</span>
                        </div><!-- /.accordion-item-header -->
                        <div class="accordion-item-body">
                            <div class="accordion-item-body-content">
                                <div class="row justify-content-start">
                                    <div class="col-md-12 col-lg-10 col-sm-12 col-xs-12">

                                        <table class="shop-table cart-table">
                                            <tbody class="border-none">
                                            <?php
                                            $total = 0;
                                            if ($cartProduct) {
                                                foreach ($cartProduct as $cartitem) {
                                                    $productData = $this->md->select_where('tbl_product', array('product_id' => $cartitem->product_id));
                                                    if ($productData) {
                                                        $product = $productData[0];
                                                        $attributes = $this->md->select_where('tbl_product_attribute', array('product_attribute_id' => $cartitem->product_attribute_id));

                                                        $photos = array(FILENOTFOUND);
                                                        if ($attributes) {
                                                            $photos = $attributes[0]->product_photos ? explode(",", $attributes[0]->product_photos) : array(FILENOTFOUND);
                                                        }

                                                        $url = base_url('product/' . $product->slug . "/" . $product->product_id . "/?color=" . $attributes[0]->color);
                                                        $total = $total + $cartitem->netprice; // Net total
                                                        ?>
                                                        <tr class="p-xs-0 mb-xs-20 border-none">
                                                            <td class="product-thumbnail m-xs-5 border-none"
                                                                width="75%">
                                                                <div class="d-flex w-100 mb-20">
                                                                    <figure class="position-relative">
                                                                        <img
                                                                            src="<?php echo base_url($photos ? ($photos[0] ? $photos[0] : FILENOTFOUND) : FILENOTFOUND); ?>"
                                                                            alt="<?php echo $product->title; ?>"
                                                                            title="<?php echo $product->title; ?>"
                                                                            style="width: 60px;height: 60px;object-fit: contain;border-radius: 10px;background: #fff"
                                                                        >
                                                                        <span
                                                                            class="position-absolute cartCounter"><?php echo $cartitem->qty; ?></span>
                                                                    </figure>
                                                                    <span
                                                                        class="font-14 englishText mt-20 ml-xs-0 ml-3"
                                                                        href="<?php echo $url; ?>"><?php echo $productData ? ($productData[0]->title) : ''; ?>
                                                                    </span>
                                                                    <span
                                                                        class="font-14 arabicText mt-20 ml-xs-0 mr-3"
                                                                        href="<?php echo $url; ?>"><?php echo $productData ? ($productData[0]->arabic_title) : ''; ?>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                            <td width="25%" class="border-none text-right">
                                                                <span
                                                                    class="amount">SAR <?php echo $cartitem->netprice ? number_format($cartitem->netprice, 2) : ''; ?></span>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>

                                        <div class="mt-30 d-flex justify-content-between mb-2">
                                            <div class="">
                                                <?php echo lang('subtotal'); ?>
                                            </div>
                                            <div class="text-right">
                                                <b class="text-000">
                                                    SAR <?php echo number_format($total, 2); ?>
                                                </b>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between mb-2">
                                            <div class="">
                                                <?php echo lang('shipping'); ?>
                                            </div>
                                            <div class="text-right">
                                                <b class="text-000">
                                                    SAR <?php
                                                    $shipping = 150;
                                                    echo number_format($shipping, 2);
                                                    ?>
                                                </b>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between">
                                            <div class="">
                                                <b class="text-000 font-20">
                                                    <?php echo lang('total'); ?>
                                                </b>
                                            </div>
                                            <div class="text-right">
                                                <b class="text-000 font-20 totalAmount1">
                                                    SAR <?php
                                                    $total = $total + $shipping;
                                                    echo number_format($total, 2);
                                                    ?>
                                                </b>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div><!-- /.accordion-item-body -->
                    </div><!-- /.accordion-item -->
                </div>
            </div>
        </div>

        <div class="checkout-left">
            <div class="p-30 p-xs-5">
                <div class="container">
                    <div class="row justify-content-end">
                        <div class="col-md-12 col-lg-10 col-sm-12 col-xs-12">


                            <form method="post" id="checkoutForm" name="checkoutForm">

                                <!-- Contact-->
                                <div class="mb-30">
                                    <h4 class="font-weight-600 font-22 mb-2"><?php echo lang('contact'); ?></h4>
                                    <div class="form-group mb-2">
                                        <input type="text" name="fname" class="customText"
                                               value="<?php
                                               if (set_value('fname') && !isset($success)) {
                                                   echo set_value('fname');
                                               }
                                               ?>"
                                               required
                                               placeholder="<?php echo lang('full_name'); ?>">
                                        <div class="error-text">
                                            <?php
                                            if (form_error('fname')) {
                                                echo form_error('fname');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-group mb-2">
                                        <input type="text" name="phone" class="customText"
                                               value="<?php
                                               if (set_value('phone') && !isset($success)) {
                                                   echo set_value('phone');
                                               }
                                               ?>"
                                               required
                                               placeholder="<?php echo lang('ph_num'); ?>">
                                        <div class="error-text">
                                            <?php
                                            if (form_error('phone')) {
                                                echo form_error('phone');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="email" name="" class="customText"
                                               value="<?php
                                               if (set_value('email') && !isset($success)) {
                                                   echo set_value('email');
                                               }
                                               ?>"
                                               placeholder="<?php echo lang('email'); ?>">
                                        <div class="error-text">
                                            <?php
                                            if (form_error('email')) {
                                                echo form_error('email');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-checkbox mt-2 mb-2">
                                        <input type="checkbox" class="custom-checkbox" id="terms-condition"
                                               name="terms-condition">
                                        <label class="form-control-label" for="terms-condition">
                                            <?php echo lang('email_me'); ?>
                                        </label>
                                    </div>
                                </div>

                                <!-- Delivery-->
                                <div class="mb-30">
                                    <h4 class="font-weight-600 font-22 mb-2"><?php echo lang('delivery'); ?></h4>
                                    <div class="form-group mb-3">
                                        <select class="customText" name="country" required id="country">
                                            <option value=""><?php echo lang('country'); ?></option>
                                            <?php
                                            if ($countries) {
                                                foreach ($countries as $country) {
                                                    echo '<option value="' . ($country->id) . '">' . ($country->name) . '</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                        <div class="error-text">
                                            <?php
                                            if (form_error('country')) {
                                                echo form_error('country');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-group mb-3">
                                        <select class="customText" name="city" required id="city">
                                            <option value=""><?php echo lang('city'); ?></option>
                                        </select>
                                        <div class="error-text">
                                            <?php
                                            if (form_error('city')) {
                                                echo form_error('city');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <!--                                        <div class="col-md-12 mb-2">-->
                                        <!--                                            <div class="form-group">-->
                                        <!--                                                <input type="text" name="fname" class="customText"-->
                                        <!--                                                       value="--><?php
                                        //                                                       if (set_value('fname') && !isset($success)) {
                                        //                                                           echo set_value('fname');
                                        //                                                       }
                                        //                                                       ?><!--"-->
                                        <!--                                                       required-->
                                        <!--                                                       placeholder="Full name">-->
                                        <!--                                                <div class="error-text">-->
                                        <!--                                                    --><?php
                                        //                                                    if (form_error('fname')) {
                                        //                                                        echo form_error('fname');
                                        //                                                    }
                                        //                                                    ?>
                                        <!--                                                </div>-->
                                        <!--                                            </div>-->
                                        <!--                                        </div>-->
                                        <div class="col-md-12 mb-2">
                                            <div class="form-group">
                                                <input type="text" name="address" class="customText"
                                                       value="<?php
                                                       if (set_value('address') && !isset($success)) {
                                                           echo set_value('address');
                                                       }
                                                       ?>"
                                                       required
                                                       placeholder="<?php echo lang('address'); ?>">
                                                <div class="error-text">
                                                    <?php
                                                    if (form_error('address')) {
                                                        echo form_error('address');
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <!--                                        <div class="col-md-12 mb-2">-->
                                        <!--                                            <div class="form-group">-->
                                        <!--                                                <input type="text" name="postal_code" class="customText"-->
                                        <!--                                                       value="--><?php
                                        //                                                       if (set_value('postal_code') && !isset($success)) {
                                        //                                                           echo set_value('postal_code');
                                        //                                                       }
                                        //                                                       ?><!--"-->
                                        <!--                                                       required-->
                                        <!--                                                       placeholder="Postal Code">-->
                                        <!--                                                <div class="error-text">-->
                                        <!--                                                    --><?php
                                        //                                                    if (form_error('postal_code')) {
                                        //                                                        echo form_error('postal_code');
                                        //                                                    }
                                        //                                                    ?>
                                        <!--                                                </div>-->
                                        <!--                                            </div>-->
                                        <!--                                        </div>-->
                                        <!--                                        <div class="col-md-6 mb-2">-->
                                        <!--                                            <div class="form-group">-->
                                        <!--                                                <input type="text" name="phone" class="customText"-->
                                        <!--                                                       value="--><?php
                                        //                                                       if (set_value('phone') && !isset($success)) {
                                        //                                                           echo set_value('phone');
                                        //                                                       }
                                        //                                                       ?><!--"-->
                                        <!--                                                       required-->
                                        <!--                                                       placeholder="Phone">-->
                                        <!--                                                <div class="error-text">-->
                                        <!--                                                    --><?php
                                        //                                                    if (form_error('phone')) {
                                        //                                                        echo form_error('phone');
                                        //                                                    }
                                        //                                                    ?>
                                        <!--                                                </div>-->
                                        <!--                                            </div>-->
                                        <!--                                        </div>-->
                                        <!--                                        <div class="col-md-6 mb-2">-->
                                        <!--                                            <div class="form-group">-->
                                        <!--                                                <input type="email" name="" class="customText"-->
                                        <!--                                                       value="--><?php
                                        //                                                       if (set_value('email') && !isset($success)) {
                                        //                                                           echo set_value('email');
                                        //                                                       }
                                        //                                                       ?><!--"-->
                                        <!--                                                       placeholder="Email">-->
                                        <!--                                                <div class="error-text">-->
                                        <!--                                                    --><?php
                                        //                                                    if (form_error('email')) {
                                        //                                                        echo form_error('email');
                                        //                                                    }
                                        //                                                    ?>
                                        <!--                                                </div>-->
                                        <!--                                            </div>-->
                                        <!--                                        </div>-->
                                    </div>
                                    <div class="form-checkbox mb-2">
                                        <input type="checkbox" class="custom-checkbox" id="saveInfo"
                                               checked
                                               name="saveInfo">
                                        <label class="form-control-label" for="saveInfo">
                                            <?php echo lang('save_info'); ?>
                                        </label>
                                    </div>
                                </div>

                                <!-- Shipping method-->
                                <!--                                <div class="mb-30">-->
                                <!--                                    <h4 class="font-weight-600 font-18 mb-2">Shipping method</h4>-->
                                <!--                                    <p>Delivery at location</p>-->
                                <!--                                    <div class="p-15"-->
                                <!--                                         style="background: #FDF4F4;border:1px solid #000;border-radius: 5px">-->
                                <!--                                        <div class="d-flex justify-content-between">-->
                                <!--                                            <div>توصيل للموقع</div>-->
                                <!--                                            <div class="font-weight-bold text-000">-->
                                <!--                                                SAR 150.00-->
                                <!--                                            </div>-->
                                <!--                                        </div>-->
                                <!--                                    </div>-->
                                <!--                                    <div class="form-checkbox mt-2 mb-2">-->
                                <!--                                        <input type="checkbox" class="custom-checkbox" id="terms-condition2"-->
                                <!--                                               name="terms-condition2">-->
                                <!--                                        <label class="form-control-label" for="terms-condition2">-->
                                <!--                                            Email me with news and offers-->
                                <!--                                        </label>-->
                                <!--                                    </div>-->
                                <!--                                </div>-->

                                <!-- Payment-->
                                <div class="mb-30 checkout">
                                    <h4 class="font-weight-600 font-22 mb-2"><?php echo lang('payment'); ?></h4>
                                    <p class="mb-2"><?php echo lang('all_trans'); ?></p>
                                    <div class="payment accordion radio-type">
                                        <div class="card">
                                            <div class="card-header d-flex justify-content-between p-15">
                                                <a href="#collapse1"
                                                   class="text-body text-normal ls-m collapse"><?php echo lang('split_into3'); ?>
                                                </a>
                                                <img src="<?php echo base_url('assets/images/tamara.svg'); ?>"/>
                                            </div>
                                            <div id="collapse1" class="expanded" style="background: #F4F4F4">
                                                <div class="card-body ls-m text-center p-15">
                                                    <div class="mt-2">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                             style="height: 80px"
                                                             viewBox="-252.3 356.1 163 80.9" class="eHdoK">
                                                            <path fill="none" stroke="currentColor"
                                                                  stroke-miterlimit="10" stroke-width="2"
                                                                  d="M-108.9 404.1v30c0 1.1-.9 2-2 2H-231c-1.1 0-2-.9-2-2v-75c0-1.1.9-2 2-2h120.1c1.1 0 2 .9 2 2v37m-124.1-29h124.1"></path>
                                                            <circle cx="-227.8" cy="361.9" r="1.8"
                                                                    fill="currentColor"></circle>
                                                            <circle cx="-222.2" cy="361.9" r="1.8"
                                                                    fill="currentColor"></circle>
                                                            <circle cx="-216.6" cy="361.9" r="1.8"
                                                                    fill="currentColor"></circle>
                                                            <path fill="none" stroke="currentColor"
                                                                  stroke-miterlimit="10" stroke-width="2"
                                                                  d="M-128.7 400.1H-92m-3.6-4.1 4 4.1-4 4.1"></path>
                                                        </svg>
                                                    </div>
                                                    <p class="mt-15 mb-0">
                                                        <?php echo lang('pay_info'); ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header d-flex justify-content-between p-15">
                                                <a href="#collapse1" class="text-body text-normal ls-m">
                                                    <?php echo lang('pay_into4'); ?>
                                                </a>
                                                <img src="<?php echo base_url('assets/images/tabby.svg'); ?>"/>
                                            </div>
                                            <div id="collapse1" class="collapsed" style="background: #F4F4F4">
                                                <div class="card-body ls-m text-center p-15">
                                                    <div class="mt-2">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                             style="height: 80px"
                                                             viewBox="-252.3 356.1 163 80.9" class="eHdoK">
                                                            <path fill="none" stroke="currentColor"
                                                                  stroke-miterlimit="10" stroke-width="2"
                                                                  d="M-108.9 404.1v30c0 1.1-.9 2-2 2H-231c-1.1 0-2-.9-2-2v-75c0-1.1.9-2 2-2h120.1c1.1 0 2 .9 2 2v37m-124.1-29h124.1"></path>
                                                            <circle cx="-227.8" cy="361.9" r="1.8"
                                                                    fill="currentColor"></circle>
                                                            <circle cx="-222.2" cy="361.9" r="1.8"
                                                                    fill="currentColor"></circle>
                                                            <circle cx="-216.6" cy="361.9" r="1.8"
                                                                    fill="currentColor"></circle>
                                                            <path fill="none" stroke="currentColor"
                                                                  stroke-miterlimit="10" stroke-width="2"
                                                                  d="M-128.7 400.1H-92m-3.6-4.1 4 4.1-4 4.1"></path>
                                                        </svg>
                                                    </div>
                                                    <p class="mt-15 mb-0">
                                                        <?php echo lang('pay_info'); ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Billing Address -->
                                <div class="mb-30 mb-xs-0 checkout">
                                    <h4 class="font-weight-600 font-18 mb-2"> <?php echo lang('billing_address'); ?></h4>
                                    <div class="payment accordion radio-type">
                                        <div class="card">
                                            <div class="card-header p-15">
                                                <a href="#collapse1"
                                                   class="text-body text-normal ls-m collapse"><?php echo lang('same_address'); ?>
                                                </a>
                                            </div>
                                            <div id="collapse1" class="expanded" style="background: #F4F4F4">

                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header p-15">
                                                <a href="#collapse1" class="text-body text-normal ls-m">
                                                    <?php echo lang('different_address'); ?>
                                                </a>
                                            </div>
                                            <div id="collapse1" class="collapsed" style="background: #F4F4F4">
                                                <div class="card-body ls-m text-center p-15">
                                                    <div class="form-group mb-3">
                                                        <select class="customText country" name="country" required>
                                                            <option value=""><?php echo lang('country'); ?></option>
                                                            <?php
                                                            if ($countries) {
                                                                foreach ($countries as $country) {
                                                                    echo '<option value="' . ($country->id) . '">' . ($country->name) . '</option>';
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('country')) {
                                                                echo form_error('country');
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6 mb-2">
                                                            <div class="form-group">
                                                                <input type="text" name="fname" class="customText"
                                                                       value="<?php
                                                                       if (set_value('fname') && !isset($success)) {
                                                                           echo set_value('fname');
                                                                       }
                                                                       ?>"
                                                                       required
                                                                       placeholder="<?php echo lang('fname'); ?>">
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('fname')) {
                                                                        echo form_error('fname');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 mb-2">
                                                            <div class="form-group">
                                                                <input type="text" name="lname" class="customText"
                                                                       value="<?php
                                                                       if (set_value('lname') && !isset($success)) {
                                                                           echo set_value('lname');
                                                                       }
                                                                       ?>"
                                                                       required
                                                                       placeholder="<?php echo lang('lname'); ?>">
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('lname')) {
                                                                        echo form_error('lname');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb-2">
                                                            <div class="form-group">
                                                                <input type="text" name="address" class="customText"
                                                                       value="<?php
                                                                       if (set_value('address') && !isset($success)) {
                                                                           echo set_value('address');
                                                                       }
                                                                       ?>"
                                                                       required
                                                                       placeholder="<?php echo lang('address'); ?>">
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('address')) {
                                                                        echo form_error('address');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 mb-2">
                                                            <div class="form-group">
                                                                <select class="customText city" name="city" required>
                                                                    <option
                                                                        value=""><?php echo lang('city'); ?></option>
                                                                </select>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('city')) {
                                                                        echo form_error('city');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 mb-2">
                                                            <div class="form-group">
                                                                <input type="text" name="postal_code" class="customText"
                                                                       value="<?php
                                                                       if (set_value('postal_code') && !isset($success)) {
                                                                           echo set_value('postal_code');
                                                                       }
                                                                       ?>"
                                                                       required
                                                                       placeholder="<?php echo lang('postalcode'); ?>">
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('postal_code')) {
                                                                        echo form_error('postal_code');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb-2">
                                                            <div class="form-group">
                                                                <input type="text" name="phone" class="customText"
                                                                       value="<?php
                                                                       if (set_value('phone') && !isset($success)) {
                                                                           echo set_value('phone');
                                                                       }
                                                                       ?>"
                                                                       required
                                                                       placeholder="<?php echo lang('ph_num'); ?>">
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('phone')) {
                                                                        echo form_error('phone');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <button class="checkoutBtn d-none d-md-block d-lg-block" type="submit" value="send"
                                        name="paynow"><?php echo lang('payment_btn'); ?>
                                </button>

                                <!--                                <a href="-->
                                <?php //echo $this->session->userdata('url'); ?><!--"-->
                                <!--                                   class="btn btn_theme btn_md mb-20">-->
                                <!--                                    Proceed to Payment-->
                                <!--                                </a>-->
                            </form>

                            <div class="pt-20 mt-100 d-none d-md-block d-lg-block"
                                 style="border-top: 1px solid #dfdfdf">
                                <p class="font-12"><?php echo lang('copyright'); ?></p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="checkout-right bg-F5F5F5 mb-xs-0">
            <div class="p-30 p-xs-5 pt-xs-20">
                <div class="container">
                    <div class="row justify-content-start">
                        <div class="col-md-12 col-lg-10 col-sm-12 col-xs-12">
                            <h4 class="font-weight-600 font-22 mb-2 d-block d-md-none d-lg-none"><?php echo lang('order_summary'); ?></h4>
                            <table class="shop-table cart-table">
                                <tbody class="border-none">
                                <?php
                                $total = 0;
                                if ($cartProduct) {
                                    foreach ($cartProduct as $cartitem) {
                                        $productData = $this->md->select_where('tbl_product', array('product_id' => $cartitem->product_id));
                                        if ($productData) {
                                            $product = $productData[0];
                                            $attributes = $this->md->select_where('tbl_product_attribute', array('product_attribute_id' => $cartitem->product_attribute_id));

                                            $photos = array(FILENOTFOUND);
                                            if ($attributes) {
                                                $photos = $attributes[0]->product_photos ? explode(",", $attributes[0]->product_photos) : array(FILENOTFOUND);
                                            }

                                            $url = base_url('product/' . $product->slug . "/" . $product->product_id . "/?color=" . $attributes[0]->color);
                                            $total = $total + $cartitem->netprice; // Net total
                                            ?>
                                            <tr class="p-xs-0 mb-xs-20 border-none">
                                                <td class="product-thumbnail m-xs-5 border-none" width="75%">
                                                    <div class="d-flex w-100 mb-15">
                                                        <figure class="position-relative">
                                                            <img
                                                                src="<?php echo base_url($photos ? ($photos[0] ? $photos[0] : FILENOTFOUND) : FILENOTFOUND); ?>"
                                                                alt="<?php echo $product->title; ?>"
                                                                title="<?php echo $product->title; ?>"
                                                                style="width: 60px;height: 60px;object-fit: contain;border-radius: 10px;background: #fff"
                                                            >
                                                            <span
                                                                class="position-absolute cartCounter"><?php echo $cartitem->qty; ?></span>
                                                        </figure>
                                                        <span
                                                            class="font-14 englishText mt-20 ml-xs-0 ml-3"
                                                            href="<?php echo $url; ?>"><?php echo $productData ? ($productData[0]->title) : ''; ?>
                                                        </span>
                                                        <span
                                                            class="font-14 arabicText mt-20 ml-xs-0 mr-3"
                                                            href="<?php echo $url; ?>"><?php echo $productData ? ($productData[0]->arabic_title) : ''; ?>
                                                        </span>
                                                    </div>
                                                </td>
                                                <td width="25%" class="border-none text-right">
                                                <span
                                                    class="amount">SAR <?php echo $cartitem->netprice ? number_format($cartitem->netprice, 2) : ''; ?></span>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                                </tbody>
                            </table>

                            <div class="mt-30 d-flex justify-content-between mb-2">
                                <div class="">
                                    <?php echo lang('subtotal'); ?>
                                </div>
                                <div class="text-right">
                                    <b class="text-000">
                                        SAR <?php echo number_format($total, 2); ?>
                                    </b>
                                </div>
                            </div>
                            <div class="d-flex justify-content-between mb-2">
                                <div class="">
                                    <?php echo lang('shipping'); ?>
                                </div>
                                <div class="text-right">
                                    <b class="text-000">
                                        SAR <?php
                                        $shipping = 150;
                                        echo number_format($shipping, 2);
                                        ?>
                                    </b>
                                </div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="">
                                    <b class="text-000 font-20">
                                        <?php echo lang('total'); ?>
                                    </b>
                                </div>
                                <div class="text-right">
                                    <b class="text-000 font-20">
                                        SAR <?php
                                        $total = $total + $shipping;
                                        echo number_format($total, 2);
                                        ?>
                                    </b>
                                </div>
                            </div>

                            <button form="checkoutForm" class="checkoutBtn mt-20 d-block d-md-none d-lg-none"
                                    type="submit" value="send" name="paynow"><?php echo lang('payment_btn'); ?>
                            </button>

                            <div class="mt-50 d-block d-md-none d-lg-none" style="border-top: 1px solid #dfdfdf">
                                <p class="font-12"><?php echo lang('copyright'); ?></p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </main>
</div>
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/parallax/parallax.min.js"></script>
<script src="vendor/elevatezoom/jquery.elevatezoom.min.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
<script src="vendor/owl-carousel/owl.carousel.min.js"></script>
<script src="js/main.min.js"></script>
<script src="js/notify.min.js"></script>
<script src="js/custom.js"></script>
<script type="text/javascript">


    /*-------------------------
   GOOGLE LANGUAGE TRANSLATE
   ---------------------------*/

    $(window).ready(function () {
        setDefaultLanguage();
    });

    function setDefaultLanguage() {
        // Set default language from session
        let lng = '<?php echo $this->session->userdata('site_lang'); ?>';    // english / arabic
        if (lng !== null) {
            if (lng === 'arabic') {
                $('html').css('direction', 'rtl');
                $('body').addClass('arabic');
            } else {
                $('html').css('direction', 'ltr');
                $('body').addClass('english');
            }
        } else {
            $('html').css('direction', 'ltr');
            $('body').addClass('english');
        }

        // Show arabic text & hide english
        if ($("body").hasClass("arabic")) {
            $(".arabicText").show();
            $(".englishText").hide();
        } else {
            $(".arabicText").hide();
            $(".englishText").show();
        }
    }

    /*--------------------
     GET CITY FROM STATE
     ---------------------*/
    $(document).on("change", "#country", function () {
        let countryId = $(this).val(); // get country id
        jQuery('#city').html('Loading...');
        var data = {
            countryId: countryId
        };
        var url = "<?php echo base_url('User/get_city'); ?>";
        jQuery.post(url, data, function (data) {
            jQuery('#city').html(data);
        });
    });
    /*--------------------
     GET CITY FROM STATE
     ---------------------*/
    $(document).on("change", ".country", function () {
        let countryId = $(this).val(); // get country id
        jQuery('.city').html('Loading...');
        var data = {
            countryId: countryId
        };
        var url = "<?php echo base_url('User/get_city'); ?>";
        jQuery.post(url, data, function (data) {
            jQuery('.city').html(data);
        });
    });


    $('.totalAmount2').html($('.totalAmount1').html());

    /*--------------------
    NOTIFY.JS INITIALIZATION
    ---------------------*/
    $.notify.defaults({
        position: "bottom right",
        gap: '5',
        autoHideDelay: 7000
    });

    // Accordion
    const accordionItemHeaders = document.querySelectorAll(".accordion-item-header");

    accordionItemHeaders.forEach(accordionItemHeader => {
        accordionItemHeader.addEventListener("click", event => {
            accordionItemHeader.classList.toggle("active");
            const accordionItemBody = accordionItemHeader.nextElementSibling;
            if (accordionItemHeader.classList.contains("active")) {
                $('.accordion-item-header').find('.acc-title').html('Hide order summary');
                accordionItemBody.style.maxHeight = accordionItemBody.scrollHeight + "px";
            } else {
                $('.accordion-item-header').find('.acc-title').html('Show order summary');
                accordionItemBody.style.maxHeight = 0;
            }
        });
    });

</script>
<?php
if (isset($success)) {
    ?>
    <script>
        $.notify('<?php echo $success; ?>', 'success');
    </script>
    <?php
}
if (isset($error)) {
    ?>
    <script>
        $.notify('<?php echo $error; ?>', 'error');
    </script>
    <?php
}
?>
<style>
    .shop-table tbody, .shop-table tr {
        display: revert !important;
    }

    .custom-accordion .accordion-item {
        background-color: #F5F5F5;
        color: #111;
        border-radius: 0.5rem;
    }

    .custom-accordion .accordion-item-header {
        padding: 25px;
        min-height: 3.5rem;
        line-height: 1.25rem;
        display: flex;
        align-items: center;
        justify-content: space-between;
        position: relative;
        cursor: pointer;
        border: 1px solid #dfdfdf;
    }

    .custom-accordion .accordion-item-header::after {
        content: "\002B";
        font-size: 2rem;
        position: absolute;
        left: 16.5rem;
        top: 24px;
    }

    .custom-accordion .accordion-item-header.active::after {
        content: "\2212";
    }

    .custom-accordion .accordion-item-body {
        max-height: 0;
        overflow: hidden;
        transition: max-height 0.2s ease-out;
    }

    .custom-accordion .accordion-item-body-content {
        padding: 1rem;
        line-height: 1.5rem;
    }

</style>
</body>