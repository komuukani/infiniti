<?php
echo $page_head;
?>
<body>
<?php echo $page_header; ?>
<main id="content" class="wrapper layout-page">
    <?php echo $page_breadcumb; ?>
    <div class="contact-form section-padding pt-lg-50 pt-md-50">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <img src="<?php echo base_url('assets/assets/images/new/register.svg') ?>"
                         class="mt-lg-100 mt-md-100 w-100"
                         alt="register"/>
                </div>
                <div class="col-lg-6">
                    <div class="contact-form p-30">
                        <h4 class="fs-34 mb-6">Sign up</h4>
                        <p class="fs-16 mb-7">Already have an account? <a href="<?php echo base_url('user-login'); ?>"
                                                                          class="text-secondary border-bottom text-decoration-none">Sign
                                in</a> for free</p>
                        <form method="post" name="register">
                            <?php
                            if (isset($error)) {
                                ?>
                                <div class="alert alert-danger p-1">
                                    <?php echo $error; ?>
                                </div>
                                <?php
                            }
                            if (isset($success)) {
                                ?>
                                <div class="alert alert-success p-1">
                                    <?php echo $success; ?>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="mb-8">
                                <input name="fname" autofocus="" value="<?php
                                if (set_value('fname') && !isset($success)) {
                                    echo set_value('fname');
                                }
                                ?>" type="text" class="form-control border-0" placeholder="Your Full Name" required="">
                                <div class="error-text p-0 m-0">
                                    <?php
                                    if (form_error('fname')) {
                                        echo form_error('fname');
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="mb-8">
                                <input name="email" value="<?php
                                if (set_value('email') && !isset($success)) {
                                    echo set_value('email');
                                }
                                ?>" type="email" class="form-control border-0" placeholder="Your email" required="">
                                <div class="error-text p-0 m-0">
                                    <?php
                                    if (form_error('email')) {
                                        echo form_error('email');
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="mb-8">
                                <input name="phone" type="number"
                                       oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                       maxlength="12" value="<?php
                                if (set_value('phone') && !isset($success)) {
                                    echo set_value('phone');
                                }
                                ?>" class="form-control border-0" placeholder="Phone" required="">
                                <div class="error-text p-0 m-0">
                                    <?php
                                    if (form_error('phone')) {
                                        echo form_error('phone');
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="mb-8">
                                <input name="password" type="password" value="<?php
                                if (set_value('password') && !isset($success)) {
                                    echo set_value('password');
                                }
                                ?>" class="form-control border-0" placeholder="Password" required="">
                                <div class="error-text p-0 m-0">
                                    <?php
                                    if (form_error('password')) {
                                        echo form_error('password');
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="mb-8">
                                <?php
                                if ($web_data[0]->captcha_visibility):
                                    echo '<div class="g-recaptcha" data-sitekey="' . $web_data[0]->captcha_site_key . '"></div>';
                                endif;
                                ?>
                            </div>

                            <button type="submit" value="send" name="register"
                                    class="btn btn-dark btn-hover-bg-primary btn-hover-border-primary px-11">
                                Submit
                            </button>

                            <a href="<?php echo base_url('user-login'); ?>"
                               class="text-secondary mt-2 text-center d-block">Already have an account?</a>
                            <div class="border-bottom mt-6"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php echo $page_footer; ?>
<?php echo $page_footerscript; ?>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
</body>  