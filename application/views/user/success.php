<?php
echo $page_head;
?>

<body class="body-wrapper">
<?php echo $page_header; ?>
<section class="py-lg-13 pb-10 pt-0">
    <div class="container container-xl">
        <div class="row">
            <div class="col-lg-8 offset-2 text-center">
                <i class="far fa-check-circle fa-5x text-success animated tada infinite"></i>
                <h1 class="font-weight-400 mt-30">Order Placed Successfully</h1>
                <p class="mt-30 font-24">Thank you for your order.</p>
                <p class="font-20">
                    Your Order number according to our system is <strong
                        class="font-22 text-000"><?php echo(!empty($this->session->userdata('order_id')) ? $this->session->userdata('order_id') : ''); ?></strong>.
                    <Br/>
                    <i> In case of any discrepancy please contact <a href="mailto:support@skintolove.co.nz">support@skintolove.co.nz</a>.</i>
                </p>
                <hr/>
                <a class="btn btn-primary" href="<?php echo base_url('product'); ?>">Buy More Product</a>
                <?php
                if (!$this->session->userdata('userType')) {
                    ?>
                    <a class="btn btn-dark ml-10" href="<?php echo base_url('myorder'); ?>">View Order</a>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</section>

<?php echo $page_footer; ?>
<?php echo $page_footerscript; ?>
</body>