<?php
echo $page_head;
?>   
<body class="body-wrapper"> 
    <?php echo $page_header; ?>   
    <div class="contact-form section-padding pt-lg-50 pt-md-50">
        <div class="container"> 
            <div class="row">
                <div class="col-lg-6">
                    <img src="<?php echo base_url('assets/img/new/password.svg') ?>" class="mt-lg-50 mt-md-50" alt="register" />
                </div>
                <div class="col-lg-6">
                    <div class="contact-form p-30">
                        <div class="contact-form__header mb-sm-35 mb-xs-30 mb-40">
                            <h6 class="sub-title fw-500 color-red text-uppercase mb-15"><img src="assets/img/home/line.svg" class="img-fluid mr-10" alt=""> Update Your new password.</h6>
                            <h3 class="title color-d_black">Update Password!</h3>
                        </div>
                        <form method="post" name="register">  
                            <div class="single-personal-info">
                                <input type="password" value="<?php
                                if (set_value('new_password') && !isset($success)) {
                                    echo set_value('new_password');
                                }
                                ?>" name="new_password" placeholder="New Password">
                                <div class="error-text p-0 m-0"> 
                                    <?php
                                    if (form_error('new_password')) {
                                        echo form_error('new_password');
                                    }
                                    ?> 
                                </div>
                            </div>
                            <div class="single-personal-info">
                                <input type="password" value="<?php
                                if (set_value('con_password') && !isset($success)) {
                                    echo set_value('con_password');
                                }
                                ?>" name="con_password" placeholder="Confirm Password">
                                <div class="error-text p-0 m-0"> 
                                    <?php
                                    if (form_error('con_password')) {
                                        echo form_error('con_password');
                                    }
                                    ?> 
                                </div>
                            </div> 
                            <?php
                            if ($web_data[0]->captcha_visibility):
                                echo '<div class="g-recaptcha" data-sitekey="' . $web_data[0]->captcha_site_key . '"></div>';
                            endif;
                            ?> 
                            <button type="submit" value="send" name="update" class="theme-btn btn-sm btn-red mt-20">Update now <i class="far fa-chevron-double-right"></i></button>                            
                        </form>
                    </div>
                </div> 
            </div>
        </div>
    </div>
    <?php echo $page_footer; ?>   
    <?php echo $page_footerscript; ?> 
    <script src="https://www.google.com/recaptcha/api.js" async defer></script> 
</body>  