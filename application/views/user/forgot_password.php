<?php
echo $page_head;
?>
<body>
<?php echo $page_header; ?>
<main id="content" class="wrapper layout-page">
    <?php echo $page_breadcumb; ?>
    <div class="contact-form section-padding pt-lg-50 pt-md-50">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <img src="<?php echo base_url('assets/assets/images/new/forgot.svg') ?>"
                         class="mt-lg-50 mt-md-50 w-75"
                         alt="forgot"/>
                </div>
                <div class="col-lg-6">
                    <div class="contact-form p-30">
                        <h4 class="fs-34 mb-6">Lost Password</h4>
                        <p class="fs-16 mb-7">Want to Generate a new password? </p>
                        <form method="post" name="forgot_password">
                            <?php
                            if (isset($error)) {
                                ?>
                                <div class="alert alert-danger p-1">
                                    <?php echo $error; ?>
                                </div>
                                <?php
                            }
                            if (isset($success)) {
                                ?>
                                <div class="alert alert-success p-1">
                                    <?php echo $success; ?>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="single-personal-info">
                                <input type="text" autofocus="" name="username" value="<?php
                                if (set_value('username') && !isset($success)) {
                                    echo set_value('username');
                                }
                                ?>" class="form-control border-0" placeholder="Your Email or Phone">
                                <div class="error-text p-0 m-0">
                                    <?php
                                    if (form_error('username')) {
                                        echo form_error('username');
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="d-flex justify-content-between mt-8 mb-8">
                                <a href="<?php echo base_url('user-register'); ?>"
                                   class="text-decoration-underline font-14 text-theme"> <i
                                        class="far font-12 fa-chevron-double-left"></i> Go to Register</a>
                                <a href="<?php echo base_url('user-login'); ?>"
                                   class="text-decoration-underline font-14 text-theme"><i
                                        class="far font-12 fa-chevron-double-left"></i> Go to Login </a>
                            </div>
                            <div class="mb-8">
                                <?php
                                if ($web_data[0]->captcha_visibility):
                                    echo '<div class="g-recaptcha" data-sitekey="' . $web_data[0]->captcha_site_key . '"></div>';
                                endif;
                                ?>
                            </div>
                            <button type="submit" value="send" name="forgot"
                                    class="btn btn-dark btn-hover-bg-primary btn-hover-border-primary px-11">
                                Submit
                            </button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php echo $page_footer; ?>
<?php echo $page_footerscript; ?>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
</body>  