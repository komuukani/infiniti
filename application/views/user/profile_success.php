<?php
echo $page_head;
?>
<body>
<?php echo $page_header; ?>
<main id="content" class="wrapper layout-page">
    <div class="contact-form section-padding pt-lg-50 pt-md-50 pb-50">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 shadow">
                    <div class="contact-form p-50 text-center">
                        <i class="fa fa-check-circle fa-5x text-success animated tada infinite"></i>
                        <h3 class="text-center mt-5">Congratulations</h3>
                        <p class="text-center font-18 mt-3">Your Account has been created successfully! <br/> Enjoy your
                            shopping!!!😊</p>
                        <a class="btn btn-dark btn-hover-bg-primary btn-hover-border-primary px-11 mr-20"
                           href="<?php echo base_url('product'); ?>">Shop Now</a>
                        <a class="btn btn-primary btn-hover-bg-dark btn-hover-border-primary px-11"
                           href="<?php echo base_url('edit-profile'); ?>">Go to Profile</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php echo $page_footer; ?>
<?php echo $page_footerscript; ?>
</body>  