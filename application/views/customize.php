<?php
echo $page_head;
$category = $this->md->select_limit_order('tbl_category', 100, 'position', 'asc');
$colours = $this->md->select('tbl_colours');
$countries = $this->md->select('tbl_country');
$products = $this->md->select_limit_order('tbl_product', 30, 'product_id', 'desc', array('customize' => 1, 'status' => 1));
?>
<body class="home">
<div class="page-wrapper">
    <?php echo $page_header; ?>
    <main class="main mt-100 mt-xs-50">
        <section class="pt-50 pb-50">
            <div class="container">
                <div class="row" style="background: #D9C12F">
                    <div class="col-md-7 pl-0 d-none d-md-block d-lg-block">
                        <div class="customize-design">
                            <div class="p-20" style="background: #e9d552">
                                <div class="mb-xs-50">
                                    <h4 class="mb-0 text-000"><?php echo lang('select_from_our_library'); ?>
                                    </h4>
                                    <div class="row customRadio">
                                        <?php
                                        if ($products) {
                                            foreach ($products as $product) {
                                                $attributes = $this->md->select_where('tbl_product_attribute', array('product_id' => $product->product_id));

                                                $photos = array(FILENOTFOUND);
                                                if ($attributes) {
                                                    $photos = $attributes[0]->product_photos ? explode(",", $attributes[0]->product_photos) : array(FILENOTFOUND);
                                                }

                                                ?>
                                                <div class="col-md-6 mt-2 mb-2 col-6">
                                                    <input type="checkbox" name="product_id"
                                                           id="cb<?php echo $product->product_id; ?>"
                                                           form="customForm"
                                                           value="<?php echo $product->product_id; ?>"/>
                                                    <label for="cb<?php echo $product->product_id; ?>">
                                                        <img
                                                            src="<?php echo base_url($photos ? ($photos[0] ? $photos[0] : FILENOTFOUND) : FILENOTFOUND); ?>"
                                                            alt="<?php echo $product->title; ?>"
                                                            title="<?php echo $product->title; ?>"
                                                            class="imgPreview cursor-pointer"
                                                            style="width: 100%;height: 290px;object-fit: contain"
                                                        >
                                                    </label>
                                                </div>
                                                <?php
                                            }
                                        } else {
                                            echo '<center>';
                                            echo "<img src='" . base_url('assets/images/no-product.jpg') . "' style='width:40%' />";
                                            echo '</center>';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <form method="post" name="customForm" id="customForm" enctype="multipart/form-data">
                            <?php
                            if (isset($error)) {
                                ?>
                                <div class="alert alert-danger p-1">
                                    <?php echo $error; ?>
                                </div>
                                <?php
                            }
                            if (isset($success)) {
                                ?>
                                <div class="alert alert-success p-1">
                                    <?php echo $success; ?>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="customize-design">
                                <div class="p-10" style="background: #D9C12F">
                                    <div class="">
                                        <!-- Step-1 -->
                                        <div class="step-item mt-20">
                                            <h5>
                                                <img src="images/step-1.svg" alt="step-1" class="thumb-icon"/>
                                                <?php echo lang('step_1'); ?>
                                                <img src="images/down-white2.png" alt="down" class="arrow"/>
                                            </h5>

                                            <div class="drop-zone">
                                                <span class="drop-zone__prompt">
                                                    <img src="images/photo.svg" alt="down" width="50"/>
                                                    <br/>
                                                   <?php echo lang('click_here_to_upload_photo'); ?>
                                                </span>
                                                <input type="file" name="custom_photo" class="drop-zone__input"
                                                       accept="image/*">
                                            </div>

                                            <div class="astrodivider">
                                                <div class="astrodividermask"></div>
                                                <span><i> Or </i></span></div>

                                            <h4 class="text-center font-20 font-weight-600 d-none d-md-block d-lg-block">
                                                <i
                                                    class="fa fa-long-arrow-alt-left"></i> <?php echo lang('you_can_select_from_our_library'); ?>
                                            </h4>

                                            <div align="center" class="d-block d-md-none d-lg-none">
                                                <img class="libraryImg mb-20"
                                                     style="display: none;width: 60%;height: 220px;object-fit: cover"/>
                                                <button type="button"
                                                        id="open-popup-modal"
                                                        class="checkoutBtn2"
                                                        style="width: 60%;background: #FFF;color: #000;border-radius: 0 !important;">
                                                    <?php echo lang('check_our_library'); ?>
                                                </button>
                                            </div>
                                        </div>
                                        <!-- Step-2 -->
                                        <div class="step-item">
                                            <h5>
                                                <img src="images/step-2.svg" alt="step-1" class="thumb-icon"/>
                                                <?php echo lang('step_2'); ?>
                                                <img src="images/down-white2.png" alt="down" class="arrow"/>
                                            </h5>
                                            <?php
                                            //                                        if (empty($category)) {
                                            //                                            echo '<div class="alert alert-warning col-md-12">Sorry, Category not available!</div>';
                                            //                                        } else {
                                            //                                            foreach ($category as $category_data) {
                                            //                                                ?>
                                            <!--                                                <div class="mb-20">-->
                                            <!--                                                    <label-->
                                            <!--                                                        class="font-16 display-block ">-->
                                            <?php //echo $category_data->title; ?><!-- </label>-->
                                            <!--                                                    <div class="form-group">-->
                                            <!--                                                        --><?php
                                            //                                                        if (strtolower($category_data->title) == 'corner sofa') {
                                            //                                                            ?>
                                            <!--                                                            <input type="text" class="form-control input-focus"-->
                                            <!--                                                                   name="length"-->
                                            <!--                                                                   placeholder="Enter Length">-->
                                            <!--                                                            --><?php
                                            //                                                        } else {
                                            //                                                            ?>
                                            <!--                                                            <select class="form-control border-radius-1"-->
                                            <!--                                                                    name="category_-->
                                            <?php //echo $category_data->category_id; ?><!--">-->
                                            <!--                                                                --><?php
                                            //                                                                for ($qty = 1; $qty <= 10; $qty++) {
                                            //                                                                    echo '<option>' . $qty . '</option>';
                                            //                                                                }
                                            //                                                                ?>
                                            <!--                                                            </select>-->
                                            <!--                                                            --><?php
                                            //                                                        }
                                            //                                                        ?>
                                            <!--                                                    </div>-->
                                            <!--                                                </div>-->
                                            <!--                                                --><?php
                                            //                                            }
                                            //                                        }
                                            ?>
                                            <div class="mb-20">
                                                <label
                                                    class="font-16 display-block "><?php echo lang('armchair'); ?></label>
                                                <div class="form-group">
                                                    <select class="form-control border-radius-1"
                                                            id="armchair"
                                                            name="armchair">
                                                        <option value=""><?php echo lang('quantity'); ?></option>
                                                        <?php
                                                        for ($qty = 1; $qty <= 10; $qty++) {
                                                            echo '<option>' . $qty . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="mb-20">
                                                <label
                                                    class="font-16 display-block "><?php echo lang('2_seater'); ?>
                                                </label>
                                                <div class="form-group">
                                                    <select class="form-control border-radius-1"
                                                            id="seater_2"
                                                            name="seater_2">
                                                        <option value=""><?php echo lang('quantity'); ?></option>
                                                        <?php
                                                        for ($qty = 1; $qty <= 10; $qty++) {
                                                            echo '<option>' . $qty . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="mb-20">
                                                <label
                                                    class="font-16 display-block "><?php echo lang('3_seater'); ?>
                                                </label>
                                                <div class="form-group">
                                                    <select class="form-control border-radius-1"
                                                            id="seater_3"
                                                            name="seater_3">
                                                        <option value=""><?php echo lang('quantity'); ?></option>
                                                        <?php
                                                        for ($qty = 1; $qty <= 10; $qty++) {
                                                            echo '<option>' . $qty . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="mb-20">
                                                <label
                                                    class="font-16 display-block "><?php echo lang('4_seater'); ?>
                                                </label>
                                                <div class="form-group">
                                                    <select class="form-control border-radius-1"
                                                            id="seater_4"
                                                            name="seater_4">
                                                        <option value=""><?php echo lang('quantity'); ?></option>
                                                        <?php
                                                        for ($qty = 1; $qty <= 10; $qty++) {
                                                            echo '<option>' . $qty . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="mb-20">
                                                <label
                                                    class="font-16 display-block "><?php echo lang('5_seater'); ?>
                                                </label>
                                                <div class="form-group">
                                                    <select class="form-control border-radius-1"
                                                            id="seater_5"
                                                            name="seater_5">
                                                        <option value=""><?php echo lang('quantity'); ?></option>
                                                        <?php
                                                        for ($qty = 1; $qty <= 10; $qty++) {
                                                            echo '<option>' . $qty . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="mb-20">
                                                <label
                                                    class="font-16 display-block "><?php echo lang('corner_sofa'); ?>
                                                </label>
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-focus"
                                                           id="corner_sofa_length"
                                                           name="corner_sofa_length"
                                                           placeholder="<?php echo lang('enter_length'); ?>">
                                                </div>
                                            </div>
                                            <div class="mb-20">
                                                <label
                                                    class="font-16 display-block "><?php echo lang('textile_type'); ?></label>
                                                <div class="range-item">
                                                    <div class="range-input d-flex position-relative">
                                                        <input type="range" min="0" max="100" class="form-range"
                                                               name="textile_type" value="0" step="50"/>
                                                        <div class="range-line">
                                                            <span class="active-line"></span>
                                                        </div>
                                                        <div class="dot-line">
                                                            <span class="active-dot"></span>
                                                        </div>
                                                    </div>
                                                    <ul class="list-inline list-unstyled">
                                                        <li class="list-inline-item">
                                                            <span><?php echo lang('velvet'); ?></span>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <span><?php echo lang('fabric'); ?></span>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <span><?php echo lang('boucle'); ?></span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="mb-20">
                                                <label
                                                    class="font-16 display-block "><?php echo lang('form_density'); ?></label>
                                                <div class="range-item">
                                                    <div class="range-input d-flex position-relative">
                                                        <input type="range" min="0" max="100" class="form-range"
                                                               name="foam_density" value="0" step="50"/>
                                                        <div class="range-line">
                                                            <span class="active-line"></span>
                                                        </div>
                                                        <div class="dot-line">
                                                            <span class="active-dot"></span>
                                                        </div>
                                                    </div>
                                                    <ul class="list-inline list-unstyled">
                                                        <li class="list-inline-item">
                                                            <span><?php echo lang('soft'); ?></span>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <span><?php echo lang('comfy'); ?></span>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <span><?php echo lang('hard'); ?></span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="mb-20">
                                                <label
                                                    class="font-16 display-block "><?php echo lang('textile_colour'); ?> </label>
                                                <div class="form-group colourGroup">
                                                    <input type="hidden" name="textile_colour" id="textile_colour"/>
                                                    <?php
                                                    if (empty($colours)) {
                                                        echo '<div class="alert alert-warning col-md-12">Sorry, Category not available!</div>';
                                                    } else {
                                                        foreach ($colours as $colour) {
                                                            ?>
                                                            <label class="font-16">
                                                            <span
                                                                onclick="$('#textile_colour').val('<?php echo $colour->code; ?>');
                                                                    $(this).parent().siblings().children().removeClass('selected');
                                                                    $(this).addClass('selected')"
                                                                class="cursor-pointer"
                                                                style="background: <?php echo $colour->code; ?>;display: inline-block;height: 20px;width: 20px;margin-top: 10px;margin-left: 2px"></span>
                                                            </label>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="mb-20">
                                                <input type="hidden" name="cost" id="cost">
                                                <label
                                                    class="font-16 display-block"><?php echo lang('cost_estimate'); ?></label>
                                                <h3 class="mb-0 cost">SAR 0.00</h3>
                                            </div>
                                        </div>
                                        <!-- Step-3 -->
                                        <div class="step-item">
                                            <h5>
                                                <img src="images/step-3.png" alt="step-1" class="thumb-icon"/>
                                                <?php echo lang('step_3'); ?>
                                                <img src="images/down-white2.png" alt="down" class="arrow"/>
                                            </h5>

                                            <section
                                                class="container pt-3 pb-5  howItWorks categorySections">
                                                <div class="owl-carousel owl-theme categorySectionSlider">

                                                    <div class="category category-circle overlay-zoom "
                                                         data-animation-options="{
                                                            'name': 'fadeInLeftShorter',
                                                            'delay': '.3s'
                                                        }">

                                                        <div class="shadow p-15 text-center text-000"
                                                             style="background: #FFF;border-radius: 15px">
                                                            <i class="fa fa-map-marker-alt mt-20 fa-3x"
                                                               style="color: #d9c12f !important;"></i>
                                                            <h4 class="mt-20 font-20 mb-20 font-weight-bold"
                                                                style="min-height: 57px"><?php echo lang('visit_your_location'); ?></h4>
                                                            <p style="height: 80px"><?php echo lang('visit_your_location_para'); ?></p>
                                                            <span
                                                                class="font-weight-bold text-000 font-12 text-transform-uppercase letter-spacing-2"><?php echo lang('visit_your_location_time'); ?> </span>
                                                        </div>

                                                    </div>

                                                    <div class="category category-circle overlay-zoom "
                                                         data-animation-options="{
                                                            'name': 'fadeInLeftShorter',
                                                            'delay': '.3s'
                                                        }">

                                                        <div class="shadow p-15 text-center text-000"
                                                             style="background: #FFF;border-radius: 15px">
                                                            <i class="fa fa-check-circle mt-20 fa-3x"
                                                               style="color: #d9c12f !important;"></i>
                                                            <h4 class="mt-20 font-20 mb-20 font-weight-bold"
                                                                style="min-height: 57px"><?php echo lang('confirm_your_order'); ?>
                                                            </h4>
                                                            <p style="height: 80px"><?php echo lang('confirm_your_order_para'); ?>
                                                            </p>
                                                            <span
                                                                class="font-weight-bold text-000 font-12 text-transform-uppercase letter-spacing-2"><?php echo lang('confirm_your_order_time'); ?></span>
                                                        </div>

                                                    </div>

                                                    <div class="category category-circle overlay-zoom "
                                                         data-animation-options="{
                                                            'name': 'fadeInLeftShorter',
                                                            'delay': '.3s'
                                                        }">

                                                        <div class="shadow p-15 text-center text-000"
                                                             style="background: #FFF;border-radius: 15px">
                                                            <i class="fa fa-check-double mt-20 fa-3x"
                                                               style="color: #d9c12f !important;"></i>
                                                            <h4 class="mt-20 font-20 mb-20 font-weight-bold"
                                                                style="min-height: 50px"><?php echo lang('receive_your_sofa'); ?>
                                                            </h4>
                                                            <p style="height: 80px"><?php echo lang('receive_your_sofa_para'); ?>
                                                            </p>
                                                            <span
                                                                class="font-weight-bold text-000 font-12 text-transform-uppercase letter-spacing-2"><?php echo lang('receive_your_sofa_time'); ?></span>
                                                        </div>

                                                    </div>

                                                </div>
                                            </section>
                                        </div>
                                        <!-- Step-4 -->
                                        <div class="step-item">
                                            <h5>
                                                <img src="images/step-4.svg" alt="step-1" class="thumb-icon"/>
                                                <?php echo lang('step_4'); ?>
                                                <img src="images/down-white2.png" alt="down" class="arrow"/>
                                            </h5>
                                            <div class="row">
                                                <div class="col-md-12 col-12 mb-3">
                                                    <input type="text" class="form-control input-focus" name="fname"
                                                           placeholder="<?php echo lang('name'); ?>"
                                                           oninvalid="this.setCustomValidity('<?php echo lang('input_error_message'); ?>')"
                                                           oninput="this.setCustomValidity('')"
                                                           required
                                                           value="<?php
                                                           if (set_value('fname') && !isset($success)) {
                                                               echo set_value('fname');
                                                           }
                                                           ?>">
                                                    <div class="error-text">
                                                        <?php
                                                        if (form_error('fname')) {
                                                            echo form_error('fname');
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-12 mb-3">
                                                    <input type="email" class="form-control input-focus"
                                                           placeholder="<?php echo lang('email'); ?> *"
                                                           required
                                                           oninvalid="this.setCustomValidity('<?php echo lang('email_error_message'); ?>')"
                                                           oninput="this.setCustomValidity('')"
                                                           name="email"
                                                           value="<?php
                                                           if (set_value('email') && !isset($success)) {
                                                               echo set_value('email');
                                                           }
                                                           ?>">
                                                    <div class="error-text">
                                                        <?php
                                                        if (form_error('email')) {
                                                            echo form_error('email');
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div>
                                                <input type="text" class="form-control input-focus mb-4"
                                                       placeholder="<?php echo lang('ph_num'); ?>"
                                                       required
                                                       oninvalid="this.setCustomValidity('<?php echo lang('input_error_message'); ?>')"
                                                       oninput="this.setCustomValidity('')"
                                                       name="phone"
                                                       value="<?php
                                                       if (set_value('phone') && !isset($success)) {
                                                           echo set_value('phone');
                                                       }
                                                       ?>">
                                                <div class="error-text">
                                                    <?php
                                                    if (form_error('phone')) {
                                                        echo form_error('phone');
                                                    }
                                                    ?>
                                                </div>
                                            </div>

                                            <div>
                                                <select class="form-control input-focus mb-4" name="country"
                                                        required
                                                        oninvalid="this.setCustomValidity('<?php echo lang('input_error_message'); ?>')"
                                                        oninput="this.setCustomValidity('')"
                                                        id="country">
                                                    <option value=""><?php echo lang('country'); ?></option>
                                                    <?php
                                                    if ($countries) {
                                                        foreach ($countries as $country) {
                                                            echo '<option value="' . ($country->id) . '">' . ($country->name) . '</option>';
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                                <div class="error-text">
                                                    <?php
                                                    if (form_error('country')) {
                                                        echo form_error('country');
                                                    }
                                                    ?>
                                                </div>
                                            </div>

                                            <div>
                                                <select class="form-control input-focus mb-4" name="city"
                                                        oninvalid="this.setCustomValidity('<?php echo lang('input_error_message'); ?>')"
                                                        oninput="this.setCustomValidity('')"
                                                        id="city">
                                                    <option value=""><?php echo lang('city'); ?></option>
                                                </select>
                                                <div class="error-text">
                                                    <?php
                                                    if (form_error('city')) {
                                                        echo form_error('city');
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <div align="center">
                                                <button type="submit" value="submit" name="submit"
                                                        class="checkoutBtn2"
                                                        style="width: 65%;background: #FFF;color: #000;border-radius: 0 !important;">
                                                    <?php echo lang('send_request'); ?>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <?php echo $page_footer; ?>
</div>
<div id="popup-modal" data-hash="#some-hash" class="custom-modal">
    <div class="modal-content animated fadeIn">
        <a class="modal-close">×</a>

        <div class="modal-text">
            <h2 class="title-popup">
                <?php echo lang('select_from_our_library'); ?>
                <img src="images/down-white2.png" alt="down"/></h2>
            <hr/>
            <div class="row customRadio">
                <?php
                if ($products) {
                    foreach ($products as $product) {
                        $attributes = $this->md->select_where('tbl_product_attribute', array('product_id' => $product->product_id));

                        $photos = array(FILENOTFOUND);
                        if ($attributes) {
                            $photos = $attributes[0]->product_photos ? explode(",", $attributes[0]->product_photos) : array(FILENOTFOUND);
                        }
                        ?>
                        <div class="col-md-6 mt-2 mb-2 col-6">
                            <input type="checkbox" name="product_id"
                                   style="width: 1px;height: 1px;padding: 0;margin: 0;"
                                   form="customForm"
                                   data-path="<?php echo base_url($photos ? ($photos[0] ? $photos[0] : FILENOTFOUND) : FILENOTFOUND); ?>"
                                   id="modalcb<?php echo $product->product_id; ?>"
                                   value="<?php echo $product->product_id; ?>"/>
                            <label for="modalcb<?php echo $product->product_id; ?>">
                                <img
                                    src="<?php echo base_url($photos ? ($photos[0] ? $photos[0] : FILENOTFOUND) : FILENOTFOUND); ?>"
                                    alt="<?php echo $product->title; ?>"
                                    title="<?php echo $product->title; ?>"
                                    class="imgPreview cursor-pointer"
                                    style="width: 100%;height: 290px;object-fit: cover"
                                >
                            </label>
                        </div>
                        <?php
                    }
                } else {
                    echo '<center>';
                    echo "<img src='" . base_url('assets/images/no-product.jpg') . "' style='width:40%' />";
                    echo '</center>';
                }
                ?>
            </div>
        </div>
        <div align="center">
            <a id="modal-close" class="customModalCloseBtn"><?php echo lang('confirm'); ?></a>
        </div>
    </div>
</div>
<?php echo $page_footerscript; ?>

<script>
    // how to close a bootstrap modal with the browser back button
    $('#popup-modal').on('show.bs.modal', function (e) {
        window.history.pushState('#popup-modal', "Modal title", document.location + '#popup-modal');
    }).on('hide.bs.modal', function (e) {
        if (window.history.state === '#popup-modal') {
            history.go(-1);
        }
    })

    window.addEventListener("hashchange", function (e) {
        $('#popup-modal').modal('hide');
    })

    // Checkbox act as a radio button
    let boxes = document.querySelectorAll("input[type=checkbox]");
    boxes.forEach(b => b.addEventListener("change", tick));

    function tick(e) {
        let path = e.target.dataset.path;
        let state = e.target.checked; // save state of changed checkbox
        if (state === true) {
            $('.libraryImg').show().attr('src', path);
        } else {
            $('.libraryImg').hide().attr('src', '');
        }
        boxes.forEach(b => b.checked = false); // clear all checkboxes
        e.target.checked = state; // restore state of changed checkbox
    }

</script>
<style>
    .owl-stage {
        padding: 0 !important;
    }

    /*@media (max-width: 620px) {*/
    /*    .howItWorks .owl-theme .owl-nav .owl-prev {*/
    /*        left: -30px; */
    /*    }*/
    /*    .howItWorks .owl-theme .owl-nav .owl-next {*/
    /*        right: -30px;*/
    /*}*/
    }
</style>
</body>