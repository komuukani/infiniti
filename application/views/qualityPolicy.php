<?php
echo $page_head;
?>
<body class="home">
<div class="page-wrapper">
    <?php echo $page_header; ?>
    <main class="main mt-100 mt-xs-50">
        <section class="pt-50 pb-50">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">

                        <div class="englishText">
                            <div class="">
                                <h2 class="font-58 font-weight-bold">Quality Policy</h2>
                            </div>
                            <div>
                                <?php
                                $quality = $this->md->select('tbl_quality');
                                if (empty($quality)) :
                                    echo "Sorry, content not available";
                                else :
                                    foreach ($quality as $quality_data) {
                                        echo $quality_data->quality;
                                    }
                                endif;
                                ?>
                            </div>
                        </div>

                        <div style="direction: rtl" class="mt-50 arabicText notranslate" translate="no">
                            <div class="">
                                <h2 class="font-58 font-weight-bold">سياسة الجودة</h2>
                            </div>
                            <div class="font-22 arabicContent">
                                <div>
                                    <?php
                                    if (empty($quality)) :
                                        echo "Sorry, content not available";
                                    else :
                                        foreach ($quality as $quality_data) {
                                            echo $quality_data->quality_arabic;
                                        }
                                    endif;
                                    ?>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </main>
    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
</body>