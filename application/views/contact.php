<?php
echo $page_head;
$admin_data = $this->md->select('tbl_web_data')[0];
$web_data = ($web_data) ? $web_data[0] : '';
?>
<body class="home">
<div class="page-wrapper">
    <?php echo $page_header; ?>
    <main class="main mt-100 mt-xs-50">
        <section class="pt-50 pb-50">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="">
                            <h2 class="font-58 font-weight-bold">
                                <?php echo lang('talk_to_us'); ?>
                            </h2>
                        </div>

                        <div class="mb-30 font-18">
                            <div>
                                <b><?php echo lang('email'); ?>:</b>
                                <p class="mt-20">
                                    <a
                                        href="mailto:<?php echo($admin_data->email_address); ?>"><?php echo($admin_data->email_address); ?></a>
                                </p>
                            </div>
                            <div>
                                <b><?php echo lang('whatsapp'); ?>:</b>
                                <p class="mt-20">
                                    <a
                                        href="https://wa.me<?php echo($admin_data->whatsapp); ?>"><?php echo($admin_data->whatsapp); ?></a>
                                </p>
                            </div>
                        </div>

                        <h2 class="mb-11 font-18"><?php echo lang('message_form'); ?>:</h2>
                        <form class="contact-form" method="post">
                            <?php
                            if (isset($error)) {
                                ?>
                                <div class="alert alert-danger p-1">
                                    <?php echo $error; ?>
                                </div>
                                <?php
                            }
                            if (isset($success)) {
                                ?>
                                <div class="alert alert-success p-1">
                                    <?php echo $success; ?>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="row">
                                <div class="col-md-6 col-12 mb-3">
                                    <input type="text" class="form-control" placeholder="Enter Extra field"
                                           name="extra_field" style="display: none !important"/>
                                    <input type="text" class="form-control input-focus" name="fname" placeholder="<?php echo lang('name'); ?>"
                                           value="<?php
                                           if (set_value('fname') && !isset($success)) {
                                               echo set_value('fname');
                                           }
                                           ?>">
                                    <div class="error-text">
                                        <?php
                                        if (form_error('fname')) {
                                            echo form_error('fname');
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12 mb-3">
                                    <input type="email" class="form-control input-focus" placeholder="<?php echo lang('email'); ?> *"
                                           name="email"
                                           value="<?php
                                           if (set_value('email') && !isset($success)) {
                                               echo set_value('email');
                                           }
                                           ?>">
                                    <div class="error-text">
                                        <?php
                                        if (form_error('email')) {
                                            echo form_error('email');
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <input type="text" class="form-control input-focus mb-4" placeholder="<?php echo lang('ph_num'); ?>"
                                   name="phone"
                                   value="<?php
                                   if (set_value('phone') && !isset($success)) {
                                       echo set_value('phone');
                                   }
                                   ?>">
                            <div class="error-text">
                                <?php
                                if (form_error('phone')) {
                                    echo form_error('phone');
                                }
                                ?>
                            </div>
                            <textarea class="form-control mb-3 input-focus" placeholder="<?php echo lang('comment'); ?>" rows="7"
                                      name="message"><?php
                                if (set_value('message') && !isset($success)) {
                                    echo set_value('message');
                                }
                                ?></textarea>
                            <div class="error-text">
                                <?php
                                if (form_error('message')) {
                                    echo form_error('message');
                                }
                                ?>
                            </div>
                            <div class="col-md-12 mt-8 mb-8">
                                <?php
                                if ($admin_data->captcha_visibility) :
                                    echo '<div class="g-recaptcha" data-sitekey="' . $admin_data->captcha_site_key . '"></div>';
                                endif;
                                ?>
                            </div>

                            <button type="submit" value="send" name="send"
                                    class="checkoutBtn2" style="width: 30%;">
                                <?php echo lang('send'); ?>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
</body>