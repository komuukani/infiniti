<!--<section class="intro-section container-fluid p-0">-->
<!--    <div-->
<!--        class="intro-slider animation-slider owl-carousel owl-theme row owl-nav-arrow cols-1 gutter-no mb-4"-->
<!--        data-owl-options="{-->
<!--                        'items': 1,-->
<!--                        'dots': true,-->
<!--                        'autoplay':true,-->
<!--                        'autoplayTimeout': 5000,-->
<!--                        'loop': true,-->
<!--                        'nav': false-->
<!--                    }">-->
<!--        --><?php
//        $banner = $this->md->my_query('SELECT * FROM `tbl_banner` ORDER BY position asc')->result();
//        if (!empty($banner)) {
//            foreach ($banner as $banner_data) {
//                ?>
<!--                <div class="intro-slide1 banner banner-fixed" style="background-color: #f8f7f5">-->
<!--                    <figure>-->
<!--                        <img src="--><?php //echo base_url($banner_data->path); ?><!--" alt="--><?php //echo $banner_data->title; ?><!--"-->
<!--                             style="height: 100vh;"-->
<!--                             title="--><?php //echo $banner_data->title; ?><!--"/>-->
<!--                    </figure>-->
<!--                    <div class="banner-content">-->
<!--                        <h3 class="banner-title mb-5 slide-animate"-->
<!--                            data-animation-options="{'name': 'blurIn', 'duration': '1.4s', 'delay': '.1s'}">-->
<!--                            <span class="text-white font-weight-bold">--><?php //echo $banner_data->title; ?><!-- </span></h3>-->
<!--                        <p class="banner-subtitle slide-animate font-weight-bold"-->
<!--                           data-animation-options="{'name': 'fadeInDownShorter', 'duration': '1.2s', 'delay': '.5s'}">-->
<!--                            --><?php //echo $banner_data->subtitle; ?><!--</p>-->
<!--                    </div>-->
<!--                </div>-->
<!--                --><?php
//            }
//        }
//        ?>
<!--    </div>-->
<!--</section>-->
<!---->
<!-- <style>
    /* .videoText h1 {
    display: none;  /* All frames are hidden initially */
    opacity: 0;     /* Set opacity to 0 for smooth transition */
    transition: opacity 1s ease-in-out;  /* Smooth transition effect */
} */

.videoText .frame1,
.videoText .frame2,
.videoText .frame3 {
    display: none;  /* Hide all frames initially */
}

.videoText h1[style*="display: block"] {
    opacity: 1;  /* Set opacity to 1 when the frame is displayed */
}

</style> -->


<section class="container-fluid p-0 videoContainer">
    <div class="row">
        <div class="col-md-12">
            <video id="mainVideo" class="d-none d-md-block d-lg-block d-xl-block" width="100%" autoplay="autoplay" loop
                   muted preload="auto">
                <source src="<?php echo base_url('assets/video/desktop.mp4') ?>" type="video/mp4"/>
            </video>
            <video id="mainVideoMobile" class="d-block d-md-none d-lg-none d-xl-none"
                   style="height: 100vh; object-fit: cover" width="100%" autoplay="autoplay" loop muted preload="auto">
                <source src="<?php echo base_url('assets/video/mobile.mp4') ?>" type="video/mp4"/>
            </video>
            <div style="width: 100%;height: 100%;background: rgba(0,0,0,0.35);position: absolute;top: 0;left: 0"></div>
        </div>
    </div>
    <div class="videoText">
        <h1 class="frame frame1" style="opacity: 0;">
            <?php echo lang('slide1'); ?>
            <a href="<?php echo current_url(); ?>#featuredProductSection"
               class="btn-product pt-15 pb-15 cursor-pointer text-000 border-none font-weight-normal letter-spacing-1 font-16 mt-30">
                <?php echo lang('explore_more'); ?>
            </a>
        </h1>
        <h1 class="frame frame2" style="opacity: 0;">
            <?php echo lang('slide2'); ?>
            <a href="<?php echo current_url(); ?>#featuredProductSection"
               class="btn-product pt-15 pb-15 cursor-pointer text-000 border-none font-weight-normal letter-spacing-1 font-16 mt-30">
                <?php echo lang('explore_more'); ?>
            </a>
        </h1>
        <h1 class="frame frame3" style="opacity: 0;">
            <?php echo lang('slide3'); ?>
            <a href="<?php echo current_url(); ?>#featuredProductSection"
               class="btn-product pt-15 pb-15 cursor-pointer text-000 border-none font-weight-normal letter-spacing-1 font-16 mt-30">
                <?php echo lang('explore_more'); ?>
            </a>
        </h1>
    </div>
</section>
